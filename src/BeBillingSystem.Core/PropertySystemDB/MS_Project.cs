﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.PropertySystemDB
{
    [Table("MS_Project")]
    public class MS_Project : AuditedEntity
    {
        public int entityID { get; set; }

        [Required]
        [StringLength(5)]
        public string projectCode { get; set; }

        [Required]
        [StringLength(40)]
        public string projectName { get; set; }

        [StringLength(300)]
        public string image { get; set; }

        [StringLength(300)]
        public string webImage { get; set; }

        [Required]
        public bool isPublish { get; set; }

        [Required]
        public short webSort { get; set; }

        [Required]
        [StringLength(200)]
        public string SADManager { get; set; }

        [Required]
        [StringLength(550)]
        public string SADStaff { get; set; }

        [Required]
        [StringLength(500)]
        public string SADContact { get; set; }

        [Required]
        [StringLength(100)]
        public string SADPhone { get; set; }

        [Required]
        [StringLength(100)]
        public string SADFax { get; set; }

        [Required]
        [StringLength(30)]
        public string SADBM { get; set; }

        [Required]
        public bool isConfirmSP { get; set; }

        public double? penaltyRate { get; set; }

        public int? startPenaltyDay { get; set; }

        [Required]
        public bool isBookingCashier { get; set; }

        [Required]
        public bool isBookingSMS { get; set; }

        [Required]
        public short unitNoGroupLength { get; set; }

        [StringLength(3)]
        public string PROJECT_ID { get; set; }

        [StringLength(2)]
        public string BIZ_UNIT_ID { get; set; }

        [StringLength(5)]
        public string ORG_ID { get; set; }

        [Required]
        [StringLength(3)]
        public string DIV_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string parentProjectName { get; set; }

        [StringLength(5)]
        public string BusinessGroup { get; set; }

        [StringLength(5)]
        public string OperationalGroup { get; set; }

        [StringLength(200)]
        public string TaxGroup { get; set; }

        [StringLength(200)]
        public string SADEmail { get; set; }

        [StringLength(500)]
        public string PGContact { get; set; }

        [StringLength(100)]
        public string PGPhone { get; set; }

        [StringLength(200)]
        public string PGEmail { get; set; }

        [StringLength(200)]
        public string FinanceContact { get; set; }

        [StringLength(200)]
        public string FinanceEmail { get; set; }

        public bool isDMT { get; set; }

        public string DMT_ProjectGroupCode { get; set; }

        public string DMT_ProjectGroupName { get; set; }

        public int? callCenterManagerID { get; set; }

        public int? callCenterStaffID { get; set; }

        public int? bankRelationManagerID { get; set; }

        public int? bankRelationStaffID { get; set; }

        public int? SADBMID { get; set; }

        public int? SADManagerID { get; set; }

        public int? SADStaffID { get; set; }

        public int? PGManagerID { get; set; }

        public int? PGStaffID { get; set; }

        public int? financeManagerID { get; set; }

        public int? financeStaffID { get; set; }

        public int? SADBMStaffID { get; set; }

        [StringLength(150)]
        public string permitLocation { get; set; }

        [StringLength(150)]
        public string areaName { get; set; }

       
    }
}
