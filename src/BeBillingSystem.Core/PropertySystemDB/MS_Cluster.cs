﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.BillingSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.PropertySystemDB
{
    [Table("MS_Cluster")]
    public class MS_Cluster : AuditedEntity
    {
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Required]
        [StringLength(35)]
        public string clusterName { get; set; }

        public DateTime? dueDateDevelopment { get; set; }

        [StringLength(500)]
        public string dueDateRemarks { get; set; }

        public int sortNo { get; set; }

        [StringLength(100)]
        public string handOverPeriod { get; set; }

        [StringLength(100)]
        public string gracePeriod { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public double penaltyRate { get; set; }

        public int startPenaltyDay { get; set; }

        public string termConditionFile { get; set; }
        public string specificationUnitFile { get; set; }

        public string website { get; set; }
        public string docCode { get; set; }
        public int? siteID { get; set; }

        //public ICollection<MS_Unit> MS_Unit { get; set; }

        //public ICollection<MS_BankOLBooking> MS_BankOLBooking { get; set; }

        //public ICollection<MS_ProjectOLBooking> MS_ProjectOLBooking { get; set; }

        //public ICollection<MP_ClusterHandOverPeriode> MP_ClusterHandOverPeriode { get; set; }

        //public ICollection<MS_ClusterPenalty> MS_ClusterPenalty { get; set; }

        //public ICollection<MS_SPPeriod> MS_SPPeriod { get; set; }

        //public ICollection<TR_AgreementDetail> TR_AgreementDetail { get; set; }

        //public ICollection<MS_UserAccountBank> MS_UserAccountBank { get; set; }

        //public ICollection<TR_UpdateRetention> TR_UpdateRetention { get; set; }

        //public ICollection<MS_ProjectCluster> MS_ProjectCluster { get; set; }

        //public ICollection<MS_CMTemplate> MS_CMTemplate { get; set; }

        //public ICollection<MS_MappingDistrict> MS_MappingDistrict { get; set; }

        //public ICollection<MS_UnitTaskList> MS_UnitTaskList { get; set; }

        //public ICollection<TR_SignificantFinancingHeader> TR_SignificantFinancingHeader { get; set; }


    }
}
