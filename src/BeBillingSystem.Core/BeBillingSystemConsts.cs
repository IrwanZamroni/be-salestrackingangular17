﻿using BeBillingSystem.Debugging;

namespace BeBillingSystem
{
    public class BeBillingSystemConsts
    {
        public const string LocalizationSourceName = "BeBillingSystem";

        public const string E3BillingSystemDbContext = "Default";
		public const string E3NewCommDbContext = "NewCommDbContext";
		public const string CoreSystemDbContext = "CoreDbContext";
        public const string PropertySystemDbContext = "PropertySystemDbContext";
        public const string PersonalsNewDbContext = "PersonalsNewDbContext";
        public const string OracleStageDbContext = "OracleStageDbContext";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "0dc7f8c2935c4c1d8194431f746e84e2";
    }
}
