﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.PersonalDB
{
    public class TR_Address : AuditedEntity<string>
    {
      
        public string entityCode { get; set; }

      
        public string psCode { get; set; }

      
        public int refID { get; set; }

     
        public string addrType { get; set; }

     
        public string address { get; set; }

        [StringLength(10)]
        public string postCode { get; set; }

        [Required]
        [StringLength(50)]
        public string city { get; set; }

        [Required]
        [StringLength(50)]
        public string country { get; set; }

        [StringLength(250)]
        public string Kelurahan { get; set; }

        [StringLength(250)]
        public string Kecamatan { get; set; }

        [StringLength(250)]
        public string province { get; set; }


    }
}