﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.PersonalDB
{
    public class TR_Phone : AuditedEntity<string>
    {
      
   
        public string entityCode { get; set; }


        public string psCode { get; set; }

      
        public int refID { get; set; }

      
        public string phoneType { get; set; }

      
        public string number { get; set; }

        [StringLength(100)]
        public string remarks { get; set; }

        [Column("modifTime")]
        public override DateTime? LastModificationTime { get; set; }

        [Column("modifUN")]
        public override long? LastModifierUserId { get; set; }

        [Column("inputTime")]
        public override DateTime CreationTime { get; set; }

        [Column("inputUN")]
        public override long? CreatorUserId { get; set; }

        //public virtual LK_PhoneType LK_PhoneType { get; set; }
    }
}
