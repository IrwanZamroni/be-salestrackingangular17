﻿using Abp.MultiTenancy;
using BeBillingSystem.Authorization.Users;

namespace BeBillingSystem.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
