﻿namespace BeBillingSystem.MultiTenancy
{
	public enum PaymentPeriodType
	{
		Monthly = 30,
        Annual = 365
	}
}