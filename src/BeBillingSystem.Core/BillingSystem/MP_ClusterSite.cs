﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MP_ClusterSite : AuditedEntity
    {

        public int SiteId { get; set; }
        public int ClusterId { get; set; }
        public int ProjectId { get; set; }
        public string ClusterName { get; set; }
        public string ClusterCode { get; set; }

    }
}