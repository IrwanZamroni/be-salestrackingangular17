﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_WaterReading : AuditedEntity
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public int PeriodId { get; set; }
        public int PrevRead { get; set; }
        public int CurrentRead { get; set; }
        public int UnitDataId { get; set; }
    }
}
