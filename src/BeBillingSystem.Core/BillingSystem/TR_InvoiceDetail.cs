﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_InvoiceDetail : AuditedEntity
    {
        public int InvoiceHeaderId { get; set; }
        public int ItemId { get; set; }
        public int ItemNominal { get; set; }
        public int PPNNominal { get; set; }
    }
}
