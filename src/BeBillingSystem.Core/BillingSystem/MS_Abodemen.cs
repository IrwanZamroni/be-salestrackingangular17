﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_Abodemen : AuditedEntity
    {
        public int SiteId { get; set; }
        public int AbodemenNominal { get; set; }
        public int AttributeId { get; set; }
    }
}
