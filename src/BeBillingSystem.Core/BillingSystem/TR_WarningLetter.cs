﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_WarningLetter : AuditedEntity
    {
        public int SiteId { get; set; }
        public int InvoiceHeaderId { get; set; }
        public int PeriodId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public int UnitDataId { get; set; }
        public string UnitCode { get; set; }
        public string SPRefNo { get; set; }
        public string UnitNo { get; set; }
        public string psCode { get; set; }
        public string Email { get; set; }
        public int SP { get; set; }
        public DateTime? SendEmailDate { get; set; }


    }
}
