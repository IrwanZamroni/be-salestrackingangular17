﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_UnitItemDetail : AuditedEntity
    {
        public int UnitItemHeaderId { get; set; }
        public int TemplateInvoiceDetailId { get; set; }
  
        public int RateNominal { get; set; }
        //public int ItemRateId { get; set; }
    }
}
