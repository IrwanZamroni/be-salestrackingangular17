﻿using Abp.Domain.Entities.Auditing;
using System;

namespace BeBillingSystem.BillingSystem
{
    public class TR_InvoiceHeader : AuditedEntity
    {
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }

        public int UnitDataId { get; set; }

        public string PsCode { get; set; }
        public string InvoiceNo { get; set; }
        public int TemplateInvoiceHeaderId { get; set; }
        public int TagihanPerbulan { get; set; } //hasil perhitungan tungggakan
        public decimal JumlahYangHarusDibayar { get; set; } //hasil perhitungan tungggakan
        public decimal TagihanBulanSebelumnya { get; set; } //hasil perhitungan tungggakan

        public bool IsSendEmail { get; set; }
        public bool IsSendWhatsapp { get; set; }
        public int RegenerateCount { get; set; }
        public string InvoiceDoc { get; set; }
        public DateTime InvoiceDate { get; set; }
    
        public DateTime JatuhTempo { get; set; }

        public decimal PenaltyNominal { get; set; }
        public DateTime? IsSendEmailDate { get; set; }
        public DateTime? IsSendWADate { get; set; }
        public int BankId { get; set; }
        public string VirtualAccNo { get; set; }
        public bool IsPenalty { get; set; }

    }
}
