﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_TemplateInvoiceHeader : AuditedEntity
    {
        public int SiteId { get; set; }
        public string TemplateInvoiceName { get; set; }
        public string TemplateUrl { get; set; }
    }
}
