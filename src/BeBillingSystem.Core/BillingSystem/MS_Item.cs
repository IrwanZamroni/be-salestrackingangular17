﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_Item : AuditedEntity
    {
        public int SiteId { get; set; }
        public string ItemName { get; set; }

        //[ForeignKey("MS_PPH")]
        public string PPHId { get; set; }
        //public virtual MS_PPH MS_PPH { get; set; }
        public int Aging { get; set; }
        public int AttributeId { get; set; }
        public int CompanyCode { get; set; }
     
    }
}
