﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_Site : FullAuditedEntity
    {

        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SiteCode { get; set; }
        public string Email { get; set; }
        public string OfficePhone { get; set; }
        public string HandPhone { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
    }
}