﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_SettingEmail : AuditedEntity
    {
        public int TypeEmailId { get; set; }
        public int SiteId { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string NoTlp { get; set; }
        public string Address { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
    }
}
