﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_JournalDetail : FullAuditedEntity
    {
        public int Id { get; set; }
        public int JournalHeaderId { get; set; }
        public DateTime JournalDate { get; set; }
        public string ReceiptNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public bool IsUploadOracle { get; set; }
        public decimal Debit { get; set; }
        public decimal Kredit { get; set; }
        public string COA1 { get; set; }
        public string COA2 { get; set; }
        public string COA3 { get; set; }
        public string COA4 { get; set; }
        public string COA5 { get; set; }
        public string COA6 { get; set; }
        public string COA7 { get; set; }
        public string Remarks { get; set; }
        public string COACodeFIN { get; set; }

        public int BankId { get; set; }
        public int PaymentMethodId { get; set; }
        public string OracleDesc { get; set; }
        public int PeriodId { get; set; }
        public DateTime AccountingDate { get; set; }
        public string GroupId { get; set; }
        public int BillingPaymentHeaderId { get; set; }
        public int BillingPaymentDetailId { get; set; }
        public int InvoiceHeaderId { get; set; }
        public int InvoiceDetailId { get; set; }
        public int InvoiceItemId { get; set; }
        public DateTime PaymentDate { get; set; }

    }

}

