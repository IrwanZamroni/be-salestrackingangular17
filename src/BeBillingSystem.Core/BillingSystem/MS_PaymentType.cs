﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_PaymentType : AuditedEntity
    {
        public int Id { get; set; }
        public string PaymentTypeName { get; set; }
        public int SortNo { get; set; }
        public int PaymentType { get; set; }
    }
}
