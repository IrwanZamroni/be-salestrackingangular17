﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BeBillingSystem.BillingSystem
{
    public class MS_OracleMapping : AuditedEntity
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string OracleGroup { get; set; }
        public string OracleTransDesc { get; set; }
        public string ItemNameTransDesc { get; set; }
        public string COA1 { get; set; }
        public string COA2 { get; set; }
        public string COA3 { get; set; }
        public string COA4 { get; set; }
        public string COA5 { get; set; }
        public string COA6 { get; set; }
        public string COA7 { get; set; }
        public string AccountType { get; set; }
    }
}
