﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_TemplateInvoiceDetail : AuditedEntity
    {
        public int TemplateInvoiceHeaderId { get; set; }
        public int ItemId { get; set; }
        public int ItemRateId { get; set; }
        public int MP_ClusterSiteId { get; set; }
    }
}
