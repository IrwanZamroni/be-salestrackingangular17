﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_Period :AuditedEntity
    {
        public int SiteId { get; set; }
        public int PeriodNumber { get; set; }
        public DateTime PeriodMonth { get; set; }
        public DateTime PeriodYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CloseDate { get; set; }
        public bool IsActive { get; set; }
    }
}
