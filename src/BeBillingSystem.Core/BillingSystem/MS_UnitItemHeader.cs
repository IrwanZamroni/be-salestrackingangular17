﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_UnitItemHeader : AuditedEntity
    {
        public int SiteId { get; set; }
        public int UnitDataId { get; set; }
        public int TemplateInvoiceHeaderId { get; set; }
        public int BankId { get; set; }

        public string VirtualAccNo { get; set; }
        public bool IsPenalty { get; set; }
    }
}
