﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_AdjustmentDetail : AuditedEntity
    {
        public int InvoiceHeaderId { get; set; }
        public int AdjustmentNominal { get; set; }
        public DateTime AdjustmentDate { get; set; }
    }
}
