﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_PaymentMethod : AuditedEntity
    {
        public string PaymentMethodName { get; set; }
        public int SortNo { get; set; }
        public int PaymentTypeId { get; set; }
    }
}
