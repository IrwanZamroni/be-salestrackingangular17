﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_BillingPaymentDetail : AuditedEntity
    {
        public int BillingHeaderId { get; set; }
        public int InvoiceHeaderId { get; set; }
        public decimal PaymentAmount { get; set; }
    }
}
