﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_ExchangeRate : AuditedEntity
    {
        public int SiteId { get; set; }
        public String Currency { get; set; }
        public String CurrencyName { get; set; }
        public int Rate { get; set; }
        public string Remarks { get; set; }
        
        public bool IsActive { get; set; }
    }
}
