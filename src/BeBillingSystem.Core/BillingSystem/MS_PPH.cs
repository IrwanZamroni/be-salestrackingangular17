﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_PPH : AuditedEntity
    {
        public string PPHName { get; set; }
        public int Value { get; set; }
    }
}
