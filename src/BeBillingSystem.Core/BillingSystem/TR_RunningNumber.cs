﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_RunningNumber : AuditedEntity
    {
        public int SiteId { get; set; }

        public int ProjectId { get; set; }
        public string DocCode { get; set; }
        public int Number { get; set; }
        public int PeriodId { get; set; }
}
}
