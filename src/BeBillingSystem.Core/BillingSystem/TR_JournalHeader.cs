﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_JournalHeader : FullAuditedEntity
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
    }
}