﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_Parameter : AuditedEntity
    {
        public string Code { get; set; }
        public string DataType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Value { get; set; }
    }
}
