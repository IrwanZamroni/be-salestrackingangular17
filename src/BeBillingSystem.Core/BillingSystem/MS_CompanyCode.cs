﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_CompanyCode : AuditedEntity
    {
        public int  SiteId { get; set; }
        public string ComCode { get; set; }
        public string ComName { get; set; }
    }
}
