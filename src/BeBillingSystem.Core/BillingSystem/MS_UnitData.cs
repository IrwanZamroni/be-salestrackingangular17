﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_UnitData : AuditedEntity
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public int UnitId { get; set; }
        public int LandArea { get; set; }
        public int BuildArea { get; set; }
        public string UnitType { get; set; }  //1 unit, 2 tenant
        public string UnitName { get; set; }  //1 unit, 2 tenant
        public int UnitStatusId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string PsCode { get; set; }
        public bool PrintInvoice { get; set; }
        public bool PrintSP { get; set; }
        public bool PrintFakturPajak { get; set; }
        public bool IsGenerateInvoice { get; set; }
    }
}
