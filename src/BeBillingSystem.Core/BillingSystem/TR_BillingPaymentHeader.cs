﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_BillingPaymentHeader : AuditedEntity
    {

        public int UnitDataId { get; set; }
        public string PsCode { get; set; }
      
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public int PaymentMethodId { get; set; }
        public string CardNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public int BankId { get; set; }
        public string Remarks { get; set; }
        public decimal AmountPayment { get; set; }
        public int Charge { get; set; }
        public decimal Total { get; set; }
        public bool PrintOfficialReceipt { get; set; }
        public int ReprintOR { get; set; }
        public string ReceiptNumber { get; set; }
        public bool CancelPayment { get; set; }
        public bool IsUploadBulk { get; set; }
        public long? UserCancelPayment { get; set; }
        public string VA { get; set; }
        public string TransactionCode { get; set; }
        public string Status { get; set; }
        public DateTime? CancelDate { get; set; }
        public bool isSendEmail { get; set; }
        public DateTime? IsSendEmailDate { get; set; }
    }
}
