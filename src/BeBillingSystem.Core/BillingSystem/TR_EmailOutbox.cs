﻿using Abp.Domain.Entities.Auditing;
using Castle.MicroKernel.SubSystems.Conversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class TR_EmailOutbox : AuditedEntity
    {
        public int SiteId { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailMessage { get; set; }
        public bool IsSent { get; set; }
        public DateTime? SentDate { get; set; }
        public string ErrMessage { get; set; }
        public bool IsAttachment { get; set; }
 
    
    }
}
