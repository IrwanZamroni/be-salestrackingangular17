﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public class MS_ItemRate : AuditedEntity
    {
        public int SiteId { get; set; }
        public string ItemRateName { get; set; }

        //[ForeignKey("MS_BillingItem")]
        public int ItemId { get; set; }
        public int ExchangeRateId { get; set; }

        public bool FixRate { get; set; }
        public int LumpSumRate { get; set; }
        public int LandRate { get; set; }
        public int BuildRate { get; set; }
        public int Range1Start { get; set; }
        public int Range1End { get; set; }
        public int Range1Nominal { get; set; }
        public int Range2Start { get; set; }
        public int Range2End { get; set; }
        public int Range2Nominal { get; set; }
        public int Range3Start { get; set; }
        public int Range3End { get; set; }
        public int Range3Nominal { get; set; }
        public double VATPercentage { get; set; }
        public string SinkingFundPercentage { get; set; }
        public int SinkingFundNominal { get; set; }
        public string ManagementFeePercentage { get; set; }
        public int ManagementFeeNominal { get; set; }
        public int PenaltyNominal { get; set; }
        public int PenaltyPercentage { get; set; }
        public int RateElectric { get; set; }
        public double LuasAwal { get; set; }
        public double LuasAkhir { get; set; }
    }
}
