﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.OracleStageDB
{
    [Table("LK_COMPANY")]
    public class LK_COMPANY_ORACLE : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return ORG_ID;
            }
            set { /* nothing */ }
        }

        [Key]
        [StringLength(5)]
        public string ORG_ID { get; set; }

        [Required]
        [StringLength(2)]
        public string PROVINCE_ID { get; set; }

        [StringLength(250)]
        public string COMPANY_NAME { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INPUT_TIME { get; set; }

        [StringLength(40)]
        public string INPUT_USER { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MODIF_TIME { get; set; }

        [StringLength(40)]
        public string MODIF_USER { get; set; }
    }
}
