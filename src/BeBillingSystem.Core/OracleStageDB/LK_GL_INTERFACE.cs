﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VDI.Demo.OracleStageDB
{
    [Table("LK_GL_INTERFACE")]
    public class LK_GL_INTERFACE : Entity<string>
    {
        [NotMapped]
        public override string Id
        {
            get
            {
                return INPUT_TIME + "";
            }
            set { /* nothing */ }
        }

        [StringLength(50)]
        public string STATUS { get; set; }

        public int? SET_OF_BOOKS_ID { get; set; }

        [StringLength(11)]
        public string ACCOUNTING_DATE { get; set; }

        [StringLength(5)]
        public string CURRENCY_CODE { get; set; }

        [StringLength(11)]
        public string DATE_CREATED { get; set; }

        public int? CREATED_BY { get; set; }

        [StringLength(1)]
        public string ACTUAL_FLAG { get; set; }

        [StringLength(25)]
        public string USER_JE_CATEGORY_NAME { get; set; }

        [StringLength(25)]
        public string USER_JE_SOURCE_NAME { get; set; }

        [StringLength(10)]
        public string SEGMENT1 { get; set; }

        [StringLength(10)]
        public string SEGMENT2 { get; set; }

        [StringLength(10)]
        public string SEGMENT3 { get; set; }

        [StringLength(10)]
        public string SEGMENT4 { get; set; }

        [StringLength(10)]
        public string SEGMENT5 { get; set; }

        [StringLength(10)]
        public string SEGMENT6 { get; set; }

        [StringLength(10)]
        public string SEGMENT7 { get; set; }

        [Column(TypeName = "money")]
        public decimal? ENTERED_DR { get; set; }

        [Column(TypeName = "money")]
        public decimal? ENTERED_CR { get; set; }

        [StringLength(11)]
        public string TRANSACTION_DATE { get; set; }

        [StringLength(15)]
        public string GROUP_ID { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE1 { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE2 { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE3 { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE4 { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE5 { get; set; }

        [StringLength(240)]
        public string LK_ATTRIBUTE6 { get; set; }

        [StringLength(50)]
        public string ACC_CODE { get; set; }

        [Key]
        [Column(TypeName = "smalldatetime")]
        public DateTime? INPUT_TIME { get; set; }

        [StringLength(40)]
        public string INPUT_UN { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? MODIF_TIME { get; set; }

        [StringLength(40)]
        public string MODIF_UN { get; set; }
    }
}
