﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace BeBillingSystem.Localization
{
    public static class BeBillingSystemLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(BeBillingSystemConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(BeBillingSystemLocalizationConfigurer).GetAssembly(),
                        "BeBillingSystem.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
