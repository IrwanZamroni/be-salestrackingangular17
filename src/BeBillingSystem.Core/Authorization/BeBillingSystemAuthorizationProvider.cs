﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace BeBillingSystem.Authorization
{
    public class BeBillingSystemAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {

            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
			context.CreatePermission(PermissionNames.Pages_Users_Management, L("UsersManagement"));
			context.CreatePermission(PermissionNames.Pages_Users_Delete, L("UsersDelete"));
			context.CreatePermission(PermissionNames.Pages_Users_Edit, L("UsersEdit"));
			context.CreatePermission(PermissionNames.Pages_Users_Create, L("UsersCreate"));
			context.CreatePermission(PermissionNames.Pages_Report_ReportDetailStatment_Excel, L("ReportDetailStatmentExcel"));
			context.CreatePermission(PermissionNames.Pages_Report_ReportDetailStatment_Pdf, L("ReportDetailStatmentPdf"));
		
			context.CreatePermission(PermissionNames.Pages_Users_Activation, L("UsersActivation"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
			context.CreatePermission(PermissionNames.Pages_Roles_Edit, L("RolesEdit"));
			context.CreatePermission(PermissionNames.Pages_Roles_Create, L("RolesCreate"));
			context.CreatePermission(PermissionNames.Pages_Roles_View, L("RolesView"));
			context.CreatePermission(PermissionNames.Pages_Roles_Delete, L("RolesDelete"));
			context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

			context.CreatePermission(PermissionNames.Pages_Cash, L("Cash"));
			context.CreatePermission(PermissionNames.Pages_Dashboard, L("Dashboard"));
            context.CreatePermission(PermissionNames.Pages_Master, L("Master"));
            context.CreatePermission(PermissionNames.Pages_Transaction, L("Transaction"));
            context.CreatePermission(PermissionNames.Pages_Cashier, L("Cashier"));
            context.CreatePermission(PermissionNames.Pages_Report, L("Report"));



            context.CreatePermission(PermissionNames.Pages_Master_MasterSite , L("MasterSite"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterSite_Create, L("MasterSiteCreate"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterSite_Edit, L("MasterSiteEdit"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterSite_View, L("MasterSiteView"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterPeriod, L("MasterPeriod"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterPeriod_Create, L("MasterPeriodCreate"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterPeriod_Edit, L("MasterPeriodEdit"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterPeriod_View, L("MasterPeriodView"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterUnitItem, L("MasterUnitItem"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterUnitItem_Create, L("MasterUnitItemCreate"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterUnitItem_Edit, L("MasterUnitItemEdit"));
			context.CreatePermission(PermissionNames.Pages_Master_MasterUnitItem_View, L("MasterUnitItemView"));


			context.CreatePermission(PermissionNames.Pages_Transaction_WaterReading, L("WaterReading"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WaterReading_Create, L("WaterReadingCreate"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WaterReading_Edit, L("WaterReadingEdit"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WaterReading_View, L("WaterReadingView"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WaterReading_ExportToExcel, L("ExportToExcelWaterReading"));

            context.CreatePermission(PermissionNames.Pages_Transaction_ElectricReading, L("ElectricReading"));
			context.CreatePermission(PermissionNames.Pages_Transaction_ElectricReading_Create, L("ElectricReadingCreate"));
			context.CreatePermission(PermissionNames.Pages_Transaction_ElectricReading_Edit, L("ElectricReadingEdit"));
			context.CreatePermission(PermissionNames.Pages_Transaction_ElectricReading_View, L("ElectricReadingView"));
			context.CreatePermission(PermissionNames.Pages_Transaction_ElectricReading_ExportToExcel, L("ExportToExcelElectricReading"));


            context.CreatePermission(PermissionNames.Pages_Transaction_Invoice, L("Invoice"));
			context.CreatePermission(PermissionNames.Pages_Transaction_Invoice_Regenerateinvoice, L("InvoiceRegenerateinvoice"));
			context.CreatePermission(PermissionNames.Pages_Transaction_Invoice_Sendemailinvoice, L("InvoiceSendemailinvoice"));
			context.CreatePermission(PermissionNames.Pages_Transaction_Invoice_Sendwhatsappinvoice, L("InvoiceSendwhatsappinvoice"));
			context.CreatePermission(PermissionNames.Pages_Transaction_Invoice_Adjustment, L("InvoiceAdjustment"));
			context.CreatePermission(PermissionNames.Pages_Transaction_Invoice_View, L("InvoiceView"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WarningLetter, L("WarningLetter"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WarningLetter_Sendemail, L("WarningLetterSendemail"));
			context.CreatePermission(PermissionNames.Pages_Transaction_WarningLetter_View, L("WarningLetterView"));
			context.CreatePermission(PermissionNames.Pages_Transaction_OracleToJournal, L("OracleToJournal"));
			context.CreatePermission(PermissionNames.Pages_Transaction_OracleToJournal_Generatepymentjournal, L("OracleToJournalGeneratepymentjournal"));
			context.CreatePermission(PermissionNames.Pages_Transaction_OracleToJournal_Exportexcel, L("OracleToJournalExportexcel"));
			context.CreatePermission(PermissionNames.Pages_Transaction_OracleToJournal_Aploadtooracle, L("OracleToJournalAploadtooracle"));
			context.CreatePermission(PermissionNames.Pages_Cashier_BillingPayment, L("BillingPayment"));
			context.CreatePermission(PermissionNames.Pages_Cashier_BillingPayment_Create, L("BillingPaymentCreate"));
			context.CreatePermission(PermissionNames.Pages_Cashier_BillingPayment_View, L("BillingPaymentView"));
			context.CreatePermission(PermissionNames.Pages_Cashier_ReprintOR, L("ReprintOR"));
			context.CreatePermission(PermissionNames.Pages_Cashier_ReprintOR_Changetransactiondate, L("ReprintORChangetransactiondate"));
			context.CreatePermission(PermissionNames.Pages_Cashier_ReprintOR_Reprintor, L("ReprintORReprintor"));
			context.CreatePermission(PermissionNames.Pages_Cashier_ReprintOR_View, L("ReprintORView"));
			context.CreatePermission(PermissionNames.Pages_Cashier_CancelPayment, L("CancelPayment"));
			context.CreatePermission(PermissionNames.Pages_Cashier_CancelPayment_Cretecancelpyment, L("CancelPaymentCretecancelpyment"));
			context.CreatePermission(PermissionNames.Pages_Cashier_CancelPayment_View, L("CancelPaymentView"));
			context.CreatePermission(PermissionNames.Pages_Cashier_UploadBulk, L("UploadBulk"));
			context.CreatePermission(PermissionNames.Pages_Cashier_UploadBulk_Createbulkpyment, L("UploadBulkCreatebulkpyment"));
			context.CreatePermission(PermissionNames.Pages_Cashier_BillingPayment_Payment, L("Payment"));
            
            context.CreatePermission(PermissionNames.Pages_Report_ReportInvoice, L("ReportInvoice"));
			context.CreatePermission(PermissionNames.Pages_Report_ReportInvoice_Generate, L("ReportInvoiceGenerate"));
			context.CreatePermission(PermissionNames.Pages_Report_DailyReport, L("DailyReport"));
			context.CreatePermission(PermissionNames.Pages_Report_DailyReport_Generate, L("DailyReportGenerate"));
			context.CreatePermission(PermissionNames.Pages_Report_ReportDetailStatment, L("ReportDetailStatment"));
            



        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, BeBillingSystemConsts.LocalizationSourceName);
        }
    }
}
