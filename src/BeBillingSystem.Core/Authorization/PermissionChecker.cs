﻿using Abp.Authorization;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;

namespace BeBillingSystem.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
