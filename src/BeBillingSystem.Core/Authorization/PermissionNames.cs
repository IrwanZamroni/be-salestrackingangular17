﻿namespace BeBillingSystem.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";
		public const string Pages_Users_Management = "Pages.Users.Management";
		public const string Pages_Users_Create = "Pages.Users.Create";
		public const string Pages_Users_Edit = "Pages.Users.Edit";
		public const string Pages_Users_Delete = "Pages.Users.Delete";
		public const string Pages_Report_ReportDetailStatment_Excel = "Pages.Report.ReportDetailStatment.Excel";
		public const string Pages_Report_ReportDetailStatment_Pdf = "Pages.Report.ReportDetailStatment.Pdf";
		
		public const string Pages_Cash = "Pages.Cash";

		public const string Pages_Users_Activation = "Pages.Users.Activation";
        public const string Pages_Roles = "Pages.Roles";
		public const string Pages_Roles_Edit = "Pages.Roles.Edit";
		public const string Pages_Roles_Create = "Pages.Roles.Create";
		public const string Pages_Roles_Delete = "Pages.Roles.Delete";
		public const string Pages_Roles_View = "Pages.Roles.View";
		public const string Pages_Dashboard = "Pages.Dashboard";
        public const string Pages_Master = "Pages.Master";
        public const string Pages_Transaction = "Pages.Transaction";
        public const string Pages_Cashier = "Pages.Cashier";
        public const string Pages_Report = "Pages.Report";




        public const string Pages_Master_MasterSite = "Pages.Master.MasterSite";
		public const string Pages_Master_MasterSite_Create = "Pages.Master.MasterSite.Create";
		public const string Pages_Master_MasterSite_Edit = "Pages.Master.MasterSite.Edit";
		public const string Pages_Master_MasterSite_View = "Pages.Master.MasterSite.View";
		public const string Pages_Master_MasterPeriod = "Pages.Master.MasterPeriod";
		public const string Pages_Master_MasterPeriod_Create = "Pages.Master.MasterPeriod.Create";
		public const string Pages_Master_MasterPeriod_Edit = "Pages.Master.MasterPeriod.Edit";
		public const string Pages_Master_MasterPeriod_View = "Pages.Master.MasterPeriod.View";
		public const string Pages_Master_MasterUnitItem = "Pages.Master.MasterUnitItem";
		public const string Pages_Master_MasterUnitItem_Create = "Pages.Master.MasterUnitItem.Create";
		public const string Pages_Master_MasterUnitItem_Edit = "Pages.Master.MasterUnitItem.Edit";
		public const string Pages_Master_MasterUnitItem_View = "Pages.Master.MasterUnitItem.View";

		//===========================================================================

		public const string Pages_Transaction_WaterReading = "Pages.Transaction.WaterReading";
		public const string Pages_Transaction_WaterReading_Create = "Pages.Transaction.WaterReading.Create";
		public const string Pages_Transaction_WaterReading_Edit = "Pages.Transaction.WaterReading.Edit";
		public const string Pages_Transaction_WaterReading_View = "Pages.Transaction.WaterReading.View";
		public const string Pages_Transaction_WaterReading_ExportToExcel = "Pages.Transaction.WaterReading.ExportToExcel";
        
        public const string Pages_Transaction_ElectricReading = "Pages.Transaction.ElectricReading";
		public const string Pages_Transaction_ElectricReading_Create = "Pages.Transaction.ElectricReading.Create";
		public const string Pages_Transaction_ElectricReading_Edit = "Pages.Transaction.ElectricReading.Edit";
		public const string Pages_Transaction_ElectricReading_View = "Pages.Transaction.ElectricReading.View";
		public const string Pages_Transaction_ElectricReading_ExportToExcel = "Pages.Transaction.ElectricReading.ExportToExcel";

        public const string Pages_Transaction_Invoice = "Pages.Transaction.Invoice";
		public const string Pages_Transaction_Invoice_Regenerateinvoice = "Pages.Transaction.Invoice.Regenerateinvoice";
		public const string Pages_Transaction_Invoice_Sendemailinvoice = "Pages.Transaction.Invoice.Sendemailinvoice";
		public const string Pages_Transaction_Invoice_Sendwhatsappinvoice = "Pages.Transaction.Invoice.Sendwhatsappinvoice";
		public const string Pages_Transaction_Invoice_Adjustment = "Pages.Transaction.Invoice.Adjustment";
		public const string Pages_Transaction_Invoice_View = "Pages.Transaction.Invoice.View";
		public const string Pages_Transaction_WarningLetter = "Pages.Transaction.WarningLetter";
		public const string Pages_Transaction_WarningLetter_Sendemail = "Pages.Transaction.WarningLetter.Sendemail";
		public const string Pages_Transaction_WarningLetter_View = "Pages.Transaction.WarningLetter.View";
		public const string Pages_Transaction_OracleToJournal  = "Pages.Transaction.OracleToJournal";
		public const string Pages_Transaction_OracleToJournal_Generatepymentjournal = "Pages.Transaction.OracleToJournal.Generatepymentjournal";
		public const string Pages_Transaction_OracleToJournal_Exportexcel = "Pages.Transaction.OracleToJournal.Exportexcel";
		public const string Pages_Transaction_OracleToJournal_Aploadtooracle = "Pages.Transaction.OracleToJournal.Aploadtooracle";
		//===============================================================================


		public const string Pages_Cashier_BillingPayment  = "Pages.Cashier.BillingPayment";
		public const string Pages_Cashier_BillingPayment_Create = "Pages.Cashier.BillingPayment.Create";
		public const string Pages_Cashier_BillingPayment_View = "Pages.Cashier.BillingPayment.View";
		public const string Pages_Cashier_ReprintOR  = "Pages.Cashier.ReprintOR";
		public const string Pages_Cashier_ReprintOR_Changetransactiondate = "Pages.Cashier.ReprintOR.Changetransactiondate";
		public const string Pages_Cashier_ReprintOR_Reprintor = "Pages.Cashier.ReprintOR.Reprintor";
		public const string Pages_Cashier_ReprintOR_View = "Pages.Cashier.ReprintOR.View";
		public const string Pages_Cashier_CancelPayment  = "Pages.Cashier.CancelPayment";
		public const string Pages_Cashier_CancelPayment_Cretecancelpyment = "Pages.Cashier.CancelPayment.Cretecancelpyment";
		public const string Pages_Cashier_CancelPayment_View = "Pages.Cashier.CancelPayment.View";
		public const string Pages_Cashier_UploadBulk = "Pages.Cashier.UploadBulk";
		public const string Pages_Cashier_UploadBulk_Createbulkpyment = "Pages.Cashier.UploadBulk.Createbulkpyment";
		public const string Pages_Cashier_BillingPayment_Payment = "Pages.Cashier.BillingPayment.Payment";

        public const string Pages_Report_ReportInvoice = "Pages.Report.ReportInvoice";
		public const string Pages_Report_ReportInvoice_Generate = "Pages.Report.ReportInvoice.Generate";
		public const string Pages_Report_DailyReport = "Pages.Report.DailyReport";
		public const string Pages_Report_DailyReport_Generate = "Pages.Report.DailyReport.Generate";
		public const string Pages_Report_ReportDetailStatment = "Pages.Report.ReportDetailStatment";
    }
}
