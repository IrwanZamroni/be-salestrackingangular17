﻿using BeBillingSystem.BillingSystem.TransactionToOracle.Dto;
using Dapper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.DataAccess
{
    public class DA_TransactionToOracle
    {
        public List<GeneratePaymentJournalDto.data> GetGeneratePaymentJournal(GeneratePaymentJournalDto.req input)
        {
            List<GeneratePaymentJournalDto.data> res = new List<GeneratePaymentJournalDto.data>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetGeneratePaymentJournal] @SiteId = {0}, @Period = {1}," +
                    "@PaymentType = {2}, @AccountingDate = '{3}', @BankPayment = {4}, " +
                    "@PaymentStartDate = '{5}', @PaymentEndDate = '{6}'",
                    input.siteId,
                    input.period,
                    input.paymentType,
                    Convert.ToDateTime(input.accountingDate.ToString("yyyy-MM-dd")),
                    input.bankPayment,
                    Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                    Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd")));

                res = conn.Query<GeneratePaymentJournalDto.data>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }

        public List<GetBillingJournalTaskListDto> GetExportToExcelJournalToOracle(GeneratePaymentJournalDto.req input)
        {
            List<GetBillingJournalTaskListDto> res = new List<GetBillingJournalTaskListDto>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetBillingJournalList] @SiteId = {0}, @Period = {1}," +
                    "@PaymentType = {2}, @AccountingDate = '{3}', @BankPayment = {4}, " +
                    "@PaymentStartDate = '{5}', @PaymentEndDate = '{6}'",
                    input.siteId,
                    input.period,
                    input.paymentType,
                    Convert.ToDateTime(input.accountingDate.ToString("yyyy-MM-dd")),
                    input.bankPayment,
                    Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                    Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd")));

                res = conn.Query<GetBillingJournalTaskListDto>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }
        public List<GetBillingJournalTaskListDto> GetFetchBillingJournalTaskList(GeneratePaymentJournalDto.req input)
        {
            List<GetBillingJournalTaskListDto> res = new List<GetBillingJournalTaskListDto>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetBillingJournalList] @SiteId = {0}, @Period = {1}," +
                    "@PaymentType = {2}, @AccountingDate = '{3}', @BankPayment = {4}, " +
                    "@PaymentStartDate = '{5}', @PaymentEndDate = '{6}'",
                    input.siteId,
                    input.period,
                    input.paymentType,
                    Convert.ToDateTime(input.accountingDate.ToString("yyyy-MM-dd")),
                    input.bankPayment,
                    Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                    Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd")));

                res = conn.Query<GetBillingJournalTaskListDto>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }
        public List<ExportToExcelJournalToOracleDto> GetFecthJournalOracleList(GeneratePaymentJournalDto.reqlist input)
        {
            List<ExportToExcelJournalToOracleDto> res = new List<ExportToExcelJournalToOracleDto>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetJournalOracleList] @SiteId = {0}, @Period = {1}," +
                    "@PaymentType = {2}, @AccountingDate = '{3}', @BankPayment = {4}, " +
                    "@PaymentStartDate = '{5}', @PaymentEndDate = '{6}'," +
                    "@SkipCount = {7}, @MaxResultCount = {8}",
                    input.siteId,
                    input.period,
                    input.paymentType,
                    Convert.ToDateTime(input.accountingDate.ToString("yyyy-MM-dd")),
                    input.bankPayment,
                    Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                    Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd")),
                    input.SkipCount,
                    input.MaxResultCount);

                res = conn.Query<ExportToExcelJournalToOracleDto>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }

        public List<ExportToExcelJournalToOracleDto> GetFecthJournalOracleListWithnoPage(GeneratePaymentJournalDto.req input)
        {
            List<ExportToExcelJournalToOracleDto> res = new List<ExportToExcelJournalToOracleDto>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetJournalOracleListWithnoPage] @SiteId = {0}, @Period = {1}," +
                    "@PaymentType = {2}, @AccountingDate = '{3}', @BankPayment = {4}, " +
                    "@PaymentStartDate = '{5}', @PaymentEndDate = '{6}'",
                    input.siteId,
                    input.period,
                    input.paymentType,
                    Convert.ToDateTime(input.accountingDate.ToString("yyyy-MM-dd")),
                    input.bankPayment,
                    Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                    Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd")));

                res = conn.Query<ExportToExcelJournalToOracleDto>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }

        public List<GetExistsJournalDto.Res> GetExistsJournal(GetExistsJournalDto.Req input)
        {
            List<GetExistsJournalDto.Res> res = new List<GetExistsJournalDto.Res>();

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
                var ConstringBilling = connectionStrings.Property("Default").Value.ToString();

                SqlConnection conn = new SqlConnection(ConstringBilling);
                conn.Open();

                string query = string.Format("exec [SP_GetExistJournal] @SiteId = {0}, @Period = {1}," +
                        "@PaymentType = {2}, @BankPayment = {3}, " +
                        "@PaymentStartDate = '{4}', @PaymentEndDate = '{5}'",
                        input.siteId,
                        input.period,
                        input.paymentType,
                        input.bankPayment,
                        Convert.ToDateTime(input.paymentStartDate.ToString("yyyy-MM-dd")),
                        Convert.ToDateTime(input.paymentEndDate.ToString("yyyy-MM-dd"))
                    );

                res = conn.Query<GetExistsJournalDto.Res>(query).ToList();

                conn.Close();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return res;
        }
    }
}
