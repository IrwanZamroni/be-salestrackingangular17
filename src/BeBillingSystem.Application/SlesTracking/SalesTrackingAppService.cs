﻿using Abp.Application.Services.Dto;

using BeBillingSystem.EntityFrameworkCore;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace BeBillingSystem.SlesTracking
{
	public class GetSalesTrackingResultDto
	{
		public string name { get; set; }
		public string Email { get; set; }
		public string PaymentStatus { get; set; }
		public string Payment { get; set; }
		public decimal TotalPayment { get; set; }
		public string PPStatus { get; set; }
		public string PPNo { get; set; }
		public DateTime? BuyDatePP { get; set; }
		public string UnitStatus { get; set; }
		public string Unit { get; set; }
		public string SignP3U { get; set; }
		public string SignAkad { get; set; }
		public string pctPaymentDp { get; set; }
		public string ConfirmationLetter { get; set; }
		public string memberCode { get; set; }
		public string psCode { get; set; }
		public string orderCode { get; set; }
		public string bookCode { get; set; }
		public int unitOrderHeaderId { get; set; }
		public string termRemarks { get; set; }
		public int PPOrderID { get; set; }
		public string bankCode { get; set; }
		public string bankName { get; set; }
		public int? unitID { get; set; }
		public int RecordCount { get; set; }


		//new field 2020 05 11

		public int? projectID { get; set; }
		public string overallStatus { get; set; }
		public string daySinceLast { get; set; }
		public string step { get; set; }
		public string stepComplete { get; set; }
		public string Document { get; set; }
		public string bookingHeaderID { get; set; }
		public string clusterID { get; set; }
		public DateTime? bookDate { get; set; }
		public DateTime? PPCreationTime { get; set; }
		public DateTime? SignP3UModifTime { get; set; }
		public DateTime? SignAkadModifTime { get; set; }
		public DateTime? maxClearDateDP { get; set; }
		public DateTime? DocumentModifTime { get; set; }
		public DateTime? ConfirmationLetterModifTime { get; set; }
		public string projectCode { get; set; }
		public string nextPayment { get; set; }
		public DateTime? dueDate { get; set; }

		//dto my commission
		public string totalKomisiPaid { get; set; }
		public string percentKomisiPaid { get; set; }
		public string eligibleCommAmount { get; set; }
		public string eligibleCommPct { get; set; }

		//reserved Field
		public string reservedField1 { get; set; }
		public string reservedField2 { get; set; }
		public string reservedField3 { get; set; }
		public string reservedField4 { get; set; }
		public string reservedField5 { get; set; }
		public string reservedField6 { get; set; }
		public string reservedField7 { get; set; }
	}
	public class GetSalesTrackingInputDto
	{
		public List<string> memberCode { get; set; }
		public List<int> projectID { get; set; }
		public string keyword { get; set; }
		public string bookCode { get; set; }
		public int termID { get; set; }

		[Range(1, AppConsts.MaxPageSize)]
		public int MaxResultCount { get; set; }

		[Range(0, int.MaxValue)]
		public int SkipCount { get; set; }

		public GetSalesTrackingInputDto()
		{
			MaxResultCount = AppConsts.DefaultPageSize;
		}

	}
	public class PersenMemberLoginDto
	{
		public string bookNo { get; set; }
		public string memberCode { get; set; }
		public string bookCode { get; set; }
		public double commPctPaid { get; set; }


	}

	public class SalesTrackingAppService : BeBillingSystemAppServiceBase
	{
		private readonly IConfiguration _configuration;
		
		private readonly NewCommDbContext _newCommDbContext;
		private readonly PropertySystemDbContext _propertySystemDbContext;
		public SalesTrackingAppService( IConfiguration configuration, NewCommDbContext newCommDbContext, PropertySystemDbContext propertySystemDbContext) { 
			_propertySystemDbContext = propertySystemDbContext;
		
			_configuration = configuration;
			_newCommDbContext = newCommDbContext;
			_configuration = configuration;
		}
		
	


		//public PagedResultDto<GetSalesTrackingResultDto> InquiryDataSalesTracking(GetSalesTrackingInputDto input)
		//{
		
		//	Logger.DebugFormat("InquiryDataSalesTracking() - started - {0}", DateTime.Now);
		//	string ProjectIDs = "All";
		//	string memberCodes = null;

		//	if (input.memberCode != null)
		//	{
		//		memberCodes = input.memberCode.Count == 0 ? null : string.Join(",", input.memberCode);
		//	}

		//	if (input.projectID != null)
		//	{
		//		ProjectIDs = string.Join(",", input.projectID);
		//	}


		//	//var offSet = input.SkipCount;
		//	//var rowOnly = input.MaxResultCount;			
		//	var skipCount = input.SkipCount;
		//	var maxCount = input.MaxResultCount;
		//	var bookCode = input.bookCode == null ? null : input.bookCode;
		//	var termID = input.termID;
		//	string Constring = _configuration.GetConnectionString("PropertySystemDbContext");

		

			
		//	List<GetSalesTrackingResultDto> result = new List<GetSalesTrackingResultDto>();

		//	int resultCount = 0;
		//	var query = "EXEC SP_GetDataSalesTracking @bookCode, @memberCodes, @ProjectIDs, @termID, @skipCount, @maxCount";
		//	var queryCount = @"EXEC SP_GetDataSalesTrackingDataCount @bookCode , @memberCodes,@ProjectIDs,@termID,@skipCount,@maxCount";

		//	using (var connection = new SqlConnection(Constring))
		//	{
		//		using (var command = new SqlCommand(query, connection))
		//		{
		//			command.Parameters.AddWithValue("@bookCode", bookCode);
		//			command.Parameters.AddWithValue("@memberCodes", memberCodes);
		//			command.Parameters.AddWithValue("@ProjectIDs", ProjectIDs);
		//			command.Parameters.AddWithValue("@termID", termID);
		//			command.Parameters.AddWithValue("@skipCount", skipCount);
		//			command.Parameters.AddWithValue("@maxCount", maxCount);

		//			connection.Open();
		//			//try
		//			//{
		//			result = (connection.Query<GetSalesTrackingResultDto>(query)).ToList();

		//			resultCount = (connection.Query<int>(queryCount)).FirstOrDefault();

		//			// Lakukan eksekusi query di sini
		//		}
				
		//	}
		//	//var query = @"EXEC SP_GetDataSalesTracking @bookCode, @memberCodes, @ProjectIDs, @termID, @skipCount, @maxCount";

		//	//var result = _sqlExecuter.GetFromPropertySystem<GetSalesTrackingResultDto>
		//	//							   (query, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount },
		//	//							   System.Data.CommandType.StoredProcedure).ToList();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - Query - {0}", DateTime.Now);
		//	//if (!string.IsNullOrWhiteSpace(input.bookCode))
		//	//{
		//	//	result = result.Where(x => (!string.IsNullOrWhiteSpace(x.bookCode) && x.bookCode.ToLowerInvariant().Contains(input.bookCode.ToLowerInvariant()))).ToList();
		//	//}

			

		//	//var resultCount = _sqlExecuter.GetFromPropertySystem<int>
		//	//							   (queryCount, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount },
		//	//							   System.Data.CommandType.StoredProcedure).FirstOrDefault();

			

		//	Logger.DebugFormat("InquiryDataSalesTracking() - QueryCount - {0}", DateTime.Now);

		//	if (!string.IsNullOrWhiteSpace(input.keyword))
		//	{
		//		result = result.Where(x => (!string.IsNullOrWhiteSpace(x.bankCode) && x.bankCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.bankName) && x.bankName.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 //|| (!string.IsNullOrWhiteSpace(x.bookCode) && x.bookCode.ToLowerInvariant().Contains(input.bookCode.ToLowerInvariant() ?? null	))
		//				 || (!string.IsNullOrWhiteSpace(x.ConfirmationLetter) && x.ConfirmationLetter.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.Email) && x.Email.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 //|| (!string.IsNullOrWhiteSpace(x.memberCode) && x.memberCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.name) && x.name.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.orderCode) && x.orderCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.Payment) && x.Payment.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.PaymentStatus) && x.PaymentStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.PPNo) && x.PPNo.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.psCode) && x.psCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.termRemarks) && x.termRemarks.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.Unit) && x.Unit.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.UnitStatus) && x.UnitStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.PPStatus) && x.PPStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.overallStatus) && x.overallStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.reservedField1) && x.reservedField1.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.reservedField2) && x.reservedField2.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 || (!string.IsNullOrWhiteSpace(x.reservedField3) && x.reservedField3.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//				 ).ToList();
		//	}


		//	var countData = Convert.ToInt32(resultCount);
		//	//var countData = result.Count();

		//	//result = result.OrderBy(x => x.BuyDatePP ?? DateTime.MaxValue).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - Filter - {0}", DateTime.Now);
		//	var getDataProject = (from x in _propertySystemDbContext.MS_Project
		//						  select x).ToList();

		//	var getBookCode = result.Where(x => !string.IsNullOrWhiteSpace(x.bookCode)).Select(x => new { x.memberCode, x.bookCode }).ToList();
							
			
		//	var query2 = @"
		//			SELECT a.bookNo, a.memberCode, a.commPctPaid 
		//			FROM TR_OverridingPct a
		//			INNER JOIN (
		//				SELECT bookCode, memberCode FROM @GetBookCode
		//			) b 
		//			ON a.bookNo = b.bookCode AND a.memberCode = b.memberCode
		//		";

		//	using (var connection = new SqlConnection(Constring))
		//	{
		//		connection.Open();
		//		var getPersenMemberLogin = connection.Query(query2, new { GetBookCode = getBookCode }).ToList();
		//	}


		//	//var getPersenMemberLogin = (from a in _newCommDbContext.TR_OverridingPct
		//	//							join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
		//	//							select new
		//	//							{
		//	//								a.bookNo,
		//	//								a.memberCode,
		//	//								a.commPctPaid
		//	//							}).ToList();

		//	var getCommisionMemberLogin = (from a in _newCommDbContext.TR_CommPayment
		//								   join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
		//								   group a by new { a.bookNo, a.memberCode } into G
		//								   select new
		//								   {
		//									   G.Key.bookNo,
		//									   G.Key.memberCode,
		//									   amountComm = G.Sum(x => x.amount)
		//								   }).ToList();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - Komisi - {0}", DateTime.Now);
		//	var tanggalSekarang = DateTime.Now;
		//	foreach (var data in result)
		//	{
		//		var countStep = 0;
		//		int daysAgo = 0;

		//		if (data.projectID != null || data.projectID != 0)
		//		{
		//			data.projectCode = getDataProject.Where(x => x.Id == data.projectID).Select(x => x.projectCode).FirstOrDefault();
		//		}

		//		//step1
		//		if ((!string.IsNullOrWhiteSpace(data.PPStatus) && (data.PPStatus.ToLower() == "done") || (!(data.PPStatus.ToLower() == "done") && data.UnitStatus.ToLower() == "done")))
		//		{
		//			data.PPStatus = "Done";
		//			countStep += 1;
		//		}

		//		//step2
		//		if (!string.IsNullOrWhiteSpace(data.UnitStatus) && data.UnitStatus.ToLower() == "done")
		//		{
		//			countStep += 1;
		//		}

		//		//step3
		//		if (!string.IsNullOrWhiteSpace(data.SignP3U) && data.SignP3U.ToLower() == "complete")
		//		{
		//			countStep += 1;
		//		}

		//		//step4
		//		if (((!string.IsNullOrWhiteSpace(data.SignAkad) && data.SignAkad.ToLower() == "complete") ||
		//			(!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
		//			!data.termRemarks.ToLower().Contains("kpr")))))
		//		{
		//			data.SignAkad = "Complete";
		//			countStep += 1;
		//		}

		//		//step5
		//		if (((!string.IsNullOrWhiteSpace(data.pctPaymentDp) && data.pctPaymentDp.ToLower() == "complete (100%)")
		//			|| (!string.IsNullOrWhiteSpace(data.termRemarks) && data.termRemarks.ToLower() == "hard cash")))
		//		{
		//			countStep += 1;
		//		}

		//		//step6
		//		if (((!string.IsNullOrWhiteSpace(data.Document) && data.Document.ToLower() == "complete")
		//			|| (!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
		//			!data.termRemarks.ToLower().Contains("kpr")))))
		//		{
		//			data.Document = "Complete";
		//			countStep += 1;
		//		}
		//		else
		//		{
		//			data.Document = "Incomplete";
		//		}

		//		//step7
		//		if (!string.IsNullOrWhiteSpace(data.ConfirmationLetter) && data.ConfirmationLetter.ToLower() == "complete")
		//		{
		//			countStep += 1;
		//		}

		//		var listDate = new List<DateTime?>();
		//		if (countStep == 7)
		//		{
		//			listDate.Add(data.SignP3UModifTime);
		//			listDate.Add(data.SignAkadModifTime);
		//			listDate.Add(data.maxClearDateDP);
		//			listDate.Add(data.DocumentModifTime);
		//			listDate.Add(data.ConfirmationLetterModifTime);

		//			var getLatestDate = listDate.OrderByDescending(x => x).FirstOrDefault();
		//			if (data.BuyDatePP != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.BuyDatePP)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else if (data.bookDate != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.bookDate)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else
		//			{
		//				data.daySinceLast = "-";
		//			}
		//		}
		//		else
		//		{
		//			if (data.BuyDatePP != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.BuyDatePP)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else if (data.bookDate != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.bookDate)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else
		//			{
		//				data.daySinceLast = "-";
		//			}
		//		}

		//		data.step = countStep + "/7";
		//		data.stepComplete = Convert.ToInt32((double)countStep / (double)7 * 100).ToString() + "% Complete";
		//		data.overallStatus = countStep == 7 ? "Complete" : daysAgo <= 2 ? "New" : daysAgo <= 7 ? "Immediate" : daysAgo <= 14 ? "Urgent" :
		//						daysAgo <= 30 ? "Critical" : daysAgo <= 60 ? "At Risk" : "At Risk";

		//		//komisi
		//		//var getKomisi = (from x in getComission
		//		//                 where x.bookNo == data.bookCode && x.memberCode == data.memberCode
		//		//                 select new
		//		//                 {
		//		//                     x.paidDate,
		//		//                     x.amountKomisi
		//		//                 }).ToList();

		//		//var komisiPaid = getKomisi.Where(x => x.paidDate != null).Sum(x => x.amountKomisi);
		//		//var totalKomisi = getKomisi.Sum(x => x.amountKomisi);

		//		//data.totalKomisiPaid = NumberHelper.IndoFormat(komisiPaid);
		//		//data.percentKomisiPaid = totalKomisi == 0 ? "0" : string.Format("{0:N2}", Convert.ToInt32((double)komisiPaid / (double)totalKomisi * 100));

		//		var singleDataPercen = getPersenMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();
		//		var singleDataAmount = getCommisionMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();

		//		data.percentKomisiPaid = singleDataPercen == null ? "0%" : (singleDataPercen.commPctPaid * 100) + "%";
		//		data.totalKomisiPaid = NumberHelper.IndoFormatWithoutTail(singleDataAmount == null ? 0 : singleDataAmount.amountComm);
		//		data.eligibleCommPct = data.eligibleCommPct == null ? "0%" : data.eligibleCommPct + "%";
		//	}


		//	Logger.DebugFormat("InquiryDataSalesTracking() - End - {0}", DateTime.Now);
		//	return new PagedResultDto<GetSalesTrackingResultDto>(countData, result);
		//}
		
		}
	}
//}
