﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem
{
    public static class NumberHelper
    {
        /// <summary>
        /// Convert a decimal into a string with the Rp. prefix.
        /// </summary>
        public static string ToRupiah(this decimal src)
        {
            var indonesianCultureInfo = new CultureInfo("id-ID");
            indonesianCultureInfo.NumberFormat.CurrencyNegativePattern = 1;
            return src.ToString("C0", indonesianCultureInfo).Replace("Rp", "Rp. ");
        }

        internal static string IndoFormat(decimal num)
        {
            var numRound = Math.Round(num);
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ",";
            numberFormatInfo.NumberGroupSeparator = ".";
            return numRound.ToString("N", numberFormatInfo);
        }

        internal static string IndoFormatNoRound(decimal num)
        {
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberDecimalSeparator = ",";
            numberFormatInfo.NumberGroupSeparator = ".";
            return num.ToString("N", numberFormatInfo);
        }

        internal static string IndoFormatWithoutTail(decimal num)
        {
            var numRound = Math.Round(num);
            var numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.NumberGroupSeparator = ".";
            return numRound.ToString("N0", numberFormatInfo);
        }

        internal static string TerbilangKoma(this decimal y)
        {
            if (y.ToString().Contains("."))
            {
                string s = y.ToString();
                string[] parts = s.Split('.');
                int i1 = int.Parse(parts[0]);
                string i2 = parts[1];
                return Terbilang(Convert.ToDecimal(i1)).TrimEnd() + TerbilangKoma(i2);
            }
            else
            {
                return TerbilangCore(y);
            }
        }

        internal static string TerbilangKoma(string y)
        {
            char[] characters = y.ToCharArray();
            var result = " Koma ";
            foreach (var chr in characters)
            {
                if (chr == '0')
                {
                    result += "Nol ";
                }
                else if (chr == '1')
                {
                    result += "Satu ";
                }
                else if (chr == '2')
                {
                    result += "Dua ";
                }
                else if (chr == '3')
                {
                    result += "Tiga ";
                }
                else if (chr == '4')
                {
                    result += "Empat ";
                }
                else if (chr == '5')
                {
                    result += "Lima ";
                }
                else if (chr == '6')
                {
                    result += "Enam ";
                }
                else if (chr == '7')
                {
                    result += "Tujuh ";
                }
                else if (chr == '8')
                {
                    result += "Delapan ";
                }
                else if (chr == '9')
                {
                    result += "Sembilan ";
                }
            }
            return result;
        }

        internal static string Terbilang(this decimal y)
        {
            return TerbilangCore(y);
        }

        internal static decimal PMT(double yearlyInterestRate, int totalNumberOfMonths, double loanAmount)
        {
            var rate = (double)yearlyInterestRate;
            var denominator = Math.Pow((1 + rate), totalNumberOfMonths) - 1;
            return Math.Abs(Convert.ToDecimal((rate + (rate / denominator)) * loanAmount));
        }

        internal static string Terbilang(this decimal? y)
        {
            return TerbilangCore(y);
        }

        private static string TerbilangCore(this decimal? y)
        {
            string[] bilangan = { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas" };
            string temp = "";

            if (y == null)
            {
                return "-";
            }

            long x = Convert.ToInt64(y);

            if (x < 12)
            {
                temp = " " + bilangan[x];
            }
            else if (x < 20)
            {
                temp = Terbilang(x - 10).ToString() + " Belas";
            }
            else if (x < 100)
            {
                temp = Terbilang(x / 10) + " Puluh" + Terbilang(x % 10);
            }
            else if (x < 200)
            {
                temp = " Seratus" + Terbilang(x - 100);
            }
            else if (x < 1000)
            {
                temp = Terbilang(x / 100) + " Ratus" + Terbilang(x % 100);
            }
            else if (x < 2000)
            {
                temp = " Seribu" + Terbilang(x - 1000);
            }
            else if (x < 1000000)
            {
                temp = Terbilang(x / 1000) + " Ribu" + Terbilang(x % 1000);
            }
            else if (x < 1000000000)
            {
                temp = Terbilang(x / 1000000) + " Juta" + Terbilang(x % 1000000);
            }
            else if (x < 1000000000000)
            {
                temp = Terbilang(x / 1000000000) + " Miliar" + Terbilang(x % 1000000000);
            }
            else if (x < 1000000000000000)
            {
                temp = Terbilang(x / 1000000000000) + " Triliun" + Terbilang(x % 1000000000000);
            }

            return temp;
        }

        public static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Rupiah";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr;//Cents  
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1} {2}", ConvertWholeNumber(wholeNo).Trim(), andStr, endStr).Replace("  ", " ");
            }
            catch { }
            return val;
        }
        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX
                bool isDone = false;//test if already translated
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1://ones' range

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros
                        //if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        private static String ConvertDecimalsIndo(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = satuan(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        private static String satuan(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "Satu";
                    break;
                case 2:
                    name = "Dua";
                    break;
                case 3:
                    name = "Tiga";
                    break;
                case 4:
                    name = "Empat";
                    break;
                case 5:
                    name = "Lima";
                    break;
                case 6:
                    name = "Enam";
                    break;
                case 7:
                    name = "Tujuh";
                    break;
                case 8:
                    name = "Delapan";
                    break;
                case 9:
                    name = "Sembilan";
                    break;
            }
            return name;
        }

        private static String puluhan(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Sepuluh";
                    break;
                case 11:
                    name = "Sebelas";
                    break;
                case 12:
                    name = "Dua Belas";
                    break;
                case 13:
                    name = "Tiga Belas";
                    break;
                case 14:
                    name = "Empat Belas";
                    break;
                case 15:
                    name = "Lima Belas";
                    break;
                case 16:
                    name = "Enam Belas";
                    break;
                case 17:
                    name = "Tujuh Belas";
                    break;
                case 18:
                    name = "Delapan Belas";
                    break;
                case 19:
                    name = "sembilan Belas";
                    break;
                case 20:
                    name = "Dua Puluh";
                    break;
                case 30:
                    name = "Tiga Puluh";
                    break;
                case 40:
                    name = "Empat Puluh";
                    break;
                case 50:
                    name = "Lima Puluh";
                    break;
                case 60:
                    name = "Enam Puluh";
                    break;
                case 70:
                    name = "Tujuh Puluh";
                    break;
                case 80:
                    name = "Delapan Puluh";
                    break;
                case 90:
                    name = "Sembilan Puluh";
                    break;
                case 100:
                    name = "Seratus";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = puluhan(Number.Substring(0, 1) + "0") + " " + satuan(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        public static String ConvertToWordsIndo(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Rupiah";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr;//Cents  
                        pointStr = ConvertDecimalsIndo(points);
                    }
                }
                val = String.Format("{0} {1} {2}", ConvertWholeNumberIndo(wholeNo).Trim(), andStr, endStr).Replace("  ", " ");
            }
            catch { }
            return val;
        }

        private static String ConvertWholeNumberIndo(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX
                bool isDone = false;//test if already translated
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1://ones' range

                            word = satuan(Number);
                            isDone = true;
                            break;
                        case 2://tens' range
                            word = puluhan(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Ratus ";
                            break;
                        case 4://thousands' range
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Ribu ";
                            break;
                        case 7://millions' range
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Juta ";
                            break;
                        case 10://Billions's range
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Miliar ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumberIndo(Number.Substring(0, pos)) + place + ConvertWholeNumberIndo(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumberIndo(Number.Substring(0, pos)) + ConvertWholeNumberIndo(Number.Substring(pos));
                        }

                        //check for trailing zeros
                        //if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
    }
}
