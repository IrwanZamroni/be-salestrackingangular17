﻿using System.Threading.Tasks;
using Abp.Application.Services;
using BeBillingSystem.Sessions.Dto;

namespace BeBillingSystem.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
