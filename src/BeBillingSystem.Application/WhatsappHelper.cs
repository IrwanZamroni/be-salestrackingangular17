﻿using Abp.Application.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BeBillingSystem.BillingSystem.Dto;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System.IO;
using Abp.Extensions;

namespace BeBillingSystem
{
    [RemoteService(IsMetadataEnabled = false)]
    public class WhatsappHelper : IWhatsappHelper
    {
       
    
        private HttpContent model;

        public object ClientHelper { get; private set; }
        public System.Net.ICredentials ServerCredentials { get { throw null; } set { } }
    
        //public async Task<bool> SendWhatsApp(WhatsAppInputDto input)
        //{
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var webConfigApp = (JObject)appsettingsjson["App"];

        //    try
        //    {

        //        using (var client = new HttpClient())
        //        {
        //            var URL = webConfigApp.Property("WhatsAppApi").Value.ToString();

        //            _logger.DebugFormat("SendWhatsApp() - WA-API URL = {0}", URL);

        //            var content = JsonConvert.SerializeObject(input);
        //            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
        //            var byteContent = new ByteArrayContent(buffer);

        //            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        //            client.BaseAddress = new Uri(URL);
        //            client.Timeout = TimeSpan.FromMinutes(10);

        //            var response = await client.PostAsync("", byteContent);
        //            return true;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        _logger.DebugFormat("SendWhatsApp() - Error = {0}", ex.Message);
        //        return false;
        //    }
        //}
        // Basic auth
        public static HttpClient GetClient(string username, string password)
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));

            var client = new HttpClient()
            {
                DefaultRequestHeaders = { Authorization = authValue }
                //Set some other client defaults like timeout / BaseAddress
            };
            return client;
        }

        public async Task<bool> SendWhatsApp(WhatsAppInputDto input)
        {

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionApp = (JObject)appsettingsjson["App"];
                var WhatsAppApi = connectionApp.Property("WhatsAppApi").Value.ToString();



                using (var client = new HttpClient())
                {
                    var url = WhatsAppApi.EnsureEndsWith('/') + "send-message";
                    var content = JsonConvert.SerializeObject(input);
                    ////var buffer = Encoding.UTF8.GetBytes(content);
                    ////var byteContent = new ByteArrayContent(buffer);
                    //StringContent postData = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded");
                    //String userName = "admin";
                    //String passWord = "password";
                    var byteArray = Encoding.ASCII.GetBytes("admin:password");
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var dict = new Dictionary<string, string>();
                    dict.Add("number", input.number);
                    dict.Add("message", input.message);
                    dict.Add("sender", input.sender);
                    var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };

                    //client.BaseAddress = new Uri(url);
                    //client.Timeout = TimeSpan.FromMinutes(10);


                    var response = await client.SendAsync(req);
                    return true;
                }

                //using (var httpClient = new HttpClient())
                //{
                //    String userName = "admin";
                //    String passWord = "password";
                //    string credentials = Convert.ToBase64String(
                //    Encoding.ASCII.GetBytes(userName + ":" + passWord));
                //    request.Headers.client.Headers[HttpRequestHeader.Authorization] = string.Format(
                //        "Basic {0}", credentials);

                //    using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://api-url/id"))
                //    {
                //        var contentList = new List<string>();
                //        request.Content = new StringContent(string.Join("&", input));
                //        request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");


                //        var response = await httpClient.SendAsync(request);
                //        return true;
                //    }
                //}




            }
            catch (Exception ex)
            {

              //  Logger. ("SendWhatsApp() - Error = {0}", ex.Message);
                return false;
            }
        }

        public async Task<string> SendWhatsAppWithResult(WhatsAppInputDto input)
        {

            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionApp = (JObject)appsettingsjson["App"];
                var WhatsAppApi = connectionApp.Property("WhatsAppApi").Value.ToString();

                using (var client = new HttpClient())
                {
                    var url = WhatsAppApi.EnsureEndsWith('/') + "send-message";
                    var content = JsonConvert.SerializeObject(input);
                    var byteArray = Encoding.ASCII.GetBytes("admin:password");
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    var dict = new Dictionary<string, string>();
                    dict.Add("number", input.number);
                    dict.Add("message", input.message);
                    dict.Add("sender", input.sender);
                    var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };

                    var response = await client.SendAsync(req);

                    string responseBody = await response.Content.ReadAsStringAsync();
                    return responseBody;
                }
            }
            catch (Exception ex)
            {
             //   _logger.DebugFormat("SendWhatsApp() - Error = {0}", ex.Message);
                return ex.Message;
            }
        }

    }
}
