﻿namespace BeBillingSystem.BillingSystem
{
	public class ListUserProjectMapDto
	{
		public int projectID { get; set; }
		public string projectCode { get; set; }
		public string projectName { get; set; }
		public string image { get; set; }
		public int projectInfoID { get; set; }
		public bool isOBActive { get; set; }
	}
}