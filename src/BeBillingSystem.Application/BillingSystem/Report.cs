﻿using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.EntityFrameworkCore;
using Dapper;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
	public class Report : BeBillingSystemAppServiceBase
	{
		private readonly IReportDataExporter _exporter;
		private readonly BeBillingSystemDbContext _contextBilling;
		// private readonly IAppFolders _appFolders;
		private readonly PropertySystemDbContext _contextProp;
		private readonly PersonalsNewDbContext _contextPersonal;
		private readonly IWebHostEnvironment _hostEnvironment;
		private readonly IWhatsappHelper _waHelper;
		private readonly IBillingSystems _iBillingSystems;

		public Report(
			  IWebHostEnvironment hostEnvironment,
					  IWhatsappHelper waHelper,
			 IReportDataExporter exporter,
			PropertySystemDbContext contextProp,
			PersonalsNewDbContext contextPersonal,
			BeBillingSystemDbContext contextBilling,
			IBillingSystems iBillingSystems


			)
		{
			_exporter = exporter;
			//     _appFolders = appFolders;
			_waHelper = waHelper;
			_contextProp = contextProp;
			_contextBilling = contextBilling;
			_hostEnvironment = hostEnvironment;
			_contextPersonal = contextPersonal;
			_iBillingSystems = iBillingSystems;


		}

		//public List<DropdownPeriodDto> GetDropdownPeriodBySiteId(int SiteId)
		//{

		//	var result = (from a in _contextBilling.MS_Period
		//				  where a.SiteId == SiteId
		//				  orderby a.Id descending
		//				  select new DropdownPeriodDto
		//				  {
		//					  PeriodId = a.Id,
		//					  PeriodName = a.PeriodMonth.ToString("MMMM") + " " + a.PeriodYear.ToString("yyyy"),
		//				  }).ToList();

		//	return result;


		//}
		//public async Task<ReportUriDto> ReportInvoice(ReportInvoiceInputDto input)
		//{
		//	List<ReportInvoiceListDto> data = new List<ReportInvoiceListDto>();
		//	FileDto file = new FileDto();

		//	#region Constring DB
		//	var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
		//	var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
		//	var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
		//	var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

		//	var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

		//	var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

		//	var connectionApp = (JObject)appsettingsjson["App"];
		//	var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


		//	SqlConnection conn = new SqlConnection(ConstringProp);

		//	conn.Open();



		//	#endregion

		//	if (input.ReportType == 1) //summary
		//	{
		//		string query = $@"SELECT DISTINCT a.SiteId,c.CloseDate, a.ProjectId, a.ClusterId, a.PeriodId, b.UnitCode, b.UnitNo, b.PsCode, a.InvoiceNo, a.JumlahYangHarusDibayar as Amount,d.projectName, e.clusterName, f.TemplateInvoiceName as InvoiceName, 
		//							per.name as PsName , phon.number as psPhone, addr.address as BillAddress, addr.city as BillCity, b.LandArea, b.BuildArea, a.TagihanPerbulan as CurrTrans,a.PenaltyNominal as Penalty, isnull(adj.AdjustmentNominal,0) as AdjustmentAmount, 
		//							a.TagihanBulanSebelumnya as PrevBalance,isnull(payments.PaymentAmount,0)as payment,(( a.TagihanPerbulan + a.TagihanBulanSebelumnya + a.PenaltyNominal+ isnull(adj.AdjustmentNominal,0) ) - (isnull(payments.PaymentAmount,0) )) as EndBalance  
		//						FROM  " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
		//						"join " + dbCatalogBillingString + "..MS_UnitData b on a.UnitDataId = b.Id  " +
		//						"join " + dbCatalogBillingString + "..MS_Period c on a.PeriodId = c.Id  " +
		//						"join " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id LEFT  " +
		//						"join " + dbCatalogBillingString + "..MP_ClusterSite e on a.ClusterId = e.ClusterId  " +
		//						"join " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader f on a.TemplateInvoiceHeaderId = f.Id  " +
		//						"join " + dbCatalogPersonalString + "..PERSONALS per on a.PsCode = per.psCode  " +
		//						"join " + dbCatalogPersonalString + "..TR_Phone phon on a.PsCode = phon.psCode  " +
		//						"join " + dbCatalogPersonalString + "..TR_Address addr on a.PsCode = addr.psCode   " +
		//						"left join (SELECT InvoiceHeaderId,bilheader.TransactionDate,SUM(PaymentAmount) as PaymentAmount  " +
		//						"FROM " + dbCatalogBillingString + "..TR_BillingPaymentHeader bilheader   " +
		//						"left join " + dbCatalogBillingString + "..TR_BillingPaymentDetail bildetail on bilheader.Id = bildetail.BillingHeaderId   " +
		//						"group by InvoiceHeaderId ,bilheader.TransactionDate ) " +
		//						"payments on a.id = payments.InvoiceHeaderId and MONTH(payments.TransactionDate) = MONTH(c.CloseDate)    " +
		//						"left join (select InvoiceHeaderId,sum(AdjustmentNominal) as AdjustmentNominal from  " + dbCatalogBillingString + "..TR_AdjustmentDetail group by InvoiceHeaderId) adj on a.id  = adj.InvoiceHeaderId " +
		//						"where a.SiteId = "+input.SiteId+" AND a.PeriodId = "+input.PeriodId+"";

		//		data = conn.Query<ReportInvoiceListDto>(query).ToList();
		//	}

		//	if (input.ReportType == 2) //detail
		//	{
		//		string query = $@"SELECT c.ProjectId, c.ClusterId, d.projectName, e.clusterName, c.UnitCode, c.UnitNo, a.PsCode, a.InvoiceNo, f.TemplateInvoiceName as InvoiceName, b.InvoiceHeaderId,g.ItemName, CONVERT(varchar(max), b.ItemNominal) as amount  
		//						from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
		//						"join " + dbCatalogBillingString + "..TR_InvoiceDetail b on a.Id = b.InvoiceHeaderId " +
		//						"join " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
		//						"join " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
		//						"LEFT join " + dbCatalogBillingString + "..MP_ClusterSite e on a.ClusterId = e.ClusterId " +
		//						"join " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader f on a.TemplateInvoiceHeaderId = f.Id " +
		//						"join " + dbCatalogBillingString + "..MS_Item g on b.ItemId = g.Id " +
		//						"where a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " " +
		//						"UNION  " +
		//						"SELECT c.ProjectId, c.ClusterId, d.projectName, e.clusterName, c.UnitCode, c.UnitNo, a.PsCode, a.InvoiceNo, f.TemplateInvoiceName as InvoiceName, b.InvoiceHeaderId,('PPN '+ g.ItemName) as ItemName , CONVERT(varchar(max), b.PPNNominal)   as amount " +
		//						"from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
		//						"join " + dbCatalogBillingString + "..TR_InvoiceDetail b on a.Id = b.InvoiceHeaderId " +
		//						"join " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
		//						"join " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
		//						"LEFT join " + dbCatalogBillingString + "..MP_ClusterSite e on a.ClusterId = e.ClusterId " +
		//						"join " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader f on a.TemplateInvoiceHeaderId = f.Id " +
		//						"join " + dbCatalogBillingString + "..MS_Item g on b.ItemId = g.Id " +
		//						"where a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " and b.PPNNominal != 0 " +
		//						//"UNION " + //billing payment
		//						//"SELECT c.ProjectId, c.ClusterId, d.projectName, e.clusterName, c.UnitCode, c.UnitNo, a.PsCode, a.InvoiceNo, f.TemplateInvoiceName as InvoiceName, b.InvoiceHeaderId, j.PaymentMethodName,  '(' + CONVERT(varchar(max), h.PaymentAmount) +')'  as amount " +
		//						//" from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
		//						//"join " + dbCatalogBillingString + "..TR_InvoiceDetail b on a.Id = b.InvoiceHeaderId " +
		//						//" join " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
		//						//"join " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
		//						//"join " + dbCatalogPropertyString + "..MS_Cluster e on a.ClusterId = e.Id " +
		//						//"join " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader f on a.TemplateInvoiceHeaderId = f.Id " +
		//						//"join " + dbCatalogBillingString + "..TR_BillingPaymentDetail h on a.Id = h.InvoiceHeaderId " +
		//						//"join " + dbCatalogBillingString + "..TR_BillingPaymentHeader i on h.BillingHeaderId = i.Id " +
		//						//"join " + dbCatalogBillingString + "..MS_PaymentMethod j on i.PaymentMethodId = j.Id " +
		//						//"where a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " " +
		//						"UNION " +
  //                              "SELECT c.ProjectId, c.ClusterId, d.projectName, e.clusterName, c.UnitCode, c.UnitNo, a.PsCode, a.InvoiceNo, f.TemplateInvoiceName as InoviceName, adj.InvoiceHeaderId,'Adjustment',  '(' + CONVERT(varchar(max), adj.AdjustmentNominal) +')'  as amount  " +
		//						"from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
		//						"inner join " + dbCatalogBillingString + "..TR_AdjustmentDetail adj on a.id = adj.InvoiceHeaderId " +
		//						"join " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
		//						"join " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
		//						"LEFT join " + dbCatalogBillingString + "..MP_ClusterSite e on a.ClusterId = e.ClusterId " +
		//						"join " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader f on a.TemplateInvoiceHeaderId = f.Id where a.SiteId = "+input.SiteId+" and a.PeriodId = "+input.PeriodId+" and adj.AdjustmentNominal != 0";

		//		data = conn.Query<ReportInvoiceListDto>(query).ToList();
		//	}

		//	if (input.ProjectId != 0 || input.ClusterId != null)
		//	{

		//		data = data
		//			.WhereIf(input.ProjectId != 0, item => item.ProjectId == input.ProjectId)
		//			.WhereIf(input.ClusterId != null, item => input.ClusterId.Contains(item.ClusterId))
		//			.ToList();
		//	}


		//	if (data.Count > 0)
		//	{
		//		var paramToExcel = new ViewReportinvoiceDto
		//		{
		//			printBy = (from x in _contextBilling.Users
		//					   where x.Id == AbpSession.GetUserId()
		//					   select x.Name).FirstOrDefault(),

		//			periodName = (from a in _contextBilling.MS_Period
		//						  where input.PeriodId.Equals(a.Id)
		//						  select
		//						  a.PeriodMonth.ToString("MMMM") + " " + a.PeriodYear.ToString("yyyy")
		//						  ).FirstOrDefault(),

		//			ReportInvoiceListDto = data

		//		};

		//		if (input.ReportType == 1) //summary
		//		{
		//			file = _exporter.ExportToExcelReportInvoiceResult(paramToExcel);


		//		}
		//		else
		//		{

		//			file = _exporter.ExportToExcelReportInvoiceDetailResult(paramToExcel);

		//		}


		//		//var tempRootPath = Path.Combine(_appFolders.UploadRootDirectory, _appFolders.TempFileDownloadDirectory);


		//		System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
		//		var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


		//		var filePath = Path.Combine(tempRootPath);
		//		if (!File.Exists(filePath))
		//		{
		//			throw new Exception("Requested File doesn't exists");
		//		}

		//		var pathExport = Path.Combine(tempRootPath);
		//		//retrieve data excel from temporary download folder
		//		var fileBytes = File.ReadAllBytes(filePath);
		//		//write excel file to share folder / local folder
		//		File.WriteAllBytes(pathExport, fileBytes);
		//		//string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
		//		string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
		//		return new ReportUriDto { Uri = URL.Replace("\\", "/") };
		//	}
		//	else
		//	{

		//		return new ReportUriDto { };
		//	}

		//}


		//public async Task<ReportUriDto> ReporDaily(ReportDailyInputDto input)
		//{

		//	FileDto file = new FileDto();

		//	#region Constring DB
		//	var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
		//	var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
		//	var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
		//	var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

		//	var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

		//	var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

		//	var connectionApp = (JObject)appsettingsjson["App"];
		//	var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


		//	SqlConnection conn = new SqlConnection(ConstringProp);

		//	conn.Open();



		//	#endregion


		//	var startDate = input.startDate.ToString("dd-MM-yyyy");
		//	var endDate = input.endDate.ToString("dd-MM-yyyy");
		//	var clusterSting = string.Join(",", input.ClusterId);
		//	string query = "";

		//	if (startDate == "01-01-0001" || endDate == "01-01-0001")

		//	{
		//		query = $@"
		//					SELECT DISTINCT  b.Id, d.ProjectId, d.ClusterId,j.ProjectName, k.ClusterName, d.UnitCode, d.UnitNo, c.InvoiceNo, e.TemplateInvoiceName, b.ReceiptNumber, f.PaymentMethodName as PaymentTypeName, 
		//					g.BankName, FORMAT (b.TransactionDate, 'dd-MM-yyyy') as TransactionDate , a.PaymentAmount, b.Charge, (a.PaymentAmount - b.Charge) as net, FORMAT (b.CancelDate, 'dd-MM-yyyy') as CancelDate, c.PsCode, h.name,
		//					h.NPWP, VirtualAccNo, l.idNo as KTP , b.CancelPayment, b.PaymentMethodId from " + dbCatalogBillingString + "..TR_BillingPaymentDetail a " +
		//					  "JOIN " + dbCatalogBillingString + "..TR_BillingPaymentHeader b on a.BillingHeaderId = b.Id " +
		//					  "JOIN " + dbCatalogBillingString + "..TR_InvoiceHeader c on a.InvoiceHeaderId = c.Id " +
		//					  "JOIN " + dbCatalogBillingString + "..MS_UnitData d on c.UnitDataId = d.Id " +
		//					  "JOIN " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader e on c.TemplateInvoiceHeaderId = e.Id " +
		//					  "JOIN " + dbCatalogBillingString + "..MS_PaymentMethod f on b.PaymentMethodId = f.id " +
		//					  "LEFT JOIN " + dbCatalogBillingString + "..MS_Bank g on b.BankId = g.Id " +
		//					  " JOIN " + dbCatalogPersonalString + "..PERSONALS h on b.PsCode = h.psCode " +
		//					  "JOIN " + dbCatalogBillingString + "..MP_ProjectSite j on c.ProjectId = j.ProjectId " +
		//					  "JOIN " + dbCatalogBillingString + "..MP_ClusterSite k on c.ClusterId = k.ClusterId " +
		//					  "JOIN " + dbCatalogPersonalString + "..TR_ID l on b.PsCode = l.psCode " +
		//					  "where c.ProjectId = " + input.ProjectId + " and c.PeriodId = " + input.PeriodId + " AND c.SiteId = " + input.SiteId + " and c.ClusterId in (" + clusterSting + ")";



		//	}
		//	else
		//	{

		//		query = $@"
		//					SELECT DISTINCT  b.Id, d.ProjectId, d.ClusterId,j.ProjectName, k.ClusterName, d.UnitCode, d.UnitNo, c.InvoiceNo, e.TemplateInvoiceName, b.ReceiptNumber, f.PaymentMethodName as PaymentTypeName, 
		//					g.BankName, FORMAT (b.TransactionDate, 'dd-MM-yyyy') as TransactionDate , a.PaymentAmount, b.Charge, (a.PaymentAmount - b.Charge) as net, FORMAT (b.CancelDate, 'dd-MM-yyyy') as CancelDate, c.PsCode, h.name,
		//					h.NPWP, VirtualAccNo, l.idNo as KTP , b.CancelPayment, b.PaymentMethodId from " + dbCatalogBillingString + "..TR_BillingPaymentDetail a " +
		//					   "JOIN " + dbCatalogBillingString + "..TR_BillingPaymentHeader b on a.BillingHeaderId = b.Id " +
		//					   "JOIN " + dbCatalogBillingString + "..TR_InvoiceHeader c on a.InvoiceHeaderId = c.Id " +
		//					   "JOIN " + dbCatalogBillingString + "..MS_UnitData d on c.UnitDataId = d.Id " +
		//					   "JOIN " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader e on c.TemplateInvoiceHeaderId = e.Id " +
		//					   "JOIN " + dbCatalogBillingString + "..MS_PaymentMethod f on b.PaymentMethodId = f.id " +
		//					   "LEFT JOIN " + dbCatalogBillingString + "..MS_Bank g on b.BankId = g.Id " +
		//					   " JOIN " + dbCatalogPersonalString + "..PERSONALS h on b.PsCode = h.psCode " +
		//					   "JOIN " + dbCatalogBillingString + "..MP_ProjectSite j on c.ProjectId = j.ProjectId " +
		//					   "JOIN " + dbCatalogBillingString + "..MP_ClusterSite k on c.ClusterId = k.ClusterId " +
		//					   "JOIN " + dbCatalogPersonalString + "..TR_ID l on b.PsCode = l.psCode " +
		//					   "where c.ProjectId = " + input.ProjectId + " and c.PeriodId = " + input.PeriodId + " AND c.SiteId = " + input.SiteId + " and FORMAT (b.TransactionDate, 'dd-MM-yyyy') >= '" + startDate + "' and FORMAT (b.TransactionDate, 'dd-MM-yyyy') <= '" + endDate + "' and c.ClusterId in (" + clusterSting + ")";

		//	}
		//	List<ReportDailyListDto> data = conn.Query<ReportDailyListDto>(query).ToList();


		//	if (input.PaymentTypeId != 0 || input.CancelPayment != 0)
		//	{

		//		data = data
		//			.WhereIf(input.PaymentTypeId != 0, item => input.PaymentTypeId.Equals(item.PaymentMethodId))
		//			.ToList();


		//		if (input.CancelPayment != 0)
		//		{
		//			var cancelpaymentBool = input.CancelPayment == 1 ? true : false;

		//			data = data
		//		   .WhereIf(input.CancelPayment != 0, item => cancelpaymentBool.Equals(item.CancelPayment))
		//		   .ToList();
		//		}
		//	}


		//	if (data.Count > 0)
		//	{
		//		var paramToExcel = new ViewReportDailyDto
		//		{
		//			printBy = (from x in _contextBilling.Users
		//					   where x.Id == AbpSession.GetUserId()
		//					   select x.Name).FirstOrDefault(),

		//			periodName = (from a in _contextBilling.MS_Period
		//						  where input.PeriodId.Equals(a.Id)
		//						  select
		//						  a.PeriodMonth.ToString("MMMM") + " " + a.PeriodYear.ToString("yyyy")
		//						  ).FirstOrDefault(),

		//			ReportDailyListDto = data

		//		};


		//		file = _exporter.ExportToExcelDailyReportResult(paramToExcel);


		//		System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
		//		var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


		//		var filePath = Path.Combine(tempRootPath);
		//		if (!File.Exists(filePath))
		//		{
		//			throw new Exception("Requested File doesn't exists");
		//		}

		//		var pathExport = Path.Combine(tempRootPath);
		//		//retrieve data excel from temporary download folder
		//		var fileBytes = File.ReadAllBytes(filePath);
		//		//write excel file to share folder / local folder
		//		File.WriteAllBytes(pathExport, fileBytes);
		//		//string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
		//		string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
		//		return new ReportUriDto { Uri = URL.Replace("\\", "/") };
		//	}
		//	else
		//	{

		//		return new ReportUriDto { };
		//	}

		//}



		//public async Task<ReportUriDto> ReportDetailStatement(ReportDetailStatementDto input)
		//{

		//	FileDto file = new FileDto();

		//	#region Constring DB
		//	var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
		//	var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
		//	var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
		//	var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

		//	var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

		//	var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

		//	var connectionApp = (JObject)appsettingsjson["App"];
		//	var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


		//	SqlConnection conn = new SqlConnection(ConstringProp);

		//	conn.Open();


		//	#endregion
		//	var StartMonth = (from a in input.PeriodYear
		//					  orderby a ascending
		//					  select a).FirstOrDefault();


		//	var EndMonth = (from a in input.PeriodYear
		//					orderby a descending
		//					select a).FirstOrDefault();

		//	var clusterSting = string.Join(",", input.ClusterId);

		//	string querySummary = "";
		//	string queryDetail = "";

		//	//if (startDate == "01-01-0001" || endDate == "01-01-0001")

		//	//{
		//	querySummary = $@"select ps.ProjectId,ps.ProjectName,cs.ClusterId,cs.ClusterName,ud.UnitCode,ud.UnitNo ,format(PeriodMonth,'MMMM') as PeriodMonth,month(periodmonth) as PeriodMonthAngka,year(PeriodYear) as PeriodYear,ih.InvoiceNo,ih.TagihanPerbulan as tagihaninvoice,isnull(pay.PaymentAmount,0) as PaymentAmount
		//						,ih.TagihanPerbulan - isnull(pay.PaymentAmount,0) as outstanding 
		//						from " + dbCatalogBillingString + "..TR_InvoiceHeader ih  " +
		//					"inner join " + dbCatalogBillingString + "..MS_Period pd on ih.periodid = pd.id  " +
		//					" inner join " + dbCatalogBillingString + "..MP_ProjectSite ps on ih.ProjectId = ps.ProjectId  " +
		//					" inner join " + dbCatalogBillingString + "..MP_ClusterSite cs on ih.ClusterId = cs.ClusterId  " +
		//					"inner join " + dbCatalogBillingString + "..MS_UnitData ud on ih.UnitDataId = ud.Id  " +
		//					" left join (	select InvoiceHeaderId,sum(PaymentAmount) as PaymentAmount  " +
		//					"    from " + dbCatalogBillingString + "..TR_BillingPaymentdetail bd  " +
		//					"   inner join " + dbCatalogBillingString + "..TR_BillingPaymentHeader bh on bd.BillingHeaderId = bh.id " +
		//					"  where cancelpayment = 0 group by InvoiceHeaderId) pay on ih.Id = pay.InvoiceHeaderId " +
		//					" where ih.SiteId = " + input.SiteId + " AND ud.UnitNo = '" + input.UnitNo + "' AND ud.UnitCode = '" + input.UnitCode + "' AND cs.ClusterId = " + input.ClusterId + " AND cs.ProjectId = " + input.ProjectId + "  ORDER BY ih.InvoiceNo ASC";

		//	queryDetail = $@"select x.ProjectId,x.ProjectName,x.ClusterId,x.ClusterName,x.UnitCode,x.UnitNo,x.PeriodMonth,x.periodmonthangka,x.PeriodYear,x.InvoiceNo,x.customer,sum(x.BPL) as 'BPL',
		//					sum(x.PpnBPL) as 'PpnBPL',x.ADJBPL,sum(x.TagihanAir) as 'TagihanAir',x.TagihanBulanSebelumnya,x.Penalty,x.CurrTrans,x.Payment 
		//				from (	select ps.ProjectId,month(periodmonth) as PeriodMonthAngka,ps.ProjectName,cs.ClusterId,cs.ClusterName,ud.UnitCode,ud.UnitNo ,
		//							format(PeriodMonth,'MMMM') as PeriodMonth,year(PeriodYear) as PeriodYear,id.InvoiceHeaderId,ih.InvoiceNo,p.name as customer,i.ItemName, 
		//							case when ItemName = 'BPL' then id.ItemNominal else 0 end as 'BPL',case when ItemName = 'BPL' then id.PPNNominal else 0 end as 'PpnBPL',isnull(adj.AdjustmentNominal,0) as 'ADJBPL', 
		//							case when ItemName = 'Water' then id.ItemNominal else 0 end as 'TagihanAir',
		//							ih.TagihanBulanSebelumnya,  ih.PenaltyNominal as 'Penalty',ih.TagihanPerbulan as 'CurrTrans',isnull(pay.PaymentAmount,0) as 'Payment'   
		//						from " + dbCatalogBillingString + "..TR_InvoiceHeader ih  " +
		//						"inner join " + dbCatalogBillingString + "..TR_InvoiceDetail id on ih.id = id.InvoiceHeaderId    " +
		//						"inner join " + dbCatalogBillingString + "..MS_Item i on id.ItemId = i.id    " +
		//						"inner join " + dbCatalogBillingString + "..MS_Period pd on ih.periodid = pd.id   " +
		//						"inner join " + dbCatalogBillingString + "..MP_ProjectSite ps on ih.ProjectId = ps.ProjectId    " +
		//						"inner join " + dbCatalogBillingString + "..MP_ClusterSite cs on ih.ClusterId = cs.ClusterId    " +
		//						"inner join " + dbCatalogBillingString + "..MS_UnitData ud on ih.UnitDataId = ud.Id   " +
		//						"inner join " +  dbCatalogPersonalString + "..personals p on ih.PsCode = p.pscode   " +
		//						"left join (	select InvoiceHeaderId,sum(PaymentAmount) as PaymentAmount   " +
		//						"from " + dbCatalogBillingString + "..TR_BillingPaymentdetail bd   " +
		//						"inner join " + dbCatalogBillingString + "..TR_BillingPaymentHeader bh on bd.BillingHeaderId = bh.id   " +
		//						"where cancelpayment = 0 group by InvoiceHeaderId) pay on ih.Id = pay.InvoiceHeaderId " +
		//						"left join (select InvoiceHeaderId,sum(AdjustmentNominal) as AdjustmentNominal from " + dbCatalogBillingString + "..TR_AdjustmentDetail group by InvoiceHeaderId) adj on ih.id  = adj.InvoiceHeaderId " +
		//						"where ih.SiteId = "+input.SiteId+" AND ud.UnitNo = '"+input.UnitNo+"' AND ud.UnitCode = '"+input.UnitCode+"' AND cs.ClusterId = "+input.ClusterId+" AND cs.ProjectId = "+input.ProjectId+"   " +
		//						")X   " +
		//						"group by x.projectid,x.ProjectName,x.periodmonthangka,x.ClusterId,x.ClusterName,x.UnitCode,x.UnitNo,x.PeriodMonth,x.PeriodYear,x.InvoiceNo,x.customer,x.ADJBPL,x.TagihanBulanSebelumnya,x.Penalty,x.CurrTrans,x.Payment  " +
		//						"order by X.InvoiceNo";






		//	//}
		//	//else
		//	//{

		//	//}
		//	List<ReportSummaryStatmentDto> summary = conn.Query<ReportSummaryStatmentDto>(querySummary).ToList();
		//	List<ReportDetailStatmentDto> detail = conn.Query<ReportDetailStatmentDto>(queryDetail).ToList();


		//	if (input.ProjectId != 0 || input.ClusterId != 0 || input.StartMonth != 0 || input.EndMonth != 0 || input.PeriodYear != null)
		//	{

		//		summary = summary
		//			//.WhereIf(input.ProjectId != 0, item => input.ProjectId.Equals(item.ProjectId))
		//			//.WhereIf(input.ClusterId != 0, item => input.ClusterId.Equals(item.ClusterId))
		//			//.WhereIf(input.UnitCode != null, item => input.UnitCode.Contains(item.UnitCode))
		//			//.WhereIf(input.UnitNo != null, item => input.UnitNo.Contains(item.UnitNo))
		//			.WhereIf(input.StartMonth != 0, item => item.periodmonthangka >= input.StartMonth)
		//			.WhereIf(input.EndMonth != 0, item => item.periodmonthangka <= input.EndMonth)
		//			.WhereIf(input.PeriodYear.Count != 0, item => input.PeriodYear.Contains(item.PeriodYear))
		//			.ToList();

		//		detail = detail
		//		   //.WhereIf(input.ProjectId != 0, item => input.ProjectId.Equals(item.ProjectId))
		//		   //.WhereIf(input.ClusterId != 0, item => input.ClusterId.Equals(item.ClusterId))
		//		   //.WhereIf(input.UnitCode != null, item => input.UnitCode.Contains(item.UnitCode))
		//		   //.WhereIf(input.UnitNo != null, item => input.UnitNo.Contains(item.UnitNo))
		//		   .WhereIf(input.StartMonth != 0, item => item.periodmonthangka >= input.StartMonth)
		//			.WhereIf(input.EndMonth != 0, item => item.periodmonthangka <= input.EndMonth)
		//			.WhereIf(input.PeriodYear.Count != 0, item => input.PeriodYear.Contains(item.PeriodYear))
		//		  .ToList();
		//	}

		//	var paramToExcel = new ViewReportDetailStetmentDto
		//	{
		//		printBy = (from x in _contextBilling.Users
		//				   where x.Id == AbpSession.GetUserId()
		//				   select x.Name).FirstOrDefault(),



		//		ReportSummaryStatmentList = summary,
		//		ReportDetailStatmentList = detail

		//	};


		//	file = _exporter.ExportToExcelDetailStetmentReportResult(paramToExcel);


		//	System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
		//	var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


		//	var filePath = Path.Combine(tempRootPath);
		//	if (!File.Exists(filePath))
		//	{
		//		throw new Exception("Requested File doesn't exists");
		//	}

		//	var pathExport = Path.Combine(tempRootPath);
		//	//retrieve data excel from temporary download folder
		//	var fileBytes = File.ReadAllBytes(filePath);
		//	//write excel file to share folder / local folder
		//	File.WriteAllBytes(pathExport, fileBytes);
		//	//string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
		//	string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
		//	return new ReportUriDto { Uri = URL.Replace("\\", "/") };


		//}

		//public string ReportPDFDetailStatement(ReportPDFDetailStatementDto input)
		//{

		//	FileDto file = new FileDto();
		//	var paramToPDF = new ParamPDFReportDetailStatment();

		//	#region Constring DB
		//	var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
		//	var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
		//	var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
		//	var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

		//	var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

		//	var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
		//	var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

		//	var connectionApp = (JObject)appsettingsjson["App"];
		//	var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


		//	SqlConnection conn = new SqlConnection(ConstringProp);

		//	conn.Open();


		//	#endregion


		//	string querySummary = "";
		//	string queryDetail = "";

		//	var LoopDeskripsiPdf = new List<String>();




		//	var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
		//					  join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
		//					  join c in _contextBilling.MS_Period on a.PeriodId equals c.Id
		//					  where a.SiteId == input.SiteId && a.ProjectId == input.ProjectId && b.ClusterId == input.ClusterId && b.UnitNo == input.UnitNo
		//					  && b.UnitCode == input.UnitCode
		//					  select new GetListReportDetailStatment
		//					  {
		//						  InvoiceHeaderID = a.Id,
		//						  UnitDataId = a.UnitDataId,
				   
		//						  UnitNo = b.UnitNo,
		//						  UnitCode = b.UnitCode,
		//						  IsPenalty = a.IsPenalty,
		//						  PenaltyNominal = a.PenaltyNominal,
		//						  PsCode = a.PsCode,
		//						  PeriodId = a.PeriodId,
		//						  Periode = c.PeriodMonth.ToString("MMMM") + " " + c.PeriodYear.ToString("yyyy"),
		//						  BulanJatuhTempo = c.CloseDate.ToString("dd/MM/yyyy"),
		//						  InvoiceNo = a.InvoiceNo,
		//						  TagihanBulanSebelumnya = a.TagihanBulanSebelumnya,
		//						  TagihanBulanIni = a.TagihanPerbulan,
		//						  TemplateInvoiceHeaderId = a.TemplateInvoiceHeaderId,
		//						  PeriodeYear = c.PeriodYear.ToString("yyyy"),
		//						  periodmonthangka = c.PeriodMonth.Month

		//					  }

		//							)
		//							  .WhereIf(input.StartMonth != 0, item => item.periodmonthangka >= input.StartMonth)
		//							  .WhereIf(input.EndMonth != 0, item => item.periodmonthangka <= input.EndMonth)
		//							  .WhereIf(input.PeriodYear != null, item => input.PeriodYear.Contains(item.PeriodeYear))


		//							.Distinct().ToList();


		//	var getInvoiceHeader = getInvoice.Select(x => new { x.UnitNo, x.UnitCode, x.PsCode }).FirstOrDefault();

		//	if (getInvoice.Count == 0)
		//	{

		//		throw new UserFriendlyException("Data report not found");

		//	}


		//	var getDataCust = (from a in _contextPersonal.PERSONALS
		//					   join b in _contextPersonal.TR_Address on a.psCode equals b.psCode into addrs
		//					   from addrsNull in addrs.DefaultIfEmpty()
		//					   where a.psCode == getInvoiceHeader.PsCode
		//					   select new
		//					   {
		//						   a.name,
		//						   addrsNull.address

		//					   }).FirstOrDefault();

		//	//generate isi Konten
		//	foreach (var item in getInvoice)
		//	{

		//		var isi = "";

		//		var LoopSubDeskripsi = new List<String>();
		//		var StingData = "";

		//		var getPeriod = (from a in _contextBilling.MS_Period
		//						 where a.Id == item.PeriodId
		//						 orderby a.CreationTime descending
		//						 select a).FirstOrDefault();

		//		PembayaranSebelumnyaDto pembayaranSebelumnya = _iBillingSystems.PembayaranSebelumnya(item.UnitDataId, getPeriod.StartDate, input.SiteId, item.TemplateInvoiceHeaderId);


		//		if (pembayaranSebelumnya.TotalPembayaran != 0)
		//		{
		//			#region getListpembayaran sebelumnya

		//			foreach (var pembayaran in pembayaranSebelumnya.DetailPembayaranSebelumnya)
		//			{

		//				StingData = " <tr> <td>" + pembayaran.TransactionDate.ToString("dd MMMM yyyy") + "</td> " +
		//			"<td>Pembayaran melalui " + pembayaran.PaymentMethod + "</td> " +
		//			" <td>(Rp. " + NumberHelper.IndoFormatWithoutTail(pembayaran.PaymentAmount) + ")</td>" +
		//			" </tr>";

		//				LoopSubDeskripsi.Add(StingData);

		//			}

		//			#endregion



		//		}


		//		var getAdjList = (from a in _contextBilling.TR_AdjustmentDetail
		//						  where a.InvoiceHeaderId == item.InvoiceHeaderID
		//						  select a).ToList();


		//		if (getAdjList.Count() > 0)
		//		{

		//			foreach (var adj in getAdjList) {
		//				#region memiliki AdjustmentNominal

		//				StingData = " <tr> <td>" + DateTime.Now.ToString("dd MMMM yyyy") + "</td> " +
		//							 "<td>Adjustment Fee</td> " +
		//							 " <td>(Rp. " + NumberHelper.IndoFormatWithoutTail(adj.AdjustmentNominal) + ")</td>" +
		//							 " </tr>";

		//				LoopSubDeskripsi.Add(StingData);
		//				#endregion


		//			}



		//		}



		//		var getDetailInvoice = (from a in _contextBilling.TR_InvoiceDetail
		//								join b in _contextBilling.MS_Item on a.ItemId equals b.Id
		//								where a.InvoiceHeaderId == item.InvoiceHeaderID
		//								select new
		//								{
		//									a.ItemNominal,
		//									a.CreationTime,
		//									a.ItemId,
		//									a.PPNNominal,
		//									b.ItemName,
		//								}).ToList();


		//		if (getDetailInvoice.Count > 0)
		//		{

		//			foreach (var detailinvoice in getDetailInvoice)
		//			{


		//				StingData = " <tr> <td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy") + "</td> " +
		//					   "<td>" + detailinvoice.ItemName + "</td> " +
		//					   " <td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" +
		//					   " </tr>";


		//				LoopSubDeskripsi.Add(StingData);

		//				if (detailinvoice.PPNNominal > 0)
		//				{

		//					StingData = " <tr> <td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy") + "</td> " +
		//					  "<td>PPN " + detailinvoice.ItemName + "</td> " +
		//					  " <td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" +
		//					  " </tr>";

		//					LoopSubDeskripsi.Add(StingData);
		//				}

		//			}
		//		}


		//		if (item.PenaltyNominal > 0)
		//		{


		//			StingData = " <tr> <td>-</td> " +
		//						 "<td>Penalty </td> " +
		//						 " <td>(Rp. " + NumberHelper.IndoFormatWithoutTail(item.PenaltyNominal) + ")</td>" +
		//						 " </tr>";

		//			LoopSubDeskripsi.Add(StingData);
		//		}



		//		string LoopSubDeskripsiString = string.Join(Environment.NewLine, LoopSubDeskripsi.ToArray());

		//		var adjCount = getAdjList.Select(x => x.AdjustmentNominal).Sum();

		//		item.TotalPayment = ( pembayaranSebelumnya.TotalPembayaran);
		//		item.EndingBalance = ((item.TagihanBulanSebelumnya + item.TagihanBulanIni + item.PenaltyNominal + adjCount) - (item.TotalPayment));



		//		isi = "<div>\r\n                <table style=\"width: 100%;\">    \r\n                    " +
		//			" <tr> " +
		//			" <td width = 33% > Periode : " + item.Periode + "</td> " +
		//			" <td width = 33% > Jatuh Tempo: " + item.BulanJatuhTempo + "</td> " +
		//			" <td width = 33% > Invoice No: " + item.InvoiceNo + "</td> " +
		//			" </tr> " +
		//			"   </table>\r\n        " +
		//			"        <table style=\"margin-left: -3px;border-collapse: collapse; width: 100%;\" class=\"main\">\r\n                    <thead>\r\n                        <tr style=\"text-align: left;background-color: #DEE2E8;\">\r\n                            <th>Date</th>\r\n                            <th>Description</th>\r\n                            <th>Amount</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                       " +


		//			" " + LoopSubDeskripsiString + " " +

		//			" </tbody> </table>\r\n                <br>\r\n                <table style=\"width: 100%;\">\r\n                    <tr>\r\n                        <td width=\"50%\">\r\n                        </td>\r\n                        <td>\r\n                            <table class=\"resume-left\">\r\n                        " +
		//			" <tr> " +
		//			"   <td>Previous Balance</td> " +
		//			"  <td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.TagihanBulanSebelumnya) + "</td> " +
		//			" </tr> " +
		//			"    <tr> " +
		//			"   <td>Current Transaction</td> " +
		//			" <td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.TagihanBulanIni) + "</td> " +
		//			" </tr> " +
		//			" <tr> " +
		//			" <td>Payment</td> " +
		//			" <td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.TotalPayment) + "</td> " +
		//			" </tr> " +
		//			"</table> " +
		//			" <p style=\"border-bottom: 1px solid black;\"></p> " +
		//			"  <table class=\"resume-right\">\r\n                             " +
		//			"   <tr>\r\n                                   " +
		//			" <td>Ending Balance</td>\r\n                         " +
		//			"           <td>\r\n                                " +
		//			"        Rp. " + NumberHelper.IndoFormatWithoutTail(item.EndingBalance) + "\r\n           " +
		//			"                         </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <br><br>\r\n            </div>\r\n\r\n";

		//		LoopDeskripsiPdf.Add(isi);


		//	}


		//	paramToPDF.UnitNo = getInvoiceHeader.UnitNo;
		//	paramToPDF.UnitCode = getInvoiceHeader.UnitCode;
		//	paramToPDF.PsCode = getInvoiceHeader.PsCode;
		//	paramToPDF.CustName = getDataCust.name;
		//	paramToPDF.CustAlamatKoresponden = getDataCust.address;



		//	string LoopDeskripsiPdfString = string.Join(Environment.NewLine, LoopDeskripsiPdf.ToArray());

		//	paramToPDF.IsiKonten = LoopDeskripsiPdfString;

		//	var fileName = GenerateReportDetailStatmentPdf(paramToPDF);


		//	return ipPublic + "/Temp/Downloads/" + fileName;



		//}

		//private string GenerateReportDetailStatmentPdf(ParamPDFReportDetailStatment input)
		//{

		//	var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
		//	var connectionApp = (JObject)appsettingsjson["App"];


		//	var destinationPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads";

		//	if (!Directory.Exists(destinationPath))
		//	{
		//		Directory.CreateDirectory(destinationPath);
		//	}

		//	var fileName = "" + input.UnitCode + input.UnitNo + input.PsCode + ".pdf";
		//	var filePath = Path.Combine(destinationPath, fileName);
		//	try
		//	{
		//		var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\MasterTemplate\reportdetailstatment.html";

		//		var htmlToConvert = File.ReadAllText(templateFile);


		//		HtmlToPdf converter = new HtmlToPdf();
		//		converter.Options.PdfPageSize = PdfPageSize.A4;
		//		converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;





		//		var doc = converter.ConvertHtmlString(htmlToConvert

		//		.Replace("{{UnitCode}}", input.UnitCode)
		//		.Replace("{{UnitNo}}", input.UnitNo)
		//		.Replace("{{CustName}}", input.CustName)
		//		.Replace("{{PsCode}}", input.PsCode)
		//		.Replace("{{CustAlamatKoresponden}}", input.CustAlamatKoresponden)
		//		.Replace("{{IsiKonten}}", input.IsiKonten)
		//		);
		//		String hasil = doc.ToString();

		//		var pdfhtml = doc;
		//		byte[] results;

		//		using (var stream = new MemoryStream())
		//		{
		//			doc.Save(stream);
		//			results = stream.ToArray();
		//			doc.Close();
		//		}


		//		if (File.Exists(filePath))
		//		{
		//			File.Delete(filePath);
		//		}

		//		File.WriteAllBytes(filePath, results);

		//	}
		//	catch (Exception ex)
		//	{

		//	}

		//	return fileName;

		//}






		//public List<GetDropdownYearDto> GetDropdownPeriodYear()
		//{


		//	var getYear = (from a in _contextBilling.MS_Period
		//				   select new GetDropdownYearDto
		//				   {

		//					   Year = a.PeriodYear.ToString("yyyy")

		//				   }).ToList();

		//	getYear = getYear.GroupBy(x => x.Year).Select(y => new GetDropdownYearDto { Year = y.Key }).ToList();



		//	return getYear;


		//}
		//public List<GetDropdownPeriodMonthDto> GetDropdownPeriodMonth(int siteID)
		//{


		//	var getYear = (from a in _contextBilling.MS_Period
		//				   where a.SiteId == siteID
		//				   select new GetDropdownPeriodMonthDto
		//				   {

		//					   NumberMonth = a.PeriodMonth.Month,
		//					   PeriodMonth = a.PeriodMonth.ToString("MMMM")


		//				   }).Distinct().ToList();



		//	return getYear;


		//}

		//public List<DropdownUnitCodeDto> GetDropdownUnitCodeByClusterID(int SiteId, int clusterID)
		//{

		//	var getData = (from a in _contextBilling.TR_InvoiceHeader
		//				   join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
		//				   where a.SiteId == SiteId && a.ClusterId == clusterID
		//				   select new DropdownUnitCodeDto
		//				   {
		//					   UnitCode = b.UnitCode,

		//				   }).Distinct().ToList();

		//	return getData;

		//}

		//public List<DropdownUnitNoDto> GetDropdownUnitNoByCluster(int SiteId, int ClusterID, string UnitCode)
		//{

		//	var getData = (from a in _contextBilling.TR_InvoiceHeader
		//				   join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
		//				   where a.SiteId == SiteId && a.ClusterId == ClusterID && b.UnitCode == UnitCode

		//				   select new DropdownUnitNoDto
		//				   {
		//					   UnitNo = b.UnitNo,

		//				   }).Distinct().ToList();

		//	return getData.OrderBy(x => x.UnitNo).ToList();

		//}


	}
}
