﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.BillingSystem.Dto;
using System.Net.Http;
using BeBillingSystem.EntityFrameworkCore;
using BeBillingSystem.Helpers;
using BeBillingSystem.PropertySystemDB;
using BeBillingSystem.SlesTracking;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Data;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using SelectPdf;

using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using System.IO;
using System.Linq;


using static BeBillingSystem.Roles.RoleAppService;
using static Castle.MicroKernel.ModelBuilder.Descriptors.InterceptorDescriptor;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Net.Http.Headers;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace BeBillingSystem.BillingSystem
{

    public class CashierSystem : BeBillingSystemAppServiceBase
    {
        private readonly IReportDataExporter _exporter;
        private readonly BeBillingSystemDbContext _contextBilling;
        private readonly IWebHostEnvironment _hostEnvironment;
		private readonly PersonalsNewDbContext _personalsDbcontext;
		//private readonly PersonalsNewDbContext _personalDbContext;
		public IAppFolders _appFolders { get; set; }
        private readonly PropertySystemDbContext _contextProp;
        private readonly PersonalsNewDbContext _contextPersonal;
		private readonly IConfiguration _configuration;
		private readonly IHttpContextAccessor _httpContextAccessor;
		private readonly NewCommDbContext _newCommDbContext;
		private readonly PropertySystemDbContext _propertySystemDbContext;

		private readonly HttpClient _httpClient;
		public CashierSystem(
            //IAppFolders appFolders,
            IWebHostEnvironment hostEnvironment,
            IReportDataExporter exporter,
            PropertySystemDbContext contextProp,
            BeBillingSystemDbContext contextBilling,
            PersonalsNewDbContext contextPersonal,
			  PersonalsNewDbContext personalsDbcontext,
			  PersonalsNewDbContext personalDbContext,
		 IConfiguration configuration, NewCommDbContext newCommDbContext,
            PropertySystemDbContext propertySystemDbContext, HttpClient httpClient


			)
        {
			_propertySystemDbContext = propertySystemDbContext;
			_personalsDbcontext = personalsDbcontext;
			_configuration = configuration;
			_newCommDbContext = newCommDbContext;
			//_personalDbContext = personalDbContext;
			_exporter = exporter;
            _hostEnvironment = hostEnvironment;
            // _appFolders = appFolders;enerate 
            _contextProp = contextProp;
            _contextBilling = contextBilling;
            _contextPersonal = contextPersonal;
			//_contextBilling = contextBilling;
			_httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));


		}
		public class UseriDto
		{
			public int Id { get; set; }
		


		}
		public class LoginResponse
		{
			public ResultData result { get; set; }
			//public string targetUrl { get; set; }
			//public bool success { get; set; }
			//public string error { get; set; }
			//public bool unAuthorizedRequest { get; set; }
			//public bool Abp { get; set; }
		}

		public class ResultData
		{
			public string accessToken { get; set; }
			public string encryptedAccessToken { get; set; }
			public int expireInSeconds { get; set; }
			public int userId { get; set; }
			public string userName { get; set; }
			public string profileImg { get; set; }
		}

		public async Task<List<LoginResponse>> Login(LoginData userLoginData)
		{
			try
			{
				var content = new StringContent(JsonConvert.SerializeObject(userLoginData));
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				var response = await _httpClient.PostAsync("http://18.140.60.145:1001/api/TokenAuth/Authenticate", content);

				if (response.IsSuccessStatusCode)
				{
					var jsonResponse = await response.Content.ReadAsStringAsync();
					var loginResponse = JsonConvert.DeserializeObject<LoginResponse>(jsonResponse);
					return new List<LoginResponse> { loginResponse };
				}
				else
				{
					// Handle bad request
					return null;
				}
			}
			catch (Exception ex)
			{
				// Handle exception
				throw new Exception($"An error occurred during login: {ex.Message}");
			}
		}
		[HttpGet] 
		public async Task<object> getCurrentLoginInformationsGET(CurrenInfoDataDto permissionLoginDataInfo)
		{
			try
			{
				var accessToken = permissionLoginDataInfo.accessToken;
				var expireInSeconds = permissionLoginDataInfo.expireInSeconds;
				var userId = permissionLoginDataInfo.userId;
				var url = $"http://18.140.60.145:1001/api/services/app/Session/GetCurrentLoginInformations?accessToken={accessToken}&expireInSeconds={expireInSeconds}&userId={userId}";

				var response = await _httpClient.GetAsync(url);

				response.EnsureSuccessStatusCode(); // Throws if not success

				var jsonResponse = await response.Content.ReadAsStringAsync();

				var responseData = JsonConvert.DeserializeObject<object>(jsonResponse);

				return responseData;
			}
			catch (HttpRequestException ex)
			{
				// Handle request exception
				Console.WriteLine($"Error accessing service: {ex.Message}");
				throw;
			}
			catch (Exception ex)
			{
				// Handle other exceptions
				Console.WriteLine($"An error occurred: {ex.Message}");
				throw;
			}
		}
		[HttpGet]
		public async Task<object> GatewayOfUserPermissions( PermissionLoginDataDto permissionLoginDataInfo)
		{
			try
			{
				var accessToken = permissionLoginDataInfo.accessToken;

				var url = $"http://18.140.60.145:1001/AbpUserConfiguration/GetAll?accessToken={accessToken}";

				var response = await _httpClient.GetAsync(url);

				response.EnsureSuccessStatusCode(); // Throws if not success

				var jsonResponse = await response.Content.ReadAsStringAsync();

				var responseData = JsonConvert.DeserializeObject<object>(jsonResponse);

				return responseData;
			}
			catch (HttpRequestException ex)
			{
				// Handle request exception
				Console.WriteLine($"Error accessing service: {ex.Message}");
				throw;
			}
			catch (Exception ex)
			{
				// Handle other exceptions
				Console.WriteLine($"An error occurred: {ex.Message}");
				throw;
			}
		}
		//[HttpPost]

		//public async Task<IActionResult> Login([FromBody] LoginData userLoginData)
		//{
		//	try
		//	{
		//		var content = new StringContent(JsonConvert.SerializeObject(userLoginData));
		//		content.Headers.ContentType.MediaType = "application/json";
		//		content.Headers.Add("Access-Control-Allow-Origin", "*");

		//		var response = await _httpClient.PostAsync("http://18.140.60.145:1001/api/TokenAuth/Authenticate", content);

		//		if (response.IsSuccessStatusCode)
		//		{
		//			var jsonResponse = await response.Content.ReadAsStringAsync();
		//			var loginResponse = JsonConvert.DeserializeObject<LoginResponse>(jsonResponse);

		//			if (loginResponse != null)
		//			{
		//				return new JsonResult(loginResponse);
		//			}
		//			else
		//			{
		//				return BadRequest(new { message = "Invalid response format during login." });
		//			}
		//		}
		//		else
		//		{
		//			return BadRequest(new { message = "Invalid response format during login." });
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		return StatusCode(500, $"An error occurred during login: {ex.Message}");
		//	}
		//}

		private IActionResult StatusCode(int v1, string v2)
		{
			throw new NotImplementedException();
		}

		private IActionResult BadRequest(object value)
		{
			throw new NotImplementedException();
		}

		public class LoginData
	{
			// Define your LoginData properties here
			public string userNameOrEmailAddress { get; set; }
			public string password { get; set; }
	}


		//public async Task<List<GetSalesTrackingResultDto>> GetListReportDetails(GetSalesTrackingInputDto input)


		//{
		//	string Constring = _configuration.GetConnectionString("PropertySystemDbContext");
		//	string ProjectIDs = "All";
		//	string memberCodes = null;

		//	if (input.memberCode != null)
		//	{
		//		memberCodes = input.memberCode.Count == 0 ? null : string.Join(",", input.memberCode);
		//	}

		//	if (input.projectID != null)
		//	{
		//		ProjectIDs = string.Join(",", input.projectID);
		//	}


		//	//var offSet = input.SkipCount;
		//	//var rowOnly = input.MaxResultCount;			
		//	var skipCount = input.SkipCount;
		//	var maxCount = input.MaxResultCount;
		//	var bookCode = input.bookCode == null ? null : input.bookCode;
		//	var termID = input.termID;

		//	using (SqlConnection conn = new SqlConnection(Constring))
		//	{
		//		await conn.OpenAsync();


		//		List<GetSalesTrackingResultDto> result = new List<GetSalesTrackingResultDto>();

		//		var query = $@"EXEC SP_GetDataSalesTracking @bookCode='" + bookCode + "' , @memberCodes='" + memberCodes + "',@ProjectIDs='" + ProjectIDs + "',@termID='" + termID + "',@skipCount='" + skipCount + "',@maxCount='" + maxCount + "'";

		//		try
		//		{
		//			result = (await conn.QueryAsync<GetSalesTrackingResultDto>(query)).ToList();
		//		}
		//		catch (Exception ex)
		//		{
		//			throw new Exception("Error while updating data", ex);
		//		}

		//		return result;
		//	}
		//}
		//public PagedResultDto<GetSalesTrackingResultDto> InquiryDataSalesTracking(GetSalesTrackingInputDto input)
		//{
		//	string connection = _configuration.GetConnectionString("PropertySystemDbContext");
		//	using IDbConnection dbConnection = new SqlConnection(connection);
		//	dbConnection.Open();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - started - {0}", DateTime.Now);
		//	string ProjectIDs = "All";
		//	string memberCodes = null;

		//	if (input.memberCode != null)
		//	{
		//              memberCodes = null;
		//	}
		//	//input.memberCode.Count == 0 ? null : string.Join(",", input.memberCode);

		//	if (input.projectID != null)
		//	{
		//		ProjectIDs = string.Join(",", input.projectID);
		//	}


		//	//var offSet = input.SkipCount;
		//	//var rowOnly = input.MaxResultCount;			
		//	var skipCount = input.SkipCount;
		//	var maxCount = input.MaxResultCount;
		//	var bookCode = input.bookCode == ""? null : input.bookCode;
		//	var termID = input.termID;
		//	var keyword = input.keyword == null ? "" : input.keyword;

		//	//SqlConnection conn = new SqlConnection(Constring);
		//	//conn.Open();


		//	//List<GetSalesTrackingResultDto> result = new List<GetSalesTrackingResultDto>();
		//	//var query = @"EXEC SP_GetDataSalesTracking @bookCode, @memberCodes, @ProjectIDs, @termID, @skipCount, @maxCount";

		//	var SP_GetDataPenalty = "SP_GetDataSalesTracking";
		//	var pram = new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword };


		//	var result = dbConnection.Query<GetSalesTrackingResultDto>(
		//			SP_GetDataPenalty, pram,
		//			commandType: CommandType.StoredProcedure).ToList();



		//	var SP_GetDataSalesTrackingDataCount = "SP_GetDataSalesTrackingDataCount";
		//	var resultCount = dbConnection.Query<int>(
		//			SP_GetDataSalesTrackingDataCount, pram,
		//			commandType: CommandType.StoredProcedure).FirstOrDefault();


		//	Logger.DebugFormat("InquiryDataSalesTracking() - Query - {0}", DateTime.Now);


		//		Logger.DebugFormat("InquiryDataSalesTracking() - QueryCount - {0}", DateTime.Now);

		//	//if (!string.IsNullOrWhiteSpace(input.keyword))
		//	//{
		//	//	result = result.Where(x => (!string.IsNullOrWhiteSpace(x.bankCode) && x.bankCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.bankName) && x.bankName.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 //|| (!string.IsNullOrWhiteSpace(x.bookCode) && x.bookCode.ToLowerInvariant().Contains(input.bookCode.ToLowerInvariant() ?? null	))
		//	//			 || (!string.IsNullOrWhiteSpace(x.ConfirmationLetter) && x.ConfirmationLetter.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.Email) && x.Email.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 //|| (!string.IsNullOrWhiteSpace(x.memberCode) && x.memberCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.name) && x.name.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.orderCode) && x.orderCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.Payment) && x.Payment.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.PaymentStatus) && x.PaymentStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.PPNo) && x.PPNo.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.psCode) && x.psCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.termRemarks) && x.termRemarks.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.Unit) && x.Unit.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.UnitStatus) && x.UnitStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.PPStatus) && x.PPStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.overallStatus) && x.overallStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.reservedField1) && x.reservedField1.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.reservedField2) && x.reservedField2.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 || (!string.IsNullOrWhiteSpace(x.reservedField3) && x.reservedField3.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
		//	//			 ).ToList();
		//	//}


		//	var countData = Convert.ToInt32(resultCount);
		//	//var countData = result.Count();

		//	//result = result.OrderBy(x => x.BuyDatePP ?? DateTime.MaxValue).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - Filter - {0}", DateTime.Now);
		//	var getDataProject = (from x in _propertySystemDbContext.MS_Project
		//						  select x).ToList();

		//	var getBookCode = result.Where(x => !string.IsNullOrWhiteSpace(x.bookCode)).Select(x => new { x.memberCode, x.bookCode }).ToList();

		//	//var getComission = (from x in _newCommDbContext.TR_CommPayment
		//	//                    join a in getBookCode on new { a = x.bookNo, b = x.memberCode } equals new { a = a.bookCode, b = a.memberCode }
		//	//                    group x by new
		//	//                    {
		//	//                        x.paidDate,
		//	//                        x.bookNo, 
		//	//                        x.memberCode
		//	//                    } into G
		//	//                    select new
		//	//                    {
		//	//                        G.Key.paidDate,
		//	//                        amountKomisi = G.Sum(x => x.amount),
		//	//                        G.Key.bookNo,
		//	//                        G.Key.memberCode
		//	//                    }).ToList();

		//	//eligibleCommission
		//	//var getPersenMemberLogin = (from a in _newCommDbContext.TR_OverridingPct
		//	//							join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
		//	//							select new
		//	//							{
		//	//								a.bookNo,
		//	//								a.memberCode,
		//	//								a.commPctPaid
		//	//							}).ToList();
		//						var getPersenMemberLogin = (
		//				from a in _newCommDbContext.TR_OverridingPct.AsEnumerable()
		//				join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
		//				select new
		//				{
		//					a.bookNo,
		//					a.memberCode,
		//					a.commPctPaid
		//				}
		//			).ToList();


		//	var getCommisionMemberLogin = (from a in _newCommDbContext.TR_CommPayment.AsEnumerable()
		//								   join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
		//								   group a by new { a.bookNo, a.memberCode } into G
		//								   select new
		//								   {
		//									   G.Key.bookNo,
		//									   G.Key.memberCode,
		//									   amountComm = G.Sum(x => x.amount)
		//								   }).ToList();

		//	Logger.DebugFormat("InquiryDataSalesTracking() - Komisi - {0}", DateTime.Now);
		//	var tanggalSekarang = DateTime.Now;
		//	foreach (var data in result)
		//	{
		//		var countStep = 0;
		//		int daysAgo = 0;

		//		if (data.projectID != null || data.projectID != 0)
		//		{
		//			data.projectCode = getDataProject.Where(x => x.Id == data.projectID).Select(x => x.projectCode).FirstOrDefault();
		//		}

		//		//step1
		//		if ((!string.IsNullOrWhiteSpace(data.PPStatus) && (data.PPStatus.ToLower() == "done") || (!(data.PPStatus.ToLower() == "done") && data.UnitStatus.ToLower() == "done")))
		//		{
		//			data.PPStatus = "Done";
		//			countStep += 1;
		//		}

		//		//step2
		//		if (!string.IsNullOrWhiteSpace(data.UnitStatus) && data.UnitStatus.ToLower() == "done")
		//		{
		//			countStep += 1;
		//		}

		//		//step3
		//		if (!string.IsNullOrWhiteSpace(data.SignP3U) && data.SignP3U.ToLower() == "complete")
		//		{
		//			countStep += 1;
		//		}

		//		//step4
		//		if (((!string.IsNullOrWhiteSpace(data.SignAkad) && data.SignAkad.ToLower() == "complete") ||
		//			(!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
		//			!data.termRemarks.ToLower().Contains("kpr")))))
		//		{
		//			data.SignAkad = "Complete";
		//			countStep += 1;
		//		}

		//		//step5
		//		if (((!string.IsNullOrWhiteSpace(data.pctPaymentDp) && data.pctPaymentDp.ToLower() == "complete (100%)")
		//			|| (!string.IsNullOrWhiteSpace(data.termRemarks) && data.termRemarks.ToLower() == "hard cash")))
		//		{
		//			countStep += 1;
		//		}

		//		//step6
		//		if (((!string.IsNullOrWhiteSpace(data.Document) && data.Document.ToLower() == "complete")
		//			|| (!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
		//			!data.termRemarks.ToLower().Contains("kpr")))))
		//		{
		//			data.Document = "Complete";
		//			countStep += 1;
		//		}
		//		else
		//		{
		//			data.Document = "Incomplete";
		//		}

		//		//step7
		//		if (!string.IsNullOrWhiteSpace(data.ConfirmationLetter) && data.ConfirmationLetter.ToLower() == "complete")
		//		{
		//			countStep += 1;
		//		}

		//		var listDate = new List<DateTime?>();
		//		if (countStep == 7)
		//		{
		//			listDate.Add(data.SignP3UModifTime);
		//			listDate.Add(data.SignAkadModifTime);
		//			listDate.Add(data.maxClearDateDP);
		//			listDate.Add(data.DocumentModifTime);
		//			listDate.Add(data.ConfirmationLetterModifTime);

		//			var getLatestDate = listDate.OrderByDescending(x => x).FirstOrDefault();
		//			if (data.BuyDatePP != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.BuyDatePP)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else if (data.bookDate != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.bookDate)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else
		//			{
		//				data.daySinceLast = "-";
		//			}
		//		}
		//		else
		//		{
		//			if (data.BuyDatePP != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.BuyDatePP)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else if (data.bookDate != null)
		//			{
		//				daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.bookDate)).TotalDays));
		//				data.daySinceLast = daysAgo.ToString() + " days ago";
		//			}
		//			else
		//			{
		//				data.daySinceLast = "-";
		//			}
		//		}

		//		data.step = countStep + "/7";
		//		data.stepComplete = Convert.ToInt32((double)countStep / (double)7 * 100).ToString() + "% Complete";
		//		data.overallStatus = countStep == 7 ? "Complete" : daysAgo <= 2 ? "New" : daysAgo <= 7 ? "Immediate" : daysAgo <= 14 ? "Urgent" :
		//						daysAgo <= 30 ? "Critical" : daysAgo <= 60 ? "At Risk" : "At Risk";

		//		//komisi
		//		//var getKomisi = (from x in getComission
		//		//                 where x.bookNo == data.bookCode && x.memberCode == data.memberCode
		//		//                 select new
		//		//                 {
		//		//                     x.paidDate,
		//		//                     x.amountKomisi
		//		//                 }).ToList();

		//		//var komisiPaid = getKomisi.Where(x => x.paidDate != null).Sum(x => x.amountKomisi);
		//		//var totalKomisi = getKomisi.Sum(x => x.amountKomisi);

		//		//data.totalKomisiPaid = NumberHelper.IndoFormat(komisiPaid);
		//		//data.percentKomisiPaid = totalKomisi == 0 ? "0" : string.Format("{0:N2}", Convert.ToInt32((double)komisiPaid / (double)totalKomisi * 100));

		//		var singleDataPercen = getPersenMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();
		//		var singleDataAmount = getCommisionMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();

		//		data.percentKomisiPaid = singleDataPercen == null ? "0%" : (singleDataPercen.commPctPaid * 100) + "%";
		//		data.totalKomisiPaid = NumberHelper.IndoFormatWithoutTail(singleDataAmount == null ? 0 : singleDataAmount.amountComm);
		//		data.eligibleCommPct = data.eligibleCommPct == null ? "0%" : data.eligibleCommPct + "%";
		//	}


		//	Logger.DebugFormat("InquiryDataSalesTracking() - End - {0}", DateTime.Now);
		//          return new PagedResultDto<GetSalesTrackingResultDto>(countData, result);

		//}
		public class getPersenMemberLogindto { 
		
			public string bookNo { get; set; }
			public string memberCode { get; set; }
			public double commPctPaid { get; set; }


		}
		private int GetDataSalesTrackingDataCount(string queryCount, string bookCode, string memberCodes, string ProjectIDs, int termID, int skipCount, int maxCount, string keyword)
		{
			using (SqlConnection dbConnection = new SqlConnection(_configuration.GetConnectionString("PropertySystemDbContext")))
			{
				dbConnection.Open();
				int timeoutInSeconds = 320;
				return dbConnection.QueryFirstOrDefault<int>(
					queryCount,
					new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword },
					commandType: CommandType.StoredProcedure, commandTimeout: timeoutInSeconds
				);
			}
		}
		private int GetDataSalesTrackingDataCount2(string queryCount, string bookCode, string memberCodes, string ProjectIDs, int termID, int skipCount, int maxCount, string keyword)
		{
			using (SqlConnection dbConnection = new SqlConnection(_configuration.GetConnectionString("PropertySystemDbContext")))
			{
				dbConnection.Open();
			
				return dbConnection.QueryFirstOrDefault<int>(
					queryCount,
					new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword },
					commandType: CommandType.StoredProcedure
				);
			}
		}

		public PagedResultDto<GetSalesTrackingResultDto> InquiryDataSalesTracking(GetSalesTrackingInputDto input)
		{
			try
			{
				string connection = _configuration.GetConnectionString("PropertySystemDbContext");

				using (IDbConnection dbConnection = new SqlConnection(connection))
				{
					dbConnection.Open();

					Logger.DebugFormat("InquiryDataSalesTracking() - started - {0}", DateTime.Now);
					string ProjectIDs = "All";
					string memberCodes = null;

					if (input.memberCode != null)
					{
						memberCodes = input.memberCode.Count == 0 ? null : string.Join(",", input.memberCode);
					}

					if (input.projectID != null)
					{
						ProjectIDs = string.Join(",", input.projectID);
					}


					//var offSet = input.SkipCount;
					//var rowOnly = input.MaxResultCount;			
					var skipCount = input.SkipCount;
					var maxCount = input.MaxResultCount;
					var bookCode = input.bookCode == null ? null : input.bookCode;
					var termID = input.termID;
					var keyword = input.keyword == null ? "" : input.keyword;

					var query = @"SP_GetDataSalesTracking";

					//var result = _sqlExecuter.GetFromPropertySystem<GetSalesTrackingResultDto>
					//							   (query, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword },
					//							   System.Data.CommandType.StoredProcedure).ToList();
					//var pram = new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword };


					var result = dbConnection.Query<GetSalesTrackingResultDto>(
							query, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword },
							commandType: CommandType.StoredProcedure).ToList();

					var getBookCode = result.Where(x => !string.IsNullOrWhiteSpace(x.bookCode)).Select(x => new { x.memberCode, x.bookCode }).ToList();

					Logger.DebugFormat("InquiryDataSalesTracking() - Query - {0}", DateTime.Now);
					//if (!string.IsNullOrWhiteSpace(input.bookCode))
					//{
					//	result = result.Where(x => (!string.IsNullOrWhiteSpace(x.bookCode) && x.bookCode.ToLowerInvariant().Contains(input.bookCode.ToLowerInvariant()))).ToList();
					//}

					var queryCount = @"SP_GetDataSalesTrackingDataCount";

					//var resultCount = _sqlExecuter.GetFromPropertySystem<int>
					//							   (queryCount, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, @keyword },
					//		
					//	
					//							   System.Data.CommandType.StoredProcedure).FirstOrDefault();
					int resultCount = 0;
					if (ProjectIDs == "1") {
						 resultCount = GetDataSalesTrackingDataCount(queryCount, bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword);
					}
					else
					{
						 resultCount = GetDataSalesTrackingDataCount2(queryCount, bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, keyword);
					}
					
					//var resultCount = dbConnection.Query<int>(
					//		queryCount, new { bookCode, memberCodes, ProjectIDs, termID, skipCount, maxCount, @keyword },
					//		commandType: CommandType.StoredProcedure).FirstOrDefault();

					Logger.DebugFormat("InquiryDataSalesTracking() - QueryCount - {0}", DateTime.Now);


					string connectiono = _configuration.GetConnectionString("NewCommDbContext");

					using (IDbConnection dbConnectioni = new SqlConnection(connectiono))
					{
						dbConnectioni.Open();
						//dbConnectioni.ConnectionTimeout=30000;
						List<getPersenMemberLogindto> getPersenMemberLogin = new List<getPersenMemberLogindto>();

						foreach (var item in getBookCode)
						{
							var qer = @"SELECT a.bookNo,
											a.memberCode,
											a.commPctPaid
                                     FROM TR_OverridingPct AS a
                                     where a.bookNo = @membercode AND a.memberCode = @getbookode";


							getPersenMemberLogin = dbConnectioni.Query<getPersenMemberLogindto>(qer, new { membercode = item.memberCode, getbookode = item.bookCode }, commandType: CommandType.Text).ToList();
						}

						//var getCommisionMemberLogin = (from a in _newCommDbContext.TR_CommPayment
						//							   join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
						//							   group a by new { a.bookNo, a.memberCode } into G
						//							   select new
						//							   {
						//								   G.Key.bookNo,
						//								   G.Key.memberCode,
						//								   amountComm = G.Sum(x => x.amount)
						//							   }).ToList();
						List<getCommMemberLogindto> getCommisionMemberLogin = new List<getCommMemberLogindto>();

						foreach (var item in getBookCode)
						{
							var qer = @"SELECT a.bookNo, a.memberCode, SUM(a.amount) AS amountComm
							FROM TR_CommPayment a where a.bookNo = @getbookode AND a.memberCode = @membercode GROUP BY a.bookNo, a.memberCode;";


							getCommisionMemberLogin = dbConnectioni.Query<getCommMemberLogindto>(qer, new { membercode = item.memberCode, getbookode = item.bookCode }, commandType: CommandType.Text).ToList();
						}

						Logger.DebugFormat("InquiryDataSalesTracking() - Komisi - {0}", DateTime.Now);
						var tanggalSekarang = DateTime.Now;

						Logger.DebugFormat("InquiryDataSalesTracking() - Filter - {0}", DateTime.Now);
						//var getDataProject = (from x in _propertySystemDbContext.MS_Project
						//					  select x).ToList();
						var queryProject = @"select * from MS_Project";

						var getDataProject = dbConnection.Query<MS_Project>(queryProject).ToList();

						foreach (var data in result)
						{
							var countStep = 0;
							int daysAgo = 0;

							if (data.projectID != null || data.projectID != 0)
							{
								data.projectCode = getDataProject.Where(x => x.Id == data.projectID).Select(x => x.projectCode).FirstOrDefault();
							}

							//step1
							if ((!string.IsNullOrWhiteSpace(data.PPStatus) && (data.PPStatus.ToLower() == "done") || (!(data.PPStatus.ToLower() == "done") && data.UnitStatus.ToLower() == "done")))
							{
								data.PPStatus = "Done";
								countStep += 1;
							}

							//step2
							if (!string.IsNullOrWhiteSpace(data.UnitStatus) && data.UnitStatus.ToLower() == "done")
							{
								countStep += 1;
							}

							//step3
							if (!string.IsNullOrWhiteSpace(data.SignP3U) && data.SignP3U.ToLower() == "complete")
							{
								countStep += 1;
							}

							//step4
							if (((!string.IsNullOrWhiteSpace(data.SignAkad) && data.SignAkad.ToLower() == "complete") ||
								(!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
								!data.termRemarks.ToLower().Contains("kpr")))))
							{
								data.SignAkad = "Complete";
								countStep += 1;
							}

							//step5
							if (((!string.IsNullOrWhiteSpace(data.pctPaymentDp) && data.pctPaymentDp.ToLower() == "complete (100%)")
								|| (!string.IsNullOrWhiteSpace(data.termRemarks) && data.termRemarks.ToLower() == "hard cash")))
							{
								countStep += 1;
							}

							//step6
							if (((!string.IsNullOrWhiteSpace(data.Document) && data.Document.ToLower() == "complete")
								|| (!string.IsNullOrWhiteSpace(data.termRemarks) && (!data.termRemarks.ToLower().Contains("kpa") &&
								!data.termRemarks.ToLower().Contains("kpr")))))
							{
								data.Document = "Complete";
								countStep += 1;
							}
							else
							{
								data.Document = "Incomplete";
							}

							//step7
							if (!string.IsNullOrWhiteSpace(data.ConfirmationLetter) && data.ConfirmationLetter.ToLower() == "complete")
							{
								countStep += 1;
							}

							var listDate = new List<DateTime?>();
							if (countStep == 7)
							{
								listDate.Add(data.SignP3UModifTime);
								listDate.Add(data.SignAkadModifTime);
								listDate.Add(data.maxClearDateDP);
								listDate.Add(data.DocumentModifTime);
								listDate.Add(data.ConfirmationLetterModifTime);

								var getLatestDate = listDate.OrderByDescending(x => x).FirstOrDefault();
								if (data.BuyDatePP != null)
								{
									daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.BuyDatePP)).TotalDays));
									data.daySinceLast = daysAgo.ToString() + " days ago";
								}
								else if (data.bookDate != null)
								{
									daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(getLatestDate - data.bookDate)).TotalDays));
									data.daySinceLast = daysAgo.ToString() + " days ago";
								}
								else
								{
									data.daySinceLast = "-";
								}
							}
							else
							{
								if (data.BuyDatePP != null)
								{
									daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.BuyDatePP)).TotalDays));
									data.daySinceLast = daysAgo.ToString() + " days ago";
								}
								else if (data.bookDate != null)
								{
									daysAgo = Convert.ToInt32(Math.Floor(((TimeSpan)(tanggalSekarang - data.bookDate)).TotalDays));
									data.daySinceLast = daysAgo.ToString() + " days ago";
								}
								else
								{
									data.daySinceLast = "-";
								}
							}

							data.step = countStep + "/7";
							data.stepComplete = Convert.ToInt32((double)countStep / (double)7 * 100).ToString() + "% Complete";
							data.overallStatus = countStep == 7 ? "Complete" : daysAgo <= 2 ? "New" : daysAgo <= 7 ? "Immediate" : daysAgo <= 14 ? "Urgent" :
											daysAgo <= 30 ? "Critical" : daysAgo <= 60 ? "At Risk" : "At Risk";

							//komisi
							//var getKomisi = (from x in getComission
							//                 where x.bookNo == data.bookCode && x.memberCode == data.memberCode
							//                 select new
							//                 {
							//                     x.paidDate,
							//                     x.amountKomisi
							//                 }).ToList();

							//var komisiPaid = getKomisi.Where(x => x.paidDate != null).Sum(x => x.amountKomisi);
							//var totalKomisi = getKomisi.Sum(x => x.amountKomisi);

							//data.totalKomisiPaid = NumberHelper.IndoFormat(komisiPaid);
							//data.percentKomisiPaid = totalKomisi == 0 ? "0" : string.Format("{0:N2}", Convert.ToInt32((double)komisiPaid / (double)totalKomisi * 100));

							var singleDataPercen = getPersenMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();
							var singleDataAmount = getCommisionMemberLogin.Where(x => x.bookNo == data.bookCode && x.memberCode == data.memberCode).FirstOrDefault();

							data.percentKomisiPaid = singleDataPercen == null ? "0%" : (singleDataPercen.commPctPaid * 100) + "%";
							data.totalKomisiPaid = NumberHelper.IndoFormatWithoutTail(singleDataAmount == null ? 0 : singleDataAmount.amountComm);
							data.eligibleCommPct = data.eligibleCommPct == null ? "0%" : data.eligibleCommPct + "%";
						}

					}

					#region old
					//tdk digunakan karena searching keyword di satukan dengan SP SP_GetDataSalesTracking
					/*if (!string.IsNullOrWhiteSpace(input.keyword))
					{
						result = result.Where(x => //(!string.IsNullOrWhiteSpace(x.bankCode) && x.bankCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.bankName) && x.bankName.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.bookCode) && x.bookCode.ToLowerInvariant().Contains(input.bookCode.ToLowerInvariant() ?? null	))
								 (!string.IsNullOrWhiteSpace(x.ConfirmationLetter) && x.ConfirmationLetter.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.Email) && x.Email.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.memberCode) && x.memberCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.name) && x.name.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.orderCode) && x.orderCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 || (!string.IsNullOrWhiteSpace(x.Payment) && x.Payment.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 || (!string.IsNullOrWhiteSpace(x.PaymentStatus) && x.PaymentStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.PPNo) && x.PPNo.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.psCode) && x.psCode.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.termRemarks) && x.termRemarks.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.Unit) && x.Unit.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.UnitStatus) && x.UnitStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.PPStatus) && x.PPStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.overallStatus) && x.overallStatus.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.reservedField1) && x.reservedField1.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.reservedField2) && x.reservedField2.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 //|| (!string.IsNullOrWhiteSpace(x.reservedField3) && x.reservedField3.ToLowerInvariant().Contains(input.keyword.ToLowerInvariant()))
								 ).ToList();
					}*/
					#endregion

					var countData = Convert.ToInt32(resultCount);
					//var countData = result.Count();

					//result = result.OrderBy(x => x.BuyDatePP ?? DateTime.MaxValue).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();					


					//eligibleCommission
					//var getPersenMemberLogin = (from a in _newCommDbContext.TR_OverridingPct
					//							join b in getBookCode on new { a = a.bookNo, b = a.memberCode } equals new { a = b.bookCode, b = b.memberCode }
					//							select new
					//							{
					//								a.bookNo,
					//								a.memberCode,
					//								a.commPctPaid
					//	
					//							}).ToList(); @GetBookCode AS b ON

					Logger.DebugFormat("InquiryDataSalesTracking() - End - {0}", DateTime.Now);
					return new PagedResultDto<GetSalesTrackingResultDto>(countData, result);
				}
			}
			catch (Exception ex)
			{
				throw new UserFriendlyException(ex.Message);
			}
			
		}
		/// <summary>
		/// //
		public string getAbsoluteUri()
		{
			var request = _httpContextAccessor.HttpContext.Request;
			UriBuilder uriBuilder = new UriBuilder();
			uriBuilder.Scheme = request.Scheme;
			uriBuilder.Host = request.Host.ToString();
			var test = uriBuilder.ToString();
			var result = test.Replace("[", "").Replace("]", "");
			return result;
		}
		public ListResultDto<ListProjectResultDto> GetListProjectBySchema(string memberCode)
		{
			string connection = _configuration.GetConnectionString("PersonalsNewDbContext");
			//using IDbConnection dbConnection = new SqlConnection(connection);
			using SqlConnection dbConnection = new SqlConnection(connection);
			dbConnection.Open();
			string connectiona = _configuration.GetConnectionString("DefaultTwo");
			//using IDbConnection dbConnectiona = new SqlConnection(connectiona);
			using SqlConnection dbConnectiona = new SqlConnection(connectiona);
			dbConnectiona.Open();
			string connectionaa = _configuration.GetConnectionString("PropertySystemDbContext");
			//using IDbConnection dbConnectioni = new SqlConnection(connectionaa);
			using SqlConnection dbConnectioni = new SqlConnection(connectionaa);
			dbConnectioni.Open();
			var result = new List<ListProjectResultDto>();
			var allList = new List<ListUserProjectMapDto>();
			var checkAdmins = 0;
			var scmCode = "";
			var getGroupSchema = new List<string>();
			var checkAdmin = new User();
			var data = new User();

			var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
			var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];

			//var dbPropertyString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
			var dbPropertyString = connectionStrings.Property("DefaultTwo").Value.ToString().Replace(" ", string.Empty).Split(";");
			var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);

			#region old 2023/08/25
			
			#endregion


			var UserRole = @"select DISTINCT a.UserName
		                          from " + dbCatalogPropertyString + @".dbo.users a
		                          inner join " + dbCatalogPropertyString + @".dbo.Permissions b on a.id = b.UserId
		                          where a.UserName not in (select membercode from personals_member)";

			var Role = @"select DISTINCT a.UserName
		                          from " + dbCatalogPropertyString + @".dbo.users a
		                          join " + dbCatalogPropertyString + @".dbo.UserRoles c on a.Id = c.UserId
		                          inner join " + dbCatalogPropertyString + @".dbo.Permissions b on c.RoleId = b.RoleId
		                          where a.UserName not in (select membercode from personals_member)
		                      ";

			//var getUserRole = _sqlExecuter.GetFromPersonals<string>(UserRole).ToList();
			var getUserRole = dbConnection.Query<string>(UserRole, commandType: CommandType.Text).ToList();


			if (!getUserRole.Contains(memberCode))
			{
				//var getRoles = _sqlExecuter.GetFromPersonals<string>(Role).ToList();
				var getRoles = dbConnection.Query<string>(Role, commandType: CommandType.Text).ToList();

				if (!getRoles.Contains(memberCode))
				{
					scmCode = (from a in _personalsDbcontext.PERSONALS_MEMBER
							   where a.memberCode == memberCode
							   select a.scmCode).FirstOrDefault();

					if (scmCode != null)
					{
						var getBooking = (from a in _propertySystemDbContext.TR_BookingHeader
										  where a.scmCode == scmCode
										  select a.bookCode.Substring(0, 3)).Distinct().ToList();

						var getPP = (from p in _propertySystemDbContext.TR_PriorityPass
									 join b in _propertySystemDbContext.MS_BatchEntry on p.batchID equals b.Id
									 join i in _propertySystemDbContext.MS_ProjectInfo on b.projectInfoID equals i.Id
									 join j in _propertySystemDbContext.MS_Project on i.projectID equals j.Id
									 where p.scmCode == scmCode
									 select j.projectCode).Distinct().ToList();

						var unionProjectCode = getBooking.Union(getPP).ToList();

						getGroupSchema = getGroupSchema.Union(unionProjectCode).ToList();
					}
				}
				else
				



				//List<GetCustomerListDto> getData = conn.Query<GetCustomerListDto>(query).ToList();
				{

					//string queri = @"SELECT a 
					//                               FROM Users a
					//                               WHERE a.Username = '" + memberCode + "'and Id= 2";
					//checkAdmins = dbConnectiona.Query<int>(queri, commandType: CommandType.Text).FirstOrDefault();
					//var query = @"SELECT a.Id 
					//                               FROM Users a
					//                              WHERE a.Username = @Username and Id= @Id";

					//var parameters = new { Username = memberCode, Id = 2 };

					//checkAdmins = dbConnectiona.Query<int>(query, parameters).FirstOrDefault();
				 checkAdmins = _newCommDbContext.MS_Users
										.Where(a => a.Username == memberCode && a.Id == 2)
										.Select(a => a.Id)
										.FirstOrDefault();





				}
			}
			else
			{

				//string queri = @"SELECT a.Id
				//                                FROM Users a
				//                                WHERE a.Username = '" + memberCode + "'and Id= 2";
				//checkAdmins = dbConnectiona.Query<int>(queri, commandType: CommandType.Text).FirstOrDefault();
				//var query = @"SELECT a.Id 
				//	                               FROM Users a
				//	                              WHERE a.Username = @Username and Id= @Id";

				//var parameters = new { Username = memberCode, Id = 2 };

				//checkAdmins = dbConnectiona.Query<int>(query, parameters).FirstOrDefault();
				checkAdmins = _newCommDbContext.MS_Users
									   .Where(a => a.Username == memberCode && a.Id == 2)
									   .Select(a => a.Id)
									   .FirstOrDefault();
			}

		



			// Membuat query string
			string queryString = @"
		                 SELECT DISTINCT

		                   A.Id AS projectID,
		                      A.projectCode,
		                      A.projectName,
		                      CONCAT('~', A.image) AS image,
		                   b.Id AS projectInfoID,
		                      b.isOBActive
		                  FROM

		                   MS_Project A
		                  JOIN
		                   MS_ProjectInfo b ON A.Id = b.projectID
		                  JOIN
		                   MS_Unit d ON A.Id = d.projectID
		                  WHERE

		                   A.isPublish = @isPublish

		                   AND b.projectStatus = @projectStatus;";
			var parameters1 = new { isPublish = 1, projectStatus = 1 };
			List<ListUserProjectMapDto> resultList = new List<ListUserProjectMapDto>();
			resultList = dbConnectioni.Query<ListUserProjectMapDto>(queryString, parameters1, commandType: CommandType.Text).ToList();


			if (checkAdmins == 0)
			{
				result = (from a in resultList
						  join b in _propertySystemDbContext.MS_ProjectOLBooking on a.projectInfoID equals b.projectInfoID
						  where getGroupSchema.Contains(a.projectCode)
						  && b.isActive == true
						  && a.isOBActive == true
						  group a by new
						  {
							  a.projectID,
							  a.projectCode,
							  a.projectName,
							  a.image
						  } into g
						  select new ListProjectResultDto
						  {
							  projectID = g.Key.projectID,
							  projectCode = g.Key.projectCode,
							  projectName = g.Key.projectName,
							  image = g.Key.image
						  }).ToList();
			}
			else
			{
				string querii = @"SELECT DISTINCT a.RoleId
		                                  FROM UserRoles a
		                                  WHERE a.UserId =  @checkAdmins ";
				var parameters2 = new { checkAdmins = checkAdmins };
				var getRoles = dbConnectiona.Query<int>(querii, parameters2, commandType: CommandType.Text).ToList();
				
				string queryStrings = @"
		                 SELECT DISTINCT

		                   A.Id AS projectID,
		                      A.projectCode,
		                      A.projectName,
		                      CONCAT('http://18.140.60.145:1001/', A.image) AS image,
		                   b.Id AS projectInfoID,
		                      b.isOBActive
		                  FROM

		                   MS_Project A
		                  JOIN
		                   MS_ProjectInfo b ON A.Id = b.projectID
		                  JOIN
		                   MS_Unit d ON A.Id = d.projectID
		                  JOIN SYS_RolesProject on A.Id = e.projectID
		                  WHERE

		                   A.isPublish = @isPublish

		                   AND b.projectStatus = @projectStatus AND  @getRoless  IN (e.rolesID);";

				List<ListUserProjectMapDto> resultList2 = new List<ListUserProjectMapDto>();
				var parameters3 = new { getRoless = getRoles, projectStatus=1, isPublish=1 };
				resultList2 = dbConnectioni.Query<ListUserProjectMapDto>(queryString, parameters3, commandType: CommandType.Text).ToList();

				result = (from a in resultList2
						  group a by new
						  {
							  a.projectID,
							  a.projectCode,
							  a.projectName,
							  a.image
						  } into g
						  select new ListProjectResultDto
						  {
							  projectID = g.Key.projectID,
							  projectCode = g.Key.projectCode,
							  projectName = g.Key.projectName,
							  image = g.Key.image
						  }).ToList();
			}

			if (resultList == null)
			{
				var error = new List<ListProjectResultDto>();

				var message = new ListProjectResultDto
				{
					message = "data not found"
				};

				error.Add(message);

				//throw new UserFriendlyException("data not found");
				return new ListResultDto<ListProjectResultDto>(error);
			}
			else
			{
				return new ListResultDto<ListProjectResultDto>(result);
			}
		}

		/// <summary>
		/// ///
		/// </summary>
		/// <param name="projectID"></param>
		/// <param name="isAdminBank"></param>
		/// <returns></returns>
		
		//public async Task<ListResultDto<ListProjectResultDto>> GetListProjectBySchemaAsync(string memberCode)
		//{
		//	string connection = _configuration.GetConnectionString("PersonalsNewDbContext");
		//	string connectiona = _configuration.GetConnectionString("DefaultTwo");
		//	string connectionaa = _configuration.GetConnectionString("PropertySystemDbContext");

		//	var result = new List<ListProjectResultDto>();
		//	var allList = new List<ListUserProjectMapDto>();
		//	var checkAdmins = 0;
		//	var scmCode = "";
		//	var getGroupSchema = new List<string>();
		//	var checkAdmin = new User();
		//	var data = new User();

		//	var appsettingsjson = JObject.Parse(await File.ReadAllTextAsync("appsettings.json").ConfigureAwait(false));
		//	var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];

		//	var dbPropertyString = connectionStrings.Property("DefaultTwo").Value.ToString().Replace(" ", string.Empty).Split(";");
		//	var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);

		//	var UserRole = @"select DISTINCT a.UserName
		//                  from " + dbCatalogPropertyString + @".dbo.users a
		//                  inner join " + dbCatalogPropertyString + @".dbo.Permissions b on a.id = b.UserId
		//                  where a.UserName not in (select membercode from personals_member)";

		//	var Role = @"select DISTINCT a.UserName
		//                  from " + dbCatalogPropertyString + @".dbo.users a
		//                  join " + dbCatalogPropertyString + @".dbo.UserRoles c on a.Id = c.UserId
		//                  inner join " + dbCatalogPropertyString + @".dbo.Permissions b on c.RoleId = b.RoleId
		//                  where a.UserName not in (select membercode from personals_member)";
		//	using SqlConnection dbConnection = new SqlConnection(connection);
		//	//using (IDbConnection dbConnection = new SqlConnection(connection))
		//	//using (IDbConnection dbConnectiona = new SqlConnection(connectiona))
		//	using SqlConnection dbConnectiona = new SqlConnection(connectiona);
		//	//using (IDbConnection dbConnectioni = new SqlConnection(connectionaa))
		//	using SqlConnection dbConnectioni = new SqlConnection(connectionaa);
		//	{
		//		await dbConnection.OpenAsync().ConfigureAwait(false);
		//		await dbConnectiona.OpenAsync().ConfigureAwait(false);
		//		await dbConnectioni.OpenAsync().ConfigureAwait(false);

		//		var getUserRole = (await dbConnection.QueryAsync<string>(UserRole, commandType: CommandType.Text).ConfigureAwait(false)).ToList();

		//		if (!getUserRole.Contains(memberCode))
		//		{
		//			var getRoles = (await dbConnection.QueryAsync<string>(Role, commandType: CommandType.Text).ConfigureAwait(false)).ToList();

		//			if (!getRoles.Contains(memberCode))
		//			{
		//				scmCode = (await _personalsDbcontext.PERSONALS_MEMBER
		//							.Where(a => a.memberCode == memberCode)
		//							.Select(a => a.scmCode)
		//							.FirstOrDefaultAsync().ConfigureAwait(false)) ?? "";

		//				if (!string.IsNullOrEmpty(scmCode))
		//				{
		//					var getBooking = (await _propertySystemDbContext.TR_BookingHeader
		//										.Where(a => a.scmCode == scmCode)
		//										.Select(a => a.bookCode.Substring(0, 3))
		//										.Distinct()
		//										.ToListAsync().ConfigureAwait(false));

		//					var getPP = (await (from p in _propertySystemDbContext.TR_PriorityPass
		//										join b in _propertySystemDbContext.MS_BatchEntry on p.batchID equals b.Id
		//										join i in _propertySystemDbContext.MS_ProjectInfo on b.projectInfoID equals i.Id
		//										join j in _propertySystemDbContext.MS_Project on i.projectID equals j.Id
		//										where p.scmCode == scmCode
		//										select j.projectCode).Distinct().ToListAsync().ConfigureAwait(false));

		//					getGroupSchema = getBooking.Union(getPP).ToList();
		//				}
		//			}
		//		}

		//		string queryString = @"
		//             SELECT DISTINCT
		//                  A.Id AS projectID,
		//                  A.projectCode,
		//                  A.projectName,
		//                  CONCAT('~', A.image) AS image,
		//                  b.Id AS projectInfoID,
		//                  b.isOBActive
		//              FROM
		//                  MS_Project A
		//              JOIN
		//                  MS_ProjectInfo b ON A.Id = b.projectID
		//              JOIN
		//                  MS_Unit d ON A.Id = d.projectID
		//              WHERE
		//                  A.isPublish = 1
		//                  AND b.projectStatus = 1;";

		//		var resultList = (await dbConnectioni.QueryAsync<ListUserProjectMapDto>(queryString, commandType: CommandType.Text).ConfigureAwait(false)).ToList();

		//		if (checkAdmins == 0)
		//		{
		//			result = (from a in resultList
		//					  join b in _propertySystemDbContext.MS_ProjectOLBooking on a.projectInfoID equals b.projectInfoID
		//					  where getGroupSchema.Contains(a.projectCode)
		//					  && b.isActive == true
		//					  && a.isOBActive == true
		//					  group a by new
		//					  {
		//						  a.projectID,
		//						  a.projectCode,
		//						  a.projectName,
		//						  a.image
		//					  } into g
		//					  select new ListProjectResultDto
		//					  {
		//						  projectID = g.Key.projectID,
		//						  projectCode = g.Key.projectCode,
		//						  projectName = g.Key.projectName,
		//						  image = g.Key.image
		//					  }).ToList();
		//		}
		//		else
		//		{
		//			var getRoles = (await dbConnectiona.QueryAsync<int>(Role, commandType: CommandType.Text).ConfigureAwait(false)).ToList();

		//			string queryStrings = @"
		//             SELECT DISTINCT
		//                  A.Id AS projectID,
		//                  A.projectCode,
		//                  A.projectName,
		//                  CONCAT('http://18.140.60.145:1001/', A.image) AS image,
		//                  b.Id AS projectInfoID,
		//                  b.isOBActive
		//              FROM
		//                  MS_Project A
		//              JOIN
		//                  MS_ProjectInfo b ON A.Id = b.projectID
		//              JOIN
		//                  MS_Unit d ON A.Id = d.projectID
		//              JOIN SYS_RolesProject e ON A.Id = e.projectID
		//              WHERE
		//                  A.isPublish = 1
		//                  AND b.projectStatus = 1
		//                  AND e.rolesID IN (@Roles);";

		//			var resultList2 = (await dbConnectioni.QueryAsync<ListUserProjectMapDto>(queryString, new { Roles = getRoles }, commandType: CommandType.Text).ConfigureAwait(false)).ToList();

		//			result = (from a in resultList2
		//					  group a by new
		//					  {
		//						  a.projectID,
		//						  a.projectCode,
		//						  a.projectName,
		//						  a.image
		//					  } into g
		//					  select new ListProjectResultDto
		//					  {
		//						  projectID = g.Key.projectID,
		//						  projectCode = g.Key.projectCode,
		//						  projectName = g.Key.projectName,
		//						  image = g.Key.image
		//					  }).ToList();
		//		}

		//		if (resultList == null)
		//		{
		//			var error = new List<ListProjectResultDto>();

		//			var message = new ListProjectResultDto
		//			{
		//				message = "data not found"
		//			};

		//			error.Add(message);

		//			return new ListResultDto<ListProjectResultDto>(error);
		//		}
		//		else
		//		{
		//			return new ListResultDto<ListProjectResultDto>(result);
		//		}
		//	}
		//}

		public List<GetSalesTrackingTermPaymentDto> GetDropdownTerm(int projectID, bool isAdminBank)
		{
			string connectionaa = _configuration.GetConnectionString("PropertySystemDbContext");
			using IDbConnection dbConnectioni = new SqlConnection(connectionaa);
			dbConnectioni.Open();
			//var getData = (from a in _propertySystemDbContext.TR_BookingHeader
			//			   join b in _propertySystemDbContext.MS_Unit on a.unitID equals b.Id
			//			   where b.projectID == projectID
			//				&& a.cancelDate == null
			//			   select new GetSalesTrackingTermPaymentDto
			//			   {
			//				   termID = a.termID,
			//				   termRemarks = a.termRemarks
			//			   });

			string queryStrings = @"
                        SELECT 
                            A.termID,
                            A.termRemarks
                        FROM
                            TR_BookingHeader A
                        JOIN
                            MS_Unit b ON A.unitID = b.Id
                        WHERE
                            b.projectID = @ProjectID
                            AND A.cancelDate IS NULL";

			List<GetSalesTrackingTermPaymentDto> getData = new List<GetSalesTrackingTermPaymentDto>();
			getData = dbConnectioni.Query<GetSalesTrackingTermPaymentDto>(queryStrings, new { ProjectID = projectID }, commandType: CommandType.Text).ToList();

			if (isAdminBank)
			{
				getData = getData.Where(a => a.termRemarks.Contains("KPR") || a.termRemarks.Contains("KPA")).ToList();
			}

			return getData.Distinct().ToList();
		}
		/// </summary>
		/// <returns></returns>
		public string GetConnString(string connection)
		{
			var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
			var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];

			string[] connString;
			if (connection.ToLower() == "personal")
			{
				connString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
			}
			else if (connection.ToLower() == "propertysystem")
			{
				connString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
			}
			else if (connection.ToLower() == "newcomm")
			{
				connString = connectionStrings.Property("NewCommDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
			}
			else
			{
				throw new UserFriendlyException("Connection String Not Found!");
			}

			var DbName = connString[2].Replace("InitialCatalog=", string.Empty);


			return DbName;
		}
		
		public List<DocumentKPRListDto> GetListDocumentKPR(GetDocumentKPRInputDto input)
		{
			var query = "";
			var propSysConnString = GetConnString("propertysystem");
			string connectionaa = _configuration.GetConnectionString("PersonalsNewDbContext");
			using IDbConnection dbConnectioni = new SqlConnection(connectionaa);
			dbConnectioni.Open();
			string connectionai = _configuration.GetConnectionString("PropertySystemDbContext");
			using IDbConnection dbConnectionii = new SqlConnection(connectionai);
			dbConnectionii.Open();
			//var getKprType = @"select psCode, mok.kprtype from personals p
			//			  join ms_mappingOccKpr mok on p.occID = mok.occID
			//			  where p.pscode = '" + input.psCode + "'";
			//var kprType = (from a in _personalDbContext.PERSONAL
			//			   join b in _personalDbContext.MS_MappingOccKpr on a.occID equals b.occID
			//			   where a.psCode == input.psCode
			//			   select new { a.psCode, b.KPRType }
			//					 ).FirstOrDefault();
			string querys  = @"
                        SELECT 
                            a.psCode, b.KPRType
                        FROM
                            PERSONALS a
                        JOIN
                            MS_MappingOccKpr b ON a.occID = b.occID
                        WHERE
                            a.psCode = "+ input.psCode + "";
			var kprType = dbConnectioni.Query<int>(querys, commandType: CommandType.Text).FirstOrDefault();
			//var queryprioritypas = @"SELECT id FROM TR_PriorityPass WHERE PPNo = '" + input.ppNo + "'";
			//int getPrioritypassIds = _propertySystemDbContext.TR_PriorityPass.Where(x => x.PPNo == input.ppNo).Select(y => y.Id).FirstOrDefault();

			var getIDs = new DocumentKPRDto();
			if (input.bookingHeaderID == 0)
			{
				string querysi = @"
                        SELECT 
                            a.Id as BookingHeaderID, b.Id as PropertyPassID
                        FROM
                            TR_BookingHeader a
                        JOIN
                            TR_PriorityPass b ON a.NUP = b.PPNo
                        WHERE
                            b.PPNo  =  "+input.ppNo+" ";


				getIDs = dbConnectionii.Query<DocumentKPRDto>(querysi, commandType: CommandType.Text).FirstOrDefault();
				//getIDs = (from a in _propertySystemDbContext.TR_BookingHeader
				//		  join b in _propertySystemDbContext.TR_PriorityPass on a.NUP equals b.PPNo
				//		  where b.PPNo == input.ppNo
				//		  select new DocumentKPRDto
				//		  {
				//			  BookingHeaderID = a.Id,
				//			  PropertyPassID = b.Id
				//		  }).FirstOrDefault();

				if (getIDs == null)
				{
					//getIDs = (from a in _propertySystemDbContext.TR_PriorityPass
					//		  where a.PPNo == input.ppNo
					//		  select new DocumentKPRDto
					//		  {
					//			  BookingHeaderID = 0,
					//			  PropertyPassID = a.Id
					//		  }
					//		  ).FirstOrDefault();
				string querysa = @"
						SELECT 
							0 AS BookingHeaderID, 
							Id AS PropertyPassID
						FROM
							TR_PriorityPass
						WHERE
							PPNo = "+input.ppNo+"";

					getIDs = dbConnectionii.Query<DocumentKPRDto>(querysa, commandType: CommandType.Text).FirstOrDefault();

				}
			}
			else
			{
				//getIDs = (from a in _propertySystemDbContext.TR_BookingHeader
				//		  join b in _propertySystemDbContext.TR_PriorityPass on a.NUP equals b.PPNo
				//		  where
				//		  a.Id == input.bookingHeaderID &&
				//		  input.ppNo.Contains(a.NUP)
				//		  select new DocumentKPRDto
				//		  {
				//			  BookingHeaderID = a.Id,
				//			  PropertyPassID = b.Id
				//		  }).FirstOrDefault();
				string querying = @"
						SELECT 
							a.Id AS BookingHeaderID, 
							b.Id AS PropertyPassID
						FROM
							TR_BookingHeader a
						JOIN
							TR_PriorityPass b ON a.NUP = b.PPNo
						WHERE
							a.Id = "+ input.bookingHeaderID + @" AND
							"+input.ppNo+@" LIKE '%' + a.NUP + '%'
					";

				getIDs = dbConnectionii.Query<DocumentKPRDto>(querying, commandType: CommandType.Text).FirstOrDefault();


				if (getIDs == null)
				{
					//getIDs = (from a in _propertySystemDbContext.TR_PriorityPass
					//		  where input.ppNo.Contains(a.PPNo)
					//		  select new DocumentKPRDto
					//		  {
					//			  BookingHeaderID = input.bookingHeaderID,
					//			  PropertyPassID = a.Id
					//		  }
					//		  ).FirstOrDefault();
					string queryw = @"
							SELECT 
								"+ input.bookingHeaderID + @" AS BookingHeaderID, 
								Id AS PropertyPassID
							FROM
								TR_PriorityPass
							WHERE
								"+ input.ppNo + @" LIKE '%' + PPNo + '%'
						";

					getIDs = dbConnectionii.Query<DocumentKPRDto>(queryw, commandType: CommandType.Text).FirstOrDefault();

				}
			}


			//int getPrioritypassIds = _sqlExecuter.GetFromPropertySystem<int>(queryprioritypas).FirstOrDefault();

			//var kprType = _sqlExecuter.GetFromPersonals<DocumentKPRListDto>(getKprType).FirstOrDefault();

			if (kprType != 0)
			{
				if (input.bookingHeaderID == 0 && getIDs == null)
				{
					query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKpr mok on p.occID = mok.occID
				  join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
						and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
				}
				if (input.bookingHeaderID != 0 && getIDs == null)
				{
					query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKpr mok on p.occID = mok.occID
				  join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
						and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
				}

				if (input.bookingHeaderID == 0 && getIDs != null)
				{

					if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID != null)
					{
						//custom
						query = @"	select 
						p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId 
					,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID 
					, 
						case when td.documentBinary is not null 
							then 1
							ELSE 0
						END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
					--,td.bookingHeaderID
					from personals p
					join ms_mappingOccKpr mok on p.occID = mok.occID
					join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
					join ms_document d on mokd.documentType = d.documentType
					left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType and p.pscode = td.pscode 
					and (td.PriorityPassId = " + getIDs.PropertyPassID + @" or td.bookingHeaderID = " + getIDs.BookingHeaderID + @")
					where p.pscode = '" + input.psCode + @"'
					order by ismandatory desc, isuploaded desc, isVerified DESC;";
					}
					if (getIDs.BookingHeaderID == 0 && getIDs.PropertyPassID != null)
					{
						//custom
						query = @"	select 
						p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId 
					,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID 
					, 
						case when td.documentBinary is not null 
							then 1
							ELSE 0
						END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
					--,td.bookingHeaderID
					from personals p
					join ms_mappingOccKpr mok on p.occID = mok.occID
					join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
					join ms_document d on mokd.documentType = d.documentType
					left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType and p.pscode = td.pscode 
					and td.PriorityPassId = " + getIDs.PropertyPassID + @"
					where p.pscode = '" + input.psCode + @"'
					order by ismandatory desc, isuploaded desc, isVerified DESC;";
					}
					else if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID == null)
					{
						//existing
						query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKpr mok on p.occID = mok.occID
				  join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
						and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";



					}

				}
				///////////////////////////////////////////////////////////////////////////////////////
				///bokingnya ada
				if (input.bookingHeaderID != 0 && getIDs != null)
				//else
				{

					if (input.bookingHeaderID != 0 && getIDs.PropertyPassID != null)
					{
						var listDocKPR2 = _propertySystemDbContext.TR_DocumentKPR.Where(x => x.psCode == input.psCode && x.bookingHeaderID == input.bookingHeaderID && x.PriorityPassId == null).Select(x => x).ToList();

						if (listDocKPR2.Count > 0)
						{
							query = @"	select 
								p.psCode
								,mokd.kprtype
								,mokd.documentType
								,d.documentName
								,mokd.isMandatory
								,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId 
								,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID 
								, 
									case when td.documentBinary is not null 
										then 1
										ELSE 0
									END AS isUploaded
								,isVerified
								,td.CreationTime
								,td.LastModificationTime
								--,td.bookingHeaderID
								from personals p
								join ms_mappingOccKpr mok on p.occID = mok.occID
								join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
								join ms_document d on mokd.documentType = d.documentType
								left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType and p.pscode = td.pscode 
								and (td.bookingHeaderID = " + input.bookingHeaderID + @" or td.PriorityPassId = " + getIDs.PropertyPassID + @")
								where p.pscode = '" + input.psCode + @"'
								order by ismandatory desc, isuploaded desc, isVerified DESC;";
						}
						else
						{
							//custom sini ron
							query = @"	select 
								p.psCode
								,mokd.kprtype
								,mokd.documentType
								,d.documentName
								,mokd.isMandatory
								,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId 
								,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID 
								, 
									case when td.documentBinary is not null 
										then 1
										ELSE 0
									END AS isUploaded
								,isVerified
								,td.CreationTime
								,td.LastModificationTime
								--,td.bookingHeaderID
								from personals p
								join ms_mappingOccKpr mok on p.occID = mok.occID
								join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
								join ms_document d on mokd.documentType = d.documentType
								left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType and p.pscode = td.pscode 
								and td.bookingHeaderID = " + input.bookingHeaderID + @" and td.PriorityPassId = " + getIDs.PropertyPassID + @"
								where p.pscode = '" + input.psCode + @"'
								order by ismandatory desc, isuploaded desc, isVerified DESC;";
						}

					}
					else
					{
						//existing
						query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKpr mok on p.occID = mok.occID
				  join ms_mappingOccKprDocument mokd on mok.KPRType = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
						and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";

					}

				}
			}
			//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
			else
			{
				if (input.bookingHeaderID == 0 && getIDs == null)
				{
					query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
				}
				if (input.bookingHeaderID != 0 && getIDs == null)
				{
					query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
				}
				//non Kpr
				if (input.bookingHeaderID == 0 && getIDs != null)
				{

					if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID != null)
					{
						//custom non kpr
						//custom non kpr
						var listDocKPR2 = _propertySystemDbContext.TR_DocumentKPR.Where(x => x.psCode == input.psCode && x.bookingHeaderID == getIDs.BookingHeaderID && x.PriorityPassId == null).Select(x => x).ToList();

						if (listDocKPR2.Count > 0)
						{
							query = @"select 
								p.psCode
								,mokd.kprtype
								,mokd.documentType
								,d.documentName
								,mokd.isMandatory
								,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId
								,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID					
								, 
								 case when td.documentBinary is not null 
								  then 1
								  ELSE 0
							  END AS isUploaded
							,isVerified
							,td.CreationTime
							,td.LastModificationTime
						  from personals p
						  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
						  join ms_document d on mokd.documentType = d.documentType
						  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
							   and p.pscode = td.pscode and (td.PriorityPassId = " + getIDs.PropertyPassID + @" or td.bookingHeaderID = " + getIDs.BookingHeaderID + @")
						  where p.pscode = '" + input.psCode + @"' 
						  order by ismandatory desc, isuploaded desc, isVerified desc";
						}
						else
						{
							query = @"select 
								p.psCode
								,mokd.kprtype
								,mokd.documentType
								,d.documentName
								,mokd.isMandatory
								,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId
								,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID					
								, 
								 case when td.documentBinary is not null 
								  then 1
								  ELSE 0
							  END AS isUploaded
							,isVerified
							,td.CreationTime
							,td.LastModificationTime
						  from personals p
						  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
						  join ms_document d on mokd.documentType = d.documentType
						  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
							   and p.pscode = td.pscode and (td.PriorityPassId = " + getIDs.PropertyPassID + @" and td.bookingHeaderID = " + getIDs.BookingHeaderID + @")
						  where p.pscode = '" + input.psCode + @"' 
						  order by ismandatory desc, isuploaded desc, isVerified desc";
						}



					}
					if (getIDs.BookingHeaderID == 0 && getIDs.PropertyPassID != null)
					{
						//custom
						query = @"select 
						p.psCode
						,mokd.kprtype
						,mokd.documentType
						,d.documentName
						,mokd.isMandatory
						,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId
						,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID					
						, 
						 case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.PriorityPassId = " + getIDs.PropertyPassID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
					}
					else if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID == null)
					{
						//existing non kpr
						query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";

					}

				}
				if (input.bookingHeaderID != 0 && getIDs != null)
				{
					if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID != null)
					{
						//custom non kpr
						//custom non kpr disni 
						query = @"select 
						p.psCode
						,mokd.kprtype
						,mokd.documentType
						,d.documentName
						,mokd.isMandatory
						,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId
						,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID					
						, 
						 case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
				  and p.pscode = td.pscode and (td.PriorityPassId = " + getIDs.PropertyPassID + @" or td.bookingHeaderID = " + getIDs.BookingHeaderID + @")
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";

					}
					if (getIDs.BookingHeaderID == 0 && getIDs.PropertyPassID != null)
					{
						//custom
						query = @"select 
						p.psCode
						,mokd.kprtype
						,mokd.documentType
						,d.documentName
						,mokd.isMandatory
						,case when " + getIDs.PropertyPassID + " is null then null else " + getIDs.PropertyPassID + @" end as PriorityPassId
						,case when " + getIDs.BookingHeaderID + " is null then null else " + getIDs.BookingHeaderID + @" end as BookingHeaderID					
						, 
						 case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.PriorityPassId = " + getIDs.PropertyPassID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";
					}
					else if (getIDs.BookingHeaderID != 0 && getIDs.PropertyPassID == null)
					{
						//existing non kpr
						query = @"select 
					 p.psCode
					,mokd.kprtype
					,mokd.documentType
					,d.documentName
					,mokd.isMandatory
					, 
					  case when td.documentBinary is not null 
						  then 1
						  ELSE 0
					  END AS isUploaded
					,isVerified
					,td.CreationTime
					,td.LastModificationTime
				  from personals p
				  join ms_mappingOccKprDocument mokd on 4 = mokd.KPRType
				  join ms_document d on mokd.documentType = d.documentType
				  left join " + propSysConnString + @"..tr_documentKPR td on mokd.documentType = td.documentType 
					   and p.pscode = td.pscode and td.bookingHeaderID = " + input.bookingHeaderID + @"
				  where p.pscode = '" + input.psCode + @"' 
				  order by ismandatory desc, isuploaded desc, isVerified desc";

					}
				}

			}

			//var data = _sqlExecuter.GetFromPersonals<DocumentKPRListDto>(query).ToList();
			//List<DocumentKPRListDto> getData = new List<DocumentKPRListDto>();
			var data = dbConnectioni.Query<DocumentKPRListDto>(query, commandType: CommandType.Text).ToList();

			// Map the data to the DTO    

			return data;
		}
		//public List<PaymentMethodDto> GetDropdownPaymentMethod()
  //      {

  //          var result = (from a in _contextBilling.MS_PaymentMethod
  //                        select new PaymentMethodDto
  //                        {
  //                            paymentName = a.PaymentMethodName,
  //                            paymentType = a.Id
  //                        }).ToList();

  //          return result;


  //      }
        //public List<BankListDto> GetDropdownBank()
        //{

        //    var result = (from a in _contextBilling.MS_Bank
        //                  select new BankListDto
        //                  {
        //                      BankID = a.Id,
        //                      BankName = a.BankName
        //                  }).ToList();

        //    return result;


        //}





        //public PagedResultDto<GetCustomerListDto> GetCustomerList(CustomerListInputDto input)
        //{

        //    #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
        //    var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
        //    var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
        //    var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

        //    var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

        //    var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);


        //    SqlConnection conn = new SqlConnection(ConstringProp);

        //    conn.Open();
        //    //conn1.Open();

        //    #endregion

        //    string query = $@"SELECT DISTINCT mud.Id as UnitDataId, mud.SiteId, mud.ClusterId, mud.ProjectId, mp.projectName, mcs.ClusterName, mud.UnitNo, mud.UnitCode, mud.UnitName, mud.PsCode, p.name AS CustomerName
        //                    FROM  " + dbCatalogBillingString + "..MS_UnitData  mud JOIN  STGPropertySystem..MS_Project mp ON mud.ProjectId = mp.Id  " +
        //                    "LEFT JOIN  " + dbCatalogBillingString + "..MP_ClusterSite mcs ON mud.ClusterId = mcs.ClusterId " +
        //                    "JOIN   " + dbCatalogPersonalString + "..PERSONALS p on mud.PsCode = p.psCode  " +
        //                    "WHERE mud.SiteId =" + input.SiteId + "";




        //    List<GetCustomerListDto> getData = conn.Query<GetCustomerListDto>(query).ToList();

        //    getData = getData
        //       .WhereIf(input.Search != null, item => item.CustomerName.ToLower().Contains(input.Search.ToLower()))
        //       .WhereIf(input.UnitCode != null, item => item.UnitCode.ToLower().Contains(input.UnitCode.ToLower()))
        //       .WhereIf(input.UnitNo != null, item => item.UnitNo.ToLower().Contains(input.UnitNo.ToLower())).ToList();

        //    var dataCount = getData.Count();

        //    getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


        //    return new PagedResultDto<GetCustomerListDto>(dataCount, getData.ToList());

        //}


//        public GetPaymentDetailByPsCodeDto GetPaymentDetailByPsCode(string PsCode, int unitDataId)
//        {
//            var listDatainvoice = new List<InvoicePaymentDto>();

//            var getprojectId = (from a in _contextBilling.MS_UnitData
//                                where a.Id == unitDataId
//                                select a).FirstOrDefault();

//            var getPeriodActive = (from a in _contextBilling.MS_Period
//                                   where a.IsActive == true && a.SiteId == getprojectId.SiteId
//                                   select a).FirstOrDefault();

//            if (getPeriodActive == null)
//            {
//                throw new UserFriendlyException("Period InActive");
//            }


//            var getInvoiceData = (from a in _contextBilling.TR_InvoiceHeader
//                                  join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id
//                                  where a.PsCode == PsCode && a.UnitDataId == unitDataId && a.PeriodId == getPeriodActive.Id
//                                  select new
//                                  {
//                                      InvoiceId = a.Id,
//                                      InvoiceNo = a.InvoiceNo,
//                                      TypeInvoice = b.TemplateInvoiceName,
//                                      TotalTunggakan = a.JumlahYangHarusDibayar,
//                                      PsCode = a.PsCode,
//                                  }).ToList();

//            if (getInvoiceData.Count > 0)
//            {

//                foreach (var item in getInvoiceData)
//                {

//                    var getPaymentDetail = (from a in _contextBilling.TR_BillingPaymentHeader
//                                            join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.BillingHeaderId
//                                            where b.InvoiceHeaderId == item.InvoiceId && a.UnitDataId == unitDataId && a.CancelPayment == false
//                                            select b.PaymentAmount).ToList();

//                    var pengurangan = getPaymentDetail.Sum();


//                    var addData = new InvoicePaymentDto
//                    {
//                        InvoiceId = item.InvoiceId,
//                        InvoiceNo = item.InvoiceNo,
//                        InvoiceName = item.TypeInvoice,
//                        Balance = item.TotalTunggakan,
//                        EndBalance = item.TotalTunggakan - pengurangan,


//                    };



//                    listDatainvoice.Add(addData);

//                }




//                var getData = new GetPaymentDetailByPsCodeDto
//                {
//                    PsCode = getInvoiceData.Select(a => a.PsCode).FirstOrDefault(),
//                    projectId = getprojectId.ProjectId,
//                    listInvoicePayment = listDatainvoice

//                };

//                return getData;
//            }
//            else
//            {
//                throw new UserFriendlyException("There is no bill for this unit");
//            }



//        }

//        public string PaymentProses(PaymentProsesInputDto input)
//        {
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


//            var desctiptionPDf = new List<string>();
//            var runNumberPdf = 0;

//            var getUnitDataID = (from a in _contextBilling.MS_UnitData
//                                 where a.Id == input.UnitDataId
//                                 select a).FirstOrDefault();

//            var getPeriodName = (from a in _contextBilling.MS_Period
//                                 where a.SiteId == input.SiteId && a.IsActive == true
//                                 select a).FirstOrDefault();

//            #region generate ORNumber
//            var getRunningNo = 0;
//            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
//                                where a.SiteId == input.SiteId && a.DocCode == "OR" && a.ProjectId == input.ProjectId && a.PeriodId == getPeriodName.Id
//                                orderby a.Number descending
//                                select a).FirstOrDefault();
//            if (getTrRunning == null)
//            {

//                getRunningNo = 1;
//                var AddRunningNumber = new TR_RunningNumber
//                {
//                    DocCode = "OR",
//                    ProjectId = input.ProjectId,
//                    Number = getRunningNo,
//                    SiteId = input.SiteId,
//                    PeriodId = getPeriodName.Id
//                };

//                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
//            }
//            else
//            {
//                getRunningNo = getTrRunning.Number + 1;
//                getTrRunning.Number = getRunningNo;

//                _contextBilling.TR_RunningNumber.Update(getTrRunning);

//            }


//            _contextBilling.SaveChanges();


//            var getDataSite = (from a in _contextBilling.MS_Site
//                               where a.SiteId == input.SiteId && a.IsActive == true
//                               select a).FirstOrDefault();



          

//            var AdminBilling = (from x in _contextBilling.Users
//                                where x.Id == AbpSession.GetUserId()
//                                select x.Name).FirstOrDefault();

//            var periodName = getPeriodName.PeriodMonth.ToString("MMMM") + " " + getPeriodName.PeriodYear.ToString("yyyy");



//            var generateRunningNo = "0000000";
//            var CountRunningNo = getRunningNo.ToString().Length;
//            var CountSub = generateRunningNo.Length - CountRunningNo;

//            var ORNoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;

//            var ORNoComb = "" + ORNoFinal + "/OR/" + getDataSite.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

//            #endregion



//            var InserPaymentHeader = new TR_BillingPaymentHeader
//            {

//                PaymentMethodId = input.PaymentType,
//                AmountPayment = input.TotalPayment,
//                BankId = input.BankId,
//                CardNumber = input.CardNumber,
//                Total = input.TotalPayment,
//                TransactionDate = input.TransactionDate,
//                Remarks = input.Remarks,
//                Charge = input.Charge,
//                PsCode = input.PsCode,
//                UnitDataId = input.UnitDataId,
//                UnitCode = input.UnitCode,
//                UnitNo = input.UnitNo,
//                ReceiptNumber = ORNoComb,
//                Status = "Success",


//            };

//            _contextBilling.TR_BillingPaymentHeader.Add(InserPaymentHeader);
//            _contextBilling.SaveChanges();

//            int PaymentHeaderId = InserPaymentHeader.Id;

//            foreach (var item in input.listInvoicePayment)
//            {
//                if (item.PaymentAmount > 0)
//                {
//                    var getItemRateName = (from a in _contextBilling.TR_InvoiceHeader
//                                           join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id
//                                           where a.Id == item.InvoiceId
//                                           select b.TemplateInvoiceName).FirstOrDefault();


//                    var InsertDetail = new TR_BillingPaymentDetail
//                    {
//                        BillingHeaderId = PaymentHeaderId,
//                        InvoiceHeaderId = item.InvoiceId,
//                        PaymentAmount = item.PaymentAmount,
//                    };

//                    _contextBilling.TR_BillingPaymentDetail.Add(InsertDetail);
//                    _contextBilling.SaveChanges();

//                    runNumberPdf++;

//                    var getInvoiceNo = (from a in _contextBilling.TR_InvoiceHeader
//                                        where a.Id == item.InvoiceId
//                                        select a.InvoiceNo).FirstOrDefault();

//                    var addDesPdf = "<tr><td>" + runNumberPdf + "</td>" +
//                        "<td>" + getInvoiceNo + " - " + " Billing Payment " + getItemRateName + "  " + periodName + "</td> " +
//                        "<td>{{CaraBayar}}</td> " +
//                        "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.PaymentAmount) + "</td> </tr>";

//                    desctiptionPDf.Add(addDesPdf);

//                }


//            }


//            var getCaraBayar = (from a in _contextBilling.MS_PaymentMethod
//                                where a.Id == input.PaymentType
//                                select a.PaymentMethodName).FirstOrDefault();

//            var getBankName = (from a in _contextBilling.MS_Bank
//                               where a.Id == input.BankId
//                               select a.BankName).FirstOrDefault();

//            var carabayarFinal = getBankName == null ? getCaraBayar : getCaraBayar + " - " + getBankName;



//            #region generate Pdf
//            var tanggalPrint = DateTime.Now;
//            var DataToPdf = new ParamGenerateOfficialReceiptDto
//            {

//                SiteLogo = getDataSite.Logo,
//                SiteAddres = getDataSite.SiteAddress,
//                SiteEmail = getDataSite.Email,
//                SiteName = getDataSite.SiteName,
//                SitePhone = "Telp. " + getDataSite.OfficePhone + " - " + getDataSite.HandPhone + "",
//                NoOR = ORNoComb,
//                PsCode = input.PsCode.ToString(),
//                CustName = (from a in _contextPersonal.PERSONALS
//                            where a.psCode == input.PsCode
//                            select a.name).FirstOrDefault(),
//                TanggalOR = DateTime.Now.ToString("dd MMMM yyyy"),
//                CaraBayar = carabayarFinal,
//                Deskripsi = string.Join(Environment.NewLine, desctiptionPDf.ToArray()),
//                TotalPayment = NumberHelper.IndoFormatWithoutTail(input.TotalPayment),
//                PrintDate = tanggalPrint.ToString("dd MMMM yyyy HH:mm:ss"),
//                AdminBilling = input.IsAddSignee == true ? AdminBilling : "",
//                unitName = getUnitDataID.UnitName,
//                unitNo = input.UnitNo,
//                periodeName = periodName,
//                BillingPayementheaderId = PaymentHeaderId,
//                TanggalORIndoFormat = tanggalPrint.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + " " + tanggalPrint.ToString("HH:mm:ss"),

//            };


//            var fileName = GenerateOfficialReceiptPdf(DataToPdf);

//            #endregion

//            #region SendEMail 

//            DataToPdf.fileNamePDF = fileName;

//            SendEmailORPaymentProses(DataToPdf);
//            #endregion

//            return ipPublic + "/Assets/Upload/PdfOutput/OR/" + fileName;

//        }




//        public PagedResultDto<ListOfficialReceiptDto> GetListOfficialReceipt(OfficialReceiptInputDto input)
//        {
//            var getData = (from b in _contextBilling.TR_BillingPaymentHeader
//                           join f in _contextBilling.TR_BillingPaymentDetail on b.Id equals f.BillingHeaderId
//                           join c in _contextBilling.TR_InvoiceHeader on f.InvoiceHeaderId equals c.Id
//                           join d in _contextBilling.MS_PaymentMethod on b.PaymentMethodId equals d.Id
//                           join e in _contextBilling.MS_TemplateInvoiceHeader on c.TemplateInvoiceHeaderId equals e.Id
//                           where b.UnitDataId == input.UnitDataId

//                           select new ListOfficialReceiptDto
//                           {
//                               BillingHeaderId = b.Id,
//                               ReceiptNumber = b.ReceiptNumber,
//                               InvoiceNumber = c.InvoiceNo,
//                               Method = d.PaymentMethodName,
//                               TotalAmount = f.PaymentAmount,
//                               TransactionDate = b.TransactionDate.ToString("dd MMMM yyyy"),
//                               Remarks = b.Remarks,
//                               InvoiceName = e.TemplateInvoiceName
//                           }).Distinct().OrderByDescending(x => x.ReceiptNumber).ToList();

//            var dataCount = getData.Count();

//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


//            return new PagedResultDto<ListOfficialReceiptDto>(dataCount, getData.ToList());

//        }
//        public void ProsesUpdateTransactionDateOR(int BillingHeaderId, DateTime newTransactionDate)
//        {

//            var updateData = (from a in _contextBilling.TR_BillingPaymentHeader
//                              where a.Id == BillingHeaderId
//                              select a).FirstOrDefault();


//            updateData.TransactionDate = newTransactionDate;

//            _contextBilling.TR_BillingPaymentHeader.Update(updateData);
//            _contextBilling.SaveChanges();

//        }

//        public string ReprintOfficialReceipt(int BillingHeaderId, int SiteId)
//        {
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

//            var desctiptionPDf = new List<string>();
//            var runNumberPdf = 0;

//            var getDataHeader = (from a in _contextBilling.TR_BillingPaymentHeader
//                                 join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                                 join c in _contextBilling.TR_BillingPaymentDetail on a.Id equals c.BillingHeaderId
//                                 join d in _contextBilling.TR_InvoiceHeader on c.InvoiceHeaderId equals d.Id
//                                 join e in _contextBilling.MS_PaymentMethod on a.PaymentMethodId equals e.Id
//                                 join f in _contextBilling.MS_Bank on a.BankId equals f.Id into banknull
//                                 from banktemp in banknull.DefaultIfEmpty()
//                                 where a.Id == BillingHeaderId
//                                 select new
//                                 {
//                                     a.CreatorUserId,
//                                     a.ReceiptNumber,
//                                     a.PsCode,
//                                     e.PaymentMethodName,
//                                     a.Total,
//                                     a.TransactionDate,
//                                     b.UnitName,
//                                     b.UnitId,
//                                     b.UnitNo,
//                                     d.PeriodId,
//                                     d.TemplateInvoiceHeaderId,
//                                     BankName = banktemp.BankName == null ? "" : banktemp.BankName
//                                 }).FirstOrDefault();



//            var getDataSite = (from a in _contextBilling.MS_Site
//                               where a.SiteId == SiteId && a.IsActive == true
//                               select a).FirstOrDefault();

           

//            var getPeriodName = (from a in _contextBilling.MS_Period
//                                 where a.Id == getDataHeader.PeriodId
//                                 select a).FirstOrDefault();

//            var AdminBilling = (from a in _contextBilling.Users
//                                where a.Id == getDataHeader.CreatorUserId
//                                select a.Name).FirstOrDefault();

//            var periodName = getPeriodName.PeriodMonth.ToString("MMMM") + " " + getPeriodName.PeriodYear.ToString("yyyy");

//            var getPaymentDetail = (from a in _contextBilling.TR_BillingPaymentDetail
//                                    join b in _contextBilling.TR_InvoiceHeader on a.InvoiceHeaderId equals b.Id
//                                    where a.BillingHeaderId == BillingHeaderId
//                                    select new
//                                    {
//                                        a.BillingHeaderId,
//                                        a.InvoiceHeaderId,
//                                        a.PaymentAmount,
//                                        b.InvoiceNo
//                                    }).ToList();

//            if (getPaymentDetail.Count > 0)
//            {
//                foreach (var item in getPaymentDetail)
//                {
//                    var getItemRateName = (from a in _contextBilling.TR_InvoiceHeader
//                                           join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id
//                                           where a.Id == item.InvoiceHeaderId
//                                           select b.TemplateInvoiceName).FirstOrDefault();

//                    runNumberPdf++;
//                    var addDesPdf = "<tr><td>" + runNumberPdf + "</td>" +
//                           "<td>" + item.InvoiceNo + " - " + " Billing Payment " + getItemRateName + "  " + periodName + "</td> " +
//                           "<td>{{CaraBayar}}</td> " +
//                           "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.PaymentAmount) + "</td> </tr>";

//                    desctiptionPDf.Add(addDesPdf);



//                };


//            }


//            var tanggalPrint = DateTime.Now;

//            var DataToPdf = new ParamGenerateOfficialReceiptDto
//            {

//                SiteLogo = getDataSite.Logo,
//                SiteAddres = getDataSite.SiteAddress,
//                SiteEmail = getDataSite.Email,
//                SiteName = getDataSite.SiteName,
//                SitePhone = "Telp. " + getDataSite.OfficePhone + " - " + getDataSite.HandPhone + "",
//                NoOR = getDataHeader.ReceiptNumber,
//                PsCode = getDataHeader.PsCode.ToString(),
//                CustName = (from a in _contextPersonal.PERSONALS
//                            where a.psCode == getDataHeader.PsCode
//                            select a.name).FirstOrDefault(),
//                TanggalOR = getDataHeader.TransactionDate.ToString("dd MMMM yyyy "),
//                CaraBayar = getDataHeader.BankName != "" ? getDataHeader.PaymentMethodName + " - " + getDataHeader.BankName : getDataHeader.PaymentMethodName,
//                Deskripsi = string.Join(Environment.NewLine, desctiptionPDf.ToArray()),
//                TotalPayment = NumberHelper.IndoFormatWithoutTail(getDataHeader.Total),
//                PrintDate = tanggalPrint.ToString("dd MMMM yyyy HH:mm:ss"),
//                AdminBilling = AdminBilling,
//                unitName = getDataHeader.UnitName,
//                unitNo = getDataHeader.UnitNo,
//                TanggalORIndoFormat = tanggalPrint.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + " " + tanggalPrint.ToString("HH:mm:ss"),

//            };


//            var fileName = GenerateOfficialReceiptPdf(DataToPdf);

//            #region proses Update ReprintOR
//            var getDataUpdate = (from a in _contextBilling.TR_BillingPaymentHeader
//                                 where a.Id == BillingHeaderId
//                                 select a).FirstOrDefault();

//            getDataUpdate.ReprintOR = getDataUpdate.ReprintOR + 1;

//            _contextBilling.TR_BillingPaymentHeader.Update(getDataUpdate);
//            _contextBilling.SaveChanges();
//            #endregion


//            return ipPublic + "/Assets/Upload/PdfOutput/OR/" + fileName;
//        }



//        private string GenerateOfficialReceiptPdf(ParamGenerateOfficialReceiptDto input)
//        {

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var pdfOutputPath = connectionApp.Property("pdfOutputOR").Value.ToString();


//            var destinationPath = pdfOutputPath;

//            if (!Directory.Exists(destinationPath))
//            {
//                Directory.CreateDirectory(destinationPath);
//            }

//            var fileName = "Official_Receipt_" + input.CustName.Replace(" ", "") + "_" + input.PsCode + ".pdf";
//            var filePath = Path.Combine(destinationPath, fileName);
//            try
//            {
//                var getLogoSite = _hostEnvironment.WebRootPath + @"\Assets\Upload\LogoSite\" + input.SiteLogo + "";
//                var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\MasterTemplate\officialreceipt.html";
//                var htmlToConvert = File.ReadAllText(templateFile);


//                HtmlToPdf converter = new HtmlToPdf();
//                converter.Options.PdfPageSize = PdfPageSize.B5;
//                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
//                converter.Options.WebPageWidth = 1024;

//                var doc = converter.ConvertHtmlString(htmlToConvert
//                .Replace("{{SiteLogo}}", getLogoSite)
//                .Replace("{{SiteName}}", input.SiteName)
//                .Replace("{{SiteAddres}}", input.SiteAddres)
//                .Replace("{{SitePhone}}", input.SitePhone)
//                .Replace("{{SiteEmail}}", input.SiteEmail)
//                .Replace("{{NoOR}}", input.NoOR)
//                .Replace("{{TanggalOR}}", input.TanggalOR)
//                .Replace("{{PsCode}}", input.PsCode)
//                .Replace("{{CustName}}", input.CustName)
//                .Replace("{{TotalPayment}}", input.TotalPayment)
//                .Replace("{{Deskripsi}}", input.Deskripsi)
//                .Replace("{{CaraBayar}}", input.CaraBayar)
//                .Replace("{{AdminBilling}}", input.AdminBilling)
//                .Replace("{{PrintDate}}", input.PrintDate)
//                .Replace("{{unitName}}", input.unitName)
//                .Replace("{{unitNo}}", input.unitNo)
//                .Replace("{{PrintDateIndo}}", input.TanggalORIndoFormat)

//);
//                byte[] results;

//                using (var stream = new MemoryStream())
//                {
//                    doc.Save(stream);
//                    results = stream.ToArray();
//                    doc.Close();
//                }

//                if (File.Exists(filePath))
//                {
//                    File.Delete(filePath);
//                }

//                File.WriteAllBytes(filePath, results);

//            }
//            catch (Exception ex)
//            {

//            }

//            return fileName;

//        }


//        public PagedResultDto<ListCancelPaymentDto> GetListCancelPayment(OfficialReceiptInputDto input)
//        {
//            var getData = (from a in _contextBilling.MS_UnitData
//                           join b in _contextBilling.TR_BillingPaymentHeader on a.Id equals b.UnitDataId
//                           join c in _contextBilling.MS_PaymentMethod on b.PaymentMethodId equals c.Id
//                           where a.Id == input.UnitDataId
//                           select new ListCancelPaymentDto
//                           {
//                               BillingHeaderId = b.Id,
//                               ReceiptNumber = b.ReceiptNumber,
//                               Method = c.PaymentMethodName,
//                               TotalAmount = b.Total,
//                               TransactionDate = b.TransactionDate.ToString("dd MMMM yyyy"),
//                               Remarks = b.Remarks,
//                               Canceled = b.CancelDate == null ? "No" : "Yes"
//                           }).Distinct().OrderByDescending(x => x.ReceiptNumber).ToList();

//            var dataCount = getData.Count();

//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


//            return new PagedResultDto<ListCancelPaymentDto>(dataCount, getData.ToList());

//        }


//        public DetailCancelPaymentDto GetDetailCancelPayment(int BillingHeaderId)
//        {

//            var final = new DetailCancelPaymentDto();
//            var getData = (from a in _contextBilling.MS_UnitData
//                           join b in _contextBilling.TR_BillingPaymentHeader on a.Id equals b.UnitDataId
//                           join c in _contextBilling.MS_PaymentMethod on b.PaymentMethodId equals c.Id
//                           where b.Id == BillingHeaderId
//                           select new
//                           {
//                               UnitID = a.UnitId,
//                               UnitCode = a.UnitCode,
//                               UnitNo = a.UnitNo,
//                               BillingHeaderId = b.Id,
//                               ReceiptNumber = b.ReceiptNumber,
//                               Method = c.PaymentMethodName,
//                               TotalAmount = b.Total,
//                               TransactionDate = b.TransactionDate.ToString("dd MMMM yyyy"),
//                               Remarks = b.Remarks,
//                               Canceled = b.CancelDate == null ? "No" : "Yes"
//                           }).FirstOrDefault();



//            var listInvoice = (from a in _contextBilling.TR_BillingPaymentDetail
//                               join b in _contextBilling.TR_InvoiceHeader on a.InvoiceHeaderId equals b.Id
//                               where a.BillingHeaderId == BillingHeaderId
//                               select new InvoiceDetailCancelPaymentDto
//                               {
//                                   InvoiceNo = b.InvoiceNo,
//                                   Amount = a.PaymentAmount
//                               }).ToList();

//            final = new DetailCancelPaymentDto
//            {

//                BillingHeaderId = getData.BillingHeaderId,
//                UnitCode = getData.UnitCode,
//                UnitNo = getData.UnitNo,
//                TransactionDate = getData.TransactionDate,
//                Method = getData.Method,
//                Remarks = getData.Remarks,
//                TotalAmount = getData.TotalAmount,
//                ReceiptNumber = getData.ReceiptNumber,
//                ListInvoiceDetailCancelPayment = listInvoice

//            };


//            return final;

//        }

//        public void CancelPayment(int BillingHeaderId, string Remarks)
//        {

//            var getCancelData = (from a in _contextBilling.TR_BillingPaymentHeader
//                                 where a.Id == BillingHeaderId
//                                 select a).FirstOrDefault();

//            getCancelData.CancelDate = DateTime.Now;
//            getCancelData.CancelPayment = true;
//            getCancelData.UserCancelPayment = (from x in _contextBilling.Users
//                                               where x.Id == AbpSession.GetUserId()
//                                               select x.Id).FirstOrDefault();
//            getCancelData.Remarks = Remarks;


//            _contextBilling.TR_BillingPaymentHeader.Update(getCancelData);
//            _contextBilling.SaveChanges();

//        }




//        public ResponseBulkPaymentDto UploadBulkPayment(UploadBulkPaymentDto input)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion



//            var desctiptionPDf = new List<string>();
//            var runNumberPdf = 0;

//            var totalData = input.DetailUploadBulkPaymentList.Count();
//            int prosesData = 0;
//            int totalgagal = 0;
//            int totalsukses = 0;
//            FileDto file = new FileDto();
//            string URL = "";
//            var gagalDataList = new List<DetailUploadBulkPaymentDto>();

//            foreach (var item in input.DetailUploadBulkPaymentList)
//            {
//                prosesData++;

//                var getBillingPaymentHeaderID = 0;
//                var periodName = "";


//                var getUnitDataId = (from a in _contextBilling.MS_UnitData
//                                     where a.UnitNo == item.UnitNo && a.UnitCode == item.UnitCode && a.SiteId == input.SiteId
//                                     select a).FirstOrDefault();

//                if (getUnitDataId == null)
//                {

//                    item.RemarksError = "Unit tidak terdaftar";
//                    //gagal
//                    totalgagal++;
//                    gagalDataList.Add(item);
//                }

//                if (getUnitDataId != null)
//                {

//                    #region Main Proses
//                    var getInvoiceId = (from a in _contextBilling.TR_InvoiceHeader
//                                        join b in _contextBilling.MS_Period on a.PeriodId equals b.Id
//                                        where a.InvoiceNo == item.InvoiceNumber && a.SiteId == input.SiteId && a.UnitDataId == getUnitDataId.Id && b.IsActive == true
//                                        select a).FirstOrDefault();


//                    if (getInvoiceId == null)
//                    {

//                        item.RemarksError = "Periode invoice tidak aktif";
//                        totalgagal++;
//                        gagalDataList.Add(item);

//                    }
//                    else
//                    {

//                        var getPeriodName = (from a in _contextBilling.MS_Period
//                                             where a.Id == getInvoiceId.PeriodId
//                                             select a).FirstOrDefault();

//                        periodName = getPeriodName.PeriodMonth.ToString("MMMM") + " " + getPeriodName.PeriodYear.ToString("yyyy");

//                    }



//                    if (getUnitDataId != null && getInvoiceId != null)
//                    {


//                        var getItemRateName = (from a in _contextBilling.MS_TemplateInvoiceHeader
//                                               where a.Id == getInvoiceId.TemplateInvoiceHeaderId
//                                               select a.TemplateInvoiceName).FirstOrDefault();

//                        var insertHedaer = new TR_BillingPaymentHeader
//                        {

//                            PaymentMethodId = input.PaymentMethodId,
//                            UnitDataId = getUnitDataId.Id,
//                            AmountPayment = item.Amount,
//                            Total = item.Amount,
//                            UnitCode = item.UnitCode,
//                            UnitNo = item.UnitNo,
//                            TransactionDate = item.TransactionDate,
//                            Status = "Success",
//                            PsCode = item.IdClient,


//                        };


//                        _contextBilling.TR_BillingPaymentHeader.Add(insertHedaer);
//                        _contextBilling.SaveChanges();

//                        getBillingPaymentHeaderID = insertHedaer.Id;

//                        if (item.Amount > 0)
//                        {

//                            var insertDetail = new TR_BillingPaymentDetail
//                            {

//                                BillingHeaderId = getBillingPaymentHeaderID,
//                                InvoiceHeaderId = getInvoiceId.Id,
//                                PaymentAmount = item.Amount,
//                            };

//                            _contextBilling.TR_BillingPaymentDetail.Add(insertDetail);
//                            _contextBilling.SaveChanges();

//                            runNumberPdf++;

//                            var getInvoiceNo = (from a in _contextBilling.TR_InvoiceHeader
//                                                where a.Id == getInvoiceId.Id
//                                                select a.InvoiceNo).FirstOrDefault();

//                            var addDesPdf = "<tr><td>" + runNumberPdf + "</td>" +
//                                "<td>" + getInvoiceNo + " - " + " Billing Payment " + getItemRateName + "  " + periodName + "</td> " +
//                                "<td>{{CaraBayar}}</td> " +
//                                "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(item.Amount) + "</td> </tr>";

//                            desctiptionPDf.Add(addDesPdf);

//                        }


//                    }


//                    //cekData sukses 
//                    if (getBillingPaymentHeaderID > 0)
//                    {
//                        var cekDataSukses = (from a in _contextBilling.TR_BillingPaymentDetail
//                                             where a.BillingHeaderId == getBillingPaymentHeaderID && a.InvoiceHeaderId == getInvoiceId.Id && a.PaymentAmount == item.Amount
//                                             select a).FirstOrDefault();

//                        if (cekDataSukses != null)
//                        {
//                            //berhasil
//                            totalsukses++;
//                            #region generate invoiceNo
//                            var getRunningNo = 0;
//                            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
//                                                where a.SiteId == input.SiteId && a.DocCode == "OR" && a.ProjectId == getUnitDataId.ProjectId && a.PeriodId == getInvoiceId.PeriodId
//                                                orderby a.Number descending
//                                                select a).FirstOrDefault();
//                            if (getTrRunning == null)
//                            {

//                                getRunningNo = 1;
//                                var AddRunningNumber = new TR_RunningNumber
//                                {
//                                    DocCode = "OR",
//                                    ProjectId = getUnitDataId.ProjectId,
//                                    Number = getRunningNo,
//                                    SiteId = input.SiteId,
//                                    PeriodId = getInvoiceId.PeriodId

//                                };

//                                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
//                            }
//                            else
//                            {
//                                getRunningNo = getTrRunning.Number + 1;
//                                getTrRunning.Number = getRunningNo;

//                                _contextBilling.TR_RunningNumber.Update(getTrRunning);

//                            }


//                            _contextBilling.SaveChanges();


//                            var getDataSite = (from a in _contextBilling.MS_Site
//                                               where a.SiteId == input.SiteId && a.IsActive == true
//                                               select a).FirstOrDefault();




//                            var AdminBilling = (from x in _contextBilling.Users
//                                                where x.Id == AbpSession.GetUserId()
//                                                select x.Name).FirstOrDefault();




//                            var generateRunningNo = "0000000";
//                            var CountRunningNo = getRunningNo.ToString().Length;
//                            var CountSub = generateRunningNo.Length - CountRunningNo;

//                            var ORNoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;

//                            var ORNoComb = "" + ORNoFinal + "/OR/" + getDataSite.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

//                            #endregion

//                            #region generate Pdf




//                            var DataToPdf = new ParamGenerateOfficialReceiptDto
//                            {

//                                SiteLogo = getDataSite.Logo,
//                                SiteAddres = getDataSite.SiteAddress,
//                                SiteEmail = getDataSite.Email,
//                                SiteName = getDataSite.SiteName,
//                                SitePhone = "Telp. " + getDataSite.OfficePhone + " - " + getDataSite.HandPhone + "",
//                                NoOR = ORNoComb,
//                                PsCode = getInvoiceId.PsCode.ToString(),
//                                CustName = (from a in _contextPersonal.PERSONALS
//                                            where a.psCode == getInvoiceId.PsCode
//                                            select a.name).FirstOrDefault(),
//                                TanggalOR = DateTime.Now.ToString("dd MMMM yyyy"),
//                                CaraBayar = (from a in _contextBilling.MS_PaymentMethod
//                                             where a.Id == input.PaymentMethodId
//                                             select a.PaymentMethodName).FirstOrDefault(),
//                                Deskripsi = string.Join(Environment.NewLine, desctiptionPDf.ToArray()),
//                                TotalPayment = NumberHelper.IndoFormatWithoutTail(item.Amount),
//                                PrintDate = DateTime.Now.ToString("dd MMMM yyyy"),
//                                AdminBilling = AdminBilling,
//                                unitName = getUnitDataId.UnitName,
//                                unitNo = getUnitDataId.UnitNo,

//                            };


//                            var fileName = GenerateOfficialReceiptPdf(DataToPdf);

//                            #endregion


//                            var UpdateOrNo = (from a in _contextBilling.TR_BillingPaymentHeader
//                                              where a.Id == getBillingPaymentHeaderID
//                                              select a).FirstOrDefault();

//                            UpdateOrNo.ReceiptNumber = ORNoComb;

//                            _contextBilling.TR_BillingPaymentHeader.Update(UpdateOrNo);
//                            _contextBilling.SaveChanges();



//                        }

//                    }

//                    #endregion

//                }




//            }

//            //proses generate excel
//            if (totalgagal > 0)
//            {

//                var paramToExcel = new UploadBulkPaymentDto
//                {
//                    DetailUploadBulkPaymentList = gagalDataList

//                };

//                file = _exporter.ExportToExcelErrorDataBulkResult(paramToExcel);

//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                URL = ipPublic + @"\Temp\Downloads\" + file.FileName;

//            }

//            var final = new ResponseBulkPaymentDto
//            {

//                TotalData = totalData,
//                TotalGagal = totalgagal,
//                TotalSukses = totalsukses,
//                urlDataGagal = URL

//            };

//            return final;


//        }



//        public void InsertPaymentBilling(InsertPaymentBillingInputDto input)
//        {

//            var getUnitData = (from a in _contextBilling.MS_UnitData
//                               where a.SiteId == input.siteID && a.UnitCode == input.unitCode && a.UnitNo == input.unitNo
//                               select a).FirstOrDefault();

//            if (getUnitData != null)
//            {


//                #region generate Pdf

//                var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
//                                  join b in _contextBilling.MS_Period on a.PeriodId equals b.Id
//                                  where a.UnitDataId == getUnitData.Id && a.SiteId == input.siteID && b.IsActive == true && a.InvoiceNo == input.InvoiceNumber
//                                  select a).FirstOrDefault();

//                if (getInvoice != null)
//                {

//                    var paramgenerateNo = new GetRunningNumberDocInputDto
//                    {
//                        DocCode = "OR",
//                        ProjectId = getUnitData.ProjectId,
//                        SiteId = getUnitData.SiteId,
//                        PeriodId =getInvoice.PeriodId,
//                    };

//                    var ORNumber = GetRunningNumberDoc(paramgenerateNo);


//                    var getItemRateName = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                           join b in _contextBilling.MS_Item on a.ItemId equals b.Id
//                                           where a.TemplateInvoiceHeaderId == getInvoice.TemplateInvoiceHeaderId
//                                           select b.ItemName).FirstOrDefault();



//                    var getDataSite = (from a in _contextBilling.MS_Site
//                                       where a.SiteId == getUnitData.SiteId && a.IsActive == true
//                                       select a).FirstOrDefault();



//                    var getPeriodName = (from a in _contextBilling.MS_Period
//                                         where a.SiteId == getUnitData.SiteId && a.IsActive == true
//                                         select a).FirstOrDefault();

//                    var periodName = getPeriodName.PeriodMonth.ToString("MMMM") + " " + getPeriodName.PeriodYear.ToString("yyyy");


//                    var getCaraBayar = (from a in _contextBilling.MS_PaymentMethod
//                                        where a.Id == input.PaymentTypeId
//                                        select a.PaymentMethodName).FirstOrDefault();

//                    //var getBankName = (from a in _contextBilling.MS_Bank
//                    //                   where a.Id == input.BankId
//                    //                   select a.BankName).FirstOrDefault();

//                    //var carabayarFinal = getBankName == null ? getCaraBayar : getCaraBayar + " - " + getBankName;

//                    var DataToPdf = new ParamGenerateOfficialReceiptDto
//                    {

//                        SiteLogo = getDataSite.Logo,
//                        SiteAddres = getDataSite.SiteAddress,
//                        SiteEmail = getDataSite.Email,
//                        SiteName = getDataSite.SiteName,
//                        SitePhone = "Telp. " + getDataSite.OfficePhone + " - " + getDataSite.HandPhone + "",
//                        NoOR = ORNumber,
//                        PsCode = getUnitData.PsCode,
//                        CustName = (from a in _contextPersonal.PERSONALS
//                                    where a.psCode == getUnitData.PsCode
//                                    select a.name).FirstOrDefault(),
//                        TanggalOR = DateTime.Now.ToString("dd MMMM yyyy"),
//                        CaraBayar = getCaraBayar,
//                        Deskripsi = "" + getUnitData.UnitName + " - " + getUnitData.UnitNo + " Billing Payment " + getItemRateName + "  " + periodName + "",
//                        TotalPayment = NumberHelper.IndoFormatWithoutTail(input.paymentAmount),
//                        PrintDate = DateTime.Now.ToString("dd MMMM yyyy"),
//                        AdminBilling = "Billing System",


//                    };


//                    var fileName = GenerateOfficialReceiptPdf(DataToPdf);

//                    #endregion



//                    var addHeader = new TR_BillingPaymentHeader
//                    {
//                        PaymentMethodId = (from a in _contextBilling.MS_PaymentMethod
//                                           where a.PaymentMethodName == input.PaymentTypeName
//                                           select a.Id).FirstOrDefault(),
//                        PsCode = input.psCode,
//                        AmountPayment = input.paymentAmount,
//                        TransactionDate = input.paymentDate,
//                        UnitCode = input.unitCode,
//                        UnitNo = input.unitNo,
//                        TransactionCode = input.paymentNumber,
//                        UnitDataId = getUnitData.Id,
//                        ReceiptNumber = ORNumber,
//                        Status = "success",
//                        VA = input.VaNumber,
//                        Total = input.paymentAmount,




//                    };

//                    _contextBilling.TR_BillingPaymentHeader.Add(addHeader);
//                    _contextBilling.SaveChanges();

//                    var getheaderID = addHeader.Id;



//                    var addDetail = new TR_BillingPaymentDetail
//                    {

//                        BillingHeaderId = getheaderID,
//                        PaymentAmount = input.paymentAmount,
//                        InvoiceHeaderId = getInvoice.Id
//                    };

//                    _contextBilling.TR_BillingPaymentDetail.Add(addDetail);
//                    _contextBilling.SaveChanges();


//                }




//            }

//        }

//        public string GetRunningNumberDoc(GetRunningNumberDocInputDto input)
//        {


//            #region generate invoiceNo
//            var getRunningNo = 0;
//            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
//                                where a.SiteId == input.SiteId && a.DocCode == input.DocCode && a.ProjectId == input.ProjectId && a.PeriodId == input.PeriodId
//                                orderby a.Number descending
//                                select a).FirstOrDefault();
//            if (getTrRunning == null)
//            {

//                getRunningNo = 1;
//                var AddRunningNumber = new TR_RunningNumber
//                {
//                    DocCode = input.DocCode,
//                    ProjectId = input.ProjectId,
//                    Number = getRunningNo,
//                    SiteId = input.SiteId,
//                    PeriodId = input.PeriodId,

//                };

//                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
//            }
//            else
//            {
//                getRunningNo = getTrRunning.Number + 1;
//                getTrRunning.Number = getRunningNo;
//                _contextBilling.TR_RunningNumber.Update(getTrRunning);

//            }


//            _contextBilling.SaveChanges();


//            var getDataSite = (from a in _contextBilling.MS_Site
//                               where a.SiteId == input.SiteId && a.IsActive == true
//                               select a).FirstOrDefault();


//            var generateRunningNo = "0000000";
//            var CountRunningNo = getRunningNo.ToString().Length;
//            var CountSub = generateRunningNo.Length - CountRunningNo;

//            var NoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;

//            var NoComb = "" + NoFinal + "/" + input.DocCode + "/" + getDataSite.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

//            #endregion

//            return NoComb;

//        }

//        public async Task SendEmailORPaymentProses(ParamGenerateOfficialReceiptDto input)
//        {

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var webConfigApp = (JObject)appsettingsjson["App"];
//            var EmailEnvironment = webConfigApp.Property("EmailEnvironment").Value.ToString();
//            var EmailTo = webConfigApp.Property("EmailTo").Value.ToString();


//            try
//            {

//                var getEmailUser = (from a in _contextPersonal.PERSONALS
//                                    join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                    where a.psCode == input.PsCode
//                                    select new
//                                    {
//                                        a.psCode,
//                                        a.name,
//                                        b.email
//                                    }).FirstOrDefault();



//                List<string> to = new List<string>();
//                List<string> cc = new List<string>();
//                List<string> bcc = new List<string>();
//                if (EmailEnvironment == "1")// dev {}
//                {
//                    if (!String.IsNullOrEmpty(EmailTo)) //item.EmailAddress
//                        to.AddRange(EmailTo.Split(';'));

//                    if (!String.IsNullOrEmpty(""))
//                        cc.AddRange("".Split(';'));

//                    if (!String.IsNullOrEmpty(""))
//                        bcc.AddRange("".Split(';'));

//                }
//                else
//                {

//                    if (!String.IsNullOrEmpty(getEmailUser.email))
//                        to.AddRange(getEmailUser.email.Split(';'));

//                    if (!String.IsNullOrEmpty(""))
//                        cc.AddRange("".Split(';'));

//                    if (!String.IsNullOrEmpty(""))
//                        bcc.AddRange("".Split(';'));

//                }


//                var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\EmailTemplate\body-email-or.html";
//                var htmlToConvert = File.ReadAllText(templateFile);

//                var bodyEmail = htmlToConvert.Replace("{{PsName}}", input.CustName)
//                    .Replace("{{unitName}}", input.unitName)
//                    .Replace("{{unitNo}}", input.unitNo)
//                    .Replace("{{periodeName}}", input.periodeName)
//                    .Replace("{{sitePhone}}", input.SitePhone)
//                    .Replace("{{siteEmail}}", input.SiteEmail)
//                    .Replace("{{siteAddress}}", input.SiteAddres)
//                    .Replace("{{siteName}}", input.SiteName)




//                    ;

//                var getAttachmentFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\PdfOutput\OR\" + input.fileNamePDF + "";

//                var result = await ServiceHelper.EmailSendAsync(new DynamicEmailInputDto()
//                {
//                    Body = bodyEmail,
//                    Subject = "Official Receipt Unit " + input.unitName + " - " + input.unitNo + "",
//                    IsHtmlBody = true,
//                    Cc = cc,
//                    To = to,
//                    Bcc = bcc,
//                    AttachmentFile = getAttachmentFile

//                });



//                var insertOutbox = new TR_EmailOutbox();
//                // jika sukses kirim email
//                if (result == true)
//                {


//                    var getUpdateIsSend = (from a in _contextBilling.TR_BillingPaymentHeader
//                                           where a.Id == input.BillingPayementheaderId
//                                           select a).FirstOrDefault();



//                    getUpdateIsSend.isSendEmail = true;
//                    getUpdateIsSend.IsSendEmailDate = DateTime.Now;

//                    _contextBilling.TR_BillingPaymentHeader.Update(getUpdateIsSend);
//                    _contextBilling.SaveChanges();



//                    insertOutbox = new TR_EmailOutbox
//                    {
//                        EmailTo = getEmailUser.email,
//                        EmailSubject = "Official Receipt Unit " + input.unitName + " - " + input.unitNo + "",
//                        IsSent = true

//                    };


//                }
//                else
//                {
//                    //jika gagal
//                    insertOutbox = new TR_EmailOutbox
//                    {
//                        EmailTo = getEmailUser.email,
//                        EmailSubject = "Official Receipt Unit " + input.unitName + " - " + input.unitNo + "",
//                        IsSent = false



//                    };

//                }




//                _contextBilling.TR_EmailOutbox.Add(insertOutbox);
//                _contextBilling.SaveChanges();




//            }
//            catch (Exception ex)
//            {
//                //
//            }




//        }





    }
}
