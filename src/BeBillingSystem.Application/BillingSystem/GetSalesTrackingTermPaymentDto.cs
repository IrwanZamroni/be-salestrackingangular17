﻿namespace BeBillingSystem.BillingSystem
{
	public class GetSalesTrackingTermPaymentDto
	{
		public int termID { get; set; }
		public string termRemarks { get; set; }
	}
}