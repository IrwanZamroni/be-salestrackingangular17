﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.UI;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.BillingSystem.TransactionToOracle.Dto;
using BeBillingSystem.DataAccess;
using BeBillingSystem.EntityFrameworkCore;
using Castle.Core.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VDI.Demo.EntityFrameworkCore;
using VDI.Demo.OracleStageDB;

namespace BeBillingSystem.BillingSystem.TransactionToOracle
{
    public class TransactionToOracleAppServices : BeBillingSystemAppServiceBase
    {
        private readonly BeBillingSystemDbContext _beBillingSystemDb;
        private readonly IReportDataExporter _reportDataExporter;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly OracleStageDbContext _oracleStageDbContext;
        private readonly ILogger _logger;

        public TransactionToOracleAppServices(
            BeBillingSystemDbContext beBillingSystemDb,
            IReportDataExporter reportDataExporter,
            IWebHostEnvironment hostEnvironment,
            OracleStageDbContext oracleStageDbContext,
            ILogger logger
            )
        {
            _beBillingSystemDb = beBillingSystemDb;
            _reportDataExporter = reportDataExporter;
            _hostEnvironment = hostEnvironment;
            _oracleStageDbContext = oracleStageDbContext;
            _logger = logger;
            
        }

        public GeneratePaymentJournalDto.res GeneratePaymentJournal(GeneratePaymentJournalDto.req input)
        {
            GeneratePaymentJournalDto.res rtn = new GeneratePaymentJournalDto.res();
            List<GeneratePaymentJournalDto.data> data = new List<GeneratePaymentJournalDto.data>();
            List<GetExistsJournalDto.Res> dataExists = new List<GetExistsJournalDto.Res>();

            DA_TransactionToOracle da = new DA_TransactionToOracle();

            var transaction = _beBillingSystemDb.Database.BeginTransaction();

            try
            {
                //cek data existing
                GetExistsJournalDto.Req dataExistsReq = new GetExistsJournalDto.Req();
                dataExistsReq.bankPayment = input.bankPayment;
                dataExistsReq.paymentEndDate = input.paymentEndDate;
                dataExistsReq.paymentStartDate = input.paymentStartDate;
                dataExistsReq.period = input.period;
                dataExistsReq.siteId = input.siteId;
                dataExistsReq.paymentType = input.paymentType;

                dataExists = da.GetExistsJournal(dataExistsReq);

                if (dataExists.Count > 0 )
                {
                    List<int> jdId = new List<int>();

                    foreach (var dt in dataExists)
                    {
                        jdId.Add(dt.journalDetailId);
                    }

                    //hapus journal header
                    TR_JournalHeader jh = _beBillingSystemDb.TR_JournalHeader
                        .Where(x => x.Id == dataExists[0].journalHeaderId).FirstOrDefault();

                    _beBillingSystemDb.TR_JournalHeader.Remove(jh);

                    //hapus journal detail
                    _beBillingSystemDb.TR_JournalDetail.RemoveRange(_beBillingSystemDb.TR_JournalDetail
                        .Where(c => jdId.Contains(c.Id)));
                }

                data = da.GetGeneratePaymentJournal(input);

                if (data.Count > 0)
                {
                    var dataDistinct = data.GroupBy(n => new 
                        { 
                            n.SiteId, 
                            n.ProjectId, 
                            n.ClusterId,
                            n.TransactionDate.Year,
                            n.TransactionDate.Month,
                            n.TransactionDate.Day
                        })
                        .Select(group => group.First()).ToList();

                    foreach (var item in dataDistinct)
                    {
                        var dataFilter = (from a in data
                                          where a.SiteId == item.SiteId
                                          && a.ProjectId == item.ProjectId
                                          && a.ClusterId == item.ClusterId
                                          && a.TransactionDate.Year == item.TransactionDate.Year
                                          && a.TransactionDate.Month == item.TransactionDate.Month
                                          && a.TransactionDate.Day == item.TransactionDate.Day
                                          select a).ToList();

                        var insertHeader = new TR_JournalHeader
                        {
                            CreationTime = DateTime.Now,
                            CreatorUserId = AbpSession.UserId,
                            SiteId = item.SiteId,
                            ProjectId = item.ProjectId,
                            ClusterId = item.ClusterId
                        };

                        _beBillingSystemDb.TR_JournalHeader.Add(insertHeader);
                        _beBillingSystemDb.SaveChanges();

                        int journalHeaderId = insertHeader.Id;

                        foreach (var itam in dataFilter)
                        {
                            var templateInvoice = (from a in _beBillingSystemDb.MS_TemplateInvoiceHeader
                                                   join b in _beBillingSystemDb.MS_TemplateInvoiceDetail
                                                   on a.Id equals b.TemplateInvoiceHeaderId
                                                   join c in _beBillingSystemDb.MS_ItemRate
                                                   on b.ItemRateId equals c.Id
                                                   where a.Id == itam.TemplateInvoiceHeaderId &&
                                                   a.SiteId == itam.SiteId
                                                   select new
                                                   {
                                                       Id = a.Id,
                                                       TemplateInvoiceName = a.TemplateInvoiceName,
                                                       ItemId = b.ItemId,
                                                       ItemRateId = b.ItemRateId,
                                                       VatPercentage = c.VATPercentage
                                                   }).FirstOrDefault();

                            var mappingOracle = (from a in _beBillingSystemDb.MS_OracleMapping
                                                 where a.SiteId == item.SiteId
                                                 && a.ProjectId == item.ProjectId
                                                 && a.ClusterId == item.ClusterId
                                                 && a.AccountType == "Credit"
                                                 && a.ItemNameTransDesc == itam.ItemName
                                                 select a).FirstOrDefault();

                            if (itam.ItemName == "BPL")
                            {
                                decimal vat = itam.PaymentAmount * (Convert.ToDecimal(templateInvoice.VatPercentage) / 100);
                                decimal pAmount = itam.PaymentAmount - vat;

                                //insert payment
                                var insertPay = new TR_JournalDetail
                                {
                                    COA1 = mappingOracle.COA1,
                                    COA2 = mappingOracle.COA2,
                                    COA3 = mappingOracle.COA3,
                                    COA4 = mappingOracle.COA4,
                                    COA5 = mappingOracle.COA5,
                                    COA6 = mappingOracle.COA6,
                                    COA7 = mappingOracle.COA7,
                                    COACodeFIN = null,
                                    CreationTime = DateTime.Now,
                                    CreatorUserId = AbpSession.UserId,
                                    Debit = 0,
                                    IsDeleted = false,
                                    JournalDate = DateTime.Now,
                                    JournalHeaderId = journalHeaderId,
                                    ReceiptNumber = itam.ReceiptNumber,
                                    IsUploadOracle = false,
                                    InvoiceNumber = itam.InvoiceNo,
                                    Kredit = pAmount,
                                    Remarks = itam.Remarks,
                                    BankId = itam.BankId,
                                    OracleDesc = "A/R Trade Cash on Hand", //itam.PaymentTypeName,
                                    PaymentMethodId = itam.PaymentMethodId,
                                    PeriodId = itam.PeriodId,
                                    AccountingDate = DateTime.Now,
                                    PaymentDate = itam.TransactionDate,
                                    BillingPaymentHeaderId = itam.BillingPaymentHeaderId,
                                    BillingPaymentDetailId = itam.BillingPaymentDetailId,
                                    InvoiceHeaderId = itam.InvoiceHeaderId,
                                    InvoiceDetailId = itam.InvoiceDetailId,
                                    InvoiceItemId = itam.InvoiceItemId
                                };

                                _beBillingSystemDb.TR_JournalDetail.Add(insertPay);
                                _beBillingSystemDb.SaveChanges();

                                //insert vat 
                                //cari yg vat di oraclemapping
                                var mappingOraclePPN = (from a in _beBillingSystemDb.MS_OracleMapping
                                                     where a.SiteId == item.SiteId
                                                     && a.ProjectId == item.ProjectId
                                                     && a.ClusterId == item.ClusterId
                                                     && a.AccountType == "Credit"
                                                     && a.ItemNameTransDesc == "PPN BPL"
                                                     select a).FirstOrDefault();
                                var insertVat = new TR_JournalDetail
                                {
                                    COA1 = mappingOraclePPN.COA1,
                                    COA2 = mappingOraclePPN.COA2,
                                    COA3 = mappingOraclePPN.COA3,
                                    COA4 = mappingOraclePPN.COA4,
                                    COA5 = mappingOraclePPN.COA5,
                                    COA6 = mappingOraclePPN.COA6,
                                    COA7 = mappingOraclePPN.COA7,
                                    COACodeFIN = null,
                                    CreationTime = DateTime.Now,
                                    CreatorUserId = AbpSession.UserId,
                                    Debit = 0,
                                    IsDeleted = false,
                                    JournalDate = DateTime.Now,
                                    JournalHeaderId = journalHeaderId,
                                    ReceiptNumber = itam.ReceiptNumber,
                                    IsUploadOracle = false,
                                    InvoiceNumber = itam.InvoiceNo,
                                    Kredit = vat,
                                    Remarks = itam.Remarks,
                                    BankId = itam.BankId,
                                    OracleDesc = "A/R Trade Cash on Hand", //itam.PaymentTypeName,
                                    PaymentMethodId = itam.PaymentMethodId,
                                    PeriodId = itam.PeriodId,
                                    AccountingDate = DateTime.Now,
                                    PaymentDate = itam.TransactionDate,
                                    BillingPaymentHeaderId = itam.BillingPaymentHeaderId,
                                    BillingPaymentDetailId = itam.BillingPaymentDetailId,
                                    InvoiceHeaderId = itam.InvoiceHeaderId,
                                    InvoiceDetailId = itam.InvoiceDetailId,
                                    InvoiceItemId = itam.InvoiceItemId
                                };

                                _beBillingSystemDb.TR_JournalDetail.Add(insertVat);
                                _beBillingSystemDb.SaveChanges();

                            }
                            else
                            {
                                var insertDetail = new TR_JournalDetail
                                {
                                    COA1 = mappingOracle.COA1,
                                    COA2 = mappingOracle.COA2,
                                    COA3 = mappingOracle.COA3,
                                    COA4 = mappingOracle.COA4,
                                    COA5 = mappingOracle.COA5,
                                    COA6 = mappingOracle.COA6,
                                    COA7 = mappingOracle.COA7,
                                    COACodeFIN = null,
                                    CreationTime = DateTime.Now,
                                    CreatorUserId = AbpSession.UserId,
                                    Debit = 0,
                                    IsDeleted = false,
                                    JournalDate = DateTime.Now,
                                    JournalHeaderId = journalHeaderId,
                                    ReceiptNumber = itam.ReceiptNumber,
                                    IsUploadOracle = false,
                                    InvoiceNumber = itam.InvoiceNo,
                                    Kredit = itam.PaymentAmount,
                                    Remarks = itam.Remarks,
                                    BankId = itam.BankId,
                                    OracleDesc = "A/R Trade Cash on Hand", //itam.PaymentTypeName,
                                    PaymentMethodId = itam.PaymentMethodId,
                                    PeriodId = itam.PeriodId,
                                    AccountingDate = DateTime.Now,
                                    PaymentDate = itam.TransactionDate,
                                    BillingPaymentHeaderId = itam.BillingPaymentHeaderId,
                                    BillingPaymentDetailId = itam.BillingPaymentDetailId,
                                    InvoiceHeaderId = itam.InvoiceHeaderId,
                                    InvoiceDetailId = itam.InvoiceDetailId,
                                    InvoiceItemId = itam.InvoiceItemId
                                };

                                _beBillingSystemDb.TR_JournalDetail.Add(insertDetail);
                                _beBillingSystemDb.SaveChanges();
                            }

                        }

                        var mappingOracleDebet = (from a in _beBillingSystemDb.MS_OracleMapping
                                                  where a.SiteId == item.SiteId
                                                  && a.ProjectId == item.ProjectId
                                                  && a.ClusterId == item.ClusterId
                                                  && a.AccountType == "Debet"
                                                  select a).FirstOrDefault();

                        decimal sumDataFilter = dataFilter.Select(t => t.PaymentAmount).Sum();

                        var insertDetail2 = new TR_JournalDetail
                        {
                            COA1 = mappingOracleDebet.COA1,
                            COA2 = mappingOracleDebet.COA2,
                            COA3 = mappingOracleDebet.COA3,
                            COA4 = mappingOracleDebet.COA4,
                            COA5 = mappingOracleDebet.COA5,
                            COA6 = mappingOracleDebet.COA6,
                            COA7 = mappingOracleDebet.COA7,
                            COACodeFIN = null,
                            CreationTime = DateTime.Now,
                            CreatorUserId = AbpSession.UserId,
                            Debit = sumDataFilter,
                            IsDeleted = false,
                            JournalDate = DateTime.Now,
                            JournalHeaderId = journalHeaderId,
                            ReceiptNumber = "", //item.ReceiptNumber,
                            InvoiceNumber = "", //item.InvoiceNo,
                            IsUploadOracle = false,
                            Kredit = 0,
                            Remarks = item.Remarks,
                            BankId = item.BankId,
                            OracleDesc = "Cash on Hand Cash on Hand",
                            PaymentMethodId = item.PaymentMethodId,
                            PeriodId = item.PeriodId,
                            AccountingDate = DateTime.Now,
                            PaymentDate = item.TransactionDate,
                            BillingPaymentHeaderId = item.BillingPaymentHeaderId,
                            BillingPaymentDetailId = item.BillingPaymentDetailId,
                            InvoiceHeaderId = item.InvoiceHeaderId,
                            InvoiceDetailId = item.InvoiceDetailId,
                            InvoiceItemId = item.InvoiceItemId
                        };

                        _beBillingSystemDb.TR_JournalDetail.Add(insertDetail2);
                        _beBillingSystemDb.SaveChanges();

                    }
                }

                transaction.Commit();

                rtn.status = "success";
                rtn.errorMessage = "";
                rtn.data = data;
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                rtn.status = "error";
                rtn.errorMessage = "error while geting data : " + ex.Message;
                rtn.data = null;
            }

            return rtn;
        }
        public async Task<ReportUriDto> ExportToExcelJournalToOracle(GeneratePaymentJournalDto.req input)
        {
            FileDto file = new FileDto();

            try
            {
                DA_TransactionToOracle da = new DA_TransactionToOracle();
                List<GetBillingJournalTaskListDto> data = new List<GetBillingJournalTaskListDto>();

                data = da.GetExportToExcelJournalToOracle(input);

                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var connectionApp = (JObject)appsettingsjson["App"];
                var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

                _logger.Info("Data to export : " + data.Count().ToString());

                if (data.Count > 0)
                {
                    file = _reportDataExporter.ExportToExcelJournalToOracle(data);

                    System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
                    var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


                    var filePath = Path.Combine(tempRootPath);

                    _logger.Info("file created at : " + filePath);

                    if (!File.Exists(filePath))
                    {
                        _logger.Error("file failed to create");
                        throw new Exception("Requested File doesn't exists");
                    }

                    var pathExport = Path.Combine(tempRootPath);
                    //retrieve data excel from temporary download folder
                    var fileBytes = File.ReadAllBytes(filePath);
                    //write excel file to share folder / local folder
                    File.WriteAllBytes(pathExport, fileBytes);
                    //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
                    string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
                    return new ReportUriDto { Uri = URL.Replace("\\", "/") };
                }
                else
                {

                    return new ReportUriDto { };
                }

            }
            catch (Exception ex)
            {
                return new ReportUriDto { };
            }
        }
        public PagedResultDto<GetBillingJournalTaskListDto> FetchBillingJournalTaskList(GeneratePaymentJournalDto.req input)
        {
            List<GetBillingJournalTaskListDto> res = new List<GetBillingJournalTaskListDto>();
            DA_TransactionToOracle da = new DA_TransactionToOracle();
            var dataCount = 0;
            try
            {
                res = da.GetFetchBillingJournalTaskList(input);
                dataCount = res.Count();
            }
            catch (Exception ex)
            {
                res = null;
            }

            return new PagedResultDto<GetBillingJournalTaskListDto>(dataCount, res.ToList());
        }
        public PagedResultDto<ExportToExcelJournalToOracleDto> FetchJournalOracleList(GeneratePaymentJournalDto.reqlist input)
        {
            List<ExportToExcelJournalToOracleDto> data = new List<ExportToExcelJournalToOracleDto>();
            DA_TransactionToOracle da = new DA_TransactionToOracle();
            PagedResultDto<ExportToExcelJournalToOracleDto> res = new PagedResultDto<ExportToExcelJournalToOracleDto>();
            try
            {
                data = da.GetFecthJournalOracleList(input);

                if(data.Count > 0)
                {
                    var dataCount = data.Count();

                    res = new PagedResultDto<ExportToExcelJournalToOracleDto>(dataCount, data);
                }
            }
            catch (Exception ex)
            {
                res = new PagedResultDto<ExportToExcelJournalToOracleDto>(0, null);
                throw;
            }

            return res;
        }
        public UploadToOracleResponse UploadJournalToOracle(GeneratePaymentJournalDto.req input)
        {
            UploadToOracleResponse res = new UploadToOracleResponse();
            List<ExportToExcelJournalToOracleDto> data = new List<ExportToExcelJournalToOracleDto>();
            DA_TransactionToOracle da = new DA_TransactionToOracle();

            try
            {
                data = da.GetFecthJournalOracleListWithnoPage(input);

                if (data.Count > 0)
                {
                    InsertGLInterfaceUniversal(data);

                    res.status = "success";
                    res.errorMessage = "";
                }
                else
                {
                    res.status = "success";
                    res.errorMessage = "there is no row from filtered journal data";
                }
            }
            catch (Exception ex)
            {
                res.status = "failed";
                res.errorMessage = ex.Message;
            }

            return res;
        }
        private void InsertGLInterfaceUniversal(List<ExportToExcelJournalToOracleDto> input)
        {
            try
            {
                //int groupID = GenerateGroupID().Result;
                var getSingleCluster = input.GroupBy(x => new
                {
                    x.SiteId,
                    x.PaymentDate.Year,
                    x.PaymentDate.Month,
                    x.PaymentDate.Day
                }).Select(x => x.First()).ToList();

                var getMaxGroupID = (from a in _oracleStageDbContext.LK_GL_INTERFACE
                                     orderby a.GROUP_ID descending
                                     select a.GROUP_ID).First();

                int maxGroupID = Convert.ToInt32(getMaxGroupID);

                int row = 0;

                foreach (var item in getSingleCluster)
                {
                    row = ++row;
                    int xMaxGroupID = maxGroupID + row;
                    foreach (var dt in input.Where(w =>  w.SiteId == item.SiteId && 
                    w.PaymentDate.ToString("yyyy-MM-dd") == item.PaymentDate.ToString("yyyy-MM-dd")))
                    {
                        dt.GroupId = xMaxGroupID.ToString();
                    }
                }

                List<LK_GL_INTERFACE> listGL = new List<LK_GL_INTERFACE>();

                foreach (var itm in input)
                {
                    listGL.Add(new LK_GL_INTERFACE
                    {
                        STATUS = "NEW",
                        SET_OF_BOOKS_ID = 1004,
                        ACCOUNTING_DATE = itm.AccountingDate.ToString("dd/MM/yyyy"),
                        CURRENCY_CODE = "IDR",
                        DATE_CREATED = DateTime.Now.ToString("dd/MM/yyyy"),
                        CREATED_BY = -1,
                        ACTUAL_FLAG = "A",
                        USER_JE_CATEGORY_NAME = itm.COA1 + "-GJV",
                        USER_JE_SOURCE_NAME = "SAD",
                        SEGMENT1 = itm.COA1,
                        SEGMENT2 = itm.COA2,
                        SEGMENT3 = itm.COA3,
                        SEGMENT4 = itm.COA4,
                        SEGMENT5 = itm.COA5,
                        SEGMENT6 = itm.COA6,
                        SEGMENT7 = itm.COA7,
                        ENTERED_DR = Math.Round(itm.Debit, 2, MidpointRounding.ToEven),
                        ENTERED_CR = Math.Round(itm.Kredit, 2, MidpointRounding.ToEven),
                        TRANSACTION_DATE = DateTime.Now.ToString("dd/MM/yyyy"),
                        GROUP_ID = itm.GroupId,
                        ACC_CODE = "",//input.account.accCode == null ? null : input.account.accCode,
                        INPUT_UN = AbpSession.UserId.ToString(), //"admin",
                        INPUT_TIME = DateTime.Now
                    });
                }

                //string flag = SaveToCCL(dataInsertInterface).Result;

                _oracleStageDbContext.LK_GL_INTERFACE.AddRange(listGL);
                _oracleStageDbContext.SaveChanges();

                foreach (var upd in input)
                {
                    TR_JournalDetail jdUpd = (from a in _beBillingSystemDb.TR_JournalDetail
                                              where a.Id == upd.JournalDetailId
                                              select a).SingleOrDefault();

                    jdUpd.GroupId = upd.GroupId;
                    jdUpd.LastModificationTime = DateTime.Now;
                    jdUpd.LastModifierUserId = AbpSession.UserId;
                    _beBillingSystemDb.SaveChanges();
                }
            }
            catch (DataException ex)
            {
                Logger.ErrorFormat("ClosingAccountUniversal() - ERROR DataException. Result = {0}", ex.Message);
                throw new UserFriendlyException("Db Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("ClosingAccountUniversal() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error: " + ex.Message);
            }
        }
        private async Task<string> SaveToCCL(LK_GL_INTERFACE input)
        {
            string flag = "";
            RootDto result = new RootDto();
            OraclestageInputDto Model = new OraclestageInputDto
            {
                STATUS = input.STATUS,
                SET_OF_BOOKS_ID = input.SET_OF_BOOKS_ID,
                ACCOUNTING_DATE = input.ACCOUNTING_DATE,
                CURRENCY_CODE = input.CURRENCY_CODE,
                DATE_CREATED = input.DATE_CREATED,
                CREATED_BY = input.CREATED_BY,
                ACTUAL_FLAG = input.ACTUAL_FLAG,
                USER_JE_CATEGORY_NAME = input.USER_JE_CATEGORY_NAME,
                USER_JE_SOURCE_NAME = input.USER_JE_SOURCE_NAME,
                SEGMENT1 = input.SEGMENT1,
                SEGMENT2 = input.SEGMENT2,
                SEGMENT3 = input.SEGMENT3,
                SEGMENT4 = input.SEGMENT4,
                SEGMENT5 = input.SEGMENT5,
                SEGMENT6 = input.SEGMENT6,
                SEGMENT7 = input.SEGMENT7,
                ENTERED_DR = input.ENTERED_DR,
                ENTERED_CR = input.ENTERED_CR,
                TRANSACTION_DATE = input.TRANSACTION_DATE,
                GROUP_ID = input.GROUP_ID,
                ACC_CODE = input.ACC_CODE,
                INPUT_UN = input.INPUT_UN,
                INPUT_TIME = input.INPUT_TIME
            };

            using (var client = new HttpClient())
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var getAppSet = (JObject)appsettingsjson["App"];
                var fmApiSet = getAppSet.Property("fmApi").Value.ToString();

                string fmApi = fmApiSet;

                string URL = fmApi.EnsureEndsWith('/') + "api/services/app/Oraclestage/SaveOraclestage";

                var content = JsonConvert.SerializeObject(Model);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);

                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                client.BaseAddress = new Uri(URL);
                client.Timeout = TimeSpan.FromMinutes(10);

                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3;

                var response = await client.PostAsync("", byteContent);
                var jsonString = await response.Content.ReadAsStringAsync();
                var responseData = JsonConvert.DeserializeObject(jsonString);
            }

            return flag;
        }
        private async Task<int> GenerateGroupID()
        {
            int groupID = 0;
            RootDto result = new RootDto();

            using (var client = new HttpClient())
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var getAppSet = (JObject)appsettingsjson["App"];
                var fmApiSet = getAppSet.Property("fmApi").Value.ToString();

                string fmApi = fmApiSet;
                string URL = fmApi.EnsureEndsWith('/') + "api/services/app/Oraclestage/GenerateGroupID";

                client.BaseAddress = new Uri(URL);
                client.Timeout = TimeSpan.FromMinutes(10);

                var response = await client.PostAsync("", null);
                var jsonString = await response.Content.ReadAsStringAsync();
                var responseData = JsonConvert.DeserializeObject(jsonString);

                result = JsonConvert.DeserializeObject<RootDto>(jsonString);

                groupID = result.result;
                return groupID;
            }

        }
        //private string GetIdUsername(long? Id)
        //{
        //    string getUsername = (from u in _demoDbContext.Users
        //                          where u.Id == Id
        //                          select u.UserName)
        //               .DefaultIfEmpty("").First();

        //    return getUsername;
        //}

    }
}
