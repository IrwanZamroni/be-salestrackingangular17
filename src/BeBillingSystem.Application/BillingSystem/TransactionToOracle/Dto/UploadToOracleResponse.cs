﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class UploadToOracleResponse
    {
        public string status { get; set; }
        public string errorMessage { get; set; }
    }
}
