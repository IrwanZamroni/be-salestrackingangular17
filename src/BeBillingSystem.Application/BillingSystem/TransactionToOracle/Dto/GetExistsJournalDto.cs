﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class GetExistsJournalDto
    {
        public class Req
        {
            public int siteId { get; set; }
            public int period { get; set; }
            public int paymentType { get; set; }
            public int bankPayment { get; set; }
            public DateTime paymentStartDate { get; set; }
            public DateTime paymentEndDate { get; set; }
        }
        public class Res
        {
            public int journalHeaderId { get; set; }
            public int journalDetailId { get; set; }
        }
    }
}
