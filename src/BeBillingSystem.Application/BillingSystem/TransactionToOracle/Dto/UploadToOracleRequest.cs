﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class UploadToOracleRequest
    {
        public int siteId { get; set; }
        public int period { get; set; }
        public int paymentType { get; set; }
        public DateTime accountingDate { get; set; }
        public int bankPayment { get; set; }
        public DateTime paymentStartDate { get; set; }
        public DateTime paymentEndDate { get; set; }
    }
}
