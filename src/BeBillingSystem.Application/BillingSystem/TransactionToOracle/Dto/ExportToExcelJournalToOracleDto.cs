﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class ExportToExcelJournalToOracleDto
    {
        public int JournalHeaderId { get; set; }
		public int JournalDetailId { get; set; }
		public int SiteId { get; set; }
		public int ProjectId { get; set; }
		public int ClusterId { get; set; }
		public DateTime JournalDate { get; set; }
		public int PeriodId { get; set; }
		public string OracleDesc { get; set; }
		public DateTime AccountingDate { get; set; }
		public string COA1 { get; set; }
		public string COA2 { get; set; }
		public string COA3 { get; set; }
		public string COA4 { get; set; }
		public string COA5 { get; set; }
		public string COA6 { get; set; }
		public string COA7 { get; set; }
		public decimal Debit { get; set; }
		public decimal Kredit { get; set; }
		public bool IsTransfered { get; set; }
		public string GroupId { get; set; }
		public string PeriodName { get; set; }
		public DateTime PaymentDate { get; set; }
		public int BankId { get; set; }
		public string BankName { get; set; }
    }
}
