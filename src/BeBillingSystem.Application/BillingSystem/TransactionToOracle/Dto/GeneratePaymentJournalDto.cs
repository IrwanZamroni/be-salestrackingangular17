﻿using BeBillingSystem.BillingSystem.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class GeneratePaymentJournalDto
    {
        public class req
        {
            public int siteId { get; set; }
            public int period { get; set; }
            public int paymentType { get; set; }
            public DateTime accountingDate { get; set; }
            public int bankPayment { get; set; }
            public DateTime paymentStartDate { get; set; }
            public DateTime paymentEndDate { get; set; }
        }

		public class res
		{
			public string status { get; set; }
			public string errorMessage { get; set; }
			public List<data> data { get; set; }
		}
        public class data
        {
            public int SiteId { get; set; }
			public int ProjectId { get; set; }
			public int ClusterId { get; set; }
			public int UnitId { get; set; }
			public int BillingPaymentHeaderId { get; set; }
            public string UnitCode { get; set; }
			public string UnitNo { get; set; }
			public string PsCode { get; set; }
			public int PaymentMethodId { get; set; }
			public string CardNumber { get; set; }
			public DateTime TransactionDate { get; set; }
			public int BankId { get; set; }
			public decimal AmountPayment { get; set; }
			public decimal Charge { get; set; }
			public decimal Total { get; set; }
			public string ReceiptNumber { get; set; }
			public bool CancelPayment { get; set; }
			public bool IsUploadBulk { get; set; }
			public DateTime CreationTime { get; set; }
			public string Status { get; set; }
			public string TransactionCode { get; set; }
			public string VA { get; set; }
			public string Remarks { get; set; }
			public int BillingPaymentDetailId { get; set; }
			public int InvoiceHeaderId { get; set; }
			public decimal PaymentAmount { get; set; }
			public string InvoiceNo { get; set; }
			public int PeriodId { get; set; }
			public int TemplateInvoiceHeaderId { get; set; }
			public decimal TagihanPerbulan { get; set; }
			public decimal JumlahYangHarusDibayar { get; set; }
			public DateTime InvoiceDate { get; set; }
			public DateTime JatuhTempo { get; set; }
			public decimal AdjustmentNominal { get; set; }
			public decimal PenaltyNominal { get; set; }
			public decimal TagihanBulanSebelumnya { get; set; }
			public string PaymentMethodName { get; set; }
			public DateTime PeriodStartDate { get; set; }
			public DateTime PeriodEndDate { get; set; }
			public DateTime PeriodCloseDate { get; set; }
			public DateTime PeriodYear { get; set; }
			public string BankName { get; set; }
			public int PaymentType { get; set; }
			public string PaymentTypeName { get; set; }
			public int InvoiceDetailId { get; set; }
			public int InvoiceItemId { get; set; }
			public string ItemName { get; set; }
        }
		public class reqlist : PagedInputDto
        {
            public int siteId { get; set; }
            public int period { get; set; }
            public int paymentType { get; set; }
            public DateTime accountingDate { get; set; }
            public int bankPayment { get; set; }
            public DateTime paymentStartDate { get; set; }
            public DateTime paymentEndDate { get; set; }
        }

    }
}
