﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.TransactionToOracle.Dto
{
    public class GetBillingJournalTaskListDto
    {
		public string ProjectName { get; set; }
		public string ClusterName { get; set; }
		public int UnitId { get; set; }
		public string UnitCode { get; set; }
		public string InvoiceNo { get; set; }
		public string ReceiptNumber { get; set; }
		public string PsCode { get; set; }
		public DateTime BillingDate { get; set; }
		public DateTime InvoiceDate { get; set; }
		public int PeriodId { get; set; }
		public string Remarks { get; set; }
		public decimal BillingPaymentAmount { get; set; }
		public int BillingHeaderId { get; set; }
		public int BillingDetailId { get; set; }
		public int InvoiceHeaderId { get; set; }
		public int InvoiceDetailId { get; set; }
		public int InvoiceItemId { get; set; }
		public int ProjectId { get; set; }
		public int ClusterId { get; set; }
		public decimal Debit { get; set; }
		public decimal Kredit { get; set; }
		public string COA1 { get; set; }
        public string COA2 { get; set; }
        public string COA3 { get; set; }
        public string COA4 { get; set; }
        public string COA5 { get; set; }
        public string COA6 { get; set; }
        public string COA7 { get; set; }
        public string OracleDesc { get; set; }
		public string GroupId { get; set; }
		public string PeriodName { get; set; }
    }
}
