﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class MasterUserDto
    {
    }

    public class UserDetailDto
    {
        public string userName { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string emailAddress { get; set; }
        public bool isActive { get; set; }
        public string phoneNumber { get; set; }
        public object photoProfile { get; set; }
        public string fullName { get; set; }
        public int id { get; set; }
        public object lastLoginTime { get; set; }
        public DateTime creationTime { get; set; }
        public List<int> siteId { get; set; }
        public List<string> roleName { get; set; }
     
    }

}
