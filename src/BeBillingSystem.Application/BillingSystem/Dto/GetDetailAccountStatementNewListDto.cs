﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class GetDetailAccountStatementNewListDto
    {
        public List<GetDetailAccountStatementListDto> ListAccountStatement { get; set; }
    }

    public class GetDetailAccountStatementListDto
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public List<GetPeriode> periode { get; set; }
    }

    public class GetPeriode
    {
        public int periodeNo { get; set; }
        public decimal totalPreviousBalance { get; set; }
        public decimal totalCurrentTransaction { get; set; }
        public decimal totalPayment { get; set; }
        public decimal totalBillingOutstanding { get; set; }
        public List<GetUnit> unit { get; set; }
    }

    public class GetUnit
    {
        public string periodeName { get; set; }
        public string periodeDate { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string virtualAccount { get; set; }
        public decimal previousBalance { get; set; }
        public decimal currentTransaction { get; set; }
        public decimal payment { get; set; }
        public decimal billingOutstanding { get; set; }
        public List<GetDetail> detail { get; set; }
    }

    public class GetDetail
    {
        public string transactionDate { get; set; }
        public string transactionDesc { get; set; }
        public decimal transactionAmount { get; set; }
        public decimal totalPayment { get; set; }
    }
}
