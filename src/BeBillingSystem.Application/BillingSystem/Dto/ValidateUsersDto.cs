﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class ValidateUsersDto
    {
    }


    public class DataMainValidate
    {
        public string Name { get; set; }
        public string PSCode { get; set; }
        public string BirthDate { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string Sex { get; set; }
        public string KTP { get; set; }
        public string NPWP { get; set; }
        public List<UnitValidate> unit { get; set; }
    }

  
    public class DataValidate
    {
        public List<DataMainValidate> data { get; set; }
    }

    public class UnitValidate
    {
        public int OrgID { get; set; }
        public int SiteID { get; set; }
        public string PSCode { get; set; }
        public string UnitDesc { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string Unit { get; set; }
        public string UnitClusterDesc { get; set; }
        public string UnitClusterCode { get; set; }
        public int SubLocationID { get; set; }
        public int LocationID { get; set; }
    }


    public class InputGetInvoiceDto
    {
        public string psCode { get; set; }
        public int siteID { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
    }

    public class ValidateLoginDto 
    {
        public string UserOrEmail { get; set; }
     
    }

 

    //====================================================================
    public class DataGetDataTotalOutstandingDto
    {
        public string psCode { get; set; }
        public string name { get; set; }
        public int siteID { get; set; }
        public List<UnitGetDataTotalOutstandingDto> unit { get; set; }
    }

    public class Detail
    {
        public string transactionDate { get; set; }
        public string transactionCategoryDesc { get; set; }
        public string transactionDesc { get; set; }
        public string transactionAmount { get; set; }
    }

    public class PaymnetSisaTagiahanDto {
        public decimal Payment { get; set; }
        public decimal sisaTagihan { get; set; }

    }

    public class InvoiceDetailGetDataTotalOutstandingDto
    {
        public decimal previousBalance { get; set; }
        public decimal currentTransaction { get; set; }
        public decimal payment { get; set; }
        public decimal billingOutstanding { get; set; }
        public string vaNo { get; set; }
        public bool isPayment { get; set; }
        public string invoiceNumber { get; set; }
        public string invoiceUrl { get; set; }
        public List<Detail> detail { get; set; }
    }

    public class PeriodeGetDataTotalOutstandingDto
    {
        public int no { get; set; }
        public string name { get; set; }
        public DateTime dueDate { get; set; }
        public List<InvoiceDetailGetDataTotalOutstandingDto> invoiceDetail { get; set; }
    }

    public class RootGetDataTotalOutstandingDto
    {
        public DataGetDataTotalOutstandingDto data { get; set; }
    }

    public class UnitGetDataTotalOutstandingDto
    {
        public string unitCode { get; set; }
        public string unitName { get; set; }
        public string unitNo { get; set; }
        public List<PeriodeGetDataTotalOutstandingDto> periode { get; set; }
    }
    


    //=========================================================================================================


    public class GetTotalDataOutDto
    {
        public string UserOrEmail { get; set; }
        public int SiteId { get; set; }

    }
    public class GetTotalOutstandingResponseDto
    {
        public decimal Amount { get; set; }
        public string PsCode { get; set; }

    }
}