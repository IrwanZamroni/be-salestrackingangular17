﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class Report
    {
    }
    public class FileDto
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FileType { get; set; }

        [Required]
        public string FileToken { get; set; }

        public FileDto()
        {

        }

        public FileDto(string fileName, string fileType)
        {
            FileName = fileName;
            FileType = fileType;
            FileToken = Guid.NewGuid().ToString("N");
        }
    }
    public class ReportInvoiceInputDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public List<int> ClusterId { get; set; }
        public int PeriodId { get; set; }
        public int ReportType { get; set; }

    }

    public class ViewReportinvoiceDto
    {
        public string printBy { get; set; }
        public string periodName { get; set; }
        public List<ReportInvoiceListDto> ReportInvoiceListDto { get; set; }

    }

    public class ReportInvoiceListDto
    {

        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public String ProjectName { get; set; }
        public String ClusterName { get; set; }
        public int PeriodId { get; set; }
        public String UnitCode { get; set; }
        public String UnitNo { get; set; }
        public String PsCode { get; set; }
        public String PsName { get; set; }
        public String BillAddress { get; set; }
        public String BillCity { get; set; }
        public String psPhone { get; set; }
        public int LandArea { get; set; }
        public int BuildArea { get; set; }
        public int PrevBalance { get; set; }
        public int CurrTrans { get; set; }
        public int Penalty { get; set; }
        public int AdjustmentAmount { get; set; }
        public int Payment { get; set; }
        public decimal EndBalance { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceName { get; set; }
        public string ItemName { get; set; }
        public string Amount { get; set; }
    }

    public class ReportDailyInputDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public List<int> ClusterId { get; set; }
        public int PeriodId { get; set; }
        public int PaymentTypeId { get; set; }
        public int CancelPayment { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

    }

    public class ReportDetailStatementDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }

        public int StartMonth { get; set; }
        public int EndMonth { get; set; }
        public List<string> PeriodYear { get; set; }

    }

    public class ReportPDFDetailStatementDto
    {

        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }

        public int StartMonth { get; set; }
        public int EndMonth { get; set; }
        public List<string> PeriodYear { get; set; }

    }

    public class ReportSummaryStatmentDto
    {
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }

        public string ProjectName { get; set; }
        public string ClusterName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string PeriodMonth { get; set; }
        public int periodmonthangka { get; set; }
        public string PeriodYear { get; set; }
        public string InvoiceNo { get; set; }
        public int tagihaninvoice { get; set; }
        public int PaymentAmount { get; set; }
        public int outstanding { get; set; }
    }


    public class ReportDetailStatmentDto
    {
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }

        public string ProjectName { get; set; }
        public string ClusterName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string PeriodMonth { get; set; }
        public int periodmonthangka { get; set; }
        public string PeriodYear { get; set; }
        public string InvoiceNo { get; set; }
        public string customer { get; set; }
        public int BPL { get; set; }
        public int PpnBPL { get; set; }
        public int ADJBPL { get; set; }
        public int TagihanAir { get; set; }
        public int TagihanBulanSebelumnya { get; set; }
        public int Penalty { get; set; }
        public int CurrTrans { get; set; }
        public int Payment { get; set; }

    }





    public class ViewReportDetailStetmentDto
    {
        public string printBy { get; set; }
        public string periodName { get; set; }
        public List<ReportSummaryStatmentDto> ReportSummaryStatmentList { get; set; }
        public List<ReportDetailStatmentDto> ReportDetailStatmentList { get; set; }

    }



    public class ViewReportDailyDto
    {
        public string printBy { get; set; }
        public string periodName { get; set; }
        public List<ReportDailyListDto> ReportDailyListDto { get; set; }

    }

    public class ReportDailyListDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string ProjectName { get; set; }
        public string ClusterName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string InvoiceNo { get; set; }
        public string TemplateInvoiceName { get; set; }
        public string ReceiptNumber { get; set; }
        public string PaymentTypeName { get; set; }
        public string BankName { get; set; }
        public string TransactionDate { get; set; }
        public int PaymentAmount { get; set; }
        public int Charge { get; set; }
        public int net { get; set; }
        public string CancelDate { get; set; }
        public string PsCode { get; set; }
        public string name { get; set; }
        public string NPWP { get; set; }
        public string VirtualAccNo { get; set; }
        public string KTP { get; set; }
        public bool CancelPayment { get; set; }
        public int PaymentMethodId { get; set; }


    }

    public class ParamPDFReportDetailStatment
    {
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string CustName { get; set; }
        public string PsCode { get; set; }
        public string CustAlamatKoresponden { get; set; }
        public string IsiKonten { get; set; }
    }
    public class GetListReportDetailStatment
    {
        public int InvoiceHeaderID { get; set; }
        public int UnitDataId { get; set; }
        public int TemplateInvoiceHeaderId { get; set; }
        public int PeriodId { get; set; }

        public bool IsPenalty { get; set; }
        public decimal PenaltyNominal { get; set; }
        public string UnitNo { get; set; }
        public string PsCode { get; set; }
        public string UnitCode { get; set; }
        public string Periode { get; set; }
        public string PeriodeYear { get; set; }
        public string BulanJatuhTempo { get; set; }
        public string InvoiceNo { get; set; }

        public decimal TagihanBulanSebelumnya { get; set; }
        public decimal TagihanBulanIni { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal EndingBalance { get; set; }
        public int periodmonthangka { get; set; }
    }

}
