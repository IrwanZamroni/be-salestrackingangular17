﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class HelperDto
    {
    }
    public class LinkPathListDto
    {
        public string filename { get; set; }
        public string linkFile { get; set; }
        public string linkServerFile { get; set; }
        public string filePhysicalPath { get; set; }
    }
}
