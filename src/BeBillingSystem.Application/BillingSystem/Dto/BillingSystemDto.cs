﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class BillingSystemDto
    {
    }


    public class DropdownProjectDto
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
    }
    public class DropdownClusterDto
    {
        public int ClusterId { get; set; }
        public string ClusterName { get; set; }
        public string ClusterCode { get; set; }
        public int ProjectId { get; set; }
        public string ProjectCode { get; set; }
    }

    public class DropdownUnitDto
    {
        public int UnitId { get; set; }
        public string UnitNo { get; set; }
    }
    public class DropdownInvoiceNameDto
    {
        public int TemplateHeaderId { get; set; }
        public string InvoiceName { get; set; }
    }
    public class DropdownUnitCodeDto
    {
        public int UnitCodeId { get; set; }
        public string UnitCode { get; set; }
    }
    public class GetDropdownPeriodMonthDto
    {
        public int NumberMonth { get; set; }
        public string PeriodMonth { get; set; }
    }
    public class GetDropdownYearDto
    {
     
        public string Year { get; set; }
    }

    public class DropdownUnitNoDto
    {
        public string UnitNo { get; set; }
    }

    public class GetSearchPSCodeDto
    {
        public string PsCode { get; set; }
        public string Name { get; set; }
    }
    public class GetSearchPSCodeInputDto
    {
        public string PsCode { get; set; }
        public string CustName { get; set; }
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
    }

    public class DropdownSiteDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
    }
    public class DropdownPeriodDto
    {
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
    }

    public class WaterReadingListInputDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string Cluster { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string invoiceName { get; set; }
        public string Search { get; set; }
        public List<int> SP { get; set; }
    }
    public class UserListinputDto : PagedInputDto
    {
      
        public string Search { get; set; }
    }

    
    public class EditDetailWaterReadingInputDto
    {
        public int WaterReadingId { get; set; }
        public int CurrentRead { get; set; }
        public int PrevRead { get; set; }
    }
    public class EditDetailElectricReadingInputDto
    {
        public int ElectricReadingId { get; set; }
        public int CurrentRead { get; set; }
        public int PrevRead { get; set; }
    }

    public class WaterReadingListDto
    {
        public int WaterReadingId { get; set; }
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public String ProjectName { get; set; }
        public String ClusterName { get; set; }
        public String Period { get; set; }
        public String UnitCode { get; set; }
        public String UnitNo { get; set; }
        public int PrevRead { get; set; }
        public int CurrentRead { get; set; }
    }

    public class ElectricReadingListDto
    {
        public int ElectricReadingId { get; set; }
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public String ProjectName { get; set; }
        public String ClusterName { get; set; }
        public String Period { get; set; }
        public String UnitCode { get; set; }
        public String UnitNo { get; set; }
        public int PrevRead { get; set; }
        public int CurrentRead { get; set; }
    }


    public class WaterReadingUploadDetailDto 
    {
      
        public string UnitNo { get; set; }
        public string UnitCode { get; set; }
        public int PrevRead { get; set; }
        public int CurrentRead { get; set; }
        public string RemarksError { get; set; }
       
    }

    public class UploadExcelChangeAdjInvoiceDto
    {
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
        public List<AdjInvoiceUploadDetailDto> AdjInvoiceUploadDetailList{ get; set; }

    }

    public class AdjInvoiceUploadDetailDto
    {

        public string UnitNo { get; set; }
        public string UnitCode { get; set; }
        public string InvoiceNo { get; set; }
        public int AdjNominal { get; set; }
        public string RemarksError { get; set; }

    }

    public class WaterReadingUploadExcelDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int PeriodId { get; set; }

        public List<int> ClusterId { get; set; }
        public List<WaterReadingUploadDetailDto> WaterReadingUploadDetailList { get; set; }
     
    }

    public class ElectricReadingUploadExcelDto
    {
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int PeriodId { get; set; }

        public List<int> ClusterId { get; set; }
        public List<WaterReadingUploadDetailDto> ElectricReadingUploadDetailList { get; set; }

    }

    public class WaterLetterListInputDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public DateTime Period { get; set; }
        public string ProjectCode { get; set; }
        public string ClusterName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string InvoiceName { get; set; }
        public string SP { get; set; }
    }

    public class WarningLetterListDto 
    {
        public int SiteId { get; set; }
        public int WarningLetterId { get; set; }
        public string Period { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ClusterName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        //public string InvoiceName { get; set; }
        public int SP { get; set; }
        public string SendEmailDate { get; set; }
        public int InvoiceHeaderId { get; set; }
        public string InvoiceNo { get; set; }
        public string EmailCust { get; set; }
    }
    public class ViewWaterReadingDto
    {
        public string printBy { get; set; }
        public List<WaterReadingListDto> ListWaterReadingDto { get; set; }

    }
    public class GetAvtivePeriodDto
    {
        public int periodId { get; set; }
        public string PeriodName { get; set; }
       

    }
    public class ListReminderDigitalComplianceDto
    {
        public int IdEmployee { get; set; }
        public long userid { get; set; }
        public int CourseId { get; set; }
        public string? UserName { get; set; }
        public string? EmailAddress { get; set; }
        public string? businessunitname { get; set; }
        public string? locationname { get; set; }
        public string? organizationunitname { get; set; }
        public string? segmentname { get; set; }
        public string? jobLevelName { get; set; }
        public string? CourseName { get; set; }
        public int organizationunitid { get; set; }
        public int segmentid { get; set; }
        public int superioruserid { get; set; }
        public DateTime ExpiredDate { get; set; }


    }

    public class ReportUriDto
    {
        public string Uri { get; set; }
    }

    public class ViewUserReportDto
    {
        public string printBy { get; set; }
        public List<UserExportInputDto> ListUser { get; set; }

    }

    public class UserExportInputDto
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Roles { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int Active { get; set; }
        public string LastLoginDate { get; set; }
        public string CreatedDate { get; set; }
    }


    public class InvoiceListDto
    {
        public int siteId { get; set; }
        public int InvoiceHeaderId { get; set; }
        public string period { get; set; }
        public int projectId { get; set; }
        public string projectName { get; set; }
        public int clusterId { get; set; }
        public string clusterName { get; set; }
        public int unitCodeID { get; set; }
        public int unitId { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string psCode { get; set; }
        public string name { get; set; }
        public string invoiceNo { get; set; }
        public string invoiceName { get; set; }
        public int totalTunggakan { get; set; }
       

    }
    public class InvoiceListInputDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public string PeriodId { get; set; }
        public int ProjectId { get; set; }
        public string Cluster { get; set; }
        public string UnitNo { get; set; }
        public string UnitCode { get; set; }
        public string psCode { get; set; }
    }


    public class CreateMasterSiteDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SiteCode { get; set; }
        public string Email { get; set; }
        public string OfficePhone { get; set; }
        public string HandPhone { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public List <ClusterDataDto> ClusterDataList { get; set; }
        public List <ProjectDataDto> ProjectDataList { get; set; }
    }

    public class UpdateMasterSiteDto
    {

        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SiteCode { get; set; }
        public string Email { get; set; }
        public string OfficePhone { get; set; }
        public string HandPhone { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public List<ClusterDataDto> ClusterDataList { get; set; }
        public List<ProjectDataDto> ProjectDataList { get; set; }
    }

    public class DetailMasterSiteDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string SiteCode { get; set; }
        public string Email { get; set; }
        public string OfficePhone { get; set; }
        public string HandPhone { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public List<ClusterDataDto> ClusterDataList { get; set; }
        public List<ProjectDataDto> ProjectDataList { get; set; }
    }

    public class ClusterDataDto
    {
        public int ClusterId { get; set; }
        public string ClusterCode { get; set; }
        public string ClusterName { get; set; }
        public int ProjectId { get; set; }
    }

    public class ProjectDataDto
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
    }

    public class InputFilterMasterSiteDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int ProjectId { get; set; }
        public string ClusterId { get; set; }
    }
    public class InputFilterMasterPeriodDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
       
    }



    public class GetListMasterSiteDto
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public bool isActive  { get; set; }
        public int ProjectCount  { get; set; }
        public int ClusterCount  { get; set; }
        public string logo  { get; set; }
    }

    public class CreateMasterPeriodDto
    {
        public int PeriodID { get; set; }
        public int SiteId { get; set; }
        public DateTime PeriodMonth { get; set; }
        public DateTime PeriodYear { get; set; }
        public int PeriodNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CloseDate { get; set; }
        public bool IsActive { get; set; }
    }
    public class ListMasterPeriodDto
    {
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
        public int PeriodNumber { get; set; }
        public String SiteName { get; set; }
        public String PeriodName { get; set; }
        public String StartDate { get; set; }
        public String EndDate { get; set; }
        public String CloseDate { get; set; }
        public bool IsActive { get; set; }
    }

    public class CountItemRateDeatail
    {
        public int BPLTotal { get; set; }
        public int itemIdBPL { get; set; }
        public int itemIdWater { get; set; }
        public decimal PPNBPL { get; set; }
        public decimal PPNWater { get; set; }
        public int WaterTotal { get; set; }
        public int ElectricTotal { get; set; }
        public string BiayaPemakaianListrik { get; set; }
        public string PencatatanAwal { get; set; }
        public string PencatatanAkhir { get; set; }
        public string Pemakaian { get; set; }
        public string Int1m3 { get; set; }
        public string Int1Nominal { get; set; }
        public string Int1Tagihan { get; set; }
        public string Int2m3 { get; set; }
        public string Int2Nominal { get; set; }
        public string Int2Tagihan { get; set; }
        public string Int3m3 { get; set; }
        public string Int3Nominal { get; set; }
        public string Int3Tagihan { get; set; }
        public string PemakaianAirBersih { get; set; }
        public string BiayaPemeliharaan { get; set; }
        public string PerhitunganPemakaianListrik { get; set; }
        public string JumlahTagihanAir { get; set; }



    }
    public class ParamPDFInvoiceDto
    {
        public string AdjPen { get; set; }
        public string SiteLogo { get; set; }
        public string SiteName { get; set; }
        public string SiteAddres { get; set; }
        public string SitePhone { get; set; }
        public string SiteEmail { get; set; }
 
        public string PsCode { get; set; }
        public string CustName { get; set; }
        public string CustPhone { get; set; }
        public string CustEmail { get; set; }
        public string CustAlamatKoresponden { get; set; }
        public string NomerInvoice { get; set; }
        public string InvoiceDate { get; set; }
        public string UnitName { get; set; }
        public string UnitNo { get; set; }
        public string Deskripsi { get; set; }
        public string VaNo { get; set; }
        public string VaBank { get; set; }
        public string TagihanBulanIni { get; set; }
        public string TagihanSampaiBulanIni { get; set; }
        public string JumlahyangHarusDibayar { get; set; }
        public string InvoiceName { get; set; }
        public string BulanPeriod { get; set; }
        public string BulanJatuhTempo { get; set; }
        public string TemplateInvoice { get; set; }

        //water 
        public string PencatatanAwal { get; set; }
        public string PencatatanAkhir { get; set; }
        public string Pemakaian { get; set; }
        public string Int1m3 { get; set; }
        public string Int1Nominal { get; set; }
        public string Int1Tagihan { get; set; }
        public string Int2m3 { get; set; }
        public string Int2Nominal { get; set; }
        public string Int2Tagihan { get; set; }
        public string Int3m3 { get; set; }
        public string Int3Nominal { get; set; }
        public string Int3Tagihan { get; set; }
        public string PemakaianAirBersih { get; set; }
        public string BiayaPemeliharaan { get; set; }
        public string JumlahTagihanAir { get; set; }
        public string DeskripsiBPL { get; set; }
        public string DeskripsiTagihanAir { get; set; }
        public string totalBpl { get; set; }
        public string totalWater { get; set; }
        public string PerhitunganPemakaianListrik { get; set; }

        public string JumlahTagihanListrik { get; set; }
        public string BiayaPemakaianListrik { get; set; }
    }

    public class PembayaranSebelumnyaDto
    {
        public decimal TotalPembayaran { get; set; }
        public List<DetailPembayaranSebelumnyaDto> DetailPembayaranSebelumnya { get; set; }


    }
    public class SPGetPembayaranSebelumnya
    {
        public int UnitDataID { get; set; }
        public int invoiceHeaderId { get; set; }
        public int PeriodId { get; set; }
        public DateTime EndDatePeriod { get; set; }
   

    }
    public class DetailPembayaranSebelumnyaDto
    {
        public int InvoiceHeaderId { get; set; }
        public string PaymentMethod { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime TransactionDate { get; set; }
     

    }
    public class WhatsAppInputDto
    {
        public string number { get; set; }
        public string message { get; set; }
        public string sender { get; set; }
    }

    public class GetLoginInfoDto
    {
        public long UserId { get; set; }
        public string ProfileUrl { get; set; }
        public string UserName { get; set; }
    }

    public class SenderWaDto
    {
        public string unitname { get; set; }
        public string unitno { get; set; }
        public string blnperiode { get; set; }
        public string nomerinvoice { get; set; }
        public string namainvoice { get; set; }
        public string totaltagihan { get; set; }
        public string tgljatuhtempo { get; set; }
        public string bank { get; set; }
        public string virtualno { get; set; }
        public string customerName { get; set; }
    
    }
    public class RootResponseWA
    {
        public bool status { get; set; }
       
    }

    public class DropdownSPDto
    {
        public int SpId { get; set; }
        public string SPName { get; set; }
    }

    public class ViewExportWarningLetterDto
    {
        public string printBy { get; set; }
        public List<WarningLetterListDto> ListWarningLetterDto { get; set; }

    }
    public class MasterUnitItemListDto
    {
        public int SiteId  { get; set; }
        public int UnitItemHeaderId  { get; set; }
        public int UnitDataId  { get; set; }
        public int ClusterId  { get; set; }
        public DateTime CreateTime  { get; set; }
        public string UnitCode  { get; set; }
        public string UnitNo { get; set; }
        public string TemplateInvoice { get; set; }
        public string Bank  { get; set; }
        public string VaNo { get; set; }
        public bool isPenalty { get; set; }
    }
    public class DetailMasterUnitItemDto
    {

        public int UnitItemHeaderId { get; set; }
        public int UnitDataId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public int TemplateInvoiceHeaderId { get; set; }
        public int BankId { get; set; }
        public int BuildArea { get; set; }
        public int LandArea { get; set; }
        public string VaNo { get; set; }
        public bool isPenalty { get; set; }
        public List<ItemDetailList> ItemDetail { get; set; }
    }
    public class ItemDetailList
    {
        public int unitItemDetailId { get; set; }
        public int TemplateInvoiceDetailId { get; set; }
        public string ItemName { get; set; }
        public string ItemRateName { get; set; }
        public int Rate { get; set; }
        public double LuasAwal { get; set; }
        public double LuasAkhir { get; set; }
        public int LandArea { get; set; }
   
       
    }
    public class DropdownMasterTemplateDto
    {
        public int TemplateInvoiceHeaderId { get; set; }
        public string templateName { get; set; }
        public string Urltemplate { get; set; }
        

    }

    public class InputFilterMasterUnitItem : PagedInputDto
    {
        public int SiteId { get; set; }
        public string Search { get; set; }
    }

    public class UploadNewUnitItemDto
    {
        public int SiteId { get; set; }
        public int TemplateInvoiceId { get; set; }
        public List<DetailUploadNewUnitItem> DetailUploadUnitItemList { get; set; }

    }

    public class DetailUploadNewUnitItem 
    {
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public string bank { get; set; }
        public string vaNo { get; set; }
        public bool Penalty { get; set; }
        public string RemarksError { get; set; }
    }





}
