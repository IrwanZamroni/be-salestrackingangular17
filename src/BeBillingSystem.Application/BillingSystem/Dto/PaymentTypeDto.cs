﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class PaymentTypeDto
    {
        public int paymentTypeId { get; set; }
        public string paymentTypeName { get; set; }
    }
}
