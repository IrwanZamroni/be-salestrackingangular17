﻿using BeBillingSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem.Dto
{
    public class CashierSystemDto
    {
    }
    public class CustomerListInputDto : PagedInputDto
    {
        public int SiteId { get; set; }
        public string Search { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
       
    }
    public class GetCustomerListDto
    {
        public int UnitDataId { get; set; }
        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int ClusterId { get; set; }
        public string ProjectName { get; set; }
        public string ClusterName { get; set; }
        public string UnitName { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string CustomerName { get; set; }
        public string PsCode { get; set; }
    }

    public class GetPaymentDetailByPsCodeDto
    {
    
        public string PsCode { get; set; }
        public int projectId { get; set; }
        public List<InvoicePaymentDto> listInvoicePayment { get; set; }
    }
    public class InvoicePaymentDto
    {

     
        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceName { get; set; }
        public decimal Balance { get; set; }
        public decimal EndBalance { get; set; }
        public int PaymentAmount { get; set; }
       
    }


    public class OfficialReceiptInputDto : PagedInputDto
    {
        public int UnitDataId { get; set; }

    }



    public class ListOfficialReceiptDto
    {
        public int BillingHeaderId { get; set; }
        public string ReceiptNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string TransactionDate { get; set; }
        public string Method { get; set; }
        public decimal TotalAmount { get; set; }
        public string Remarks { get; set; }
        public string InvoiceName { get; set; }

    }
    public class ParamGenerateOfficialReceiptDto
    {


        public string SiteLogo { get; set; }
        public string SiteName { get; set; }
        public string SiteAddres { get; set; }
        public string SitePhone { get; set; }
        public string SiteEmail { get; set; }
        public string NoOR { get; set; }
        public string TanggalOR { get; set; }
        public string TanggalORIndoFormat { get; set; }
        public string PsCode { get; set; }
        public string CustName { get; set; }
        public string TotalPayment { get; set; }
        public string Deskripsi { get; set; }
        public string CaraBayar { get; set; }
        public string AdminBilling { get; set; }
        public string PrintDate { get; set; }
        public string unitName { get; set; }
        public string unitNo { get; set; }
        public string fileNamePDF { get; set; }
        public string periodeName { get; set; }
        public int BillingPayementheaderId { get; set; }

    }

    public class ListCancelPaymentDto
    {
        public int BillingHeaderId { get; set; }
        public string ReceiptNumber { get; set; }
        public string TransactionDate { get; set; }
        public string Method { get; set; }
        public decimal TotalAmount { get; set; }
        public string Remarks { get; set; }
        public string Canceled { get; set; }


    }

    public class DetailCancelPaymentDto
    {
        public int BillingHeaderId { get; set; }
        public string ReceiptNumber { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string TransactionDate { get; set; }
        public string Method { get; set; }
        public decimal TotalAmount { get; set; }
        public string Remarks { get; set; }
        public List<InvoiceDetailCancelPaymentDto> ListInvoiceDetailCancelPayment { get; set; }
    }

    public class InvoiceDetailCancelPaymentDto
    {
        public string InvoiceNo { get; set; }
        public decimal Amount { get; set; }
        
    }

    public class ResponseBulkPaymentDto
    {
        public int TotalData { get; set; }
        public int TotalSukses { get; set; }
        public int TotalGagal { get; set; }
        public string urlDataGagal { get; set; }
        public bool isError { get; set; }
        public string PesanError { get; set; }
    }



    public class ViewDetailBalanceDto
    {
       
        public string Deskripsi { get; set; }
        public string Tanggal { get; set; }
        public decimal Jumlah { get; set; }
     
    }

    public class PaymentProsesInputDto
    {

        public int SiteId { get; set; }
        public int ProjectId { get; set; }
        public int PaymentType { get; set; }
        public string CardNumber { get; set; }
        public int TotalPayment { get; set; }
        public int Charge { get; set; }
        public int UnitDataId { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string PsCode { get; set; }
        public int BankId { get; set; }
        public string Remarks { get; set; }
        public bool IsAddSignee { get; set; }
        public DateTime TransactionDate { get; set; }
        public List<InvoicePaymentDto> listInvoicePayment { get; set; }

    }
    public class PaymentMethodDto
    {
        public string paymentName { get; set; }
        public int paymentType { get; set; }

    }
    public class BankListDto
    {
        public string BankName { get; set; }
        public int BankID { get; set; }

    }

    public class UploadBulkPaymentDto
    {
        public int SiteId { get; set; }
        public int PaymentMethodId { get; set; }
        public List<DetailUploadBulkPaymentDto> DetailUploadBulkPaymentList { get; set; }

    }
    public class DetailUploadBulkPaymentDto
    {
        public string IdClient { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Amount { get; set; }
        public string RemarksError { get; set; }
       

    }

    public class InsertPaymentBillingInputDto
    {
        public int siteID { get; set; }
        public string psCode { get; set; }
        public decimal paymentAmount { get; set; }
        public DateTime paymentDate { get; set; }
        public string paymentNumber { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public string VaNumber { get; set; }
        public string InvoiceNumber { get; set; }
    }

    public class viewDetailWarLettDto
    {
     
        public string CustName { get; set; }
        public string refNo { get; set; }
        public string alamatKorespondensi { get; set; }
        public string CustPhone { get; set; }
        public string siteEmail { get; set; }
        public string SiteHandPhone { get; set; }
        public string OfficePhone { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string invoiceNo { get; set; }
        public string invoiceType { get; set; }
        public string totalOutstanding { get; set; }
        public string totalTagihan { get; set; }
        public string bank { get; set; }
        public string vaNo { get; set; }
        public string location { get; set; }
        public string peringatanNo { get; set; }
        public string SPDate { get; set; }
        public string jatuhtempoDate { get; set; }
        public string periodName { get; set; }

      
    
    }

    public class GetRunningNumberDocInputDto
    {
        public int ProjectId { get; set; }
        public string DocCode { get; set; }
        public int SiteId { get; set; }
        public int PeriodId { get; set; }
     
    }
}