﻿using BeBillingSystem.BillingSystem.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public interface IBillingSystems
    {
        PembayaranSebelumnyaDto PembayaranSebelumnya(int UnitDataID, DateTime StartDate, int SiteID, int InvoiceHeaderId);

    }
}
