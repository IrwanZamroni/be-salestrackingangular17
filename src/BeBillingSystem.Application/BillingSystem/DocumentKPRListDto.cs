﻿using System;

namespace BeBillingSystem.BillingSystem
{
	public class DocumentKPRListDto
	{
		public int PriorityPassId { get; set; }
		public int kprtype { get; set; }
		public string documentType { get; set; }
		public string documentName { get; set; }
		public bool isMandatory { get; set; }
		public string psCode { get; set; }
		public bool isUploaded { get; set; }
		public bool isVerified { get; set; }
		public DateTime? creationTime { get; set; }
		public DateTime? lastModificationTime { get; set; }
	}
}