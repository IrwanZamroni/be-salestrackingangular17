﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.EntityFrameworkCore;
using BeBillingSystem.Helpers;
using BeBillingSystem.Migrations;
using Castle.Windsor.Installer;
//using BeBillingSystem.Report;
using Dapper;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using SelectPdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{


    public class BillingSystems : BeBillingSystemAppServiceBase
    {
        private readonly IReportDataExporter _exporter;
        private readonly BeBillingSystemDbContext _contextBilling;
        // private readonly IAppFolders _appFolders;
        private readonly PropertySystemDbContext _contextProp;
        private readonly PersonalsNewDbContext _contextPersonal;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IWhatsappHelper _waHelper;

        public BillingSystems(
              IWebHostEnvironment hostEnvironment,
                      IWhatsappHelper waHelper,
             IReportDataExporter exporter,
            PropertySystemDbContext contextProp,
            PersonalsNewDbContext contextPersonal,
            BeBillingSystemDbContext contextBilling


            )
        {
            _exporter = exporter;
            //     _appFolders = appFolders;
            _waHelper = waHelper;
            _contextProp = contextProp;
            _contextBilling = contextBilling;
            _hostEnvironment = hostEnvironment;
            _contextPersonal = contextPersonal;


        }
        //public List<DropdownProjectDto> GetDropdownProject()
        //{

        //    var result = (from a in _contextProp.MS_Project
        //                  select new DropdownProjectDto
        //                  {
        //                      ProjectId = a.Id,
        //                      ProjectCode = a.projectCode,
        //                      ProjectName = a.projectName
        //                  }).ToList();

        //    return result;


        //}

        //public List<DropdownClusterDto> GetDropdownClusterByProjectEngine3(List<int> ProjectId)
        //{

        //    var getData = (from a in _contextProp.MS_Cluster
        //                   join b in _contextProp.MS_Project on a.projectID equals b.Id
        //                   where ProjectId.Contains(a.projectID)
        //                   select new DropdownClusterDto
        //                   {
        //                       ClusterId = a.Id,
        //                       ClusterCode = a.clusterCode,
        //                       ClusterName = a.clusterName,
        //                       ProjectCode = b.projectCode,
        //                       ProjectId = a.projectID,
        //                   }).Distinct().ToList();

        //    return getData;

        //}


//        public List<DropdownProjectDto> GetDropdownProjectBySiteId(int SiteId)
//        {

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();
//            //conn1.Open();


//            #endregion


//            string query = $@"SELECT a.id AS ProjectId, a.projectName AS ProjectName, a.projectCode AS ProjectCode FROM " + dbCatalogPropertyString + "..MS_Project a " +
//                            "JOIN " + dbCatalogBillingString + "..MP_ProjectSite b ON a.Id = b.ProjectId " +
//                            "JOIN " + dbCatalogBillingString + "..MS_Site c ON b.SiteId = c.SiteId" +
//                            " WHERE c.SiteId = " + SiteId + "";

//            List<DropdownProjectDto> result = conn.Query<DropdownProjectDto>(query).ToList();

//            return result;




//        }
//        public List<DropdownProjectDto> GetDropdownProjectInvoice(int SiteId, int periodId)
//        {

//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MP_ProjectSite on a.ProjectId equals b.ProjectId
//                           where a.SiteId == SiteId && a.PeriodId == periodId
//                           select new DropdownProjectDto
//                           {
//                               ProjectCode = b.ProjectCode,
//                               ProjectName = b.ProjectName,
//                               ProjectId = b.ProjectId
//                           }).Distinct().ToList();


//            return getData;

//        }

//        public List<DropdownClusterDto> GetDropdownClusterInvoice(int SiteId, int periodId)
//        {

//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MP_ClusterSite on a.ClusterId equals b.ClusterId
//                           where a.SiteId == SiteId && a.PeriodId == periodId
//                           select new DropdownClusterDto
//                           {
//                               ClusterId = b.ClusterId,
//                               ClusterCode = b.ClusterCode,
//                               ClusterName = b.ClusterName
//                           }).Distinct().ToList();


//            return getData;

//        }

//        public List<DropdownClusterDto> GetDropdownClusterByProject(int ProjectId)
//        {

//            var getData = (from a in _contextBilling.MP_ClusterSite
//                           where a.ProjectId == ProjectId
//                           select new DropdownClusterDto
//                           {
//                               ClusterId = a.ClusterId,
//                               ClusterCode = a.ClusterCode,
//                               ClusterName = a.ClusterName,
//                           }).Distinct().ToList();

//            return getData;

//        }
//        public List<DropdownUnitCodeDto> GetDropdownUnitCodeByCluster(int SiteId, int periodId)
//        {

//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                           where a.SiteId == SiteId && a.PeriodId == periodId
//                           orderby b.UnitCode ascending
//                           select new DropdownUnitCodeDto
//                           {
//                               UnitCode = b.UnitCode,

//                           }).Distinct().ToList();

//            return getData;

//        }

//        public List<DropdownUnitCodeDto> GetDropdownUnitCodeByClusterID(int SiteId, int ClusterID)
//        {

//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                           where a.SiteId == SiteId && a.ClusterId == ClusterID
//                           select new DropdownUnitCodeDto
//                           {
//                               UnitCode = b.UnitCode,

//                           }).Distinct().ToList();

//            return getData;

//        }

//        public List<DropdownUnitDto> GetDropdownUnitInvoice(int SiteId, int periodId)
//        {
//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                           where a.SiteId == SiteId && a.PeriodId == periodId
//                           orderby b.UnitNo ascending
//                           select new DropdownUnitDto
//                           {
//                               UnitId = b.UnitId,
//                               UnitNo = b.UnitNo,

//                           }).Distinct().ToList();




//            return getData;

//        }

//        public List<DropdownInvoiceNameDto> GetDropdownInvoiceNameWarLett(int SiteId, int periodId)
//        {
//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id
//                           where a.SiteId == SiteId && a.PeriodId == periodId
//                           select new DropdownInvoiceNameDto
//                           {
//                               TemplateHeaderId = b.Id,
//                               InvoiceName = b.TemplateInvoiceName

//                           }).Distinct().ToList();

//            return getData;

//        }

//        public List<DropdownSPDto> GetDropdownSPWarLett()
//        {


//            var getdata = (from a in _contextBilling.MS_SP
//                           select new DropdownSPDto
//                           {
//                               SpId = a.Id,
//                               SPName = a.SPName
//                           }).ToList();

//            return getdata;

//        }


//        public List<GetSearchPSCodeDto> GetSearchPSCode(GetSearchPSCodeInputDto input)
//        {
//            var getData = new List<GetSearchPSCodeDto>();
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
//            var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();

//            #endregion

//            if (input.CustName == null && input.PsCode != null)
//            {
//                string query = $@"SELECT DISTINCT b.psCode, b.name from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
//                                "join " + dbCatalogPersonalString + "..PERSONALS b on a.PsCode = b.psCode " +
//                                " WHERE  b.psCode like '" + input.PsCode + "%' AND a.PeriodId = " + input.PeriodId + " AND a.SiteId = " + input.SiteId + "";

//                getData = conn.Query<GetSearchPSCodeDto>(query).ToList();

//            }
//            else if (input.PsCode == null && input.CustName != null)
//            {
//                string query = $@"SELECT DISTINCT b.psCode, b.name from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
//                               "join " + dbCatalogPersonalString + "..PERSONALS b on a.PsCode = b.psCode " +
//                               " WHERE b.name like '" + input.CustName + "%' AND a.PeriodId = " + input.PeriodId + " AND a.SiteId = " + input.SiteId + "";

//                getData = conn.Query<GetSearchPSCodeDto>(query).ToList();

//            }
//            else
//            {
//                string query = $@"SELECT DISTINCT b.psCode, b.name from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
//                "join " + dbCatalogPersonalString + "..PERSONALS b on a.PsCode = b.psCode " +
//                "WHERE b.name like '" + input.CustName + "%' AND b.psCode like '" + input.PsCode + "%' AND a.PeriodId = " + input.PeriodId + " AND a.SiteId = " + input.SiteId + "";
//                getData = conn.Query<GetSearchPSCodeDto>(query).ToList();
//            }
//            return getData;

//        }





//        public List<DropdownSiteDto> GetDropdownSite()
//        {
//            var getUserId = (from x in _contextBilling.Users
//                             where x.Id == AbpSession.GetUserId()
//                             select x.Id).FirstOrDefault();

//            var getData = (from a in _contextBilling.MS_Site
//                           join b in _contextBilling.MP_UserSite on a.SiteId equals b.SiteId
//                           where a.IsActive == true && b.UserId == getUserId
//                           select new DropdownSiteDto
//                           {
//                               SiteId = a.SiteId,
//                               SiteName = a.SiteName,

//                           }).ToList();
//            return getData;

//        }



//        //[AbpAuthorize(PermissionNames.Pages_Transaction_WaterReading)]
//        public PagedResultDto<WaterReadingListDto> GetWaterReadingList(WaterReadingListInputDto input)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();


//            #endregion


//            string query = $@"SELECT DISTINCT twr.id AS WaterReadingId, SiteId = twr.SiteId, twr.ProjectId, twr.ClusterId, mp.projectName AS ProjectName, mc.clusterName, (FORMAT(mp1.PeriodMonth, 'MMMM ') + FORMAT(mp1.PeriodYear, 'yyyy'))  AS Period,mp1.PeriodMonth, twr.UnitNo, twr.UnitCode, twr.CurrentRead, twr.PrevRead  
//                            FROM  " + dbCatalogBillingString + "..TR_WaterReading twr " +
//                            "JOIN " + dbCatalogPropertyString + "..MS_Project mp ON twr.ProjectId = mp.Id " +
//                            "LEFT JOIN " + dbCatalogBillingString + "..MP_ClusterSite mc ON twr.ClusterId = mc.ClusterId " +
//                            "JOIN " + dbCatalogBillingString + "..MS_Period mp1 ON twr.PeriodId = mp1.Id " +
//                            "WHERE twr.SiteId = " + input.SiteId + " AND twr.ProjectId = " + input.ProjectId + " AND twr.ClusterId = " + input.ClusterId + " and twr.UnitNo like '%" + input.Search + "%' ORDER by mp1.PeriodMonth DESC ";

//            List<WaterReadingListDto> getData = conn.Query<WaterReadingListDto>(query).ToList();




//            var dataCount = getData.Count();

//            if (input.Search != null)
//            {

//                getData = getData.Where(x => x.UnitNo.ToLower().Contains(input.Search.ToLower()) || x.ProjectName.ToLower().Contains(input.Search.ToLower())
//                || x.ClusterName.ToLower().Contains(input.Search.ToLower())
//                || x.UnitCode.ToLower().Contains(input.Search.ToLower()) || x.UnitNo.ToLower().Contains(input.Search.ToLower())).ToList();

//            }

//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


//            return new PagedResultDto<WaterReadingListDto>(dataCount, getData.ToList());

//        }



//        public GetAvtivePeriodDto GetActivePeriod(int SiteId)
//        {
//            var final = new GetAvtivePeriodDto();
//            var getData = (from a in _contextBilling.MS_Period
//                           where a.SiteId == SiteId && a.IsActive == true
//                           orderby a.Id descending
//                           select a).FirstOrDefault();

//            if (getData != null)
//            {
//                final = new GetAvtivePeriodDto
//                {

//                    periodId = getData.Id,
//                    PeriodName = getData.PeriodMonth.ToString("MMMM") + " " + getData.PeriodYear.ToString("yyyy"),
//                };

//            }
//            return final;

//        }



//        public ResponseBulkPaymentDto UploadExcelWaterReading(WaterReadingUploadExcelDto input)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion


//            var totalData = input.WaterReadingUploadDetailList.Count();
//            int prosesData = 0;
//            int totalgagal = 0;
//            int totalsukses = 0;
//            FileDto file = new FileDto();
//            string URL = "";
//            var gagalDataList = new List<WaterReadingUploadDetailDto>();

//            var cekPeriod = (from a in _contextBilling.MS_Period
//                             where a.SiteId == input.SiteId && a.IsActive == true
//                             select a).FirstOrDefault();

//            if (cekPeriod == null)
//            {

//                throw new UserFriendlyException("Period InActive");
//            }


//            foreach (var item in input.WaterReadingUploadDetailList)
//            {
//                prosesData++;

//                var checkUnit = (from a in _contextBilling.MS_UnitData
//                                 where a.UnitNo == item.UnitNo && a.UnitCode == item.UnitCode && a.ProjectId == input.ProjectId && input.ClusterId.Contains(a.ClusterId)
//                                 select a).FirstOrDefault();

              

//                if (checkUnit != null)
//                {

//                    var getExistData = (from a in _contextBilling.TR_WaterReading
//                                        where a.PeriodId == input.PeriodId && a.UnitNo == item.UnitNo && a.UnitCode == item.UnitCode && a.ProjectId == input.ProjectId && input.ClusterId.Contains(a.ClusterId) && a.SiteId == input.SiteId
//                                        select a).FirstOrDefault();

//                    if (getExistData != null)
//                    {

//                        _contextBilling.TR_WaterReading.Remove(getExistData);
//                        _contextBilling.SaveChanges();

//                    }

//                    //berhasil
//                    totalsukses++;
//                    var data = new TR_WaterReading
//                    {
//                        SiteId = input.SiteId,
//                        ProjectId = input.ProjectId,
//                        ClusterId = checkUnit.ClusterId,
//                        UnitCode = item.UnitCode,
//                        UnitNo = item.UnitNo,
//                        PrevRead = item.PrevRead,
//                        CurrentRead = item.CurrentRead,
//                        PeriodId = input.PeriodId,
//                        UnitDataId = checkUnit.Id

//                    };
//                    _contextBilling.TR_WaterReading.Add(data);
//                    _contextBilling.SaveChanges();

//                }
//                else
//                {
//                    //Jika gagal
//                    totalgagal++;
//                    item.RemarksError = "Data tidak ditemukan";
//                    gagalDataList.Add(item);

//                }

//            }

//            if (totalgagal > 0)
//            {

//                var paramToExcel = new WaterReadingUploadExcelDto
//                {
//                    WaterReadingUploadDetailList = gagalDataList

//                };

//                file = _exporter.ExportToExcelErrorUploadWaterResult(paramToExcel);

//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                URL = ipPublic + @"\Temp\Downloads\" + file.FileName;

//            }

//            var final = new ResponseBulkPaymentDto
//            {

//                TotalData = totalData,
//                TotalGagal = totalgagal,
//                TotalSukses = totalsukses,
//                urlDataGagal = URL,


//            };

//            return final;

//        }

//        public void ProsesUpdateDetailWaterReading(EditDetailWaterReadingInputDto input)
//        {


//            var getData = (from a in _contextBilling.TR_WaterReading
//                           where a.Id == input.WaterReadingId
//                           select a).FirstOrDefault();

//            if (getData != null)
//            {
//                getData.CurrentRead = input.CurrentRead;
//                getData.PrevRead = input.PrevRead;


//                _contextBilling.TR_WaterReading.Update(getData);
//                _contextBilling.SaveChanges();
//            }



//        }



//        public ResponseBulkPaymentDto UploadExcelElectricReading(ElectricReadingUploadExcelDto input)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion


//            var totalData = input.ElectricReadingUploadDetailList.Count();
//            int prosesData = 0;
//            int totalgagal = 0;
//            int totalsukses = 0;
//            FileDto file = new FileDto();
//            string URL = "";
//            var gagalDataList = new List<WaterReadingUploadDetailDto>();

//            var cekPeriod = (from a in _contextBilling.MS_Period
//                             where a.SiteId == input.SiteId && a.IsActive == true
//                             select a).FirstOrDefault();

//            if (cekPeriod == null)
//            {

//                throw new UserFriendlyException("Period InActive");
//            }


//            foreach (var item in input.ElectricReadingUploadDetailList)
//            {
//                prosesData++;

//                var checkUnit = (from a in _contextBilling.MS_UnitData
//                                 where a.UnitNo == item.UnitNo && a.UnitCode == item.UnitCode && a.ProjectId == input.ProjectId && input.ClusterId.Contains(a.ClusterId)
//                                 select a).FirstOrDefault();


                

//                if (checkUnit != null)
//                {

//                    var getExistData = (from a in _contextBilling.TR_ElectricReading
//                                        where a.PeriodId == input.PeriodId &&  a.UnitNo == item.UnitNo && a.UnitCode == item.UnitCode && a.ProjectId == input.ProjectId && input.ClusterId.Contains(a.ClusterId) && a.SiteId == input.SiteId
//                                        select a).FirstOrDefault();

//                    if (getExistData != null) {

//                        _contextBilling.TR_ElectricReading.Remove(getExistData);
//                        _contextBilling.SaveChanges();
                    
//                    }

//                    //berhasil
//                    totalsukses++;
//                    var data = new TR_ElectricReading
//                    {
//                        SiteId = input.SiteId,
//                        ProjectId = input.ProjectId,
//                        ClusterId = checkUnit.ClusterId,
//                        UnitCode = item.UnitCode,
//                        UnitNo = item.UnitNo,
//                        PrevRead = item.PrevRead,
//                        CurrentRead = item.CurrentRead,
//                        PeriodId = input.PeriodId,
//                        UnitDataId = checkUnit.Id

//                    };
//                    _contextBilling.TR_ElectricReading.Add(data);
//                    _contextBilling.SaveChanges();

//                }
//                else
//                {
//                    //Jika gagal
//                    totalgagal++;
//                    item.RemarksError = "Data tidak ditemukan";
//                    gagalDataList.Add(item);

//                }

//            }

//            if (totalgagal > 0)
//            {

//                var paramToExcel = new WaterReadingUploadExcelDto
//                {
//                    WaterReadingUploadDetailList = gagalDataList

//                };

//                file = _exporter.ExportToExcelErrorUploadElectricResult(paramToExcel);

//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                URL = ipPublic + @"\Temp\Downloads\" + file.FileName;

//            }

//            var final = new ResponseBulkPaymentDto
//            {

//                TotalData = totalData,
//                TotalGagal = totalgagal,
//                TotalSukses = totalsukses,
//                urlDataGagal = URL,


//            };

//            return final;

//        }


//        public void ProsesUpdateDetailElectricReading(EditDetailElectricReadingInputDto input)
//        {


//            var getData = (from a in _contextBilling.TR_ElectricReading
//                           where a.Id == input.ElectricReadingId
//                           select a).FirstOrDefault();

//            if (getData != null)
//            {
//                getData.CurrentRead = input.CurrentRead;
//                getData.PrevRead = input.PrevRead;


//                _contextBilling.TR_ElectricReading.Update(getData);
//                _contextBilling.SaveChanges();
//            }



//        }



//        public async Task<ReportUriDto> ExportToExcelWaterReading(WaterReadingListInputDto input)
//        {

//            FileDto file = new FileDto();

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();
//            #endregion


//            string query = $@"SELECT DISTINCT twr.id AS WaterReadingId, SiteId = twr.SiteId, twr.ProjectId, twr.ClusterId, mp.projectName AS ProjectName, mc.clusterName, (FORMAT(mp1.PeriodMonth, 'MMMM ') + FORMAT(mp1.PeriodYear, 'yyyy'))  AS Period, twr.UnitNo, twr.UnitCode, twr.CurrentRead, twr.PrevRead  
//                            FROM  " + dbCatalogBillingString + "..TR_WaterReading twr " +
//                            "JOIN " + dbCatalogPropertyString + "..MS_Project mp ON twr.ProjectId = mp.Id " +
//                            "LEFT JOIN " + dbCatalogBillingString + "..MP_ClusterSite mc ON twr.ClusterId = mc.ClusterId " +
//                            "JOIN " + dbCatalogBillingString + "..MS_Period mp1 ON twr.PeriodId = mp1.Id " +
//                            "WHERE twr.SiteId = " + input.SiteId + " AND twr.ProjectId = " + input.ProjectId + " AND twr.ClusterId = " + input.ClusterId + " and twr.UnitNo like '%" + input.Search + "%'";

//            List<WaterReadingListDto> data = conn.Query<WaterReadingListDto>(query).ToList();

//            if (data.Count > 0)
//            {
//                var paramToExcel = new ViewWaterReadingDto
//                {
//                    printBy = (from x in _contextBilling.Users
//                               where x.Id == AbpSession.GetUserId()
//                               select x.Name).FirstOrDefault(),

//                    ListWaterReadingDto = data

//                };


//                file = _exporter.ExportToExcelWaterReadingResult(paramToExcel);

//                //var tempRootPath = Path.Combine(_appFolders.UploadRootDirectory, _appFolders.TempFileDownloadDirectory);


//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
//                return new ReportUriDto { Uri = URL.Replace("\\", "/") };
//            }
//            else
//            {

//                return new ReportUriDto { };
//            }

//        }


//        public PagedResultDto<WarningLetterListDto> ProsesGetWarningLetterList(WaterReadingListInputDto input)
//        {


//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
//            var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();

//            #endregion


//            string query = $@"SELECT DISTINCT a.Id as WarningLetterId,a.InvoiceHeaderId, g.InvoiceNo, (FORMAT(b.PeriodMonth, 'MMMM ') + FORMAT(b.PeriodYear, 'yyyy'))  AS Period,  mps.ProjectName,e.clustername, c.ProjectId, c.UnitNo, c.UnitCode, c.SiteId, a.SP, a.SendEmailDate, f.email as EmailCust
//                            FROM " + dbCatalogBillingString + "..TR_WarningLetter a " +
//                            "join " + dbCatalogBillingString + "..MS_Period b on a.PeriodId = b.Id " +
//                            "join  " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
//                            "LEFT join  " + dbCatalogBillingString + "..MP_ProjectSite mps on c.ProjectId = mps.ProjectId " +
//                            "LEFT join  " + dbCatalogBillingString + "..MP_ClusterSite  e on c.ClusterId = e.ClusterId " +
//                            "join " + dbCatalogPersonalString + "..TR_Email f on c.PsCode = f.psCode " +
//                            "join " + dbCatalogBillingString + "..TR_InvoiceHeader g on a.InvoiceHeaderId = g.Id " +
//                            "where a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " and a.ProjectId = " + input.ProjectId + "   ORDER BY c.UnitCode, c.UnitNo, a.SP ASC";

//            List<WarningLetterListDto> getData = conn.Query<WarningLetterListDto>(query).ToList();

//            if (input.Cluster != null || input.UnitCode != null || input.UnitNo != null || input.invoiceName != null || input.Search != null || input.SP != null)
//            {

//                getData = getData

//              .WhereIf(input.Cluster != null, item => input.Cluster.Contains(item.ClusterName))
//              .WhereIf(input.UnitNo != null, item => input.UnitNo.Contains(item.UnitNo))
//              .WhereIf(input.UnitCode != null, item => input.UnitCode.Contains(item.UnitCode))
//              .WhereIf(input.Search != null, item => item.UnitCode.ToLower().Contains(input.Search.ToLower()) || item.EmailCust.ToLower().Contains(input.Search.ToLower()) || item.ClusterName.ToLower().Contains(input.Search.ToLower()) || item.ProjectName.ToLower().Contains(input.Search.ToLower()) || item.UnitNo.ToLower().Contains(input.Search.ToLower()) || item.InvoiceNo.ToLower().Contains(input.Search.ToLower()))
//              .WhereIf(input.SP != null, item => input.SP.Contains(item.SP))
//              /*.WhereIf(input.invoiceName != null, item => input.invoiceName.ToLower().Contains(item.InvoiceName.ToLower()))*/.ToList();
//            }

//            var dataCount = getData.Count();


//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

//            return new PagedResultDto<WarningLetterListDto>(dataCount, getData.ToList());

//        }


//        //SERVICE INVOICE

//        public PagedResultDto<InvoiceListDto> GetInvoiceList(InvoiceListInputDto input)
//        {

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
//            var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
//            var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
//            var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

//            var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
//            var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

//            var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
//            var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);


//            SqlConnection conn = new SqlConnection(ConstringProp);

//            conn.Open();

//            #endregion


//            string query = $@"select DISTINCT a.Id as InvoiceHeaderId, a.SiteId,
//                             (FORMAT(c.PeriodMonth, 'MMMM ') + FORMAT(c.PeriodYear, 'yyyy'))  AS Period,
//                             d.projectName,
//                             a.ProjectId,
//                             f.clusterName,
//                             b.ClusterId, 
//                             b.UnitCode, 
//                             b.UnitNo,
//                             b.UnitId, 
//                             a.PsCode,
//                             g.name, 
//                             a.InvoiceNo, 
//                             h.TemplateInvoiceName as invoiceName,
//                             a.JumlahYangHarusDibayar as totalTunggakan
//                            from " + dbCatalogBillingString + "..TR_InvoiceHeader a " +
//                            "JOIN " + dbCatalogBillingString + "..MS_UnitData b on a.UnitDataId = b.Id " +
//                            "JOIN " + dbCatalogBillingString + "..MS_Period c on a.PeriodId = c.Id " +
//                            "JOIN " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
//                            "LEFT JOIN " + dbCatalogBillingString + "..MP_ClusterSite f on b.ClusterId = f.ClusterId  " +
//                            "JOIN " + dbCatalogPersonalString + "..PERSONALS g on a.PsCode = g.psCode " +
//                            "JOIN " + dbCatalogBillingString + "..MS_TemplateInvoiceHeader h on a.TemplateInvoiceHeaderId = h.Id " +
//                            "WHERE a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " ";

//            List<InvoiceListDto> getData = conn.Query<InvoiceListDto>(query).ToList();


//            if (input.ProjectId != 0 || input.Cluster != null || input.UnitCode != null || input.UnitNo != null || input.psCode != null)
//            {

//                getData = getData
//              .WhereIf(input.ProjectId != 0, item => input.ProjectId.Equals(item.projectId))
//              .WhereIf(input.Cluster != null, item => input.Cluster.Contains(item.clusterName))
//              .WhereIf(input.UnitNo != null, item => input.UnitNo.Contains(item.unitNo))
//              .WhereIf(input.UnitCode != null, item => input.UnitCode.Contains(item.unitCode))
//              .WhereIf(input.psCode != null, item => input.psCode.ToLower().Contains(item.psCode.ToLower())).ToList();
//            }


//            var dataCount = getData.Count();


//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

//            return new PagedResultDto<InvoiceListDto>(dataCount, getData.ToList());

//        }

//        public string GetPreviewInvoicePdf(int InvoiceId)
//        {
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

//            var getdata = (from a in _contextBilling.TR_InvoiceHeader
//                           where a.Id == InvoiceId
//                           select a.InvoiceDoc).FirstOrDefault();

//            return ipPublic + "/Assets/Upload/PdfOutput/Invoice/" + getdata;
//        }

//        public void ScheduleerGenerateInvoice()
//        {
//            try
//            {

//                var getInvoice = (from a in _contextBilling.MS_Site
//                                  where a.IsActive == true
//                                  select a.SiteId).ToList();

//                foreach (var siteID in getInvoice)
//                {

//                    GenerateInvoiceBySiteID(siteID);
//                }

//            }
//            catch { }

//        }




//        public void GenerateInvoiceBySiteID(int SiteID)
//        {
//            try
//            {
//                var paramtoPdf = new ParamPDFInvoiceDto();

//                var getSite = (from a in _contextBilling.MS_Site
//                               where a.SiteId == SiteID
//                               select a).FirstOrDefault();

//                //ambil periode terakhir yang aktif di ms_period
//                var getPeriod = (from a in _contextBilling.MS_Period
//                                 where a.SiteId == SiteID && a.IsActive == true
//                                 orderby a.Id descending
//                                 select a).FirstOrDefault();

//                // ambil semua unit by siteID
//                var getUnit = (from a in _contextBilling.MS_UnitData
//                               join b in _contextBilling.MS_UnitItemHeader on a.Id equals b.UnitDataId
//                               join c in _contextBilling.MS_TemplateInvoiceHeader on b.TemplateInvoiceHeaderId equals c.Id
//                               where a.SiteId == SiteID && a.CreationTime <= getPeriod.StartDate
//                               select new
//                               {
//                                   a,
//                                   b,
//                                   c,

//                               }).Distinct().ToList();
//                //1017

//                foreach (var unit in getUnit) //ini jalan tiap per invoice
//                {
//                    //pengecekan apakah sudah pernah di generate apa belum
//                    var getisGenerate = (from a in _contextBilling.TR_InvoiceHeader
//                                         where a.UnitDataId == unit.a.Id && a.PeriodId == getPeriod.Id && a.TemplateInvoiceHeaderId == unit.c.Id 
//                                         select a).FirstOrDefault();

//                    if (getisGenerate == null)
//                    {
//                        try
//                        {
//                            #region Main Global Proses
//                            var getPScode = (from a in _contextPersonal.PERSONALS
//                                             join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                             join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
//                                             where a.psCode == unit.a.PsCode && c.addrType == "3"
//                                             select new
//                                             {
//                                                 a.psCode,
//                                                 a.name,
//                                                 b.email,
//                                                 c.address
//                                             }).FirstOrDefault();

//                            if (getPScode == null)
//                            {
//                                getPScode = (from a in _contextPersonal.PERSONALS
//                                             join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                             join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
//                                             where a.psCode == unit.a.PsCode && c.addrType == "2"
//                                             select new
//                                             {
//                                                 a.psCode,
//                                                 a.name,
//                                                 b.email,
//                                                 c.address
//                                             }).FirstOrDefault();
//                            }

//                            var getPhoneCust = (from a in _contextPersonal.TR_Phone
//                                                where a.psCode == unit.a.PsCode && a.refID == 1 && a.phoneType == "3"
//                                                select a.number).FirstOrDefault();



//                            var getVa = (from a in _contextBilling.MS_UnitItemHeader
//                                         join b in _contextBilling.MS_Bank on a.BankId equals b.Id
//                                         where a.TemplateInvoiceHeaderId == unit.c.Id && a.SiteId == unit.a.SiteId && a.UnitDataId == unit.a.Id
//                                         select new
//                                         {
//                                             a.VirtualAccNo,
//                                             b.BankName
//                                         }).FirstOrDefault();



//                            #region Perhitungan tagihan

//                            decimal tagihanSebelumnya = TagihanSebelumnya(unit.a.Id, getPeriod.PeriodMonth, unit.a.SiteId, unit.c.Id);



//                            PembayaranSebelumnyaDto pembayaranSebelumnya = PembayaranSebelumnya(unit.a.Id, getPeriod.StartDate, unit.a.SiteId, unit.c.Id);

//                            var getDetailPPNWater = CountTagianBplWater(unit.b.Id, getPeriod.Id);

//                            int TotalTagiahPerbulan = (Convert.ToInt32(getDetailPPNWater.BPLTotal) + Convert.ToInt32(getDetailPPNWater.WaterTotal) + Convert.ToInt32(getDetailPPNWater.PPNBPL) + Convert.ToInt32(getDetailPPNWater.PPNWater) + Convert.ToInt32(getDetailPPNWater.ElectricTotal));

//                            decimal penaltyNominal = 0;
//                            var isHavePenalty = (from a in _contextBilling.MS_UnitItemHeader
//                                                 join c in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceHeaderId equals c.TemplateInvoiceHeaderId
//                                                 join d in _contextBilling.MS_ItemRate on c.ItemRateId equals d.Id
//                                                 where a.UnitDataId == unit.b.UnitDataId && a.IsPenalty == true && a.TemplateInvoiceHeaderId == unit.b.TemplateInvoiceHeaderId
//                                                 select new
//                                                 {

//                                                     a.UnitDataId,
//                                                     a.IsPenalty,
//                                                     d.PenaltyNominal,
//                                                     d.PenaltyPercentage,


//                                                 }).FirstOrDefault();

//                            paramtoPdf.AdjPen = isHavePenalty != null ? "+ Penalty" : "";

//                            if (isHavePenalty != null)
//                            {

//                                if (isHavePenalty.PenaltyPercentage > 0)
//                                {
//                                    var tagihanSebelumnyaPayment = (tagihanSebelumnya - pembayaranSebelumnya.TotalPembayaran);
//                                    penaltyNominal = (tagihanSebelumnyaPayment * isHavePenalty.PenaltyPercentage / 100);

//                                    if (penaltyNominal < 0)
//                                    {

//                                        penaltyNominal = 0;

//                                    }
//                                }
//                                else
//                                {
//                                    penaltyNominal = isHavePenalty.PenaltyNominal;
//                                }

//                            }

//                            var TotalJumlahYangHarusDibayar = (tagihanSebelumnya + TotalTagiahPerbulan + penaltyNominal) - pembayaranSebelumnya.TotalPembayaran;


//                            #endregion


//                            #region generate invoiceNo
//                            var getRunningNo = 0;
//                            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
//                                                where a.SiteId == SiteID && a.DocCode == "INV" && a.ProjectId == unit.a.ProjectId && a.PeriodId == getPeriod.Id
//                                                orderby a.Number descending
//                                                select a).FirstOrDefault();

//                            if (getTrRunning == null)
//                            {
//                                getRunningNo = 1;

//                                var AddRunningNumber = new TR_RunningNumber
//                                {
//                                    DocCode = "INV",
//                                    ProjectId = unit.a.ProjectId,
//                                    Number = getRunningNo,
//                                    SiteId = SiteID,
//                                    PeriodId = getPeriod.Id

//                                };

//                                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
                     
//                            }
//                            else
//                            {
//                                getRunningNo = getTrRunning.Number + 1;
//                                getTrRunning.Number = getRunningNo;
                              

//                                _contextBilling.TR_RunningNumber.Update(getTrRunning);

//                            }

//                            _contextBilling.SaveChanges();


//                            var generateRunningNo = "0000000";
//                            var CountRunningNo = getRunningNo.ToString().Length;
//                            var CountSub = generateRunningNo.Length - CountRunningNo;

//                            var invNoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;


//                            var InvoiceNoComb = "" + invNoFinal + "/INV/" + getSite.SiteCode + "/" + getPeriod.PeriodMonth.ToString("MM") + "/" + getPeriod.PeriodYear.ToString("yyyy") + "";

//                            #endregion



//                            var inserData = new TR_InvoiceHeader
//                            {
//                                PeriodId = getPeriod.Id,
//                                ClusterId = unit.a.ClusterId,
//                                ProjectId = unit.a.ProjectId,
//                                PsCode = unit.a.PsCode,
//                                SiteId = unit.a.SiteId,
//                                UnitDataId = unit.a.Id,
//                                TemplateInvoiceHeaderId = unit.b.TemplateInvoiceHeaderId,
//                                InvoiceDate = DateTime.Now,
//                                JumlahYangHarusDibayar = TotalJumlahYangHarusDibayar,
//                                TagihanPerbulan = TotalTagiahPerbulan,
//                                InvoiceNo = InvoiceNoComb,
//                                IsSendEmailDate = null,
//                                IsSendWADate = null,
//                                JatuhTempo = getPeriod.CloseDate,
//                                InvoiceDoc = InvoiceNoComb.Replace("/", "") + ".pdf",
//                                PenaltyNominal = penaltyNominal,
//                                TagihanBulanSebelumnya = tagihanSebelumnya,
//                                VirtualAccNo = unit.b.VirtualAccNo,
//                                BankId = unit.b.BankId,
//                                IsPenalty = unit.b.IsPenalty,




//                            };

//                            _contextBilling.TR_InvoiceHeader.Add(inserData);
//                            _contextBilling.SaveChanges();

//                            int InvoiceHeaderID = inserData.Id;

//                            var getitemID = (from a in _contextBilling.MS_UnitItemDetail
//                                             join b in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceDetailId equals b.Id
//                                             where a.UnitItemHeaderId == unit.b.Id
//                                             select new
//                                             {
//                                                 a.UnitItemHeaderId,
//                                                 a.TemplateInvoiceDetailId,
//                                                 b.ItemId,
                                                 
//                                             }).ToList();


//                            foreach (var item in getitemID)
//                            {
//                                var getItemElectric = (from a in _contextBilling.MS_Item
//                                                       where a.Id == item.ItemId && a.ItemName == "Electric"
//                                                       select a).FirstOrDefault();

//                                var insertDetail = new TR_InvoiceDetail
//                                {
//                                    InvoiceHeaderId = InvoiceHeaderID,
//                                    ItemId = item.ItemId,
//                                    ItemNominal = getItemElectric != null ? getDetailPPNWater.ElectricTotal : getDetailPPNWater.itemIdBPL == item.ItemId ? getDetailPPNWater.BPLTotal : getDetailPPNWater.WaterTotal ,
//                                    PPNNominal = getDetailPPNWater.itemIdBPL == item.ItemId ? Convert.ToInt32(getDetailPPNWater.PPNBPL) : Convert.ToInt32(getDetailPPNWater.PPNWater),

//                                };


//                                _contextBilling.TR_InvoiceDetail.Add(insertDetail);
//                                _contextBilling.SaveChanges();




//                            }





//                            #region Proses generate PDF
//                            var LoopDeskripsiPdf = new List<String>();
//                            var StingData = "";
//                            int NoDetail = 0;

//                            if (tagihanSebelumnya != 0)
//                            {
//                                NoDetail++;
//                                StingData = " <tr> <td>" + NoDetail + "</td> " + //No
//                              "<td>Tagihan Sebelumnya</td>" + //deskripsi
//                              "<td>-</td>" +  //tanggal
//                              "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(tagihanSebelumnya) + "</td>" +      //jumlah
//                              "</tr>";

//                                LoopDeskripsiPdf.Add(StingData);

//                            }




//                            if (pembayaranSebelumnya.TotalPembayaran != 0)
//                            {
//                                #region getListpembayaran sebelumnya

//                                foreach (var pembayaran in pembayaranSebelumnya.DetailPembayaranSebelumnya)
//                                {
//                                    NoDetail++;

//                                    StingData = " <tr> <td>" + NoDetail + "</td> " + //No
//                                    "<td>Pembayaran melalui " + pembayaran.PaymentMethod + "</td>" + //deskripsi
//                                    "<td>" + pembayaran.TransactionDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                    "<td>(Rp. " + NumberHelper.IndoFormatWithoutTail(pembayaran.PaymentAmount) + ")</td>" +      //jumlah
//                                    "</tr>";

//                                    LoopDeskripsiPdf.Add(StingData);

//                                }

//                                #endregion



//                            }

//                            var getDetailInvoice = (from a in _contextBilling.TR_InvoiceDetail
//                                                    join b in _contextBilling.MS_Item on a.ItemId equals b.Id
//                                                    where a.InvoiceHeaderId == InvoiceHeaderID
//                                                    select new
//                                                    {
//                                                        a.ItemNominal,
//                                                        a.CreationTime,
//                                                        a.ItemId,
//                                                        a.PPNNominal,
//                                                        b.ItemName,



//                                                    }).ToList();



//                            if (unit.c.TemplateInvoiceName == "Water" || unit.c.TemplateInvoiceName == "BPL & Water" || unit.c.TemplateInvoiceName == "Electric")
//                            {


//                                var totalBPL = (getDetailPPNWater.BPLTotal + getDetailPPNWater.PPNBPL + penaltyNominal);
//                                var totalWater = (getDetailPPNWater.WaterTotal + getDetailPPNWater.PPNWater);

//                                paramtoPdf.totalBpl = totalBPL == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalBPL);
//                                paramtoPdf.totalWater = totalWater == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalWater);


//                                paramtoPdf.PencatatanAwal = getDetailPPNWater.PencatatanAwal == null ? "0" : getDetailPPNWater.PencatatanAwal;
//                                paramtoPdf.PencatatanAkhir = getDetailPPNWater.PencatatanAkhir == null ? "0" : getDetailPPNWater.PencatatanAkhir;
//                                paramtoPdf.Pemakaian = getDetailPPNWater.Pemakaian == null ? "0" : getDetailPPNWater.Pemakaian;
//                                paramtoPdf.Int1m3 = getDetailPPNWater.Int1m3;
//                                paramtoPdf.Int1Nominal = getDetailPPNWater.Int1Nominal;
//                                paramtoPdf.Int1Tagihan = getDetailPPNWater.Int1Tagihan == null ? "0" : getDetailPPNWater.Int1Tagihan;
//                                paramtoPdf.Int2m3 = getDetailPPNWater.Int2m3;
//                                paramtoPdf.Int2Nominal = getDetailPPNWater.Int2Nominal;
//                                paramtoPdf.Int2Tagihan = getDetailPPNWater.Int2Tagihan == null ? "0" : getDetailPPNWater.Int2Tagihan;
//                                paramtoPdf.Int3m3 = getDetailPPNWater.Int3m3;
//                                paramtoPdf.Int3Nominal = getDetailPPNWater.Int3Nominal;
//                                paramtoPdf.Int3Tagihan = getDetailPPNWater.Int3Tagihan == null ? "0" : getDetailPPNWater.Int3Tagihan;
//                                paramtoPdf.PemakaianAirBersih = getDetailPPNWater.PemakaianAirBersih == null ? "0" : getDetailPPNWater.PemakaianAirBersih;
//                                paramtoPdf.BiayaPemeliharaan = getDetailPPNWater.BiayaPemeliharaan == null ? "0" : getDetailPPNWater.BiayaPemeliharaan;
//                                paramtoPdf.JumlahTagihanAir = getDetailPPNWater.WaterTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.WaterTotal);
//                                paramtoPdf.JumlahTagihanListrik = getDetailPPNWater.ElectricTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.ElectricTotal);
//                                paramtoPdf.PerhitunganPemakaianListrik = getDetailPPNWater.PerhitunganPemakaianListrik;
//                                paramtoPdf.BiayaPemakaianListrik = getDetailPPNWater.BiayaPemakaianListrik;



//                            }

//                            //perbaikan pengurutan angka
//                            if (unit.c.TemplateInvoiceName != "BPL & Water")
//                            {

//                                foreach (var detailinvoice in getDetailInvoice)
//                                {
//                                    NoDetail++;
//                                    StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                        "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                                        "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                        "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                                        "</tr>";

//                                    LoopDeskripsiPdf.Add(StingData);

//                                    if (detailinvoice.PPNNominal > 0)
//                                    {
//                                        NoDetail++;
//                                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                           "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                                           "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                           "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                                           "</tr>";

//                                        LoopDeskripsiPdf.Add(StingData);
//                                    }

//                                }

//                                //if (penaltyNominal > 0) {
//                                if (isHavePenalty != null && penaltyNominal >= 0)
//                                {


//                                    NoDetail++;
//                                    StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                          "<td>Penalty </td>" + //deskripsi
//                                          "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                          "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
//                                          "</tr>";


//                                    LoopDeskripsiPdf.Add(StingData);

//                                }

//                            }
//                            else
//                            {
//                                //jika bpl &  water

//                                foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "BPL").ToList())
//                                {
//                                    NoDetail++;
//                                    StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                        "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                                        "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                        "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                                        "</tr>";

//                                    LoopDeskripsiPdf.Add(StingData);

//                                    if (detailinvoice.PPNNominal > 0)
//                                    {
//                                        NoDetail++;
//                                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                           "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                                           "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                           "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                                           "</tr>";

//                                        LoopDeskripsiPdf.Add(StingData);
//                                    }

//                                }

//                                //if (penaltyNominal > 0) {
//                                if (isHavePenalty != null && penaltyNominal >= 0)
//                                {

//                                    NoDetail++;
//                                    StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                          "<td>Penalty </td>" + //deskripsi
//                                          "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                          "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
//                                          "</tr>";


//                                    LoopDeskripsiPdf.Add(StingData);

//                                }


//                                //water 

//                                foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "Water").ToList())
//                                {
//                                    NoDetail++;
//                                    StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                        "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                                        "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                        "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                                        "</tr>";

//                                    LoopDeskripsiPdf.Add(StingData);

//                                    if (detailinvoice.PPNNominal > 0)
//                                    {
//                                        NoDetail++;
//                                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                                           "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                                           "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                                           "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                                           "</tr>";

//                                        LoopDeskripsiPdf.Add(StingData);
//                                    }

//                                }





//                            }






//                            string LoopDeskripsiPdfString = string.Join(Environment.NewLine, LoopDeskripsiPdf.ToArray());

//                            string DeskripsiBPLString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("BPL") || x.Contains("Tagihan Sebelumnya") || x.Contains("Penalty") || x.Contains("Adjustment Fee") || x.Contains("Pembayaran melalui ")).ToList().ToArray());

//                            string DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Water")).ToList().ToArray());


//                            if (unit.c.TemplateInvoiceName == "Electric") {

//                                DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Electric")).ToList().ToArray());

//                            }




//                            paramtoPdf.SiteLogo = getSite.Logo;
//                            paramtoPdf.SiteAddres = getSite.SiteAddress;
//                            paramtoPdf.SiteEmail = getSite.Email;
//                            paramtoPdf.SiteName = getSite.SiteName;
//                            paramtoPdf.SitePhone = "Telp. " + getSite.OfficePhone + " - " + getSite.HandPhone + "";
//                            paramtoPdf.PsCode = unit.a.PsCode;
//                            paramtoPdf.CustName = getPScode.name;
//                            paramtoPdf.CustEmail = getPScode.email;
//                            paramtoPdf.CustPhone = getPhoneCust;
//                            paramtoPdf.CustAlamatKoresponden = getPScode.address;
//                            paramtoPdf.InvoiceDate = inserData.InvoiceDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                            paramtoPdf.NomerInvoice = inserData.InvoiceNo;
//                            paramtoPdf.TagihanSampaiBulanIni = NumberHelper.IndoFormatWithoutTail(inserData.JumlahYangHarusDibayar);
//                            paramtoPdf.JumlahyangHarusDibayar = NumberHelper.IndoFormatWithoutTail(inserData.JumlahYangHarusDibayar);
//                            paramtoPdf.TagihanBulanIni = NumberHelper.IndoFormatWithoutTail(inserData.TagihanPerbulan);
//                            paramtoPdf.UnitName = unit.a.UnitName;
//                            paramtoPdf.UnitNo = unit.a.UnitNo;
//                            paramtoPdf.VaNo = getVa.VirtualAccNo;
//                            paramtoPdf.VaBank = getVa.BankName;
//                            paramtoPdf.Deskripsi = LoopDeskripsiPdfString;
//                            paramtoPdf.InvoiceName = InvoiceNoComb.Replace("/", "");
//                            paramtoPdf.BulanPeriod = getPeriod.PeriodMonth.ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                            paramtoPdf.BulanJatuhTempo = getPeriod.CloseDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                            paramtoPdf.TemplateInvoice = unit.c.TemplateUrl;
//                            paramtoPdf.DeskripsiBPL = DeskripsiBPLString;
//                            paramtoPdf.DeskripsiTagihanAir = DeskripsiWaterString;







//                            GenerateInvoicePdf(paramtoPdf);
//                            #endregion




//                            #endregion
//                        }
//                        catch
//                        {

//                        }
//                    }


//                }



//            }
//            catch
//            {


//            }



//        }







//        public void ChangeAdjustmentInvoice(int InvoiceHeaderId, int adjNominal)
//        {

//            var paramInvoice = new List<int>();

//            var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
//                              join b in _contextBilling.MS_Period on a.PeriodId equals b.Id
//                              where a.Id == InvoiceHeaderId && b.IsActive == true
//                              select a).FirstOrDefault();

//            if (getInvoice == null)
//            {

//                throw new UserFriendlyException("Period InActive");

//            }

//            var addData = new TR_AdjustmentDetail
//            {
//                InvoiceHeaderId =  InvoiceHeaderId,
//                AdjustmentDate = DateTime.Now,
//                AdjustmentNominal = adjNominal,

//            };


//            _contextBilling.TR_AdjustmentDetail.Add(addData);
//            _contextBilling.SaveChanges();

//            paramInvoice.Add(InvoiceHeaderId);


//            ReGenerateInvoiceByInvoiceIdList(paramInvoice);




//        }


//        public ResponseBulkPaymentDto UploadExcelChangeAdjInvoice(UploadExcelChangeAdjInvoiceDto input)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion

//            bool isError = false;
//            string Pesanerror = "";

//            var totalData = input.AdjInvoiceUploadDetailList.Count();
//            int prosesData = 0;
//            int totalgagal = 0;
//            int totalsukses = 0;
//            FileDto file = new FileDto();
//            string URL = "";
//            var gagalDataList = new List<AdjInvoiceUploadDetailDto>();

//            var cekPeriod = (from a in _contextBilling.MS_Period
//                             where a.SiteId == input.SiteId && a.Id == input.PeriodId && a.IsActive == true
//                             select a).FirstOrDefault();

//            if (cekPeriod == null)
//            {
//                isError = true;
//                Pesanerror = "Period InActive";
//            }

//            if (cekPeriod != null)
//            {


//                foreach (var item in input.AdjInvoiceUploadDetailList)
//                {
//                    prosesData++;
//                    var paramInvoice = new List<int>();

//                    var checkUnit = (from a in _contextBilling.MS_UnitData
//                                     join b in _contextBilling.TR_InvoiceHeader on a.Id equals b.UnitDataId
//                                     where a.UnitNo.ToLower() == item.UnitNo.ToLower() && a.UnitCode.ToLower() == item.UnitCode.ToLower()
//                                     && b.PeriodId == input.PeriodId
//                                     && b.InvoiceNo.ToLower() == item.InvoiceNo.ToLower()
//                                     select b).FirstOrDefault();

//                    if (checkUnit != null)
//                    {
//                        //berhasil
//                        totalsukses++;

//                        var addData = new TR_AdjustmentDetail
//                        {
//                            InvoiceHeaderId = checkUnit.Id,
//                            AdjustmentDate = DateTime.Now,
//                            AdjustmentNominal = item.AdjNominal,

//                        };


//                        _contextBilling.TR_AdjustmentDetail.Add(addData);
//                        _contextBilling.SaveChanges();

                   

//                        paramInvoice.Add(checkUnit.Id);

//                        ReGenerateInvoiceByInvoiceIdList(paramInvoice);


//                    }
//                    else
//                    {
//                        //Jika gagal
//                        totalgagal++;
//                        item.RemarksError = "Data tidak ditemukan";
//                        gagalDataList.Add(item);

//                    }

//                }



//            }
//            if (totalgagal > 0)
//            {

//                var paramToExcel = new UploadExcelChangeAdjInvoiceDto
//                {
//                    AdjInvoiceUploadDetailList = gagalDataList

//                };

//                file = _exporter.ExportToExcelErrorUploadAdjResult(paramToExcel);

//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                URL = ipPublic + @"\Temp\Downloads\" + file.FileName;

//            }

//            var final = new ResponseBulkPaymentDto
//            {

//                TotalData = totalData,
//                TotalGagal = totalgagal,
//                TotalSukses = totalsukses,
//                urlDataGagal = URL,
//                isError = isError,
//                PesanError = Pesanerror

//            };

//            return final;

//        }


//        public void ReGenerateInvoiceByInvoiceIdList(List<int> InvoiceHeaderId)
//        {

//            var paramtoPdf = new ParamPDFInvoiceDto();



//            foreach (int invoiceID in InvoiceHeaderId)
//            {

//                var getDataToUpdate = (from a in _contextBilling.TR_InvoiceHeader
//                                       where a.Id == invoiceID
//                                       select a).FirstOrDefault();

//                var getUnit = (from a in _contextBilling.MS_UnitData
//                               join b in _contextBilling.MS_UnitItemHeader.Where(x => x.TemplateInvoiceHeaderId == getDataToUpdate.TemplateInvoiceHeaderId) on a.Id equals b.UnitDataId
//                               join c in _contextBilling.MS_TemplateInvoiceHeader on b.TemplateInvoiceHeaderId equals c.Id
//                               join d in _contextBilling.TR_InvoiceHeader on a.Id equals d.UnitDataId
//                               where d.Id == invoiceID
//                               select new
//                               {
//                                   a,
//                                   b,
//                                   c,
//                                   d
//                               }).FirstOrDefault();


//                var getSite = (from a in _contextBilling.MS_Site
//                               where a.SiteId == getUnit.a.SiteId
//                               select a).FirstOrDefault();

//                //ambil periode terakhir yang aktif di ms_period
//                var getPeriod = (from a in _contextBilling.MS_Period
//                                 where a.Id == getUnit.d.PeriodId
//                                 orderby a.Id descending
//                                 select a).FirstOrDefault();

//                var getPScode = (from a in _contextPersonal.PERSONALS
//                                 join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                 join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
//                                 where a.psCode == getUnit.a.PsCode && c.addrType == "3"
//                                 select new
//                                 {
//                                     a.psCode,
//                                     a.name,
//                                     b.email,
//                                     c.address
//                                 }).FirstOrDefault();

//                if (getPScode == null)
//                {
//                    getPScode = (from a in _contextPersonal.PERSONALS
//                                 join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                 join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
//                                 where a.psCode == getUnit.a.PsCode && c.addrType == "2"
//                                 select new
//                                 {
//                                     a.psCode,
//                                     a.name,
//                                     b.email,
//                                     c.address
//                                 }).FirstOrDefault();
//                }

//                var getPhoneCust = (from a in _contextPersonal.TR_Phone
//                                    where a.psCode == getUnit.a.PsCode && a.refID == 1 && a.phoneType == "3"
//                                    select a.number).FirstOrDefault();



//                var getVa = (from a in _contextBilling.MS_UnitItemHeader
//                             join b in _contextBilling.MS_Bank on a.BankId equals b.Id
//                             where a.TemplateInvoiceHeaderId == getUnit.c.Id && a.SiteId == getUnit.a.SiteId && a.UnitDataId == getUnit.a.Id
//                             select new
//                             {
//                                 a.VirtualAccNo,
//                                 b.BankName
//                             }).FirstOrDefault();

//                #region Perhitungan tagihan

//                decimal tagihanSebelumnya = TagihanSebelumnya(getUnit.a.Id, getPeriod.PeriodMonth, getUnit.a.SiteId, getUnit.d.TemplateInvoiceHeaderId);
//                PembayaranSebelumnyaDto pembayaranSebelumnya = PembayaranSebelumnya(getUnit.a.Id, getPeriod.StartDate, getUnit.a.SiteId, getUnit.d.TemplateInvoiceHeaderId);



//                var getDetailPPNWater = CountTagianBplWater(getUnit.b.Id, getPeriod.Id);


//                int TotalTagiahPerbulan = (Convert.ToInt32(getDetailPPNWater.BPLTotal) + Convert.ToInt32(getDetailPPNWater.WaterTotal) + Convert.ToInt32(getDetailPPNWater.PPNBPL) + Convert.ToInt32(getDetailPPNWater.PPNWater) + Convert.ToInt32(getDetailPPNWater.ElectricTotal));

//                decimal penaltyNominal = 0;
//                var isHavePenalty = (from a in _contextBilling.MS_UnitItemHeader
//                                     join c in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceHeaderId equals c.TemplateInvoiceHeaderId
//                                     join d in _contextBilling.MS_ItemRate on c.ItemRateId equals d.Id
//                                     where a.UnitDataId == getUnit.b.UnitDataId && a.IsPenalty == true && a.TemplateInvoiceHeaderId == getUnit.b.TemplateInvoiceHeaderId
//                                     select new
//                                     {

//                                         a.UnitDataId,
//                                         a.IsPenalty,
//                                         d.PenaltyNominal,
//                                         d.PenaltyPercentage,


//                                     }).FirstOrDefault();

//                paramtoPdf.AdjPen = isHavePenalty != null ? "+ Penalty" : "";

//                if (isHavePenalty != null)
//                {

//                    if (isHavePenalty.PenaltyPercentage > 0)
//                    {
//                        var tagihanSebelumnyaPayment = (tagihanSebelumnya - pembayaranSebelumnya.TotalPembayaran);
//                        penaltyNominal = (tagihanSebelumnyaPayment * isHavePenalty.PenaltyPercentage / 100);

//                        if (penaltyNominal < 0)
//                        {
//                            penaltyNominal = 0;
//                        }
//                    }
//                    else
//                    {
//                        penaltyNominal = isHavePenalty.PenaltyNominal;
//                    }

//                }

//                var getAdjList = (from a in _contextBilling.TR_AdjustmentDetail
//                                   where a.InvoiceHeaderId == invoiceID
//                                   select a).ToList();

//                var adjCount = getAdjList.Select(x => x.AdjustmentNominal).Sum();

//                decimal TotalJumlahYangHarusDibayar = 0;
           
//                    TotalJumlahYangHarusDibayar = (tagihanSebelumnya + TotalTagiahPerbulan + penaltyNominal + adjCount ) - (pembayaranSebelumnya.TotalPembayaran);



//                #endregion



//                getDataToUpdate.InvoiceDate = DateTime.Now;
//                getDataToUpdate.JumlahYangHarusDibayar = TotalJumlahYangHarusDibayar;
//                getDataToUpdate.TagihanPerbulan = TotalTagiahPerbulan;
//                getDataToUpdate.InvoiceNo = getUnit.d.InvoiceNo;
//                getDataToUpdate.IsSendEmailDate = null;
//                getDataToUpdate.IsSendWADate = null;
//                getDataToUpdate.JatuhTempo = getPeriod.CloseDate;
//                getDataToUpdate.InvoiceDoc = getUnit.d.InvoiceNo.Replace("/", "") + ".pdf";
//                getDataToUpdate.PenaltyNominal = penaltyNominal;
//                getDataToUpdate.TagihanBulanSebelumnya = tagihanSebelumnya;
//                getDataToUpdate.VirtualAccNo = getUnit.b.VirtualAccNo;
//                getDataToUpdate.BankId = getUnit.b.BankId;
//                getDataToUpdate.IsPenalty = getUnit.b.IsPenalty;

//                _contextBilling.TR_InvoiceHeader.Update(getDataToUpdate);
//                _contextBilling.SaveChanges();



//                var getitemID = (from a in _contextBilling.MS_UnitItemDetail
//                                 join b in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceDetailId equals b.Id
//                                 where a.UnitItemHeaderId == getUnit.b.Id
//                                 select new
//                                 {
//                                     a.UnitItemHeaderId,
//                                     a.TemplateInvoiceDetailId,
//                                     b.ItemId
//                                 }).ToList();

//                var getDateialInvoice = (from a in _contextBilling.TR_InvoiceDetail
//                                         where a.InvoiceHeaderId == invoiceID
//                                         select a).ToList();

//                _contextBilling.TR_InvoiceDetail.RemoveRange(getDateialInvoice);
//                _contextBilling.SaveChanges();


//                foreach (var item in getitemID)
//                {
//                    var getItemElectric = (from a in _contextBilling.MS_Item
//                                           where a.Id == item.ItemId && a.ItemName == "Electric"
//                                           select a).FirstOrDefault();


//                    var insertDetail = new TR_InvoiceDetail
//                    {
//                        InvoiceHeaderId = invoiceID,
//                        ItemId = item.ItemId,
//                        ItemNominal = getItemElectric != null ? getDetailPPNWater.ElectricTotal : getDetailPPNWater.itemIdBPL == item.ItemId ? getDetailPPNWater.BPLTotal : getDetailPPNWater.WaterTotal,

//                        PPNNominal = getDetailPPNWater.itemIdBPL == item.ItemId ? Convert.ToInt32(getDetailPPNWater.PPNBPL) : Convert.ToInt32(getDetailPPNWater.PPNWater),

//                    };


//                    _contextBilling.TR_InvoiceDetail.Add(insertDetail);
//                    _contextBilling.SaveChanges();




//                }



//                #region Proses generate PDF
//                var LoopDeskripsiPdf = new List<String>();
//                var StingData = "";
//                int NoDetail = 0;

//                if (tagihanSebelumnya != 0)
//                {
//                    NoDetail++;
//                    StingData = " <tr> <td>" + NoDetail + "</td> " + //No
//                  "<td>Tagihan Sebelumnya</td>" + //deskripsi
//                  "<td>-</td>" +  //tanggal
//                  "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(tagihanSebelumnya) + "</td>" +      //jumlah
//                  "</tr>";

//                    LoopDeskripsiPdf.Add(StingData);

//                }




//                if (pembayaranSebelumnya.TotalPembayaran != 0)
//                {
//                    #region getListpembayaran sebelumnya

//                    foreach (var pembayaran in pembayaranSebelumnya.DetailPembayaranSebelumnya)
//                    {
//                        NoDetail++;

//                        StingData = " <tr> <td>" + NoDetail + "</td> " + //No
//                        "<td>Pembayaran melalui " + pembayaran.PaymentMethod + "</td>" + //deskripsi
//                        "<td>" + pembayaran.TransactionDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                        "<td>(Rp. " + NumberHelper.IndoFormatWithoutTail(pembayaran.PaymentAmount) + ")</td>" +      //jumlah
//                        "</tr>";

//                        LoopDeskripsiPdf.Add(StingData);

//                    }

//                    #endregion



//                }

//                if ( getAdjList.Count() > 0)
//                {
//                    #region memiliki AdjustmentNominal


//                    foreach (var itemAdj in getAdjList) 
//                    {

//                        NoDetail++;

//                        var adjNominalString = "";
//                        if (itemAdj.AdjustmentNominal < 0)
//                        {
//                            adjNominalString = "(Rp. " + NumberHelper.IndoFormatWithoutTail(itemAdj.AdjustmentNominal) + ")";
//                        }
//                        else
//                        {
//                            adjNominalString = "Rp. " + NumberHelper.IndoFormatWithoutTail(itemAdj.AdjustmentNominal) + "";


//                        }

//                        StingData = " <tr> <td>" + NoDetail + "</td> " + //No
//                        "<td>Adjustment Fee</td>" + //deskripsi
//                        "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                        "<td>" + adjNominalString + "</td>" +      //jumlah
//                        "</tr>";

//                        LoopDeskripsiPdf.Add(StingData);



//                    }

//                    #endregion


//                }


//                var getDetailInvoice = (from a in _contextBilling.TR_InvoiceDetail
//                                        join b in _contextBilling.MS_Item on a.ItemId equals b.Id
//                                        where a.InvoiceHeaderId == invoiceID
//                                        select new
//                                        {
//                                            a.ItemNominal,
//                                            a.CreationTime,
//                                            a.ItemId,
//                                            a.PPNNominal,
//                                            b.ItemName,
//                                        }).ToList();



//                if (getUnit.c.TemplateInvoiceName == "Water" || getUnit.c.TemplateInvoiceName == "BPL & Water" || getUnit.c.TemplateInvoiceName == "Electric") 
//                { 


//                    var totalBPL = (getDetailPPNWater.BPLTotal + getDetailPPNWater.PPNBPL + penaltyNominal);
//                    var totalWater = (getDetailPPNWater.WaterTotal + getDetailPPNWater.PPNWater);

//                    paramtoPdf.totalBpl = totalBPL == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalBPL);
//                    paramtoPdf.totalWater = totalWater == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalWater);


//                    paramtoPdf.PencatatanAwal = getDetailPPNWater.PencatatanAwal == null ? "0" : getDetailPPNWater.PencatatanAwal;
//                    paramtoPdf.PencatatanAkhir = getDetailPPNWater.PencatatanAkhir == null ? "0" : getDetailPPNWater.PencatatanAkhir;
//                    paramtoPdf.Pemakaian = getDetailPPNWater.Pemakaian == null ? "0" : getDetailPPNWater.Pemakaian;
//                    paramtoPdf.Int1m3 = getDetailPPNWater.Int1m3;
//                    paramtoPdf.Int1Nominal = getDetailPPNWater.Int1Nominal;
//                    paramtoPdf.Int1Tagihan = getDetailPPNWater.Int1Tagihan == null ? "0" : getDetailPPNWater.Int1Tagihan;
//                    paramtoPdf.Int2m3 = getDetailPPNWater.Int2m3;
//                    paramtoPdf.Int2Nominal = getDetailPPNWater.Int2Nominal;
//                    paramtoPdf.Int2Tagihan = getDetailPPNWater.Int2Tagihan == null ? "0" : getDetailPPNWater.Int2Tagihan;
//                    paramtoPdf.Int3m3 = getDetailPPNWater.Int3m3;
//                    paramtoPdf.Int3Nominal = getDetailPPNWater.Int3Nominal;
//                    paramtoPdf.Int3Tagihan = getDetailPPNWater.Int3Tagihan == null ? "0" : getDetailPPNWater.Int3Tagihan;
//                    paramtoPdf.PemakaianAirBersih = getDetailPPNWater.PemakaianAirBersih == null ? "0" : getDetailPPNWater.PemakaianAirBersih;
//                    paramtoPdf.BiayaPemeliharaan = getDetailPPNWater.BiayaPemeliharaan == null ? "0" : getDetailPPNWater.BiayaPemeliharaan;
//                    paramtoPdf.JumlahTagihanAir = getDetailPPNWater.WaterTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.WaterTotal);
//                    paramtoPdf.JumlahTagihanListrik = getDetailPPNWater.ElectricTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.ElectricTotal);
//                    paramtoPdf.PerhitunganPemakaianListrik = getDetailPPNWater.PerhitunganPemakaianListrik;
//                    paramtoPdf.BiayaPemakaianListrik = getDetailPPNWater.BiayaPemakaianListrik;


//                }


//                //perbaikan pengurutan angka
//                if (getUnit.c.TemplateInvoiceName != "BPL & Water")
//                {

//                    foreach (var detailinvoice in getDetailInvoice)
//                    {
//                        NoDetail++;
//                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                            "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                            "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                            "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                            "</tr>";

//                        LoopDeskripsiPdf.Add(StingData);

//                        if (detailinvoice.PPNNominal > 0)
//                        {
//                            NoDetail++;
//                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                               "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                               "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                               "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                               "</tr>";

//                            LoopDeskripsiPdf.Add(StingData);
//                        }

//                    }

//                    //if (penaltyNominal > 0) {
//                    if (isHavePenalty != null && penaltyNominal >= 0)
//                    {


//                        NoDetail++;
//                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                              "<td>Penalty </td>" + //deskripsi
//                              "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                              "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
//                              "</tr>";


//                        LoopDeskripsiPdf.Add(StingData);

//                    }

//                }
//                else
//                {
//                    //jika bpl &  water

//                    foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "BPL").ToList())
//                    {
//                        NoDetail++;
//                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                            "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                            "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                            "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                            "</tr>";

//                        LoopDeskripsiPdf.Add(StingData);

//                        if (detailinvoice.PPNNominal > 0)
//                        {
//                            NoDetail++;
//                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                               "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                               "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                               "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                               "</tr>";

//                            LoopDeskripsiPdf.Add(StingData);
//                        }

//                    }

//                    //if (penaltyNominal > 0) {
//                    if (isHavePenalty != null && penaltyNominal >= 0)
//                    {

//                        NoDetail++;
//                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                              "<td>Penalty </td>" + //deskripsi
//                              "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                              "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
//                              "</tr>";


//                        LoopDeskripsiPdf.Add(StingData);

//                    }


//                    //water 

//                    foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "Water").ToList())
//                    {
//                        NoDetail++;
//                        StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                            "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
//                            "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                            "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
//                            "</tr>";

//                        LoopDeskripsiPdf.Add(StingData);

//                        if (detailinvoice.PPNNominal > 0)
//                        {
//                            NoDetail++;
//                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
//                               "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
//                               "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
//                               "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
//                               "</tr>";

//                            LoopDeskripsiPdf.Add(StingData);
//                        }

//                    }





//                }






//                string LoopDeskripsiPdfString = string.Join(Environment.NewLine, LoopDeskripsiPdf.ToArray());

//                string DeskripsiBPLString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("BPL") || x.Contains("Penalty") || x.Contains("Adjustment Fee") || x.Contains("Pembayaran melalui ") || x.Contains("Tagihan Sebelumnya")).ToList().ToArray());

//                string DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Water")).ToList().ToArray());

//                if (getUnit.c.TemplateInvoiceName == "Electric")
//                {

//                    DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Electric")).ToList().ToArray());

//                }





//                paramtoPdf.SiteLogo = getSite.Logo;
//                paramtoPdf.SiteAddres = getSite.SiteAddress;
//                paramtoPdf.SiteEmail = getSite.Email;
//                paramtoPdf.SiteName = getSite.SiteName;
//                paramtoPdf.SitePhone = "Telp. " + getSite.OfficePhone + " - " + getSite.HandPhone + "";
//                paramtoPdf.PsCode = getUnit.a.PsCode;
//                paramtoPdf.CustName = getPScode.name;
//                paramtoPdf.CustEmail = getPScode.email;
//                paramtoPdf.CustPhone = getPhoneCust;
//                paramtoPdf.CustAlamatKoresponden = getPScode.address;
//                paramtoPdf.InvoiceDate = DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                paramtoPdf.NomerInvoice = getUnit.d.InvoiceNo;
//                paramtoPdf.TagihanSampaiBulanIni = NumberHelper.IndoFormatWithoutTail(TotalJumlahYangHarusDibayar);
//                paramtoPdf.JumlahyangHarusDibayar = NumberHelper.IndoFormatWithoutTail(TotalJumlahYangHarusDibayar);
//                paramtoPdf.TagihanBulanIni = NumberHelper.IndoFormatWithoutTail(TotalTagiahPerbulan);
//                paramtoPdf.UnitName = getUnit.a.UnitName;
//                paramtoPdf.UnitNo = getUnit.a.UnitNo;
//                paramtoPdf.VaNo = getVa.VirtualAccNo;
//                paramtoPdf.VaBank = getVa.BankName;
//                paramtoPdf.Deskripsi = LoopDeskripsiPdfString;
//                paramtoPdf.InvoiceName = getUnit.d.InvoiceNo.Replace("/", "");
//                paramtoPdf.BulanPeriod = getPeriod.PeriodMonth.ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                paramtoPdf.BulanJatuhTempo = getPeriod.CloseDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
//                paramtoPdf.TemplateInvoice = getUnit.c.TemplateUrl;
//                paramtoPdf.DeskripsiBPL = DeskripsiBPLString;
//                paramtoPdf.DeskripsiTagihanAir = DeskripsiWaterString;







//                GenerateInvoicePdf(paramtoPdf);
//                #endregion




//            }




//        }



//        private string GenerateInvoicePdf(ParamPDFInvoiceDto input)
//        {

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var pdfOutputPath = connectionApp.Property("pdfOutputInvoice").Value.ToString();


//            var destinationPath = pdfOutputPath;

//            if (!Directory.Exists(destinationPath))
//            {
//                Directory.CreateDirectory(destinationPath);
//            }

//            var fileName = "" + input.InvoiceName + ".pdf";
//            var filePath = Path.Combine(destinationPath, fileName);
//            try
//            {
//                var getLogoSite = _hostEnvironment.WebRootPath + @"\Assets\Upload\LogoSite\" + input.SiteLogo + "";
//                var templateFile = _hostEnvironment.WebRootPath + input.TemplateInvoice;
//                var htmlToConvert = File.ReadAllText(templateFile);


//                HtmlToPdf converter = new HtmlToPdf();
//                converter.Options.PdfPageSize = PdfPageSize.A4;
//                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;





//                var doc = converter.ConvertHtmlString(htmlToConvert

//                .Replace("{{AdjPen}}", input.AdjPen)
//                .Replace("{{SiteLogo}}", getLogoSite)
//                .Replace("{{SiteName}}", input.SiteName)
//                .Replace("{{SiteAddres}}", input.SiteAddres)
//                .Replace("{{SitePhone}}", input.SitePhone)
//                .Replace("{{SiteEmail}}", input.SiteEmail)

//                .Replace("{{PsCode}}", input.PsCode)
//                .Replace("{{CustName}}", input.CustName)
//                .Replace("{{CustPhone}}", input.CustPhone)
//                .Replace("{{CustEmail}}", input.CustEmail)
//                .Replace("{{CustAlamatKoresponden}}", input.CustAlamatKoresponden)
//                .Replace("{{NomerInvoice}}", input.NomerInvoice)
//                .Replace("{{InvoiceDate}}", input.InvoiceDate)
//                .Replace("{{UnitName}}", input.UnitName)
//                .Replace("{{UnitNo}}", input.UnitNo)
//                .Replace("{{Deskripsi}}", input.Deskripsi)
//                .Replace("{{VaNo}}", input.VaNo)
//                .Replace("{{VaBank}}", input.VaBank)
//                .Replace("{{TagihanBulanIni}}", input.TagihanBulanIni)
//                .Replace("{{TagihanSampaiBulanIni}}", input.TagihanSampaiBulanIni)
//                .Replace("{{JumlahyangHarusDibayar}}", input.JumlahyangHarusDibayar)
//                .Replace("{{BulanPeriod}}", input.BulanPeriod)
//                .Replace("{{BulanJatuhTempo}}", input.BulanJatuhTempo)

//                .Replace("{{PencatatanAwal}}", input.PencatatanAwal)
//                .Replace("{{PencatatanAkhir}}", input.PencatatanAkhir)
//                .Replace("{{Pemakaian}}", input.Pemakaian)
//                .Replace("{{Int1m3}}", input.Int1m3)
//                .Replace("{{Int1Nominal}}", input.Int1Nominal)
//                .Replace("{{Int1Tagihan}}", input.Int1Tagihan)
//                .Replace("{{Int2m3}}", input.Int2m3)
//                .Replace("{{Int2Nominal}}", input.Int2Nominal)
//                .Replace("{{Int2Tagihan}}", input.Int2Tagihan)
//                .Replace("{{Int3m3}}", input.Int3m3)
//                .Replace("{{Int3Nominal}}", input.Int3Nominal)
//                .Replace("{{Int3Tagihan}}", input.Int3Tagihan)
//                .Replace("{{PemakaianAirBersih}}", input.PemakaianAirBersih)
//                .Replace("{{BiayaPemeliharaan}}", input.BiayaPemeliharaan)
//                .Replace("{{JumlahTagihanAir}}", input.JumlahTagihanAir)
//                .Replace("{{DeskripsiBPL}}", input.DeskripsiBPL)
//                .Replace("{{DeskripsiTagihanAir}}", input.DeskripsiTagihanAir)
//                .Replace("{{totalBpl}}", input.totalBpl)
//                .Replace("{{totalWater}}", input.totalWater)
//                .Replace("{{PerhitunganPemakaianListrik}}", input.PerhitunganPemakaianListrik)
//                .Replace("{{JumlahTagihanListrik}}", input.JumlahTagihanListrik)
//                   .Replace("{{BiayaPemakaianListrik}}", input.BiayaPemakaianListrik)




//);
//                String hasil = doc.ToString();

//                var pdfhtml = doc;
//                byte[] results;

//                using (var stream = new MemoryStream())
//                {
//                    doc.Save(stream);
//                    results = stream.ToArray();
//                    doc.Close();
//                }

//                if (File.Exists(filePath))
//                {
//                    File.Delete(filePath);
//                }

//                File.WriteAllBytes(filePath, results);

//            }
//            catch (Exception ex)
//            {

//            }

//            return filePath;

//        }




//        public CountItemRateDeatail CountTagianBplWater(int UnitItemHeaderId, int periodID)
//        {

//            var final = new CountItemRateDeatail();


//            //get deatail dia punya apa aja detailnya
//            var getMainData = (from a in _contextBilling.MS_UnitItemHeader
//                               join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id
//                               join e in _contextBilling.MS_UnitItemDetail on a.Id equals e.UnitItemHeaderId
//                               join c in _contextBilling.MS_TemplateInvoiceDetail on e.TemplateInvoiceDetailId equals c.Id
//                               join d in _contextBilling.MS_Item on c.ItemId equals d.Id
                         
//                               where a.Id == UnitItemHeaderId
//                               select new
//                               {
//                                   a.UnitDataId,
//                                   a.SiteId,
//                                   b.TemplateInvoiceName,
//                                   c.ItemId,
//                                   c.ItemRateId,
//                                   templateInvoiceDetailID = c.Id,
//                                   d.ItemName,
//                                   d.AttributeId,

//                               }).Distinct().ToList();




//            foreach (var main in getMainData)
//            {
//                #region main code

//                if (main.ItemName == "BPL")
//                {
//                    //pengambilan item detail ada apa aja ; 
//                    var getItemDetail = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                         join b in _contextBilling.MS_UnitItemDetail on a.Id equals b.TemplateInvoiceDetailId
//                                         where a.Id == main.templateInvoiceDetailID && b.UnitItemHeaderId == UnitItemHeaderId
//                                         select new
//                                         {
//                                             a,
//                                             b
//                                         }).FirstOrDefault();

//                    var getItemRate = (from a in _contextBilling.MS_ItemRate
//                                       where a.Id == getItemDetail.a.ItemRateId
//                                       select a).FirstOrDefault();

//                    if (getItemDetail.b.RateNominal == 0)
//                    {


//                        //jika pakai BPL Nominal
//                        if (getItemRate.FixRate == true)
//                        {
//                            double bplNominal = 0;

//                            //jika true maka akan langsung menggunakan lumpsum
//                            if (getItemRate.VATPercentage != 0) {

//                                bplNominal = getItemRate.LumpSumRate * 10 / 11.1;
//                            }
//                            else {

//                                bplNominal = getItemRate.LumpSumRate;
                            
//                            }



//                            int doubleRound = (int)Math.Round(bplNominal, 0);


//                            var Countbpl = doubleRound.ToString().Length - 1; //5


//                            var bplFinalNominal = doubleRound.ToString().Substring(0, Countbpl) + 0;


//                            final.BPLTotal = Convert.ToInt32(bplFinalNominal);

//                        }
//                        else
//                        {

//                            var getUnit = (from a in _contextBilling.MS_UnitData
//                                           where a.Id == main.UnitDataId
//                                           select a).FirstOrDefault();


//                            //total land = land * landRate
//                            var totalLand = (getUnit.LandArea * getItemRate.LandRate);

//                            //total build = build * buildRate
//                            var totalBuild = (getUnit.BuildArea * getItemRate.BuildRate);

//                            var total = (totalLand + totalBuild);

//                            double bplNominal = 0;

//                            //jika true maka akan langsung menggunakan lumpsum
//                            if (getItemRate.VATPercentage != 0)
//                            {

//                                bplNominal = total * 10 / 11.1;
//                            }
//                            else
//                            {

//                                bplNominal = total;

//                            }


//                            int doubleRound = (int)Math.Round(bplNominal, 0); //567568

//                            var Countbpl = doubleRound.ToString().Length - 1; //5


//                            var bplFinalNominal = doubleRound.ToString().Substring(0, Countbpl) + 0;


//                            final.BPLTotal = Convert.ToInt32(bplFinalNominal);


//                        }
//                    }
//                    else
//                    {
//                        double bplNominal = 0;

//                        //jika true maka akan langsung menggunakan lumpsum
//                        if (getItemRate.VATPercentage != 0)
//                        {

//                            bplNominal = getItemDetail.b.RateNominal * 10 / 11.1;
//                        }
//                        else
//                        {

//                            bplNominal = getItemDetail.b.RateNominal;

//                        }


//                        final.BPLTotal = Convert.ToInt32(bplNominal);

//                    }

//                    double ppnNominal = (final.BPLTotal * getItemRate.VATPercentage / 100);


//                    int ppnRound = (int)Math.Round(ppnNominal, 0);

//                    final.PPNBPL = ppnRound;
//                    final.itemIdBPL = main.ItemId;


//                }
//                if (main.ItemName == "Water")
//                {

//                    var getItemDetail = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                         join b in _contextBilling.MS_UnitItemDetail on a.Id equals b.TemplateInvoiceDetailId
//                                         where a.Id == main.templateInvoiceDetailID && b.UnitItemHeaderId == UnitItemHeaderId
//                                         select new
//                                         {
//                                             a,
//                                             b
//                                         }).FirstOrDefault();

//                    if (getItemDetail != null)
//                    {
//                        var getAbodemen = (from a in _contextBilling.MS_Abodemen
//                                           join b in _contextBilling.MS_Attribute on a.AttributeId equals b.Id
//                                           where a.SiteId == main.SiteId && a.AttributeId == main.AttributeId
//                                           select a.AbodemenNominal).FirstOrDefault();

//                        getAbodemen = getAbodemen == 0 ? 0 : getAbodemen;

//                        var getItemRate = (from a in _contextBilling.MS_ItemRate
//                                           where a.Id == getItemDetail.a.ItemRateId
//                                           select a).FirstOrDefault();

//                        var getUnit = (from a in _contextBilling.MS_UnitData
//                                       where a.Id == main.UnitDataId
//                                       select a).FirstOrDefault();

//                        var getWaterReading = (from a in _contextBilling.TR_WaterReading
//                                               where a.ProjectId == getUnit.ProjectId && a.ClusterId == getUnit.ClusterId
//                                               && a.UnitNo == getUnit.UnitNo && a.UnitCode == getUnit.UnitCode && a.SiteId == main.SiteId
//                                               && a.PeriodId == periodID
//                                               select a).FirstOrDefault();

//                        if (getItemDetail.b.RateNominal == 0)
//                        {


//                            //jika pakai BPL Nominal
//                            if (getItemRate.FixRate == true)
//                            {

//                                //jika true maka akan langsung menggunakan lumpsum
//                                final.WaterTotal = getItemRate.LumpSumRate + getAbodemen;

//                            }
//                            else
//                            {

//                                var getWaterReadingCurrentRead = getWaterReading == null ? 0 : getWaterReading.CurrentRead;
//                                var getWaterReadingPrevRead = getWaterReading == null ? 0 : getWaterReading.PrevRead;


//                                var pemakaian = (getWaterReadingCurrentRead - getWaterReadingPrevRead);

//                                final.Pemakaian = pemakaian.ToString();
//                                final.PencatatanAwal = getWaterReadingPrevRead.ToString();
//                                final.PencatatanAkhir = getWaterReadingCurrentRead.ToString();

//                                if (pemakaian == 0)
//                                {

//                                    var tagihan = (pemakaian * getItemRate.Range1Nominal);


//                                    final.WaterTotal = (0 + getAbodemen);


//                                }
//                                else if (pemakaian >= getItemRate.Range1Start && pemakaian <= getItemRate.Range1End)
//                                {

//                                    var tagihan = (pemakaian * getItemRate.Range1Nominal);


//                                    final.Int1Tagihan = tagihan == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(tagihan);
//                                    final.PemakaianAirBersih = NumberHelper.IndoFormatWithoutTail(tagihan);

//                                    final.WaterTotal = ((pemakaian * getItemRate.Range1Nominal) + getAbodemen);


//                                }
//                                else if (pemakaian >= getItemRate.Range2Start && pemakaian <= getItemRate.Range2End)
//                                {

//                                    var tagihan = (pemakaian * getItemRate.Range2Nominal);


//                                    final.Int2Tagihan = tagihan == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(tagihan);
//                                    final.PemakaianAirBersih = NumberHelper.IndoFormatWithoutTail(tagihan);

//                                    final.WaterTotal = ((pemakaian * getItemRate.Range2Nominal) + getAbodemen);

//                                }
//                                else if (pemakaian >= getItemRate.Range3Start)
//                                {

//                                    var tagihan = (pemakaian * getItemRate.Range3Nominal);


//                                    final.Int3Tagihan = tagihan == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(tagihan);
//                                    final.PemakaianAirBersih = NumberHelper.IndoFormatWithoutTail(tagihan);

//                                    final.WaterTotal = ((pemakaian * getItemRate.Range3Nominal) + getAbodemen);

//                                }

//                                final.Int1m3 = "" + getItemRate.Range1Start + " - " + getItemRate.Range1End + " m3";
//                                final.Int1Nominal = NumberHelper.IndoFormatWithoutTail(getItemRate.Range1Nominal);
//                                final.Int2m3 = "" + getItemRate.Range2Start + " - " + getItemRate.Range2End + " m3";
//                                final.Int2Nominal = NumberHelper.IndoFormatWithoutTail(getItemRate.Range2Nominal);
//                                final.Int3m3 = ">" + getItemRate.Range3Start + " m3";
//                                final.Int3Nominal = NumberHelper.IndoFormatWithoutTail(getItemRate.Range3Nominal);




//                            }
//                        }
//                        else
//                        {

//                            final.WaterTotal = getItemDetail.b.RateNominal;

//                        }

//                        if (getItemRate.VATPercentage != 0)
//                        {
//                            //final.PPNWater = (final.WaterTotal * getItemRate.VATPercentage / 100);
//                        }

//                        final.itemIdWater = main.ItemId;

//                        final.BiayaPemeliharaan = NumberHelper.IndoFormatWithoutTail(getAbodemen);






//                    }


//                }
//                if (main.ItemName == "Electric")
//                {

//                    var getItemDetail = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                         join b in _contextBilling.MS_UnitItemDetail on a.Id equals b.TemplateInvoiceDetailId
//                                         where a.Id == main.templateInvoiceDetailID && b.UnitItemHeaderId == UnitItemHeaderId
//                                         select new
//                                         {
//                                             a,
//                                             b
//                                         }).FirstOrDefault();

//                    if (getItemDetail != null)
//                    {
//                        var getAbodemen = (from a in _contextBilling.MS_Abodemen
//                                           join b in _contextBilling.MS_Attribute on a.AttributeId equals b.Id
//                                           where a.SiteId == main.SiteId && a.AttributeId == main.AttributeId
//                                           select a.AbodemenNominal).FirstOrDefault();

//                        getAbodemen = getAbodemen == 0 ? 0 : getAbodemen;

//                        var getItemRate = (from a in _contextBilling.MS_ItemRate
//                                           where a.Id == getItemDetail.a.ItemRateId
//                                           select a).FirstOrDefault();

//                        var getUnit = (from a in _contextBilling.MS_UnitData
//                                       where a.Id == main.UnitDataId
//                                       select a).FirstOrDefault();

//                        var getElectricReading = (from a in _contextBilling.TR_ElectricReading
//                                               where a.ProjectId == getUnit.ProjectId && a.ClusterId == getUnit.ClusterId
//                                               && a.UnitNo == getUnit.UnitNo && a.UnitCode == getUnit.UnitCode && a.SiteId == main.SiteId
//                                               && a.PeriodId == periodID
//                                               select a).FirstOrDefault();

//                        if (getItemDetail.b.RateNominal == 0)
//                        {


//                            //jika pakai BPL Nominal
//                            if (getItemRate.FixRate == true)
//                            {

//                                //jika true maka akan langsung menggunakan lumpsum
//                                final.ElectricTotal = getItemRate.LumpSumRate + getAbodemen;

//                            }
//                            else
//                            {

//                                var getElectricReadingCurrentRead = getElectricReading == null ? 0 : getElectricReading.CurrentRead;
//                                var getElectricReadingPrevRead = getElectricReading == null ? 0 : getElectricReading.PrevRead;


//                                var pemakaian = (getElectricReadingCurrentRead - getElectricReadingPrevRead);

//                                final.Pemakaian = pemakaian.ToString();
//                                final.PencatatanAwal = getElectricReadingPrevRead.ToString();
//                                final.PencatatanAkhir = getElectricReadingCurrentRead.ToString();

//                                if (pemakaian == 0)
//                                {

//                                    var tagihan = (pemakaian * getItemRate.Range1Nominal);

//                                    final.BiayaPemakaianListrik = tagihan == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(tagihan);


//                                    final.ElectricTotal = (0 + getAbodemen);


//                                }
//                                else 
//                                {

//                                    var tagihan = (pemakaian * getItemRate.RateElectric);


//                                    final.BiayaPemakaianListrik = tagihan == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(tagihan);
                      

//                                    final.ElectricTotal = (tagihan + getAbodemen);


//                                }


//                            }
//                        }
//                        else
//                        {

//                            final.ElectricTotal = getItemDetail.b.RateNominal;

//                        }

//                        //if (getItemRate.VATPercentage != 0)
//                        //{
//                        //    final.PPNWater = (final.WaterTotal * getItemRate.VATPercentage / 100);
//                        //}

//                        final.itemIdWater = main.ItemId;

//                        final.BiayaPemeliharaan = NumberHelper.IndoFormatWithoutTail(getAbodemen);
//                        final.PerhitunganPemakaianListrik = NumberHelper.IndoFormatWithoutTail(getItemRate.RateElectric);






//                    }


//                }

//                #endregion


//            }




//            return final;
//        }



//        public decimal TagihanSebelumnya(int UnitDataID, DateTime StartDate, int SiteID, int TemplateInvoiceHeaderID)
//        {

//            decimal final = 0;
//            //mengambil tagihan bulan lalu 
//            DateTime periodLalu = StartDate.AddMonths(-1);
//            decimal getdataJumlah = 0;


//            var getPeriod = (from a in _contextBilling.MS_Period
//                             where a.StartDate.Month == periodLalu.Month && a.PeriodYear.Year == StartDate.Year && a.SiteId == SiteID
//                             select a).FirstOrDefault();

//            if (getPeriod != null)
//            {

//                var getdata = (from a in _contextBilling.TR_InvoiceHeader
//                               where a.UnitDataId == UnitDataID && a.PeriodId == getPeriod.Id
//                               select a.JumlahYangHarusDibayar).ToList();


//                if (getdata.Count > 1)
//                {
//                    getdataJumlah = (from a in _contextBilling.TR_InvoiceHeader
//                                     where a.UnitDataId == UnitDataID && a.PeriodId == getPeriod.Id && a.TemplateInvoiceHeaderId == TemplateInvoiceHeaderID
//                                     select a.JumlahYangHarusDibayar).FirstOrDefault();


//                    if (getdataJumlah == 0)
//                    {
//                        getdataJumlah = (from a in _contextBilling.TR_InvoiceHeader
//                                         where a.UnitDataId == UnitDataID && a.PeriodId == getPeriod.Id
//                                         select a.JumlahYangHarusDibayar).FirstOrDefault();

//                    }
//                }
//                else
//                {

//                    getdataJumlah = (from a in _contextBilling.TR_InvoiceHeader
//                                     where a.UnitDataId == UnitDataID && a.PeriodId == getPeriod.Id
//                                     select a.JumlahYangHarusDibayar).FirstOrDefault();

//                }


//                final = getdataJumlah;

//            }



//            return final;
//        }

//        public PembayaranSebelumnyaDto PembayaranSebelumnya(int UnitDataID, DateTime StartDate, int SiteID, int TemplateInvoiceHeaderID)
//        {

//            var final = new PembayaranSebelumnyaDto();
//            //mengambil tagihan bulan lalu 
//            DateTime periodLalu = StartDate.AddMonths(-1);


//            var getPeriod = (from a in _contextBilling.MS_Period
//                             where a.StartDate.Month == periodLalu.Month && a.PeriodYear.Year == StartDate.Year && a.SiteId == SiteID
//                             orderby a.Id descending
//                             select a).FirstOrDefault();



//            if (getPeriod != null)
//            {

//                var getInvoice = 0;
//                var getInvoiceData = (from a in _contextBilling.TR_InvoiceHeader
//                                      where a.PeriodId == getPeriod.Id && a.UnitDataId == UnitDataID
//                                      select a).ToList();

//                if (getInvoiceData.Count > 1)
//                {
//                    getInvoice = (from a in _contextBilling.TR_InvoiceHeader
//                                  where a.PeriodId == getPeriod.Id && a.UnitDataId == UnitDataID && a.TemplateInvoiceHeaderId == TemplateInvoiceHeaderID
//                                  select a.Id).FirstOrDefault();

//                }
//                else
//                {


//                    getInvoice = (from a in _contextBilling.TR_InvoiceHeader
//                                  where a.PeriodId == getPeriod.Id && a.UnitDataId == UnitDataID
//                                  select a.Id).FirstOrDefault();



//                }

//                var GetInvoiceAmount = (from a in _contextBilling.TR_InvoiceHeader
//                                        join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.InvoiceHeaderId
//                                        join c in _contextBilling.TR_BillingPaymentHeader on b.BillingHeaderId equals c.Id
//                                        join e in _contextBilling.MS_PaymentMethod on c.PaymentMethodId equals e.Id
//                                        join f in _contextBilling.MS_Bank on c.BankId equals f.Id into banknull
//                                        from banktemp in banknull.DefaultIfEmpty()
//                                        where a.PeriodId == getPeriod.Id && a.UnitDataId == UnitDataID && c.CancelPayment == false
//                                  && c.TransactionDate > getPeriod.EndDate && c.TransactionDate <= getPeriod.CloseDate && a.Id == getInvoice
//                                        select new DetailPembayaranSebelumnyaDto
//                                        {
//                                            InvoiceHeaderId = b.InvoiceHeaderId,
//                                            PaymentMethod = banktemp.BankName == null ? e.PaymentMethodName : e.PaymentMethodName + " - " + banktemp.BankName,
//                                            PaymentAmount = b.PaymentAmount,
//                                            TransactionDate = c.TransactionDate
//                                        }).ToList();

//                if (GetInvoiceAmount != null)
//                {

//                    final = new PembayaranSebelumnyaDto
//                    {
//                        TotalPembayaran = GetInvoiceAmount.Select(x => x.PaymentAmount).Sum(),
//                        DetailPembayaranSebelumnya = GetInvoiceAmount
//                    };

//                }



//            }



//            return final;
//        }


//        public PembayaranSebelumnyaDto SPGetPembayaranSebelumnya(SPGetPembayaranSebelumnya input)
//        {

//            var final = new PembayaranSebelumnyaDto();

//            //bulan demo sementara, jika real data pakai daterime now 
//            var bulan = DateTime.Now.AddMonths(-2);


//            var GetInvoiceAmount = (from a in _contextBilling.TR_InvoiceHeader
//                                    join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.InvoiceHeaderId
//                                    join c in _contextBilling.TR_BillingPaymentHeader on b.BillingHeaderId equals c.Id
//                                    join e in _contextBilling.MS_PaymentMethod on c.PaymentMethodId equals e.Id
//                                    where a.Id == input.invoiceHeaderId && a.PeriodId == input.PeriodId && a.UnitDataId == input.UnitDataID && c.CancelPayment == false && c.TransactionDate > input.EndDatePeriod && c.TransactionDate < bulan
//                                    select new DetailPembayaranSebelumnyaDto
//                                    {
//                                        InvoiceHeaderId = b.InvoiceHeaderId,
//                                        PaymentMethod = e.PaymentMethodName,
//                                        PaymentAmount = b.PaymentAmount,
//                                        TransactionDate = c.TransactionDate
//                                    }).ToList();

//            if (GetInvoiceAmount != null)
//            {

//                final = new PembayaranSebelumnyaDto
//                {
//                    TotalPembayaran = GetInvoiceAmount.Select(x => x.PaymentAmount).Sum(),
//                    DetailPembayaranSebelumnya = GetInvoiceAmount
//                };

//            }




//            return final;
//        }


//        public async Task SendEmailInvoiceByInvoiceHeaderId(List<int> InvoiceHeaderId)
//        {


//            DateTime timeStartCall = DateTime.Now;
//            var sb = new StringBuilder();

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var webConfigApp = (JObject)appsettingsjson["App"];
//            var EmailEnvironment = webConfigApp.Property("EmailEnvironment").Value.ToString();
//            var EmailTo = webConfigApp.Property("EmailTo").Value.ToString();

//            foreach (var item in InvoiceHeaderId)
//            {
//                try
//                {

//                    var getData = (from a in _contextBilling.TR_InvoiceHeader
//                                   join b in _contextBilling.TR_InvoiceDetail on a.Id equals b.InvoiceHeaderId
//                                   join c in _contextBilling.MS_Item on b.ItemId equals c.Id
//                                   //join d in _contextBilling.MS_UnitItemHeader on a.TemplateInvoiceHeaderId equals d.TemplateInvoiceHeaderId
//                                   join e in _contextBilling.MS_Bank on a.BankId equals e.Id
//                                   join f in _contextBilling.MS_UnitData on a.UnitDataId equals f.Id
//                                   join g in _contextBilling.MS_Period on a.PeriodId equals g.Id
//                                   join h in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals h.Id
//                                   join i in _contextBilling.MS_Site on a.SiteId equals i.SiteId

//                                   where a.Id == item
//                                   select new
//                                   {
//                                       invoiceHeaderId = a.Id,
//                                       a.InvoiceNo,
//                                       a.InvoiceDoc,
//                                       a.PsCode,
//                                       b.ItemId,
//                                       c.ItemName,
//                                       a.VirtualAccNo,
//                                       e.BankName,
//                                       a.JumlahYangHarusDibayar,
//                                       f.UnitName,
//                                       f.UnitNo,
//                                       f.UnitCode,
//                                       h.TemplateInvoiceName,
//                                       i.SiteName,


//                                       bulanPeriode = g.PeriodMonth.ToString("MMMM yyyy")
//                                   }).FirstOrDefault();

//                    // var getUnit = getData.FirstOrDefault();


//                    var getEmailUser = (from a in _contextPersonal.PERSONALS
//                                        join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                        where a.psCode == getData.PsCode
//                                        select new
//                                        {
//                                            a.psCode,
//                                            a.name,
//                                            b.email
//                                        }).FirstOrDefault();



//                    List<string> to = new List<string>();
//                    List<string> cc = new List<string>();
//                    List<string> bcc = new List<string>();
//                    if (EmailEnvironment == "1")// dev {}
//                    {
//                        if (!String.IsNullOrEmpty(EmailTo)) //item.EmailAddress
//                            to.AddRange(EmailTo.Split(';'));

//                        if (!String.IsNullOrEmpty(""))
//                            cc.AddRange("".Split(';'));

//                        if (!String.IsNullOrEmpty(""))
//                            bcc.AddRange("".Split(';'));

//                    }
//                    else
//                    {

//                        if (!String.IsNullOrEmpty(getEmailUser.email))
//                            to.AddRange(getEmailUser.email.Split(';'));

//                        if (!String.IsNullOrEmpty(""))
//                            cc.AddRange("".Split(';'));

//                        if (!String.IsNullOrEmpty(""))
//                            bcc.AddRange("".Split(';'));

//                    }


//                    var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\EmailTemplate\email-invoice.html";
//                    var htmlToConvert = File.ReadAllText(templateFile);

//                    var bodyEmail = htmlToConvert.Replace("{{CustName}}", getEmailUser.name)
//                        .Replace("{{UnitCode}}", getData.UnitCode)
//                        .Replace("{{UnitNo}}", getData.UnitNo)
//                        .Replace("{{BulanPeriod}}", getData.bulanPeriode)
//                        .Replace("{{invoiceNo}}", getData.InvoiceNo)
//                        .Replace("{{invoiceType}}", getData.TemplateInvoiceName)
//                        .Replace("{{totalPembayaran}}", NumberHelper.IndoFormatWithoutTail(getData.JumlahYangHarusDibayar))
//                        .Replace("{{bank}}", getData.BankName)
//                        .Replace("{{vaNo}}", getData.VirtualAccNo)
//                        .Replace("{{SiteName}}", getData.SiteName)



//                        ;

//                    var getAttachmentFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\PdfOutput\Invoice\" + getData.InvoiceDoc + "";

//                    var result = await ServiceHelper.EmailSendAsync(new DynamicEmailInputDto()
//                    {
//                        Body = bodyEmail,
//                        Subject = "Tagihan Unit " + getData.UnitCode + " " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                        IsHtmlBody = true,
//                        Cc = cc,
//                        To = to,
//                        Bcc = bcc,
//                        AttachmentFile = getAttachmentFile

//                    });



//                    var insertOutbox = new TR_EmailOutbox();
//                    // jika sukses kirim email
//                    if (result == true)
//                    {


//                        var getUpdateIsSend = (from a in _contextBilling.TR_InvoiceHeader
//                                               where a.Id == getData.invoiceHeaderId
//                                               select a).FirstOrDefault();



//                        getUpdateIsSend.IsSendEmail = true;
//                        getUpdateIsSend.IsSendEmailDate = DateTime.Now;

//                        _contextBilling.TR_InvoiceHeader.Update(getUpdateIsSend);
//                        _contextBilling.SaveChanges();



//                        insertOutbox = new TR_EmailOutbox
//                        {
//                            EmailTo = getEmailUser.email,
//                            EmailSubject = "Tagihan Unit " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                            IsSent = true

//                        };


//                    }
//                    else
//                    {
//                        //jika gagal
//                        insertOutbox = new TR_EmailOutbox
//                        {
//                            EmailTo = getEmailUser.email,
//                            EmailSubject = "Tagihan Unit " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                            IsSent = false



//                        };

//                    }




//                    _contextBilling.TR_EmailOutbox.Add(insertOutbox);
//                    _contextBilling.SaveChanges();




//                }
//                catch (Exception ex)
//                {
//                    //
//                }


//            }




//            var timeEndCall = DateTime.Now;
//            sb.AppendLine("Time Start RunSendEmailInvoice : " + timeStartCall);
//            sb.AppendLine("Time End RunSendEmailInvoice : " + timeEndCall);


//            var fileName = "Log Call RunSendEmailInvoice" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt";
//            var folderPath = Path.Combine(_hostEnvironment.WebRootPath, "Assets", "Upload", "LogScheduler", "RunSendEmailInvoice");
//            if (!Directory.Exists(folderPath))
//            {
//                Directory.CreateDirectory(folderPath);
//            }
//            File.WriteAllText(Path.Combine(folderPath, fileName), sb.ToString());
//            GC.Collect();

//        }



//        public async Task SendEmailInvoiceByPsCode(string psCode)
//        {


//            DateTime timeStartCall = DateTime.Now;
//            var sb = new StringBuilder();

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var webConfigApp = (JObject)appsettingsjson["App"];
//            var EmailEnvironment = webConfigApp.Property("EmailEnvironment").Value.ToString();
//            var EmailTo = webConfigApp.Property("EmailTo").Value.ToString();



//            var queueData = (from a in _contextBilling.TR_InvoiceHeader
//                             join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                             where a.PsCode == psCode && a.IsSendEmail == false && a.PeriodId == 1017
//                             select new
//                             {
//                                 a.PsCode,
//                                 unitDataId = b.Id,
//                                 a.PeriodId
//                             }).Distinct().ToList();

//            if (queueData.Count > 0)
//            {

//                foreach (var item in queueData)
//                {
//                    try
//                    {

//                        var getData = (from a in _contextBilling.TR_InvoiceHeader
//                                       join b in _contextBilling.TR_InvoiceDetail on a.Id equals b.InvoiceHeaderId
//                                       join c in _contextBilling.MS_Item on b.ItemId equals c.Id
//                                       //join d in _contextBilling.MS_UnitItemHeader on a.TemplateInvoiceHeaderId equals d.TemplateInvoiceHeaderId
//                                       join e in _contextBilling.MS_Bank on a.BankId equals e.Id
//                                       join f in _contextBilling.MS_UnitData on a.UnitDataId equals f.Id
//                                       join g in _contextBilling.MS_Period on a.PeriodId equals g.Id
//                                       join h in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals h.Id
//                                       join i in _contextBilling.MS_Site on a.SiteId equals i.SiteId

//                                       where a.PsCode == item.PsCode && a.IsSendEmail == false && f.Id == item.unitDataId && a.PeriodId == item.PeriodId
//                                       select new
//                                       {
//                                           invoiceHeaderId = a.Id,
//                                           a.InvoiceNo,
//                                           a.InvoiceDoc,
//                                           b.ItemId,
//                                           c.ItemName,
//                                           a.VirtualAccNo,
//                                           e.BankName,
//                                           a.JumlahYangHarusDibayar,
//                                           f.UnitName,
//                                           f.UnitNo,
//                                           f.UnitCode,
//                                           h.TemplateInvoiceName,
//                                           i.SiteName,


//                                           bulanPeriode = g.PeriodMonth.ToString("MMMM yyyy")
//                                       }).FirstOrDefault();

//                        // var getUnit = getData.FirstOrDefault();


//                        var getEmailUser = (from a in _contextPersonal.PERSONALS
//                                            join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
//                                            where a.psCode == item.PsCode
//                                            select new
//                                            {
//                                                a.psCode,
//                                                a.name,
//                                                b.email
//                                            }).FirstOrDefault();



//                        List<string> to = new List<string>();
//                        List<string> cc = new List<string>();
//                        List<string> bcc = new List<string>();
//                        if (EmailEnvironment == "1")// dev {}
//                        {
//                            if (!String.IsNullOrEmpty(EmailTo)) //item.EmailAddress
//                                to.AddRange(EmailTo.Split(';'));

//                            if (!String.IsNullOrEmpty(""))
//                                cc.AddRange("".Split(';'));

//                            if (!String.IsNullOrEmpty(""))
//                                bcc.AddRange("".Split(';'));

//                        }
//                        else
//                        {

//                            if (!String.IsNullOrEmpty(getEmailUser.email))
//                                to.AddRange(getEmailUser.email.Split(';'));

//                            if (!String.IsNullOrEmpty(""))
//                                cc.AddRange("".Split(';'));

//                            if (!String.IsNullOrEmpty(""))
//                                bcc.AddRange("".Split(';'));

//                        }


//                        var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\EmailTemplate\email-invoice.html";
//                        var htmlToConvert = File.ReadAllText(templateFile);

//                        var bodyEmail = htmlToConvert.Replace("{{CustName}}", getEmailUser.name)
//                            .Replace("{{UnitCode}}", getData.UnitCode)
//                            .Replace("{{UnitNo}}", getData.UnitNo)
//                            .Replace("{{BulanPeriod}}", getData.bulanPeriode)
//                            .Replace("{{invoiceNo}}", getData.InvoiceNo)
//                            .Replace("{{invoiceType}}", getData.TemplateInvoiceName)
//                            .Replace("{{totalPembayaran}}", NumberHelper.IndoFormatWithoutTail(getData.JumlahYangHarusDibayar))
//                            .Replace("{{bank}}", getData.BankName)
//                            .Replace("{{vaNo}}", getData.VirtualAccNo)
//                            .Replace("{{SiteName}}", getData.SiteName)



//                            ;

//                        var getAttachmentFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\PdfOutput\Invoice\" + getData.InvoiceDoc + "";

//                        var result = await ServiceHelper.EmailSendAsync(new DynamicEmailInputDto()
//                        {
//                            Body = bodyEmail,
//                            Subject = "Tagihan Unit " + getData.UnitCode + " " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                            IsHtmlBody = true,
//                            Cc = cc,
//                            To = to,
//                            Bcc = bcc,
//                            AttachmentFile = getAttachmentFile

//                        });



//                        var insertOutbox = new TR_EmailOutbox();
//                        // jika sukses kirim email
//                        if (result == true)
//                        {


//                            var getUpdateIsSend = (from a in _contextBilling.TR_InvoiceHeader
//                                                   where a.Id == getData.invoiceHeaderId
//                                                   select a).FirstOrDefault();



//                            getUpdateIsSend.IsSendEmail = true;
//                            getUpdateIsSend.IsSendEmailDate = DateTime.Now;

//                            _contextBilling.TR_InvoiceHeader.Update(getUpdateIsSend);
//                            _contextBilling.SaveChanges();



//                            insertOutbox = new TR_EmailOutbox
//                            {
//                                EmailTo = getEmailUser.email,
//                                EmailSubject = "Tagihan Unit " + getData.UnitCode + " " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                                IsSent = true

//                            };


//                        }
//                        else
//                        {
//                            //jika gagal
//                            insertOutbox = new TR_EmailOutbox
//                            {
//                                EmailTo = getEmailUser.email,
//                                EmailSubject = "Tagihan Unit " + getData.UnitCode + " " + getData.UnitNo + " - " + getData.bulanPeriode + "",
//                                IsSent = false



//                            };

//                        }




//                        _contextBilling.TR_EmailOutbox.Add(insertOutbox);
//                        _contextBilling.SaveChanges();




//                    }
//                    catch (Exception ex)
//                    {
//                        //
//                    }

//                }

//            }


//            var timeEndCall = DateTime.Now;
//            sb.AppendLine("Time Start RunSendEmailInvoice : " + timeStartCall);
//            sb.AppendLine("Time End RunSendEmailInvoice : " + timeEndCall);


//            var fileName = "Log Call RunSendEmailInvoice" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt";
//            var folderPath = Path.Combine(_hostEnvironment.WebRootPath, "Assets", "Upload", "LogScheduler", "RunSendEmailInvoice");
//            if (!Directory.Exists(folderPath))
//            {
//                Directory.CreateDirectory(folderPath);
//            }
//            File.WriteAllText(Path.Combine(folderPath, fileName), sb.ToString());
//            GC.Collect();

//        }



//        public List<ViewDetailBalanceDto> ViewDetailBalance(int InvoiceHeaderId)
//        {

//            var final = new List<ViewDetailBalanceDto>();

//            var getData = (from a in _contextBilling.TR_InvoiceHeader
//                           join b in _contextBilling.MS_Period on a.PeriodId equals b.Id
//                           where a.Id == InvoiceHeaderId
//                           select new
//                           {
//                               a.Id,
//                               a.SiteId,
//                               a.CreationTime,
//                               a.UnitDataId,
//                               b.PeriodMonth,
//                               b.StartDate,
//                               a.PenaltyNominal,
//                               //a.AdjustmentNominal,
//                               a.TemplateInvoiceHeaderId

//                           }).FirstOrDefault();

//            var getTagihanSebelumnya = TagihanSebelumnya(getData.UnitDataId, getData.PeriodMonth, getData.SiteId, getData.TemplateInvoiceHeaderId);

//            if (getTagihanSebelumnya != 0)
//            {
//                var addTagSeb = new ViewDetailBalanceDto
//                {
//                    Deskripsi = "Tagihan Sebelumnya",
//                    Tanggal = "-",
//                    Jumlah = getTagihanSebelumnya
//                };

//                final.Add(addTagSeb);

//            }

//            var getPembayaranSebelumnya = PembayaranSebelumnya(getData.UnitDataId, getData.StartDate, getData.SiteId, getData.TemplateInvoiceHeaderId);

//            if (getPembayaranSebelumnya.TotalPembayaran != 0)
//            {
//                foreach (var item in getPembayaranSebelumnya.DetailPembayaranSebelumnya)
//                {

//                    var addPemSeb = new ViewDetailBalanceDto
//                    {
//                        Deskripsi = "Pembayaran melalui " + item.PaymentMethod,
//                        Tanggal = item.TransactionDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                        Jumlah = item.PaymentAmount
//                    };

//                    final.Add(addPemSeb);



//                }


//            }

//            var getDetailInvoice = (from a in _contextBilling.TR_InvoiceDetail
//                                    join b in _contextBilling.MS_Item on a.ItemId equals b.Id
//                                    where a.InvoiceHeaderId == getData.Id
//                                    select new
//                                    {
//                                        a.ItemId,
//                                        a.CreationTime,
//                                        b.ItemName,
//                                        a.ItemNominal,
//                                        a.PPNNominal
//                                    }).ToList();

//            if (getDetailInvoice.Count > 0)
//            {
//                foreach (var item in getDetailInvoice)
//                {
//                    var addDetInv = new ViewDetailBalanceDto
//                    {
//                        Deskripsi = item.ItemName,
//                        Tanggal = item.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                        Jumlah = item.ItemNominal
//                    };

//                    final.Add(addDetInv);


//                    if (item.PPNNominal > 0)
//                    {

//                        var addDetPPN = new ViewDetailBalanceDto
//                        {
//                            Deskripsi = "PPN " + item.ItemName,
//                            Tanggal = item.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                            Jumlah = item.PPNNominal
//                        };

//                        final.Add(addDetPPN);

//                    }


//                }





//            }


//            if (getData.PenaltyNominal > 0)
//            {
//                var addPenalty = new ViewDetailBalanceDto
//                {
//                    Deskripsi = "Penalty",
//                    Tanggal = getData.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                    Jumlah = getData.PenaltyNominal
//                };

//                final.Add(addPenalty);


//            }



//            return final;

//        }

//        public async Task SendWAInvoice(List<int> invoiceID)
//        {

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var whatsAppType = connectionApp.Property("WhatsAppType").Value.ToString();
//            var whatsAppTo = connectionApp.Property("WhatsAppTo").Value.ToString();

//            var filePath = _hostEnvironment.WebRootPath + @"\Assets\Upload\MasterTemplate\TemplateWAInvoice.html";


//            foreach (var item in invoiceID)
//            {
//                var getData = (from a in _contextBilling.TR_InvoiceHeader
//                               join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                               join c in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals c.Id
//                               join d in _contextBilling.MS_Period on a.PeriodId equals d.Id
//                               join e in _contextBilling.MS_Bank on a.BankId equals e.Id
//                               where a.Id == item
//                               select new
//                               {
//                                   a.Id,
//                                   a.UnitDataId,
//                                   a.InvoiceNo,
//                                   a.PsCode,
//                                   a.JumlahYangHarusDibayar,
//                                   a.JatuhTempo,
//                                   a.TemplateInvoiceHeaderId,
//                                   b.UnitName,
//                                   b.UnitNo,
//                                   c.TemplateInvoiceName,
//                                   d.PeriodMonth,
//                                   a.VirtualAccNo,
//                                   e.BankName


//                               }).FirstOrDefault();


//                var getCustName = (from a in _contextPersonal.PERSONALS
//                                   where a.psCode == getData.PsCode
//                                   select a.name).FirstOrDefault();

//                var getCustomer = (from a in _contextPersonal.TR_Phone
//                                   where a.psCode == getData.PsCode && a.refID == 1 && a.phoneType == "3"
//                                   select a.number).FirstOrDefault();

//                var paramWA = new SenderWaDto
//                {
//                    unitname = getData.UnitName,
//                    unitno = getData.UnitNo,
//                    blnperiode = getData.PeriodMonth.ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                    nomerinvoice = getData.InvoiceNo,
//                    namainvoice = getData.TemplateInvoiceName,
//                    totaltagihan = NumberHelper.IndoFormatWithoutTail(getData.JumlahYangHarusDibayar),
//                    tgljatuhtempo = getData.JatuhTempo.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
//                    bank = getData.BankName,
//                    virtualno = getData.VirtualAccNo,
//                    customerName = getCustName


//                };


//                var html = "";
//                using (StreamReader reader = new StreamReader(filePath))
//                {
//                    html = reader.ReadToEnd();
//                }

//                string content = html.Replace("{{unitname}}", paramWA.unitname)
//                    .Replace("{{unitno}}", paramWA.unitno)
//                    .Replace("{{blnperiode}}", paramWA.blnperiode)
//                    .Replace("{{nomerinvoice}}", paramWA.nomerinvoice)
//                    .Replace("{{namainvoice}}", paramWA.namainvoice)
//                    .Replace("{{totaltagihan}}", paramWA.totaltagihan)
//                    .Replace("{{tgljatuhtempo}}", paramWA.tgljatuhtempo)
//                    .Replace("{{bank}}", paramWA.bank)
//                    .Replace("{{virtualno}}", paramWA.virtualno)
//                    .Replace("{{customerName}}", paramWA.customerName)
//                    ;

//                if (whatsAppType == "1")
//                {
//                    whatsAppTo = getCustomer != null ? getCustomer : whatsAppTo;
//                }
//                string[] listNumber = whatsAppTo.Split(',');
//                foreach (var whatsAppNumber in listNumber)
//                {
//                    var sendParam = new WhatsAppInputDto
//                    {
//                        number = whatsAppNumber,
//                        message = content,
//                        sender = "bls"
//                    };

//                    var resultWA = await _waHelper.SendWhatsAppWithResult(sendParam);

//                    var resultWaDto = JsonConvert.DeserializeObject<RootResponseWA>(resultWA);


//                    if (resultWaDto.status == true)
//                    {
//                        var updateData = (from a in _contextBilling.TR_InvoiceHeader
//                                          where a.Id == item
//                                          select a).FirstOrDefault();

//                        updateData.IsSendWhatsapp = true;
//                        updateData.IsSendWADate = DateTime.Now;

//                        _contextBilling.TR_InvoiceHeader.Update(updateData);
//                        _contextBilling.SaveChanges();
//                    }
//                }
//            }

//        }



//        public void GenerateSP1(int siteID)
//        {

//            //var bulan = DateTime.Now.AddMonths(-2);
//            var bulan = DateTime.Now.AddMonths(-4);

//            var getParamMinSP = (from a in _contextBilling.MS_Parameter
//                                 where a.Code == "SPN"
//                                 select a.Value).FirstOrDefault();


//            var getPeriod = (from a in _contextBilling.MS_Period
//                             where a.StartDate.Month == bulan.Month && a.StartDate.Year == DateTime.Now.Year
//                             orderby a.Id descending
//                             select a).FirstOrDefault();

//            var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
//                              join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                              join c in _contextBilling.MS_Site on a.SiteId equals c.SiteId
//                              where a.PeriodId == getPeriod.Id && a.SiteId == siteID
//                              select new
//                              {
//                                  a.Id,
//                                  a.JumlahYangHarusDibayar,
//                                  a.PeriodId,
//                                  a.ProjectId,
//                                  a.PsCode,
//                                  b.UnitCode,
//                                  b.UnitNo,
//                                  a.UnitDataId,
//                                  b.ClusterId,
//                                  b.SiteId,
//                                  c.SiteCode,

//                              }).Distinct().ToList();


//            foreach (var item in getInvoice)
//            {
//                var getEmailCust = (from a in _contextPersonal.TR_Email
//                                    where a.psCode == item.PsCode
//                                    select a.email).FirstOrDefault();

//                var paramTotalPembayaran = new SPGetPembayaranSebelumnya
//                {
//                    invoiceHeaderId = item.Id,
//                    PeriodId = getPeriod.Id,
//                    UnitDataID = item.UnitDataId,
//                    EndDatePeriod = getPeriod.EndDate

//                };

//                var totalPembayaran = SPGetPembayaranSebelumnya(paramTotalPembayaran);

//                var totalSisaTagiahan = (item.JumlahYangHarusDibayar - totalPembayaran.TotalPembayaran);

//                //proses masuk ke table warning letter
//                if (totalSisaTagiahan > Convert.ToDecimal(getParamMinSP))
//                {

//                    #region generate Sp ref no
//                    var getRunningNo = 0;
//                    var getTrRunning = (from a in _contextBilling.TR_RunningNumber
//                                        where a.SiteId == item.SiteId && a.DocCode == "WRL" && a.ProjectId == item.ProjectId && a.PeriodId == getPeriod.Id
//                                        orderby a.Number descending
//                                        select a).FirstOrDefault();

//                    if (getTrRunning == null)
//                    {

//                        getRunningNo = 1;

//                        var AddRunningNumber = new TR_RunningNumber
//                        {
//                            DocCode = "WRL",
//                            ProjectId = item.ProjectId,
//                            Number = getRunningNo,
//                            SiteId = item.SiteId,
//                            PeriodId = getPeriod.Id
//                        };

//                        _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
//                    }
//                    else
//                    {
//                        getRunningNo = getTrRunning.Number + 1;
//                        getTrRunning.Number = getRunningNo;
//                        _contextBilling.TR_RunningNumber.Update(getTrRunning);
//                    }

//                    _contextBilling.SaveChanges();

//                    var generateRunningNo = "0000000";
//                    var CountRunningNo = getRunningNo.ToString().Length;
//                    var CountSub = generateRunningNo.Length - CountRunningNo;

//                    var NoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;


//                    var SPNoComb = "" + NoFinal + "/SP1/" + item.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

//                    #endregion


//                    var insertData = new TR_WarningLetter
//                    {

//                        ClusterId = item.ClusterId,
//                        InvoiceHeaderId = item.Id,
//                        PeriodId = item.PeriodId,
//                        ProjectId = item.ProjectId,
//                        psCode = item.PsCode,
//                        UnitCode = item.UnitCode,
//                        UnitNo = item.UnitNo,
//                        UnitDataId = item.UnitDataId,
//                        SiteId = item.SiteId,
//                        SP = 1,
//                        Email = getEmailCust,
//                        SPRefNo = SPNoComb


//                    };

//                    _contextBilling.TR_WarningLetter.Add(insertData);
//                    _contextBilling.SaveChanges();


//                }

//            }

//        }

        //public void GenerateSP2(int siteID)
        //{

        //    //var bulan = DateTime.Now.AddMonths(-2);
        //    var bulan = DateTime.Now.AddMonths(-4);

        //    var getParamMinSP = (from a in _contextBilling.MS_Parameter
        //                         where a.Code == "SPN"
        //                         select a.Value).FirstOrDefault();


        //    var getPeriod = (from a in _contextBilling.MS_Period
        //                     where a.StartDate.Month == bulan.Month && a.StartDate.Year == DateTime.Now.Year
        //                     orderby a.Id descending
        //                     select a).FirstOrDefault();

        //    var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
        //                      join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
        //                      join d in _contextBilling.MS_Site on a.SiteId equals d.SiteId
        //                      where a.PeriodId == getPeriod.Id && a.SiteId == siteID
        //                      select new
        //                      {
        //                          invoiceheaderId = a.Id,
        //                          a.JumlahYangHarusDibayar,
        //                          a.PeriodId,
        //                          a.ProjectId,
        //                          a.PsCode,
        //                          b.UnitCode,
        //                          b.UnitNo,
        //                          a.UnitDataId,
        //                          b.ClusterId,
        //                          b.SiteId,
        //                          d.SiteCode,

        //                      }).ToList();


        //    foreach (var item in getInvoice)
        //    {



        //        var getEmailCust = (from a in _contextPersonal.TR_Email
        //                            where a.psCode == item.PsCode
        //                            select a.email).FirstOrDefault();

        //        var paramTotalPembayaran = new SPGetPembayaranSebelumnya
        //        {
        //            invoiceHeaderId = item.invoiceheaderId,
        //            PeriodId = getPeriod.Id,
        //            UnitDataID = item.UnitDataId,
        //            EndDatePeriod = getPeriod.EndDate

        //        };

        //        var totalPembayaran = SPGetPembayaranSebelumnya(paramTotalPembayaran);

        //        var totalSisaTagiahan = (item.JumlahYangHarusDibayar - totalPembayaran.TotalPembayaran);

        //        //proses masuk ke table warning letter
        //        if (totalSisaTagiahan > Convert.ToDecimal(getParamMinSP))
        //        {

        //            #region generate Sp ref no
        //            var getRunningNo = 0;
        //            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
        //                                where a.SiteId == item.SiteId && a.DocCode == "WRL" && a.ProjectId == item.ProjectId && a.PeriodId == getPeriod.Id
        //                                orderby a.Number descending
        //                                select a).FirstOrDefault();
        //            if (getTrRunning == null)
        //            {

        //                getRunningNo = 1;
        //                var AddRunningNumber = new TR_RunningNumber
        //                {
        //                    DocCode = "WRL",
        //                    ProjectId = item.ProjectId,
        //                    Number = getRunningNo,
        //                    SiteId = item.SiteId,
        //                    PeriodId = getPeriod.Id
        //                };

        //                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
        //            }
        //            else
        //            {
        //                getRunningNo = getTrRunning.Number + 1;
        //                getTrRunning.Number = getRunningNo;
                     

        //                _contextBilling.TR_RunningNumber.Update(getTrRunning);
        //            }

        //            _contextBilling.SaveChanges();

        //            var generateRunningNo = "0000000";
        //            var CountRunningNo = getRunningNo.ToString().Length;
        //            var CountSub = generateRunningNo.Length - CountRunningNo;

        //            var NoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;


        //            var SPNoComb = "" + NoFinal + "/SP2/" + item.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

        //            #endregion

        //            var insertData = new TR_WarningLetter
        //            {

        //                ClusterId = item.ClusterId,
        //                InvoiceHeaderId = item.invoiceheaderId,
        //                PeriodId = item.PeriodId,
        //                ProjectId = item.ProjectId,
        //                psCode = item.PsCode,
        //                UnitCode = item.UnitCode,
        //                UnitNo = item.UnitNo,
        //                UnitDataId = item.UnitDataId,
        //                SiteId = item.SiteId,
        //                SP = 2,
        //                Email = getEmailCust,
        //                SPRefNo = SPNoComb


        //            };

        //            _contextBilling.TR_WarningLetter.Add(insertData);
        //            _contextBilling.SaveChanges();





        //        }

        //    }

        //}
        //public void GenerateSP3(int siteID)
        //{

        //    //var bulan = DateTime.Now.AddMonths(-2);
        //    var bulan = DateTime.Now.AddMonths(-4);

        //    var getParamMinSP = (from a in _contextBilling.MS_Parameter
        //                         where a.Code == "SPN"
        //                         select a.Value).FirstOrDefault();


        //    var getPeriod = (from a in _contextBilling.MS_Period
        //                     where a.StartDate.Month == bulan.Month && a.StartDate.Year == DateTime.Now.Year
        //                     orderby a.Id descending
        //                     select a).FirstOrDefault();

        //    var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
        //                      join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
        //                      join d in _contextBilling.MS_Site on a.SiteId equals d.SiteId
        //                      where a.PeriodId == getPeriod.Id && a.SiteId == siteID
        //                      select new
        //                      {
        //                          invoiceheaderId = a.Id,
        //                          a.JumlahYangHarusDibayar,
        //                          a.PeriodId,
        //                          a.ProjectId,
        //                          a.PsCode,
        //                          b.UnitCode,
        //                          b.UnitNo,
        //                          a.UnitDataId,
        //                          b.ClusterId,
        //                          b.SiteId,
        //                          d.SiteCode,

        //                      }).ToList();


        //    foreach (var item in getInvoice)
        //    {



        //        var getEmailCust = (from a in _contextPersonal.TR_Email
        //                            where a.psCode == item.PsCode
        //                            select a.email).FirstOrDefault();

        //        var paramTotalPembayaran = new SPGetPembayaranSebelumnya
        //        {
        //            invoiceHeaderId = item.invoiceheaderId,
        //            PeriodId = getPeriod.Id,
        //            UnitDataID = item.UnitDataId,
        //            EndDatePeriod = getPeriod.EndDate

        //        };

        //        var totalPembayaran = SPGetPembayaranSebelumnya(paramTotalPembayaran);

        //        var totalSisaTagiahan = (item.JumlahYangHarusDibayar - totalPembayaran.TotalPembayaran);

        //        //proses masuk ke table warning letter
        //        if (totalSisaTagiahan > Convert.ToDecimal(getParamMinSP))
        //        {

        //            #region generate Sp ref no
        //            var getRunningNo = 0;
        //            var getTrRunning = (from a in _contextBilling.TR_RunningNumber
        //                                where a.SiteId == item.SiteId && a.DocCode == "WRL" && a.ProjectId == item.ProjectId && a.PeriodId == getPeriod.Id
        //                                orderby a.Number descending
        //                                select a).FirstOrDefault();
        //            if (getTrRunning == null)
        //            {

        //                getRunningNo = 1;
        //                var AddRunningNumber = new TR_RunningNumber
        //                {
        //                    DocCode = "WRL",
        //                    ProjectId = item.ProjectId,
        //                    Number = getRunningNo,
        //                    SiteId = item.SiteId,
        //                    PeriodId = getPeriod.Id

        //                };

        //                _contextBilling.TR_RunningNumber.Add(AddRunningNumber);
        //            }
        //            else
        //            {
        //                getRunningNo = getTrRunning.Number + 1;
        //                getTrRunning.Number = getRunningNo;

        //                _contextBilling.TR_RunningNumber.Update(getTrRunning);
        //            }

                  
        //            _contextBilling.SaveChanges();

        //            var generateRunningNo = "0000000";
        //            var CountRunningNo = getRunningNo.ToString().Length;
        //            var CountSub = generateRunningNo.Length - CountRunningNo;

        //            var NoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;


        //            var SPNoComb = "" + NoFinal + "/SP3/" + item.SiteCode + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy") + "";

        //            #endregion


        //            var insertData = new TR_WarningLetter
        //            {

        //                ClusterId = item.ClusterId,
        //                InvoiceHeaderId = item.invoiceheaderId,
        //                PeriodId = item.PeriodId,
        //                ProjectId = item.ProjectId,
        //                psCode = item.PsCode,
        //                UnitCode = item.UnitCode,
        //                UnitNo = item.UnitNo,
        //                UnitDataId = item.UnitDataId,
        //                SiteId = item.SiteId,
        //                SP = 3,
        //                Email = getEmailCust,
        //                SPRefNo = SPNoComb


        //            };

        //            _contextBilling.TR_WarningLetter.Add(insertData);
        //            _contextBilling.SaveChanges();





        //        }

        //    }

        //}

        //public viewDetailWarLettDto viewDetailWarLett(int warningLetterId)
        //{

        //    var getData = (from a in _contextBilling.TR_WarningLetter
        //                   join b in _contextBilling.MS_Site on a.SiteId equals b.SiteId
        //                   join c in _contextBilling.TR_InvoiceHeader on a.InvoiceHeaderId equals c.Id
        //                   join d in _contextBilling.MS_TemplateInvoiceHeader on c.TemplateInvoiceHeaderId equals d.Id
        //                   //join e in _contextBilling.MS_UnitItemHeader on d.Id equals e.TemplateInvoiceHeaderId
        //                   join f in _contextBilling.MS_Bank on c.BankId equals f.Id
        //                   join g in _contextBilling.MS_Period on a.PeriodId equals g.Id

        //                   where a.Id == warningLetterId
        //                   select new
        //                   {
        //                       periodClose = g.CloseDate,
        //                       spCreate = a.CreationTime,
        //                       a.UnitDataId,
        //                       a.SPRefNo,
        //                       a.UnitNo,
        //                       a.UnitCode,
        //                       a.SP,
        //                       a.SiteId,
        //                       a.ProjectId,
        //                       b.SiteName,
        //                       siteEmail = b.Email,
        //                       b.SiteAddress,
        //                       b.HandPhone,
        //                       b.OfficePhone,
        //                       a.psCode,
        //                       d.TemplateInvoiceName,
        //                       c.VirtualAccNo,
        //                       f.BankName,
        //                       c.JumlahYangHarusDibayar,
        //                       c.TemplateInvoiceHeaderId,
        //                       periodName = g.PeriodMonth.ToString("MMMM") + " " + g.PeriodYear.ToString("yyyy"),
        //                       c.InvoiceNo,
        //                   }
        //                              ).FirstOrDefault();

        //    var getLocation = (from a in _contextBilling.MP_ProjectSite
        //                       where a.SiteId == getData.SiteId && a.ProjectId == getData.ProjectId
        //                       select a).FirstOrDefault();



        //    var getEmailUser = (from a in _contextPersonal.PERSONALS
        //                        join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
        //                        join c in _contextPersonal.TR_Address.Where(x => x.addrType == "3") on a.psCode equals c.psCode
        //                        join d in _contextPersonal.TR_Phone on a.psCode equals d.psCode
        //                        where a.psCode == getData.psCode
        //                        select new
        //                        {
        //                            a.psCode,
        //                            a.name,
        //                            b.email,
        //                            c.address,
        //                            d.number
        //                        }).FirstOrDefault();

        //    var totalPembayaran = PembayaranSebelumnya(getData.UnitDataId, getData.periodClose.AddMonths(+1), getData.SiteId, getData.TemplateInvoiceHeaderId);

        //    var totalSisaTagiahan = (getData.JumlahYangHarusDibayar - totalPembayaran.TotalPembayaran);



        //    var final = new viewDetailWarLettDto
        //    {
        //        CustName = getEmailUser.name,
        //        refNo = getData.SPRefNo,
        //        alamatKorespondensi = getEmailUser.address,
        //        CustPhone = getEmailUser.number,
        //        siteEmail = getData.siteEmail,
        //        SiteHandPhone = getData.HandPhone,
        //        OfficePhone = getData.OfficePhone,
        //        SiteAddress = getData.SiteAddress,
        //        SiteName = getData.SiteName,
        //        UnitCode = getData.UnitCode,
        //        UnitNo = getData.UnitNo,
        //        invoiceNo = getData.InvoiceNo,
        //        invoiceType = getData.TemplateInvoiceName,
        //        totalOutstanding = NumberHelper.IndoFormatWithoutTail(totalSisaTagiahan),
        //        totalTagihan = NumberHelper.IndoFormatWithoutTail(getData.JumlahYangHarusDibayar),
        //        bank = getData.BankName,
        //        vaNo = getData.VirtualAccNo,
        //        location = getLocation.Location,
        //        peringatanNo = getData.SP.ToString(),
        //        jatuhtempoDate = getData.SP == 1 ? "19 " + DateTime.Now.ToString("MMMM yyyy") + "" : getData.SP == 2 ? "26 " + DateTime.Now.ToString("MMMM yyyy") + "" : "30 " + DateTime.Now.ToString("MMMM yyyy") + "",
        //        SPDate = DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")),
        //        periodName = getData.periodName


        //    };

        //    return final;



        //}


        //public async Task SendEmailWarningLetter(List<int> warningLetterId)
        //{


        //    DateTime timeStartCall = DateTime.Now;
        //    var sb = new StringBuilder();

        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var webConfigApp = (JObject)appsettingsjson["App"];
        //    var EmailEnvironment = webConfigApp.Property("EmailEnvironment").Value.ToString();
        //    var EmailTo = webConfigApp.Property("EmailTo").Value.ToString();


        //    foreach (var item in warningLetterId)
        //    {
        //        try
        //        {

        //            var getData = (from a in _contextBilling.TR_WarningLetter
        //                           join b in _contextBilling.MS_Site on a.SiteId equals b.SiteId
        //                           join c in _contextBilling.TR_InvoiceHeader on a.InvoiceHeaderId equals c.Id
        //                           join d in _contextBilling.MS_TemplateInvoiceHeader on c.TemplateInvoiceHeaderId equals d.Id
        //                           // join e in _contextBilling.MS_UnitItemHeader on d.Id equals e.TemplateInvoiceHeaderId
        //                           join f in _contextBilling.MS_Bank on c.BankId equals f.Id
        //                           join g in _contextBilling.MS_Period on a.PeriodId equals g.Id

        //                           where a.Id == item
        //                           select new
        //                           {
        //                               periodClose = g.CloseDate,
        //                               a.UnitDataId,
        //                               a.SPRefNo,
        //                               a.UnitNo,
        //                               a.UnitCode,
        //                               a.SP,
        //                               c.SiteId,
        //                               c.TemplateInvoiceHeaderId,
        //                               a.ProjectId,
        //                               b.SiteName,
        //                               siteEmail = b.Email,
        //                               b.SiteAddress,
        //                               b.HandPhone,
        //                               b.OfficePhone,
        //                               a.psCode,
        //                               d.TemplateInvoiceName,
        //                               c.VirtualAccNo,
        //                               f.BankName,
        //                               c.JumlahYangHarusDibayar,
        //                               periodName = g.PeriodMonth.ToString("MMMM") + " " + g.PeriodYear.ToString("yyyy"),


        //                               c.InvoiceNo,
        //                           }
        //                           ).FirstOrDefault();

        //            var getLocation = (from a in _contextBilling.MP_ProjectSite
        //                               where a.SiteId == getData.SiteId && a.ProjectId == getData.ProjectId
        //                               select a).FirstOrDefault();



        //            var getEmailUser = (from a in _contextPersonal.PERSONALS
        //                                join b in _contextPersonal.TR_Email on a.psCode equals b.psCode
        //                                join c in _contextPersonal.TR_Address.Where(x => x.addrType == "3") on a.psCode equals c.psCode
        //                                join d in _contextPersonal.TR_Phone on a.psCode equals d.psCode
        //                                where a.psCode == getData.psCode
        //                                select new
        //                                {
        //                                    a.psCode,
        //                                    a.name,
        //                                    b.email,
        //                                    c.address,
        //                                    d.number
        //                                }).FirstOrDefault();


        //            var EmailCC = "";
        //            var EmailBCC = "";

        //            var getCCBCC = (from a in _contextBilling.MS_TypeEmail
        //                            join b in _contextBilling.MS_SettingEmail on a.Id equals b.TypeEmailId
        //                            where a.TypeEmail == "SP" && b.SiteId == getData.SiteId
        //                            select b).FirstOrDefault();

        //            if (getCCBCC != null) 
        //            {
        //                EmailCC = getCCBCC.Cc;
        //                EmailBCC = getCCBCC.Bcc;
        //            }

                    

        //            List<string> to = new List<string>();
        //            List<string> cc = new List<string>();
        //            List<string> bcc = new List<string>();

        //            if (EmailEnvironment == "1")// dev {}
        //            {
        //                if (!String.IsNullOrEmpty(EmailTo)) //item.EmailAddress
        //                    to.AddRange(EmailTo.Split(';'));

        //                if (!String.IsNullOrEmpty(EmailCC))
        //                    cc.AddRange(EmailCC.Split(';'));

        //                if (!String.IsNullOrEmpty(EmailBCC))
        //                    bcc.AddRange(EmailBCC.Split(';'));

        //            }
        //            else
        //            {

        //                if (!String.IsNullOrEmpty(getEmailUser.email))
        //                    to.AddRange(getEmailUser.email.Split(';'));

        //                if (!String.IsNullOrEmpty(EmailCC))
        //                    cc.AddRange(EmailCC.Split(';'));

        //                if (!String.IsNullOrEmpty(EmailBCC))
        //                    bcc.AddRange(EmailBCC.Split(';'));

        //            }

        //            var totalPembayaran = PembayaranSebelumnya(getData.UnitDataId, getData.periodClose.AddMonths(+1), getData.SiteId, getData.TemplateInvoiceHeaderId);

        //            var totalSisaTagiahan = (getData.JumlahYangHarusDibayar - totalPembayaran.TotalPembayaran);



        //            var templateFile = _hostEnvironment.WebRootPath + @"\Assets\Upload\EmailTemplate\email-warningletter.html";
        //            var htmlToConvert = File.ReadAllText(templateFile);

        //            var bodyEmail = htmlToConvert.Replace("{{CustName}}", getEmailUser.name)
        //                .Replace("{{refNo}}", getData.SPRefNo)
        //                .Replace("{{alamatKorespondensi}}", getEmailUser.address)
        //                .Replace("{{CustPhone}}", getEmailUser.number)
        //                .Replace("{{siteEmail}}", getData.siteEmail)
        //                .Replace("{{SiteHandPhone}}", getData.HandPhone)
        //                .Replace("{{OfficePhone}}", getData.OfficePhone)
        //                .Replace("{{SiteName}}", getData.SiteName)
        //                .Replace("{{SiteAddress}}", getData.SiteAddress)
        //                .Replace("{{periodName}}", getData.periodName)


        //                .Replace("{{UnitCode}}", getData.UnitCode)
        //                .Replace("{{UnitNo}}", getData.UnitNo)
        //                .Replace("{{invoiceNo}}", getData.InvoiceNo)
        //                .Replace("{{invoiceType}}", getData.TemplateInvoiceName)
        //                .Replace("{{totalPembayaran}}", NumberHelper.IndoFormatWithoutTail(getData.JumlahYangHarusDibayar))
        //                .Replace("{{totalOutstanding}}", NumberHelper.IndoFormatWithoutTail(totalSisaTagiahan))
        //                .Replace("{{bank}}", getData.BankName)
        //                .Replace("{{vaNo}}", getData.VirtualAccNo)
        //                .Replace("{{location}}", getLocation.Location)
        //                .Replace("{{peringatanNo}}", getData.SP.ToString())
        //                .Replace("{{jatuhtempoDate}}", getData.SP == 1 ? "19 " + DateTime.Now.ToString("MMMM yyyy") + "" : getData.SP == 2 ? "26 " + DateTime.Now.ToString("MMMM yyyy") + "" : "30 " + DateTime.Now.ToString("MMMM yyyy") + "")
        //                .Replace("{{SPDate}}", DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")))



        //                ;



        //            var result = await ServiceHelper.EmailSendAsync(new DynamicEmailInputDto()
        //            {
        //                Body = bodyEmail,
        //                Subject = "Surat Peringatan Batas Jatuh Tempo Pembayaran Billing " + getData.SPRefNo + "",
        //                IsHtmlBody = true,
        //                Cc = cc,
        //                To = to,
        //                Bcc = bcc,

        //            });



        //            var insertOutbox = new TR_EmailOutbox();
        //            // jika sukses kirim email
        //            if (result == true)
        //            {


        //                var getUpdateIsSend = (from a in _contextBilling.TR_WarningLetter
        //                                       where a.Id == item
        //                                       select a).FirstOrDefault();



        //                getUpdateIsSend.SendEmailDate = DateTime.Now;

        //                _contextBilling.TR_WarningLetter.Update(getUpdateIsSend);
        //                _contextBilling.SaveChanges();



        //                insertOutbox = new TR_EmailOutbox
        //                {
        //                    EmailTo = getEmailUser.email,
        //                    EmailSubject = "Surat Peringatan Batas Jatuh Tempo Pembayaran Billing " + getData.SPRefNo + "",
        //                    IsSent = true

        //                };


        //            }
        //            else
        //            {
        //                //jika gagal
        //                insertOutbox = new TR_EmailOutbox
        //                {
        //                    EmailTo = getEmailUser.email,
        //                    EmailSubject = "Surat Peringatan Batas jatuh tempo Pembayaran Billing " + getData.SPRefNo + "",
        //                    IsSent = false



        //                };

        //            }

        //            _contextBilling.TR_EmailOutbox.Add(insertOutbox);
        //            _contextBilling.SaveChanges();




        //        }
        //        catch (Exception ex)
        //        {
        //            //
        //        }

        //    }




        //    var timeEndCall = DateTime.Now;
        //    sb.AppendLine("Time Start RunSendEmailSP : " + timeStartCall);
        //    sb.AppendLine("Time End RunSendEmailSP : " + timeEndCall);


        //    var fileName = "Log Call RunSendEmailSP" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt";
        //    var folderPath = Path.Combine(_hostEnvironment.WebRootPath, "Assets", "Upload", "LogSendEmail", "RunSendEmailSP");
        //    if (!Directory.Exists(folderPath))
        //    {
        //        Directory.CreateDirectory(folderPath);
        //    }
        //    File.WriteAllText(Path.Combine(folderPath, fileName), sb.ToString());
        //    GC.Collect();

        //}

        //public async Task<ReportUriDto> ExportToExcelWarningLetter(WaterReadingListInputDto input)
        //{

        //    FileDto file = new FileDto();

        //    #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
        //    var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
        //    var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
        //    var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

        //    var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

        //    var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

        //    var connectionApp = (JObject)appsettingsjson["App"];
        //    var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

        //    SqlConnection conn = new SqlConnection(ConstringProp);

        //    conn.Open();

        //    #endregion


        //    string query = $@"SELECT DISTINCT a.Id as WarningLetterId,a.InvoiceHeaderId, g.InvoiceNo, (FORMAT(b.PeriodMonth, 'MMMM ') + FORMAT(b.PeriodYear, 'yyyy'))  AS Period, d.projectName,e.clustername, c.ProjectId, c.UnitNo, c.UnitCode, c.SiteId, a.SP, a.SendEmailDate, f.email as EmailCust 
        //                    FROM " + dbCatalogBillingString + "..TR_WarningLetter a " +
        //                    "join " + dbCatalogBillingString + "..MS_Period b on a.PeriodId = b.Id " +
        //                    "join  " + dbCatalogBillingString + "..MS_UnitData c on a.UnitDataId = c.Id " +
        //                    "join  " + dbCatalogPropertyString + "..MS_Project d on a.ProjectId = d.Id " +
        //                    "left join " + dbCatalogBillingString + "..MP_ClusterSite e on c.ClusterId = e.ClusterId " +
        //                    "join " + dbCatalogPersonalString + "..TR_Email f on c.PsCode = f.psCode " +
        //                    "join " + dbCatalogBillingString + "..TR_InvoiceHeader g on a.InvoiceHeaderId = g.Id " +
        //                    "where a.SiteId = " + input.SiteId + " and a.PeriodId = " + input.PeriodId + " and a.ProjectId = " + input.ProjectId + "  ORDER BY c.UnitCode, c.UnitNo, a.SP ASC";

        //    List<WarningLetterListDto> getData = conn.Query<WarningLetterListDto>(query).ToList();

        //    if (input.Cluster != null || input.UnitCode != null || input.UnitNo != null || input.Search != null || input.SP != null)
        //    {

        //        getData = getData

        //      .WhereIf(input.Cluster != null, item => input.Cluster.Contains(item.ClusterName))
        //      .WhereIf(input.UnitNo != null, item => input.UnitNo.Contains(item.UnitNo))
        //      .WhereIf(input.UnitCode != null, item => input.UnitCode.Contains(item.UnitCode))
        //      .WhereIf(input.Search != null, item => input.Search.Contains(item.UnitCode) || input.Search.Contains(item.UnitNo) || input.Search.Contains(item.InvoiceNo))
        //      .WhereIf(input.SP != null, item => input.SP.Contains(item.SP))
        //      .ToList();
        //    }
        //    if (getData.Count > 0)
        //    {
        //        var paramToExcel = new ViewExportWarningLetterDto
        //        {
        //            printBy = (from x in _contextBilling.Users
        //                       where x.Id == AbpSession.GetUserId()
        //                       select x.Name).FirstOrDefault(),

        //            ListWarningLetterDto = getData

        //        };


        //        file = _exporter.ExportToExcelWarningLetterResult(paramToExcel);

        //        //var tempRootPath = Path.Combine(_appFolders.UploadRootDirectory, _appFolders.TempFileDownloadDirectory);


        //        System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
        //        var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


        //        var filePath = Path.Combine(tempRootPath);
        //        if (!File.Exists(filePath))
        //        {
        //            throw new Exception("Requested File doesn't exists");
        //        }

        //        var pathExport = Path.Combine(tempRootPath);
        //        //retrieve data excel from temporary download folder
        //        var fileBytes = File.ReadAllBytes(filePath);
        //        //write excel file to share folder / local folder
        //        File.WriteAllBytes(pathExport, fileBytes);
        //        //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
        //        //  string URL = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;

        //        string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
        //        return new ReportUriDto { Uri = URL.Replace("\\", "/") };


        //    }
        //    else
        //    {

        //        return new ReportUriDto { };
        //    }

        //}

       // [AbpAuthorize(PermissionNames.Pages_Transaction_WaterReading)]
        //public PagedResultDto<ElectricReadingListDto> GetElectricReadingList(WaterReadingListInputDto input)
        //{
        //    #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
        //    var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
        //    var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();

        //    var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

        //    var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);


        //    SqlConnection conn = new SqlConnection(ConstringProp);

        //    conn.Open();


        //    #endregion


        //    string query = $@"SELECT DISTINCT twr.id AS ElectricReadingId, SiteId = twr.SiteId, twr.ProjectId, twr.ClusterId, mp.projectName AS ProjectName, mc.clusterName, (FORMAT(mp1.PeriodMonth, 'MMMM ') + FORMAT(mp1.PeriodYear, 'yyyy'))  AS Period,mp1.PeriodMonth, twr.UnitNo, twr.UnitCode, twr.CurrentRead, twr.PrevRead  
        //                    FROM  " + dbCatalogBillingString + "..TR_ElectricReading twr " +
        //                    "JOIN " + dbCatalogPropertyString + "..MS_Project mp ON twr.ProjectId = mp.Id " +
        //                    "LEFT JOIN " + dbCatalogBillingString + "..MP_ClusterSite mc ON twr.ClusterId = mc.ClusterId " +
        //                    "JOIN " + dbCatalogBillingString + "..MS_Period mp1 ON twr.PeriodId = mp1.Id " +
        //                    "WHERE twr.SiteId = " + input.SiteId + " AND twr.ProjectId = " + input.ProjectId + " AND twr.ClusterId = " + input.ClusterId + " and twr.UnitNo like '%" + input.Search + "%' ORDER by mp1.PeriodMonth DESC ";

        //    List<ElectricReadingListDto> getData = conn.Query<ElectricReadingListDto>(query).ToList();




        //    var dataCount = getData.Count();

        //    if (input.Search != null)
        //    {

        //        getData = getData.Where(x => x.UnitNo.ToLower().Contains(input.Search.ToLower()) || x.ProjectName.ToLower().Contains(input.Search.ToLower())
        //        || x.ClusterName.ToLower().Contains(input.Search.ToLower())
        //        || x.UnitCode.ToLower().Contains(input.Search.ToLower()) || x.UnitNo.ToLower().Contains(input.Search.ToLower())).ToList();

        //    }

        //    getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


        //    return new PagedResultDto<ElectricReadingListDto>(dataCount, getData.ToList());

        //}


        //public void GenerateInvoiceByUnitDataId(int SiteID, int UnitDataId)
        //{
        //    try
        //    {
        //        var paramtoPdf = new ParamPDFInvoiceDto();

        //        var getSite = (from a in _contextBilling.MS_Site
        //                       where a.SiteId == SiteID
        //                       select a).FirstOrDefault();

        //        //ambil periode terakhir yang aktif di ms_period
        //        var getPeriod = (from a in _contextBilling.MS_Period
        //                         where a.SiteId == SiteID && a.IsActive == true
        //                         orderby a.Id descending
        //                         select a).FirstOrDefault();

        //        // ambil semua unit by siteID
        //        var getUnit = (from a in _contextBilling.MS_UnitData
        //                       join b in _contextBilling.MS_UnitItemHeader on a.Id equals b.UnitDataId
        //                       join c in _contextBilling.MS_TemplateInvoiceHeader on b.TemplateInvoiceHeaderId equals c.Id
        //                       where a.SiteId == SiteID && a.CreationTime <= getPeriod.StartDate && a.Id == UnitDataId
        //                       select new
        //                       {
        //                           a,
        //                           b,
        //                           c,

        //                       }).Distinct().ToList();
        //        //1017

        //        foreach (var unit in getUnit) //ini jalan tiap per invoice
        //        {
        //            //pengecekan apakah sudah pernah di generate apa belum
        //            var getisGenerate = (from a in _contextBilling.TR_InvoiceHeader
        //                                 where a.UnitDataId == unit.a.Id && a.PeriodId == getPeriod.Id && a.TemplateInvoiceHeaderId == unit.c.Id
        //                                 select a).FirstOrDefault();

        //            if (getisGenerate == null)
        //            {
        //                try
        //                {
        //                    #region Main Global Proses
        //                    var getPScode = (from a in _contextPersonal.PERSONALS
        //                                     join b in _contextPersonal.TR_Email on a.psCode equals b.psCode into emailnull
        //                                     from emailtemp in emailnull.DefaultIfEmpty()
        //                                     join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
        //                                     where a.psCode == unit.a.PsCode && c.addrType == "3"
        //                                     select new
        //                                     {
        //                                         a.psCode,
        //                                         a.name,
        //                                         email = emailtemp.email == null ? "" : emailtemp.email,
        //                                         c.address
        //                                     }).FirstOrDefault();

        //                    if (getPScode == null)
        //                    {
        //                        getPScode = (from a in _contextPersonal.PERSONALS
        //                                     join b in _contextPersonal.TR_Email on a.psCode equals b.psCode into emailnull
        //                                     from emailtemp in emailnull.DefaultIfEmpty()
        //                                     join c in _contextPersonal.TR_Address on a.psCode equals c.psCode
        //                                     where a.psCode == unit.a.PsCode && c.addrType == "2"
        //                                     select new
        //                                     {
        //                                         a.psCode,
        //                                         a.name,
        //                                         email = emailtemp.email == null ? "" : emailtemp.email,
        //                                         c.address
        //                                     }).FirstOrDefault();
        //                    }

        //                    var getPhoneCust = (from a in _contextPersonal.TR_Phone
        //                                        where a.psCode == unit.a.PsCode && a.refID == 1 && a.phoneType == "3"
        //                                        select a.number).FirstOrDefault();



        //                    var getVa = (from a in _contextBilling.MS_UnitItemHeader
        //                                 join b in _contextBilling.MS_Bank on a.BankId equals b.Id
        //                                 where a.TemplateInvoiceHeaderId == unit.c.Id && a.SiteId == unit.a.SiteId && a.UnitDataId == unit.a.Id
        //                                 select new
        //                                 {
        //                                     a.VirtualAccNo,
        //                                     b.BankName
        //                                 }).FirstOrDefault();



        //                    #region Perhitungan tagihan

        //                    decimal tagihanSebelumnya = TagihanSebelumnya(unit.a.Id, getPeriod.PeriodMonth, unit.a.SiteId, unit.c.Id);



        //                    PembayaranSebelumnyaDto pembayaranSebelumnya = PembayaranSebelumnya(unit.a.Id, getPeriod.StartDate, unit.a.SiteId, unit.c.Id);

        //                    var getDetailPPNWater = CountTagianBplWater(unit.b.Id, getPeriod.Id);

        //                    int TotalTagiahPerbulan = (Convert.ToInt32(getDetailPPNWater.BPLTotal) + Convert.ToInt32(getDetailPPNWater.WaterTotal) + Convert.ToInt32(getDetailPPNWater.PPNBPL) + Convert.ToInt32(getDetailPPNWater.PPNWater) + Convert.ToInt32(getDetailPPNWater.ElectricTotal));

        //                    decimal penaltyNominal = 0;
        //                    var isHavePenalty = (from a in _contextBilling.MS_UnitItemHeader
        //                                         join c in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceHeaderId equals c.TemplateInvoiceHeaderId
        //                                         join d in _contextBilling.MS_ItemRate on c.ItemRateId equals d.Id
        //                                         where a.UnitDataId == unit.b.UnitDataId && a.IsPenalty == true && a.TemplateInvoiceHeaderId == unit.b.TemplateInvoiceHeaderId
        //                                         select new
        //                                         {

        //                                             a.UnitDataId,
        //                                             a.IsPenalty,
        //                                             d.PenaltyNominal,
        //                                             d.PenaltyPercentage,


        //                                         }).FirstOrDefault();

        //                    paramtoPdf.AdjPen = isHavePenalty != null ? "+ Penalty" : "";

        //                    if (isHavePenalty != null)
        //                    {

        //                        if (isHavePenalty.PenaltyPercentage > 0)
        //                        {
        //                            var tagihanSebelumnyaPayment = (tagihanSebelumnya - pembayaranSebelumnya.TotalPembayaran);
        //                            penaltyNominal = (tagihanSebelumnyaPayment * isHavePenalty.PenaltyPercentage / 100);

        //                            if (penaltyNominal < 0)
        //                            {

        //                                penaltyNominal = 0;

        //                            }
        //                        }
        //                        else
        //                        {
        //                            penaltyNominal = isHavePenalty.PenaltyNominal;
        //                        }

        //                    }

        //                    var TotalJumlahYangHarusDibayar = (tagihanSebelumnya + TotalTagiahPerbulan + penaltyNominal) - pembayaranSebelumnya.TotalPembayaran;


        //                    #endregion


        //                    #region generate invoiceNo
        //                    var getRunningNo = 0;
        //                    var getTrRunning = (from a in _contextBilling.TR_RunningNumber
        //                                        where a.SiteId == SiteID && a.DocCode == "INV" && a.ProjectId == unit.a.ProjectId && a.PeriodId == getPeriod.Id
        //                                        orderby a.Number descending
        //                                        select a).FirstOrDefault();

        //                    if (getTrRunning == null)
        //                    {
        //                        getRunningNo = 1;

        //                        var AddRunningNumber = new TR_RunningNumber
        //                        {
        //                            DocCode = "INV",
        //                            ProjectId = unit.a.ProjectId,
        //                            Number = getRunningNo,
        //                            SiteId = SiteID,
        //                            PeriodId = getPeriod.Id

        //                        };

        //                        _contextBilling.TR_RunningNumber.Add(AddRunningNumber);

        //                    }
        //                    else
        //                    {
        //                        getRunningNo = getTrRunning.Number + 1;
        //                        getTrRunning.Number = getRunningNo;


        //                        _contextBilling.TR_RunningNumber.Update(getTrRunning);

        //                    }

        //                    _contextBilling.SaveChanges();


        //                    var generateRunningNo = "0000000";
        //                    var CountRunningNo = getRunningNo.ToString().Length;
        //                    var CountSub = generateRunningNo.Length - CountRunningNo;

        //                    var invNoFinal = generateRunningNo.Substring(0, CountSub) + getRunningNo;


        //                    var InvoiceNoComb = "" + invNoFinal + "/INV/" + getSite.SiteCode + "/" + getPeriod.PeriodMonth.ToString("MM") + "/" + getPeriod.PeriodYear.ToString("yyyy") + "";

        //                    #endregion



        //                    var inserData = new TR_InvoiceHeader
        //                    {
        //                        PeriodId = getPeriod.Id,
        //                        ClusterId = unit.a.ClusterId,
        //                        ProjectId = unit.a.ProjectId,
        //                        PsCode = unit.a.PsCode,
        //                        SiteId = unit.a.SiteId,
        //                        UnitDataId = unit.a.Id,
        //                        TemplateInvoiceHeaderId = unit.b.TemplateInvoiceHeaderId,
        //                        InvoiceDate = DateTime.Now,
        //                        JumlahYangHarusDibayar = TotalJumlahYangHarusDibayar,
        //                        TagihanPerbulan = TotalTagiahPerbulan,
        //                        InvoiceNo = InvoiceNoComb,
        //                        IsSendEmailDate = null,
        //                        IsSendWADate = null,
        //                        JatuhTempo = getPeriod.CloseDate,
        //                        InvoiceDoc = InvoiceNoComb.Replace("/", "") + ".pdf",
        //                        PenaltyNominal = penaltyNominal,
        //                        TagihanBulanSebelumnya = tagihanSebelumnya,
        //                        VirtualAccNo = unit.b.VirtualAccNo,
        //                        BankId = unit.b.BankId,
        //                        IsPenalty = unit.b.IsPenalty,




        //                    };

        //                    _contextBilling.TR_InvoiceHeader.Add(inserData);
        //                    _contextBilling.SaveChanges();

        //                    int InvoiceHeaderID = inserData.Id;

        //                    var getitemID = (from a in _contextBilling.MS_UnitItemDetail
        //                                     join b in _contextBilling.MS_TemplateInvoiceDetail on a.TemplateInvoiceDetailId equals b.Id
        //                                     where a.UnitItemHeaderId == unit.b.Id
        //                                     select new
        //                                     {
        //                                         a.UnitItemHeaderId,
        //                                         a.TemplateInvoiceDetailId,
        //                                         b.ItemId,

        //                                     }).ToList();


        //                    foreach (var item in getitemID)
        //                    {
        //                        var getItemElectric = (from a in _contextBilling.MS_Item
        //                                               where a.Id == item.ItemId && a.ItemName == "Electric"
        //                                               select a).FirstOrDefault();

        //                        var insertDetail = new TR_InvoiceDetail
        //                        {
        //                            InvoiceHeaderId = InvoiceHeaderID,
        //                            ItemId = item.ItemId,
        //                            ItemNominal = getItemElectric != null ? getDetailPPNWater.ElectricTotal : getDetailPPNWater.itemIdBPL == item.ItemId ? getDetailPPNWater.BPLTotal : getDetailPPNWater.WaterTotal,
        //                            PPNNominal = getDetailPPNWater.itemIdBPL == item.ItemId ? Convert.ToInt32(getDetailPPNWater.PPNBPL) : Convert.ToInt32(getDetailPPNWater.PPNWater),

        //                        };


        //                        _contextBilling.TR_InvoiceDetail.Add(insertDetail);
        //                        _contextBilling.SaveChanges();




        //                    }





        //                    #region Proses generate PDF
        //                    var LoopDeskripsiPdf = new List<String>();
        //                    var StingData = "";
        //                    int NoDetail = 0;

        //                    if (tagihanSebelumnya != 0)
        //                    {
        //                        NoDetail++;
        //                        StingData = " <tr> <td>" + NoDetail + "</td> " + //No
        //                      "<td>Tagihan Sebelumnya</td>" + //deskripsi
        //                      "<td>-</td>" +  //tanggal
        //                      "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(tagihanSebelumnya) + "</td>" +      //jumlah
        //                      "</tr>";

        //                        LoopDeskripsiPdf.Add(StingData);

        //                    }




        //                    if (pembayaranSebelumnya.TotalPembayaran != 0)
        //                    {
        //                        #region getListpembayaran sebelumnya

        //                        foreach (var pembayaran in pembayaranSebelumnya.DetailPembayaranSebelumnya)
        //                        {
        //                            NoDetail++;

        //                            StingData = " <tr> <td>" + NoDetail + "</td> " + //No
        //                            "<td>Pembayaran melalui " + pembayaran.PaymentMethod + "</td>" + //deskripsi
        //                            "<td>" + pembayaran.TransactionDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                            "<td>(Rp. " + NumberHelper.IndoFormatWithoutTail(pembayaran.PaymentAmount) + ")</td>" +      //jumlah
        //                            "</tr>";

        //                            LoopDeskripsiPdf.Add(StingData);

        //                        }

        //                        #endregion



        //                    }

        //                    var getDetailInvoice = (from a in _contextBilling.TR_InvoiceDetail
        //                                            join b in _contextBilling.MS_Item on a.ItemId equals b.Id
        //                                            where a.InvoiceHeaderId == InvoiceHeaderID
        //                                            select new
        //                                            {
        //                                                a.ItemNominal,
        //                                                a.CreationTime,
        //                                                a.ItemId,
        //                                                a.PPNNominal,
        //                                                b.ItemName,



        //                                            }).ToList();



        //                    if (unit.c.TemplateInvoiceName == "Water" || unit.c.TemplateInvoiceName == "BPL & Water" || unit.c.TemplateInvoiceName == "Electric")
        //                    {


        //                        var totalBPL = (getDetailPPNWater.BPLTotal + getDetailPPNWater.PPNBPL + penaltyNominal);
        //                        var totalWater = (getDetailPPNWater.WaterTotal + getDetailPPNWater.PPNWater);

        //                        paramtoPdf.totalBpl = totalBPL == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalBPL);
        //                        paramtoPdf.totalWater = totalWater == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(totalWater);


        //                        paramtoPdf.PencatatanAwal = getDetailPPNWater.PencatatanAwal == null ? "0" : getDetailPPNWater.PencatatanAwal;
        //                        paramtoPdf.PencatatanAkhir = getDetailPPNWater.PencatatanAkhir == null ? "0" : getDetailPPNWater.PencatatanAkhir;
        //                        paramtoPdf.Pemakaian = getDetailPPNWater.Pemakaian == null ? "0" : getDetailPPNWater.Pemakaian;
        //                        paramtoPdf.Int1m3 = getDetailPPNWater.Int1m3;
        //                        paramtoPdf.Int1Nominal = getDetailPPNWater.Int1Nominal;
        //                        paramtoPdf.Int1Tagihan = getDetailPPNWater.Int1Tagihan == null ? "0" : getDetailPPNWater.Int1Tagihan;
        //                        paramtoPdf.Int2m3 = getDetailPPNWater.Int2m3;
        //                        paramtoPdf.Int2Nominal = getDetailPPNWater.Int2Nominal;
        //                        paramtoPdf.Int2Tagihan = getDetailPPNWater.Int2Tagihan == null ? "0" : getDetailPPNWater.Int2Tagihan;
        //                        paramtoPdf.Int3m3 = getDetailPPNWater.Int3m3;
        //                        paramtoPdf.Int3Nominal = getDetailPPNWater.Int3Nominal;
        //                        paramtoPdf.Int3Tagihan = getDetailPPNWater.Int3Tagihan == null ? "0" : getDetailPPNWater.Int3Tagihan;
        //                        paramtoPdf.PemakaianAirBersih = getDetailPPNWater.PemakaianAirBersih == null ? "0" : getDetailPPNWater.PemakaianAirBersih;
        //                        paramtoPdf.BiayaPemeliharaan = getDetailPPNWater.BiayaPemeliharaan == null ? "0" : getDetailPPNWater.BiayaPemeliharaan;
        //                        paramtoPdf.JumlahTagihanAir = getDetailPPNWater.WaterTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.WaterTotal);
        //                        paramtoPdf.JumlahTagihanListrik = getDetailPPNWater.ElectricTotal == 0 ? "0" : NumberHelper.IndoFormatWithoutTail(getDetailPPNWater.ElectricTotal);
        //                        paramtoPdf.PerhitunganPemakaianListrik = getDetailPPNWater.PerhitunganPemakaianListrik;
        //                        paramtoPdf.BiayaPemakaianListrik = getDetailPPNWater.BiayaPemakaianListrik;



        //                    }

        //                    //perbaikan pengurutan angka
        //                    if (unit.c.TemplateInvoiceName != "BPL & Water")
        //                    {

        //                        foreach (var detailinvoice in getDetailInvoice)
        //                        {
        //                            NoDetail++;
        //                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
        //                                "</tr>";

        //                            LoopDeskripsiPdf.Add(StingData);

        //                            if (detailinvoice.PPNNominal > 0)
        //                            {
        //                                NoDetail++;
        //                                StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                   "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                   "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                   "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
        //                                   "</tr>";

        //                                LoopDeskripsiPdf.Add(StingData);
        //                            }

        //                        }

        //                        //if (penaltyNominal > 0) {
        //                        if (isHavePenalty != null && penaltyNominal >= 0)
        //                        {


        //                            NoDetail++;
        //                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                  "<td>Penalty </td>" + //deskripsi
        //                                  "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                  "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
        //                                  "</tr>";


        //                            LoopDeskripsiPdf.Add(StingData);

        //                        }

        //                    }
        //                    else
        //                    {
        //                        //jika bpl &  water

        //                        foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "BPL").ToList())
        //                        {
        //                            NoDetail++;
        //                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
        //                                "</tr>";

        //                            LoopDeskripsiPdf.Add(StingData);

        //                            if (detailinvoice.PPNNominal > 0)
        //                            {
        //                                NoDetail++;
        //                                StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                   "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                   "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                   "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
        //                                   "</tr>";

        //                                LoopDeskripsiPdf.Add(StingData);
        //                            }

        //                        }

        //                        //if (penaltyNominal > 0) {
        //                        if (isHavePenalty != null && penaltyNominal >= 0)
        //                        {

        //                            NoDetail++;
        //                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                  "<td>Penalty </td>" + //deskripsi
        //                                  "<td>" + DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                  "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(penaltyNominal) + "</td>" + //jumlah
        //                                  "</tr>";


        //                            LoopDeskripsiPdf.Add(StingData);

        //                        }


        //                        //water 

        //                        foreach (var detailinvoice in getDetailInvoice.Where(x => x.ItemName == "Water").ToList())
        //                        {
        //                            NoDetail++;
        //                            StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                "<td>" + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.ItemNominal) + "</td>" + //jumlah
        //                                "</tr>";

        //                            LoopDeskripsiPdf.Add(StingData);

        //                            if (detailinvoice.PPNNominal > 0)
        //                            {
        //                                NoDetail++;
        //                                StingData = " <tr><td>" + NoDetail + "</td> " + //No
        //                                   "<td>PPN " + detailinvoice.ItemName + "</td>" + //deskripsi
        //                                   "<td>" + detailinvoice.CreationTime.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")) + "</td>" + //tanggal
        //                                   "<td>Rp. " + NumberHelper.IndoFormatWithoutTail(detailinvoice.PPNNominal) + "</td>" + //jumlah
        //                                   "</tr>";

        //                                LoopDeskripsiPdf.Add(StingData);
        //                            }

        //                        }





        //                    }






        //                    string LoopDeskripsiPdfString = string.Join(Environment.NewLine, LoopDeskripsiPdf.ToArray());

        //                    string DeskripsiBPLString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("BPL") || x.Contains("Tagihan Sebelumnya") || x.Contains("Penalty") || x.Contains("Adjustment Fee") || x.Contains("Pembayaran melalui ")).ToList().ToArray());

        //                    string DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Water")).ToList().ToArray());


        //                    if (unit.c.TemplateInvoiceName == "Electric")
        //                    {

        //                        DeskripsiWaterString = string.Join(Environment.NewLine, LoopDeskripsiPdf.Where(x => x.Contains("Electric")).ToList().ToArray());

        //                    }




        //                    paramtoPdf.SiteLogo = getSite.Logo;
        //                    paramtoPdf.SiteAddres = getSite.SiteAddress;
        //                    paramtoPdf.SiteEmail = getSite.Email;
        //                    paramtoPdf.SiteName = getSite.SiteName;
        //                    paramtoPdf.SitePhone = "Telp. " + getSite.OfficePhone + " - " + getSite.HandPhone + "";
        //                    paramtoPdf.PsCode = unit.a.PsCode;
        //                    paramtoPdf.CustName = getPScode.name;
        //                    paramtoPdf.CustEmail = getPScode.email;
        //                    paramtoPdf.CustPhone = getPhoneCust;
        //                    paramtoPdf.CustAlamatKoresponden = getPScode.address;
        //                    paramtoPdf.InvoiceDate = inserData.InvoiceDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
        //                    paramtoPdf.NomerInvoice = inserData.InvoiceNo;
        //                    paramtoPdf.TagihanSampaiBulanIni = NumberHelper.IndoFormatWithoutTail(inserData.JumlahYangHarusDibayar);
        //                    paramtoPdf.JumlahyangHarusDibayar = NumberHelper.IndoFormatWithoutTail(inserData.JumlahYangHarusDibayar);
        //                    paramtoPdf.TagihanBulanIni = NumberHelper.IndoFormatWithoutTail(inserData.TagihanPerbulan);
        //                    paramtoPdf.UnitName = unit.a.UnitName;
        //                    paramtoPdf.UnitNo = unit.a.UnitNo;
        //                    paramtoPdf.VaNo = getVa.VirtualAccNo;
        //                    paramtoPdf.VaBank = getVa.BankName;
        //                    paramtoPdf.Deskripsi = LoopDeskripsiPdfString;
        //                    paramtoPdf.InvoiceName = InvoiceNoComb.Replace("/", "");
        //                    paramtoPdf.BulanPeriod = getPeriod.PeriodMonth.ToString("MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
        //                    paramtoPdf.BulanJatuhTempo = getPeriod.CloseDate.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
        //                    paramtoPdf.TemplateInvoice = unit.c.TemplateUrl;
        //                    paramtoPdf.DeskripsiBPL = DeskripsiBPLString;
        //                    paramtoPdf.DeskripsiTagihanAir = DeskripsiWaterString;







        //                    GenerateInvoicePdf(paramtoPdf);
        //                    #endregion




        //                    #endregion
        //                }
        //                catch
        //                {

        //                }
        //            }


        //        }



        //    }
        //    catch
        //    {


        //    }



        //}

        //public async Task<ReportUriDto> ExportToExcelUser()
        //{
        //    #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionApp = (JObject)appsettingsjson["App"];
        //    var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
        //    #endregion
        //    FileDto file = new FileDto();
        //    var userList = new List<UserExportInputDto>();
            


        //    var getDataUser = (from a in _contextBilling.Users
        //                       select a).ToList();

        //    foreach (var item in getDataUser) 
        //    {

        //        var getUserRole = (from a in _contextBilling.UserRoles
        //                           join b in _contextBilling.Roles on a.RoleId equals b.Id
        //                           where a.UserId == item.Id

        //                           select b.Name).Distinct().ToList();
        //        var getLastLogin = (from a in _contextBilling.UserLoginAttempts
        //                            where a.UserId == item.Id
        //                            orderby a.CreationTime descending
        //                            select a).FirstOrDefault();


        //        var addUser = new UserExportInputDto
        //        {
        //            Active = item.IsActive == true ? 1 : 0,
        //            CreatedDate = item.CreationTime.ToString("dd MMM yyyy"),
        //            Email = item.EmailAddress,
        //            LastLoginDate = getLastLogin != null ? getLastLogin.CreationTime.ToString("dd MMM yyyy") : "",
        //            Name = item.Name,
        //            Surname = item.Surname,
        //            UserName = item.UserName,
        //            PhoneNumber = item.PhoneNumber,
        //            Roles = string.Join(",", getUserRole),
        //         };

        //        userList.Add(addUser);
        //    }


        //    if (userList.Count > 0)
        //    {
        //        var paramToExcel = new ViewUserReportDto
        //        {
        //            printBy = (from x in _contextBilling.Users
        //                       where x.Id == AbpSession.GetUserId()
        //                       select x.Name).FirstOrDefault(),

        //            ListUser = userList,
        //        };


        //        file = _exporter.ExportToExcelUserResult(paramToExcel);

        //        //var tempRootPath = Path.Combine(_appFolders.UploadRootDirectory, _appFolders.TempFileDownloadDirectory);


        //        System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
        //        var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


        //        var filePath = Path.Combine(tempRootPath);
        //        if (!File.Exists(filePath))
        //        {
        //            throw new Exception("Requested File doesn't exists");
        //        }

        //        var pathExport = Path.Combine(tempRootPath);
        //        //retrieve data excel from temporary download folder
        //        var fileBytes = File.ReadAllBytes(filePath);
        //        //write excel file to share folder / local folder
        //        File.WriteAllBytes(pathExport, fileBytes);
        //        //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
        //        string URL = ipPublic + @"\Temp\Downloads\" + file.FileName;
        //        return new ReportUriDto { Uri = URL.Replace("\\", "/") };
        //    }
        //    else
        //    {

        //        return new ReportUriDto { };
        //    }

        //}



    }
}
