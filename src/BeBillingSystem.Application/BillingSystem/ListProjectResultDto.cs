﻿namespace BeBillingSystem.BillingSystem
{
	public class ListProjectResultDto
	{
		public int projectID { get; set; }

		public int productID { get; set; }

		public string productName { get; set; }
		public string parentProjectName { get; set; }

		public string projectCode { get; set; }

		public string projectName { get; set; }

		public string regionName { get; set; }

		public string message { get; set; }

		public string image { get; set; }

		public string type { get; set; }

		public bool? isRequiredPP { get; set; }
	}
}