﻿using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.BillingSystem.TransactionToOracle.Dto;
using BeBillingSystem.Roles.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public interface IReportDataExporter
    {
        FileDto ExportToExcelWaterReadingResult(ViewWaterReadingDto data);
        FileDto ExportToExcelWarningLetterResult(ViewExportWarningLetterDto data);
        FileDto ExportToExcelReportInvoiceResult(ViewReportinvoiceDto data);
        FileDto ExportToExcelReportInvoiceDetailResult(ViewReportinvoiceDto data);
        FileDto ExportToExcelErrorDataBulkResult(UploadBulkPaymentDto data);
        FileDto ExportToExcelDailyReportResult(ViewReportDailyDto data);
        FileDto ExportToExcelErrorUploadWaterResult(WaterReadingUploadExcelDto data);
        FileDto ExportToExcelErrorUploadElectricResult(WaterReadingUploadExcelDto data);
        FileDto ExportToExcelErrorUploadUnitItemResult(UploadNewUnitItemDto data);
        FileDto ExportToExcelErrorUploadAdjResult(UploadExcelChangeAdjInvoiceDto data);
        FileDto ExportToExcelDetailStetmentReportResult(ViewReportDetailStetmentDto data);
        FileDto ExportToExcelJournalToOracle(List<GetBillingJournalTaskListDto> data);

        FileDto ExportToExcelUserResult(ViewUserReportDto data);
    }
}
