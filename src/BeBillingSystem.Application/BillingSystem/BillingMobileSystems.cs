﻿using Abp.UI;
using Abp.Web.Models;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.EntityFrameworkCore;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Dapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text.Json.Serialization;
using static BeBillingSystem.BillingSystem.TransactionToOracle.Dto.GeneratePaymentJournalDto;

namespace BeBillingSystem.BillingSystem
{
    public class BillingMobileSystems : BeBillingSystemAppServiceBase
    {

        private readonly BeBillingSystemDbContext _contextBilling;
        private readonly IReportDataExporter _exporter;
        private readonly FilesHelper _filesHelper;
        public IAppFolders _appFolders { get; set; }
        private readonly PropertySystemDbContext _contextProp;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly PersonalsNewDbContext _contextPersonal;

        public BillingMobileSystems(
            FilesHelper filesHelper,
            IWebHostEnvironment hostEnvironment,
            PropertySystemDbContext contextProp,
              IReportDataExporter exporter,
              BeBillingSystemDbContext contextBilling,
                 PersonalsNewDbContext contextPersonal
            //IDbContextProvider<PropertySystemDbContext> contextProp


            //BeBillingSystemDbContext contextBilling

            )
        {

            _filesHelper = filesHelper;
            _exporter = exporter;
            _contextProp = contextProp;
            _contextBilling = contextBilling;
            _hostEnvironment = hostEnvironment;
            _contextPersonal = contextPersonal;
            //_contextProp = contextProp.GetDbContext();
            //_contextBilling = contextBilling;


        }

        //[DontWrapResult(WrapOnError = false, WrapOnSuccess = false, LogError = false)]
        //public DataValidate ValidateLoginByEmailAsync(ValidateLoginDto input )
        //{
      
        //    var dataValidate = new DataValidate();
        //    var data = new  List<DataMainValidate>();
        //    var datamain = new List<DataMainValidate>();

        //    #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
        //    var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
        //    var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
        //    var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

        //    var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

        //    var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

        //    var connectionApp = (JObject)appsettingsjson["App"];
        //    var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

        //    SqlConnection conn = new SqlConnection(ConstringProp);

        //    conn.Open();

        //    #endregion




        //    string query = $@"select top 1 d.name, d.psCode, d.birthDate, b.email, c.number as MobilePhone, d.sex, e.idNo as KTP, d.NPWP 
        //                    from " + dbCatalogBillingString + "..MS_UnitData a " +
        //                    "join " + dbCatalogPersonalString + "..TR_Email b on a.PsCode = b.psCode " +
        //                    "join " + dbCatalogPersonalString + "..TR_Phone c on a.PsCode = c.psCode " +
        //                    "join " + dbCatalogPersonalString + "..PERSONALS d on a.PsCode = d.psCode " +
        //                    "join " + dbCatalogPersonalString + "..TR_ID e on a.PsCode = e.psCode " +
        //                    "where b.email = '" + input.UserOrEmail + "' or c.number = '" + input.UserOrEmail + "'";

        //    data = conn.Query<DataMainValidate>(query).ToList();

        //    if (data == null) {

        //        throw new UserFriendlyException("Data tidak ditemukan");
        //    }

        //    foreach(var item in data)
        //    {
        //        var addIrtem = new DataMainValidate();

        //        var getListUnit = (from a in _contextBilling.MS_UnitData
        //                           join b in _contextBilling.MP_ClusterSite on a.ClusterId equals b.ClusterId
        //                           where a.PsCode == item.PSCode
        //                           select new UnitValidate
        //                           {
        //                               OrgID = 1,
        //                               SiteID = a.SiteId,
        //                               PSCode = a.PsCode,
        //                               UnitDesc = a.UnitName,
        //                               UnitCode = a.UnitCode,
        //                               UnitNo = a.UnitNo,
        //                               Unit = a.UnitCode + "-" + a.UnitNo,
        //                               UnitClusterDesc = b.ClusterName,
        //                               UnitClusterCode = b.ClusterCode,
        //                               SubLocationID = 0,
        //                               LocationID = 0
        //                           }).ToList();




        //        addIrtem = new DataMainValidate
        //        {
        //            BirthDate = item.BirthDate,
        //            Email = item.Email,
        //            KTP = item.KTP,
        //            MobilePhone = item.MobilePhone,
        //            Name = item.Name,
        //            NPWP = item.NPWP,
        //            PSCode = item.PSCode,
        //            Sex = item.Sex,
        //            unit = getListUnit
        //        };

        //        datamain.Add(addIrtem);
        //    }



        //    dataValidate.data = datamain;



        //    return dataValidate;

        //}

        //[DontWrapResult(WrapOnError = false, WrapOnSuccess = false, LogError = false)]
        //public GetTotalOutstandingResponseDto ProsesGetTotalDataOutstanding(GetTotalDataOutDto input) {



        //    var final = new GetTotalOutstandingResponseDto();

        //        #region Constring DB
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
        //    var ConstringBilling = connectionStrings.Property("Default").Value.ToString();
        //    var ConstringProp = connectionStrings.Property("PropertySystemDbContext").Value.ToString();
        //    var ConstringPersonal = connectionStrings.Property("PersonalsNewDbContext").Value.ToString();

        //    var dbBillingString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPropertyString = connectionStrings.Property("PropertySystemDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");
        //    var dbPersonalString = connectionStrings.Property("PersonalsNewDbContext").Value.ToString().Replace(" ", string.Empty).Split(";");

        //    var dbCatalogBillingString = dbBillingString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPropertyString = dbPropertyString[2].Replace("InitialCatalog=", string.Empty);
        //    var dbCatalogPersonalString = dbPersonalString[2].Replace("InitialCatalog=", string.Empty);

        //    var connectionApp = (JObject)appsettingsjson["App"];
        //    var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

        //    SqlConnection conn = new SqlConnection(ConstringProp);

        //    conn.Open();

        //    #endregion




        //    string query = $@"select top 1 d.name, d.psCode, d.birthDate, b.email, c.number as MobilePhone, d.sex, e.idNo as KTP, d.NPWP 
        //                    from " + dbCatalogBillingString + "..MS_UnitData a " +
        //                    "join " + dbCatalogPersonalString + "..TR_Email b on a.PsCode = b.psCode " +
        //                    "join " + dbCatalogPersonalString + "..TR_Phone c on a.PsCode = c.psCode " +
        //                    "join " + dbCatalogPersonalString + "..PERSONALS d on a.PsCode = d.psCode " +
        //                    "join " + dbCatalogPersonalString + "..TR_ID e on a.PsCode = e.psCode " +
        //                    "where b.email = '" + input.UserOrEmail + "' or c.number = '" + input.UserOrEmail + "'";

        //    var dataPsCode = conn.Query<DataMainValidate>(query).FirstOrDefault();

        //    var getPeriod = (from a in _contextBilling.MS_Period
        //                     where a.SiteId == input.SiteId && a.IsActive == true
        //                     select a).FirstOrDefault();

        //    var getData = (from a in _contextBilling.TR_InvoiceHeader
        //                   where a.PsCode == dataPsCode.PSCode && a.SiteId == input.SiteId && a.PeriodId == getPeriod.Id
        //                   select a
        //                   ).ToList();

        //    var totalTagihan = getData.Select(x => x.JumlahYangHarusDibayar).Sum();
        //    var invoiceHeaderId = getData.Select(x => x.Id).ToList();


        //    var getPayment = (from a in _contextBilling.TR_BillingPaymentHeader
        //                      join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.BillingHeaderId
        //                      where b.InvoiceHeaderId.Equals(invoiceHeaderId)
        //                      select b.PaymentAmount).Sum();


        //    var jumlah = totalTagihan - getPayment;

        //    final.Amount = jumlah;
        //    final.PsCode = dataPsCode.PSCode;

        //    return final;








        //}



        //public PaymnetSisaTagiahanDto JumlahSisaTagihan(int InvoiceID)
        //{

        //    var getInvoice = (from a in _contextBilling.TR_InvoiceHeader
        //                      where a.Id == InvoiceID
        //                      select a).FirstOrDefault();


        //    var getPaymentDetail = (from a in _contextBilling.TR_BillingPaymentHeader
        //                            join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.BillingHeaderId
        //                            where b.InvoiceHeaderId == InvoiceID && a.UnitDataId == getInvoice.UnitDataId && a.CancelPayment == false
        //                            select b.PaymentAmount).ToList();

        //    var getAdjCount = (from a in _contextBilling.TR_AdjustmentDetail
        //                       where a.InvoiceHeaderId == InvoiceID
        //                       select a.AdjustmentNominal).Sum();

        //    var pengurangan = getPaymentDetail.Sum();


        //    decimal Hasil = (getInvoice.JumlahYangHarusDibayar + getAdjCount) - pengurangan;

        //    var final = new PaymnetSisaTagiahanDto
        //    {
        //        Payment = pengurangan,
        //        sisaTagihan = Hasil,
        //    };


        //    return final;
        //}


        //public List<Detail> DeskripsiInvoice(int InvoiceID)
        //{
        //    var finaldata = new List<Detail>();

        //    var getInvoiceDetail = (from a in _contextBilling.TR_InvoiceDetail
        //                            join b in _contextBilling.MS_Item on a.ItemId equals b.Id
        //                            where a.InvoiceHeaderId == InvoiceID
        //                            select new
        //                            {
        //                                a.InvoiceHeaderId,
        //                                a.ItemId,
        //                                a.ItemNominal,
        //                                a.PPNNominal,
        //                                b.ItemName,

        //                            }).Distinct().ToList();

        //    foreach (var item in getInvoiceDetail)
        //    {
        //        //addmain
        //        var addMain = new Detail
        //        {
        //            transactionDate = item.ItemName,
        //            transactionAmount = item.ItemNominal.ToString(),
        //            transactionCategoryDesc = item.ItemName,
        //        };
        //        finaldata.Add(addMain);

        //        //addPPN
        //        if (item.PPNNominal != 0)
        //        {
        //            var addPPN = new Detail
        //            {
        //                transactionDesc = "PPN " + item.ItemName,
        //                transactionAmount = item.PPNNominal.ToString(),
        //                transactionCategoryDesc = "PPN " + item.ItemName,
        //            };
        //            finaldata.Add(addPPN);

        //        }

        //    };

        //    //addPayment

        //    var addPayment = (from a in _contextBilling.TR_BillingPaymentHeader
        //                      join b in _contextBilling.TR_BillingPaymentDetail on a.Id equals b.BillingHeaderId
        //                      where b.InvoiceHeaderId == InvoiceID
        //                      select new Detail
        //                      {
        //                          transactionCategoryDesc = "Payment",
        //                          transactionAmount = b.PaymentAmount.ToString(),
        //                          transactionDate = a.TransactionDate.ToString("dd/MMMM/yyyy"),
        //                          transactionDesc = ""
        //                      }).ToList();

        //    finaldata.AddRange(addPayment);


        //    var getAdjCount = (from a in _contextBilling.TR_AdjustmentDetail
        //                       where a.InvoiceHeaderId == InvoiceID
        //                       select a).ToList();

        //    foreach (var item in getAdjCount) {
        //        var addAdj = new Detail
        //        {
        //            transactionDesc = "Adjustment",
        //            transactionAmount = item.AdjustmentNominal < 0 ? "(" + item.AdjustmentNominal + ")" : item.AdjustmentNominal.ToString(),
        //            transactionCategoryDesc = "Adjustment",
        //            transactionDate = item.AdjustmentDate.ToString("dd/MMMM/yyyy"),
        //        };
        //        finaldata.Add(addAdj);




        //    }


        




        //    return finaldata;
        //}



        //[DontWrapResult(WrapOnError = false, WrapOnSuccess = false, LogError = false)]
        //public RootGetDataTotalOutstandingDto ProsesGetDataTotalOutstanding(InputGetInvoiceDto input)
        //{
        //    var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
        //    var connectionApp = (JObject)appsettingsjson["App"];
        //    var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

        //    var final = new RootGetDataTotalOutstandingDto();


        //    var getHeader = new DataGetDataTotalOutstandingDto();
        //    var getListUnit = new List<UnitGetDataTotalOutstandingDto>();


        //    var getMain = (from a in _contextBilling.MS_UnitData
        //                   where a.SiteId == input.siteID && a.PsCode == input.psCode
        //                   // && a.UnitCode == input.unitCode && a.UnitNo == input.unitNo
        //                   select a).ToList();

        //    if (getMain != null)
        //    {

        //        getHeader.siteID = input.siteID;
        //        getHeader.psCode = input.psCode;
        //        getHeader.name = (from a in _contextPersonal.PERSONALS
        //                          where a.psCode == input.psCode
        //                          select a.name).FirstOrDefault();


        //        foreach (var item in getMain)
        //        {
        //            var ListPeriod = new List<PeriodeGetDataTotalOutstandingDto>();


        //            var GetPeriodLast = (from a in _contextBilling.MS_Period
        //                                 where a.SiteId == input.siteID
        //                                 orderby a.Id descending
        //                                 select a
        //                           ).Take(3).ToList();

        //            foreach (var period in GetPeriodLast)
        //            {
        //                var ListInvoiceDetail = new List<InvoiceDetailGetDataTotalOutstandingDto>();


        //                var getListInvoice = (from a in _contextBilling.TR_InvoiceHeader
        //                                      where a.PeriodId == period.Id && a.UnitDataId == item.Id
        //                                      select a).ToList();

        //                foreach (var invoice in getListInvoice) 
        //                {

        //                    List<Detail> deskripsiInvoice = DeskripsiInvoice(invoice.Id);

        //                    PaymnetSisaTagiahanDto sisaTagihan = JumlahSisaTagihan(invoice.Id);

        //                    var isPayment = period.IsActive == true && sisaTagihan.sisaTagihan != 0 ? false : true; 

        //                    var addInvoiceDetail = new InvoiceDetailGetDataTotalOutstandingDto
        //                    {

        //                        detail = deskripsiInvoice,
        //                        previousBalance = invoice.TagihanBulanSebelumnya,
        //                        currentTransaction = sisaTagihan.sisaTagihan,
        //                        billingOutstanding = invoice.TagihanBulanSebelumnya + sisaTagihan.sisaTagihan,
        //                        invoiceNumber = invoice.InvoiceNo,
        //                        payment = sisaTagihan.Payment,
        //                        vaNo = invoice.VirtualAccNo,
        //                        isPayment = isPayment,
        //                        invoiceUrl = ipPublic + @"\Assets\Upload\PdfOutput\Invoice\" + invoice.InvoiceDoc
        //                };

        //                    ListInvoiceDetail.Add(addInvoiceDetail);

        //                }



        //                var addPeriod = new PeriodeGetDataTotalOutstandingDto
        //                {
        //                    no = period.PeriodNumber,
        //                    dueDate = period.CloseDate,
        //                    name = period.PeriodMonth.ToString("MMMM") + " " + period.PeriodYear.ToString("yyyy"),
        //                    invoiceDetail = ListInvoiceDetail,

        //                };

        //                ListPeriod.Add(addPeriod);

        //            };




        //            var addData = new UnitGetDataTotalOutstandingDto
        //            {
        //                unitCode = item.UnitCode,
        //                unitName = item.UnitName,
        //                unitNo = item.UnitNo,
        //                periode = ListPeriod,
        //            };

        //            getListUnit.Add(addData);

        //        }
        //        getHeader.unit = getListUnit;


        //        final.data = getHeader;




        //        //mapping untuk period


        
        //    }
        //    return final;
        //}
    }
}
