﻿namespace BeBillingSystem.BillingSystem
{
	public class CurrenInfoDataDto
	{
		public string accessToken { get; set; }
		public int expireInSeconds { get; set; }
		public int userId { get; set; } 
	}
}