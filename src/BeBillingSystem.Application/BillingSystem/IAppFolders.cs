﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.BillingSystem
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }

        string SampleProfileImagesFolder { get; }

        string WebLogsFolder { get; set; }

        string FileDownloadDirectory { get; set; }

        string TempFileDownloadDirectory { get; set; }

        string UploadRootDirectory { get; set; }

        string UploadPrefixUrl { get; set; }
    }
}