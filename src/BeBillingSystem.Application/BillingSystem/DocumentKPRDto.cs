﻿namespace BeBillingSystem.BillingSystem
{
	public class DocumentKPRDto
	{
		public int? BookingHeaderID { get; set; }
		public int PropertyPassID { get; set; }
	}
}