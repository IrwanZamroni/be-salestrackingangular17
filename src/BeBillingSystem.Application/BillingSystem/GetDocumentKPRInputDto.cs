﻿namespace BeBillingSystem.BillingSystem
{
	public class GetDocumentKPRInputDto
	{
		public string psCode { get; set; }
		public int? bookingHeaderID { get; set; }
		public string ppNo { get; set; }
	}
}