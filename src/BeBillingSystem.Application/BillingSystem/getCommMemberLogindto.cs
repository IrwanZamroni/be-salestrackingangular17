﻿namespace BeBillingSystem.BillingSystem
{
	public class getCommMemberLogindto
	{
		public string bookNo { get; set; }
		public string memberCode { get; set; }
		public decimal amountComm { get; set; }
	}
}