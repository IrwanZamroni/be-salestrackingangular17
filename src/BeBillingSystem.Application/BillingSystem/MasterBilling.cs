﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.EntityFrameworkCore;
using BeBillingSystem.Migrations;
using BeBillingSystem.PersonalDB;
using BeBillingSystem.Sessions.Dto;
using Castle.Core.Internal;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static AutoMapper.Internal.ExpressionFactory;

namespace BeBillingSystem.BillingSystem
{
    public class MasterBilling : BeBillingSystemAppServiceBase
    {
 
        private readonly BeBillingSystemDbContext _contextBilling;
        private readonly IReportDataExporter _exporter;
        private readonly FilesHelper _filesHelper;
        public IAppFolders _appFolders { get; set; }
        private readonly PropertySystemDbContext _contextProp;
        private readonly IWebHostEnvironment _hostEnvironment;

        public MasterBilling(
            FilesHelper filesHelper,
            IWebHostEnvironment hostEnvironment,
            PropertySystemDbContext contextProp,
              IReportDataExporter exporter, 
              BeBillingSystemDbContext contextBilling
            //IDbContextProvider<PropertySystemDbContext> contextProp


            //BeBillingSystemDbContext contextBilling

            )
        {

            _filesHelper = filesHelper;
            _exporter = exporter;
            _contextProp = contextProp;
            _contextBilling = contextBilling;
            _hostEnvironment = hostEnvironment;
            //_contextProp = contextProp.GetDbContext();
            //_contextBilling = contextBilling;


        }


//        public void CreatMasterSite (CreateMasterSiteDto input)
//        {
//            string imageUrl;
//            if (input.Logo == null)
//            {
//                imageUrl = "-";
//            }
//            else
//            {
//                imageUrl = UploadImage(input.Logo);
//                GetURLWithoutHost(imageUrl, out imageUrl);
//            }


//            var addData = new MS_Site { 
//                Logo = imageUrl,
//                SiteAddress= input.SiteAddress,
//                SiteName= input.SiteName,
//                Email= input.Email,
//                HandPhone= input.HandPhone,
//                IsActive= input.IsActive,
//                OfficePhone= input.OfficePhone,
//                SiteCode= input.SiteCode,
//                SiteId = input.SiteId,

                
//            };

//            _contextBilling.MS_Site.Add( addData );
//            _contextBilling.SaveChanges();

//            foreach (var pro in input.ProjectDataList) {

//                var addPro = new MP_ProjectSite
//                {
//                    ProjectId = pro.ProjectId,
//                    ProjectName = pro.ProjectName,
//                    SiteId = input.SiteId,
//                    ProjectCode = pro.ProjectCode,
                    
//                };

//                _contextBilling.MP_ProjectSite.Add(addPro);
//                _contextBilling.SaveChanges();
//            }

//            foreach (var item in input.ClusterDataList) {

//                var addCluster = new MP_ClusterSite { 
                
//                    ClusterId= item.ClusterId,
//                    SiteId= input.SiteId,
//                    ClusterCode = item.ClusterCode,
//                    ClusterName = item.ClusterName,
//                    ProjectId = item.ProjectId,
                    
//                };

//                _contextBilling.MP_ClusterSite.Add( addCluster );
//                _contextBilling.SaveChanges();
//            }
           

//        }
//        public GetLoginInfoDto GetLoginUserInfo(int UserId) 
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion
//            var getData = (from a in _contextBilling.Users
//                           where a.Id == UserId
//                           select new GetLoginInfoDto { 
//                               UserId = a.Id,
//                               UserName = a.UserName,
//                               ProfileUrl = ipPublic + "/Assets/Upload/Image/ProfileUser/"+ a.ProfileImg,
//                           }).FirstOrDefault();

//            return getData;

//        }

//        public PagedResultDto<GetListMasterSiteDto> GetListMasterSite(InputFilterMasterSiteDto input)
//        {

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion

//            var getData = (from a in _contextBilling.MS_Site
//                           orderby a.CreationTime descending
//                           select new GetListMasterSiteDto {
//                               SiteId = a.SiteId,
//                               SiteName = a.SiteName,
//                               isActive = a.IsActive,
//                               ProjectCount = (from pro in _contextBilling.MP_ProjectSite
//                                               where pro.SiteId == a.SiteId
//                                               select pro).Count(),
//                               ClusterCount = (from clu in _contextBilling.MP_ClusterSite
//                                               where clu.SiteId == a.SiteId
//                                               select clu).Count(),
//                               logo =  ipPublic + @"\Assets\Upload\LogoSite\" + a.Logo,
//        })
//                           .ToList();

//            var dataCount = getData.Count();
//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();




//            return new PagedResultDto<GetListMasterSiteDto>(dataCount, getData.ToList());

//        }

//        public List<String> GetListProjectBySiteID(int SiteID )
//        {


//            var getData = (from a in _contextBilling.MP_ProjectSite
//                           where a.SiteId == SiteID
//                           orderby a.ProjectName ascending
//                           select a.ProjectName).Distinct().ToList();

         
//            return getData;

//        }
//        public List<String> GetListCulsterBySiteID(int SiteID)
//        {


//            var getData = (from a in _contextBilling.MP_ClusterSite
//                           where a.SiteId == SiteID
//                           orderby a.ClusterName ascending
//                           select a.ClusterName).Distinct().ToList();


//            return getData;

//        }

//        public DetailMasterSiteDto GetDetailMasterSite(int SiteID) {

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion

//            var getdata = (from a in _contextBilling.MS_Site
//                          where a.SiteId == SiteID
//                          select new DetailMasterSiteDto
//                          {
//                              SiteId = a.SiteId,
//                              SiteName = a.SiteName,
//                              SiteAddress = a.SiteAddress,
//                              SiteCode = a.SiteCode,
//                              Email = a.Email,
//                              OfficePhone = a.OfficePhone,
//                              HandPhone = a.HandPhone,
//                              Logo = ipPublic + @"\Assets\Upload\LogoSite\" + a.Logo,
//                              IsActive = a.IsActive,
//                          }).FirstOrDefault();

//            getdata.ProjectDataList = (from a in _contextBilling.MP_ProjectSite
//                                       where a.SiteId == SiteID
//                                       select new ProjectDataDto { 
                                       
//                                       ProjectCode = a.ProjectCode,
//                                       ProjectName = a.ProjectName,
//                                       ProjectId = a.ProjectId,
                                      
//                                       }).ToList();

//            getdata.ClusterDataList = (from a in _contextBilling.MP_ClusterSite
//                                       where a.SiteId == SiteID
//                                       select new ClusterDataDto { 
//                                           ClusterCode = a.ClusterCode,
//                                           ClusterId    = a.ClusterId,
//                                           ClusterName = a.ClusterName,
//                                           ProjectId = a.ProjectId,
//                                       }).ToList();

//            return getdata;
        
        
        
//        }

//        public void ProsesUpdateMasterSite(UpdateMasterSiteDto input)
//        {
//            string imageUrl;
//            var getDatatoUpdate = (from a in _contextBilling.MS_Site
//                                   where a.Id == input.SiteId
//                                   select a).FirstOrDefault();

//            //updateHeader
//            getDatatoUpdate.SiteId = input.SiteId;
//            getDatatoUpdate.SiteName = input.SiteName;
//            getDatatoUpdate.SiteAddress = input.SiteAddress;
//            getDatatoUpdate.SiteCode = input.SiteCode;
//            getDatatoUpdate.Email = input.Email;
//            getDatatoUpdate.OfficePhone = input.OfficePhone;
//            getDatatoUpdate.HandPhone = input.HandPhone;
//            getDatatoUpdate.IsActive = input.IsActive;

//            //update image
//            if (input.Logo == null)
//            {
//                imageUrl = "-";
//            }
//            else
//            {
//                imageUrl = UploadImage(input.Logo);
//                GetURLWithoutHost(imageUrl, out imageUrl);
//                getDatatoUpdate.Logo = imageUrl;
//            }

            

//            _contextBilling.MS_Site.Update(getDatatoUpdate);
          
//            //update mapping
//            var getMPProject = (from a in _contextBilling.MP_ProjectSite
//                                where a.SiteId == getDatatoUpdate.SiteId
//                                select a).ToList();

//            var getMPCluster = (from a in _contextBilling.MP_ClusterSite
//                                where a.SiteId == getDatatoUpdate.SiteId
//                                select a).ToList();

//            _contextBilling.MP_ProjectSite.RemoveRange(getMPProject);
//            _contextBilling.MP_ClusterSite.RemoveRange(getMPCluster);
//            _contextBilling.SaveChanges();

//            foreach (var pro in input.ProjectDataList)
//            {

//                var addPro = new MP_ProjectSite
//                {
//                    ProjectId = pro.ProjectId,
//                    ProjectName = pro.ProjectName,
//                    SiteId = input.SiteId,
//                    ProjectCode = pro.ProjectCode,

//                };

//                _contextBilling.MP_ProjectSite.Add(addPro);
//                _contextBilling.SaveChanges();
//            }

//            foreach (var item in input.ClusterDataList)
//            {

//                var addCluster = new MP_ClusterSite
//                {

//                    ClusterId = item.ClusterId,
//                    SiteId = input.SiteId,
//                    ClusterCode = item.ClusterCode,
//                    ClusterName = item.ClusterName,
//                    ProjectId = item.ProjectId,

//                };

//                _contextBilling.MP_ClusterSite.Add(addCluster);
//                _contextBilling.SaveChanges();
//            }




//        }

//        public void CreateMasterPeriod(CreateMasterPeriodDto input)
//        {


//            var cekData = (from a in _contextBilling.MS_Period
//                           where a.SiteId == input.SiteId
//                           select a).ToList();

//            var cekPeriod = (from a in cekData
//                             where a.PeriodMonth.Month == input.PeriodMonth.Month && a.PeriodYear.Year == input.PeriodYear.Year
//                             select a).FirstOrDefault();

//            if (cekPeriod != null)
//            {
//                throw new UserFriendlyException("Period name already exist");

//            }




//            if (cekData != null && cekData.Any(x=> x.IsActive == true) )
//            {
//                var updateStatus = (from a in cekData
//                                    where a.IsActive == true
//                                    select a).FirstOrDefault();


//                updateStatus.IsActive = false;

//                _contextBilling.MS_Period.Update(updateStatus);
//                _contextBilling.SaveChanges();

//                var addData = new MS_Period
//                {
//                    IsActive = input.IsActive,
//                    PeriodMonth = input.PeriodMonth,
//                    PeriodYear = input.PeriodYear,
//                    SiteId = input.SiteId,
//                    CloseDate = input.CloseDate,
//                    EndDate = input.EndDate,
//                    PeriodNumber = input.PeriodNumber,
//                    StartDate = input.StartDate,

//                };

//                _contextBilling.MS_Period.Add(addData);
//                _contextBilling.SaveChanges();
               
//            }
//            else {
//                var addData = new MS_Period
//                {
//                    IsActive = input.IsActive,
//                    PeriodMonth = input.PeriodMonth,
//                    PeriodYear = input.PeriodYear,
//                    SiteId = input.SiteId,
//                    CloseDate = input.CloseDate,
//                    EndDate = input.EndDate,
//                    PeriodNumber = input.PeriodNumber,
//                    StartDate = input.StartDate,

//                };

//                _contextBilling.MS_Period.Add(addData);
//                _contextBilling.SaveChanges();

//            }

//        }

//        public int GetLastPeriodNo (int SiteId)
//        {

//            var getData = (from a in _contextBilling.MS_Period
//                           where a.SiteId == SiteId
//                           orderby a.Id descending
//                           select a.PeriodNumber).FirstOrDefault();

//            if (getData == 0)
//            {
//                getData = 1;

//            }
//            else {
//               getData = getData + 1;
//            }

//            return getData;

//        }

//        public PagedResultDto<ListMasterPeriodDto> GetListMasterPeriod(InputFilterMasterPeriodDto input)
//        {

//            var getData = (from a in _contextBilling.MS_Period
//                           join b in _contextBilling.MS_Site on a.SiteId equals b.SiteId
//                           where a.SiteId == input.SiteId
//                           orderby a.Id descending
//                           select new ListMasterPeriodDto {
//                               PeriodId = a.Id,
//                               SiteId= a.SiteId,
//                               SiteName = b.SiteName,
//                               PeriodNumber = a.PeriodNumber,
//                               PeriodName= a.PeriodMonth.ToString("MMMM") +" "+ a.PeriodYear.ToString("yyyy"),
//                               StartDate = a.StartDate.ToString("dd MMMM yyyy"),
//                               CloseDate = a.CloseDate.ToString("dd MMMM yyyy"),
//                               EndDate= a.EndDate.ToString("dd MMMM yyyy"),
//                               IsActive = a.IsActive,

//                           })
//                             .WhereIf(input.SiteName != null, item => input.SiteName.Contains(item.SiteName))
//                             .ToList();


//            var dataCount = getData.Count();

//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


//            return new PagedResultDto<ListMasterPeriodDto>(dataCount, getData.ToList());

//        }

//        public MS_Period GetEditMSPeriodById(int periodID)
//        {

//            var getData = (from a in _contextBilling.MS_Period
//                           where a.Id == periodID
//                           orderby a.CreationTime descending
//                           select a).FirstOrDefault();

//            return getData;

//        }

//        public void ProsesUpdateMasterPeriod(CreateMasterPeriodDto input)
//        {
//            var getData = (from a in _contextBilling.MS_Period
//                                         where a.Id == input.PeriodID
//                                         orderby a.CreationTime descending
//                                         select a).FirstOrDefault();

//            getData.IsActive = input.IsActive;

//            if (input.IsActive == true) {

//                var cekforUpdate = (from a in _contextBilling.MS_Period
//                                    where a.SiteId == getData.SiteId && a.IsActive == true
//                                    select a).FirstOrDefault();

//                if (cekforUpdate != null)
//                {
//                    throw new UserFriendlyException("Another period is active");

//                }
//                else
//                {
//                    _contextBilling.MS_Period.Update(getData);
//                    _contextBilling.SaveChanges();

//                }


//            }
//            else {
//                _contextBilling.MS_Period.Update(getData);
//                _contextBilling.SaveChanges();

//            }

          

//        }


//        private void GetURLWithoutHost(string path, out string finalpath)
//        {
//            finalpath = path;
//            try
//            {
//                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
//                if (RegexObj.IsMatch(path))
//                {
//                    finalpath = RegexObj.Match(path).Groups[1].Value;
//                }
//            }
//            catch (ArgumentException ex)
//            {
//            }
//        }
//        private string UploadImage(string filename)
//        {
//            try
//            {
//                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\LogoSite\", @"Assets\Upload\LogoSite\");
//            }
//            catch (Exception ex)
//            {
//                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
//                throw new UserFriendlyException("Error : {0}", ex.Message);
//            }
//        }

//        //Master unit item
//        public PagedResultDto<MasterUnitItemListDto> GetListMasterUnitItem(InputFilterMasterUnitItem input) { 
        
//        var getData = (from a in _contextBilling.MS_UnitItemHeader
//                      join b in _contextBilling.MS_TemplateInvoiceHeader on a.TemplateInvoiceHeaderId equals b.Id into template
//                      from tempnull in template.DefaultIfEmpty()
//                      join c in _contextBilling.MS_UnitData on a.UnitDataId equals c.Id
//                      join d in _contextBilling.MS_Bank on a.BankId equals d.Id into bank
//                      from banknull in bank.DefaultIfEmpty()
//                      where a.SiteId == input.SiteId
//                      select new MasterUnitItemListDto
//                      {
//                          UnitItemHeaderId = a.Id,
//                          CreateTime = a.CreationTime,
//                          SiteId = a.SiteId,
//                          UnitDataId = a.UnitDataId,
//                          ClusterId = c.ClusterId,
//                          UnitCode = c.UnitCode,
//                          UnitNo = c.UnitNo,
//                          TemplateInvoice = tempnull.TemplateInvoiceName == null ? "": tempnull.TemplateInvoiceName,
//                          Bank = banknull.BankName == null ? "" : banknull.BankName,
//                          VaNo = a.VirtualAccNo,
//                          isPenalty = a.IsPenalty
//                      })
//                      .WhereIf(input.Search != null, item => input.Search.Contains(item.UnitCode) || input.Search.Contains(item.UnitNo) || input.Search.Contains(item.VaNo) || input.Search.Contains(item.TemplateInvoice) || input.Search.Contains(item.Bank))
//                      .OrderByDescending(x =>  x.VaNo == "" ).ThenByDescending(x => x.CreateTime).ToList();

           

//            getData = getData.GroupBy(a => new { a.ClusterId, a.UnitItemHeaderId, a.SiteId, a.UnitDataId, a.UnitCode, a.UnitNo, a.TemplateInvoice, a.Bank, a.VaNo, a.isPenalty}).Select (y => new MasterUnitItemListDto { 
//                UnitItemHeaderId = y.Key.UnitItemHeaderId,
//                SiteId = y.Key.SiteId,
//                UnitDataId = y.Key.UnitDataId,
//                ClusterId = y.Key.ClusterId,
//                UnitCode = y.Key.UnitCode,
//                UnitNo = y.Key.UnitNo,
//                TemplateInvoice = y.Key.TemplateInvoice,
//                Bank = y.Key.Bank,
//                VaNo = y.Key.VaNo,
//                isPenalty = y.Key.isPenalty}).ToList();


//            var dataCount = getData.Count();
//            getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();




//            return new PagedResultDto<MasterUnitItemListDto>(dataCount, getData.ToList());


//        }


//        public String ProsesGenerateTemplateDemo(int templateInvoiceheaderID) 
//        {

//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
//            var connectionApp = (JObject)appsettingsjson["App"];
//            var pdfOutputPath = connectionApp.Property("DemoInvoice").Value.ToString();
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();

//            var destinationPath = pdfOutputPath;

//            if (!Directory.Exists(destinationPath))
//            {
//                Directory.CreateDirectory(destinationPath);
//            }

//            var getTemplate = (from a in _contextBilling.MS_TemplateInvoiceHeader
//                               where a.Id == templateInvoiceheaderID
//                               select a).FirstOrDefault();

//            //var getLogo = (from a in _contextBilling.MS_Site
//            //               where a.SiteId == getTemplate.SiteId
//            //               select a).FirstOrDefault();

//            var fileName = "Template_Invoice_" + getTemplate.TemplateInvoiceName.Replace(" ", "_") + ".pdf";
//            var filePath = Path.Combine(destinationPath, fileName);
//            try
//            {
//                //var getLogoSite = _hostEnvironment.WebRootPath + @"\Assets\Upload\LogoSite\" + getLogo.Logo + "";
//                var templateFile = _hostEnvironment.WebRootPath + getTemplate.TemplateUrl;
//                var htmlToConvert = File.ReadAllText(templateFile);


//                HtmlToPdf converter = new HtmlToPdf();
//                converter.Options.PdfPageSize = PdfPageSize.B5;
//                converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
//                converter.Options.WebPageWidth = 1024;

//                var doc = converter.ConvertHtmlString(htmlToConvert
//              //  .Replace("{{SiteLogo}}", getLogoSite)
              

//);
//                byte[] results;

//                using (var stream = new MemoryStream())
//                {
//                    doc.Save(stream);
//                    results = stream.ToArray();
//                    doc.Close();
//                }

//                if (File.Exists(filePath))
//                {
//                    File.Delete(filePath);
//                }

//                File.WriteAllBytes(filePath, results);

//            }
//            catch (Exception ex)
//            {

//            }

//            var final = ipPublic + "/Assets/Upload/MasterTemplate/DemoInvoice/" + fileName;

//            return final;






//        }
//        public DetailMasterUnitItemDto GetDetailMasterUnitItem(int UnitItemHeaderId) {

//            var getda = (from a in _contextBilling.MS_UnitItemHeader
//                         join b in _contextBilling.MS_UnitData on a.UnitDataId equals b.Id
//                         where a.Id == UnitItemHeaderId
//                         select new DetailMasterUnitItemDto { 
                         
//                             UnitItemHeaderId = a.Id,
//                             UnitDataId = a.UnitDataId,
//                             UnitCode = b.UnitCode,
//                             UnitNo = b.UnitNo,
//                             TemplateInvoiceHeaderId = a.TemplateInvoiceHeaderId,
//                             BankId = a.BankId,
//                             BuildArea = b.BuildArea,
//                             LandArea = b.LandArea,
//                             VaNo = a.VirtualAccNo,
//                             isPenalty = a.IsPenalty,
//                         }).FirstOrDefault();
//            return getda;
        
//        }

//        public List<ItemDetailList> GetDetailListMsUnitItem( int unitItemHeaderId, int templateInvoiceHeaderId, int ClusterId) 
//        {


//            var finalData = new List<ItemDetailList>();


//            var getItemDetail = (from a in _contextBilling.MS_UnitItemHeader
//                                 join b in _contextBilling.MS_UnitItemDetail on a.Id equals b.UnitItemHeaderId
//                                 join c in _contextBilling.MS_TemplateInvoiceDetail on b.TemplateInvoiceDetailId equals c.Id
//                                 join d in _contextBilling.MS_Item on c.ItemId equals d.Id
//                                 join e in _contextBilling.MS_ItemRate on c.ItemRateId equals e.Id
//                                 join f in _contextBilling.MS_UnitData on a.UnitDataId equals f.Id
//                                 join g in _contextBilling.MP_ClusterSite on c.TemplateInvoiceHeaderId equals g.Id into mpresult
//                                 from mpnull in mpresult.DefaultIfEmpty()
//                                 where a.Id == unitItemHeaderId && c.TemplateInvoiceHeaderId == templateInvoiceHeaderId &&  f.ClusterId == ClusterId
//                                 select new ItemDetailList
//                                 {
//                                     unitItemDetailId = b.Id,
//                                     ItemName = d.ItemName,
//                                     TemplateInvoiceDetailId = b.TemplateInvoiceDetailId,
//                                     ItemRateName = e.ItemRateName,
//                                     Rate = b.RateNominal,
//                                     LuasAwal = e.LuasAwal,
//                                     LuasAkhir = e.LuasAkhir,
//                                     LandArea = f.LandArea,
//                                     //ItemRateId = e.Id

//                                 }).ToList();

//            foreach (var item in getItemDetail) 
//            {
//                if (item.LandArea >= item.LuasAwal && item.LandArea <= item.LuasAkhir) {

//                    finalData.Add(item);
                
                
//                }

            
//            }

//            if(getItemDetail.Count() == 0)
//            {



//                getItemDetail = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                 join b in _contextBilling.MS_Item on a.ItemId equals b.Id
//                                 join c in _contextBilling.MS_ItemRate on a.ItemRateId equals c.Id
//                                 where a.TemplateInvoiceHeaderId == templateInvoiceHeaderId
//                                 select new ItemDetailList 
//                                 {
//                                     unitItemDetailId = 0,
//                                     TemplateInvoiceDetailId = a.Id,
//                                     ItemName = b.ItemName,
//                                     ItemRateName = c.ItemRateName,
//                                     Rate = 0
//                                 }).ToList();

//            }




//            return getItemDetail;

//        }
//        public List<DropdownMasterTemplateDto> GetDropdownMasterTemplate(int SiteID)
//        {
//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion

//            var result = (from a in _contextBilling.MS_TemplateInvoiceHeader
//                          where a.SiteId == SiteID
//                          select new DropdownMasterTemplateDto
//                          {
//                              TemplateInvoiceHeaderId = a.Id,
//                              templateName = a.TemplateInvoiceName,
//                              Urltemplate = ipPublic + a.TemplateUrl
//                          }).ToList();

//            return result;


//        }


//        public ResponseBulkPaymentDto UploadExcelNewUnitItem(UploadNewUnitItemDto input)
//        {


//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion


//            var totalData = input.DetailUploadUnitItemList.Count();
//            int prosesData = 0;
//            int totalgagal = 0;
//            int totalsukses = 0;
//            FileDto file = new FileDto();
//            string URL = "";
//            var gagalDataList = new List<DetailUploadNewUnitItem>();


//            foreach (var item in input.DetailUploadUnitItemList)
//            {
//                prosesData++;
//                var cekInsert = true;
//                var checkUnit = (from a in _contextBilling.MS_UnitData
//                                 where a.UnitNo == item.unitNo && a.UnitCode == item.unitCode && a.SiteId == input.SiteId
//                                 select a).FirstOrDefault();

//                if (checkUnit == null)
//                {
//                    item.RemarksError = "Unit tidak ditemukan di master data";
//                }
//                else 
//                {

//                    var getcekInsert = (from a in _contextBilling.MS_UnitItemHeader
//                                     where a.UnitDataId == checkUnit.Id && a.TemplateInvoiceHeaderId == input.TemplateInvoiceId
//                                     select a).FirstOrDefault();

//                    cekInsert = getcekInsert != null ? true : false;

//                    if(cekInsert == true)
//                    {
//                      item.RemarksError = "Mappingan Unit dan template sudah ada di master unit item";
//                    }

//                }

//                var checkBank = (from a in _contextBilling.MS_Bank
//                                 where a.BankName.ToLower() == item.bank.ToLower()
//                                 select a).FirstOrDefault();

//                if (checkBank == null)
//                {
//                    item.RemarksError = "Bank tidak di temukan di master bank";
//                }

//                var checkVANumber = item.vaNo.All(char.IsDigit);

//                if (checkVANumber == false)
//                {
//                    item.RemarksError = "VA Number harus berupa angka";
//                }

               


//                if (checkUnit != null && checkBank != null && checkVANumber == true && cekInsert == false)
//                {
//                    //berhasil
//                    totalsukses++;

//                    var insertHeader = new MS_UnitItemHeader
//                    {
//                        TemplateInvoiceHeaderId = input.TemplateInvoiceId,
//                        VirtualAccNo = item.vaNo,
//                        IsPenalty = item.Penalty,
//                        BankId = checkBank.Id,
//                        SiteId = input.SiteId,
//                        UnitDataId = checkUnit.Id,
                        


//                };

//                    //proses update data header
               

//                    _contextBilling.MS_UnitItemHeader.Add(insertHeader);
//                    _contextBilling.SaveChanges();
//                    int unitItemHeaderId = insertHeader.Id;


//                    //insert Detail 

//                    var getInvoiceDetail = (from a in _contextBilling.MS_TemplateInvoiceDetail
//                                            where a.TemplateInvoiceHeaderId == input.TemplateInvoiceId
//                                            select a).ToList();

//                    foreach (var detail in getInvoiceDetail)
//                    {

//                        var insertDetail = new MS_UnitItemDetail
//                        {
//                            UnitItemHeaderId = unitItemHeaderId,
//                            TemplateInvoiceDetailId = detail.Id,
//                            RateNominal = 0
//                        };

//                        _contextBilling.MS_UnitItemDetail.Add(insertDetail);
//                        _contextBilling.SaveChanges();


//                    }


//                }
//                else
//                {
//                    //Jika gagal
//                    totalgagal++;
//                    gagalDataList.Add(item);

//                }

//            }

//            if (totalgagal > 0)
//            {

//                var paramToExcel = new UploadNewUnitItemDto
//                {
//                    DetailUploadUnitItemList = gagalDataList

//                };

//                file = _exporter.ExportToExcelErrorUploadUnitItemResult(paramToExcel);
//                System.IO.File.Move(_hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileToken, _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName);
//                var tempRootPath = _hostEnvironment.WebRootPath + @"\Temp\Downloads\" + file.FileName;


//                var filePath = Path.Combine(tempRootPath);
//                if (!File.Exists(filePath))
//                {
//                    throw new Exception("Requested File doesn't exists");
//                }

//                var pathExport = Path.Combine(tempRootPath);
//                //retrieve data excel from temporary download folder
//                var fileBytes = File.ReadAllBytes(filePath);
//                //write excel file to share folder / local folder
//                File.WriteAllBytes(pathExport, fileBytes);
//                //string URL = Path.Combine(_appFolders.UploadPrefixUrl, _appFolders.TempFileDownloadDirectory, file.FileName);
//                URL = ipPublic + @"\Temp\Downloads\" + file.FileName;

//            }

//            var final = new ResponseBulkPaymentDto
//            {

//                TotalData = totalData,
//                TotalGagal = totalgagal,
//                TotalSukses = totalsukses,
//                urlDataGagal = URL

//            };

//            return final;

//        }

//        public void ProsesUpdateMasterUnitItem(DetailMasterUnitItemDto input)
//        { 
//            var getupdate = (from a in _contextBilling.MS_UnitItemHeader
//                             where a.Id == input.UnitItemHeaderId
//                             select a).FirstOrDefault();


//            getupdate.TemplateInvoiceHeaderId = input.TemplateInvoiceHeaderId;
//            getupdate.BankId = input.BankId;
//            getupdate.VirtualAccNo = input.VaNo;
//            getupdate.IsPenalty = input.isPenalty;

//            //update build and land area 

//            var getUpdateUnit = (from a in _contextBilling.MS_UnitData
//                                 where a.Id == input.UnitDataId
//                                 select a).FirstOrDefault();

//            getUpdateUnit.BuildArea = input.BuildArea;
//            getUpdateUnit.LandArea = input.LandArea;



//            //delete detail
//            var getdatadetail = (from a in _contextBilling.MS_UnitItemDetail
//                                 where a.UnitItemHeaderId == input.UnitItemHeaderId
//                                 select a).ToList();

//            _contextBilling.MS_UnitItemDetail.RemoveRange(getdatadetail);
//            _contextBilling.MS_UnitItemHeader.Update(getupdate);
//            _contextBilling.MS_UnitData.Update(getUpdateUnit);
//            _contextBilling.SaveChanges();

//            //proses update detail 
          
//            foreach (var item in input.ItemDetail) 
//            {
                
//                    var insert = new MS_UnitItemDetail
//                    {
                        
//                        TemplateInvoiceDetailId = item.TemplateInvoiceDetailId,
//                        UnitItemHeaderId = input.UnitItemHeaderId,
//                        RateNominal = item.Rate,
//                        LastModificationTime = DateTime.Now,
//                        LastModifierUserId = AbpSession.GetUserId(),
//                        //ItemRateId = item.ItemRateId
//                    };

//                    _contextBilling.MS_UnitItemDetail.Add(insert);


//                _contextBilling.SaveChanges();

//            };
        
//        }


//        //>> add by nanda1211, 08 Mei 2023
//        public List<PaymentTypeDto> GetDropdownPaymentType()
//        {
//            var result = (from a in _contextBilling.MS_PaymentType
//                          select new PaymentTypeDto
//                          {
//                              paymentTypeId = a.Id,
//                              paymentTypeName = a.PaymentTypeName
//                          }).ToList();
//            return result;
//        }

//        //<< add by nanda1211, 08 Mei 2023


//        public async Task<String> ProsesUploadImage(IFormFile photo)
//        {

//            var path = Path.Combine(_hostEnvironment.WebRootPath, "Temp/Downloads/LogoSite", photo.FileName);
//            using (FileStream stream = new FileStream(path, FileMode.Create))
//            {
//                await photo.CopyToAsync(stream);
//                stream.Close();
//            }

//            var ImageName = photo.FileName;

//            return ImageName;

//        }


//        public async Task<String> ProsesUploadUserProfile(IFormFile photo)
//        {

//            var path = Path.Combine(_hostEnvironment.WebRootPath, "Temp/Downloads/ProfileUser", photo.FileName);
//            using (FileStream stream = new FileStream(path, FileMode.Create))
//            {
//                await photo.CopyToAsync(stream);
//                stream.Close();
//            }

//            var ImageName = photo.FileName;

//            return ImageName;

//        }

//        public UserDetailDto GetDetailUser(int UserId) {

//            #region Constring DB
//            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));

//            var connectionApp = (JObject)appsettingsjson["App"];
//            var ipPublic = connectionApp.Property("ipPublic").Value.ToString();
//            #endregion

//            var final = new UserDetailDto();
//            var getuser = (from a in _contextBilling.Users
//                           where a.Id == UserId
//                           select a).FirstOrDefault();

//            var getSiteID = (from a in _contextBilling.MP_UserSite
//                             where a.UserId == UserId
//                             select a.SiteId).ToList();

//            var getRoleName = (from a in _contextBilling.UserRoles
//                               join b in _contextBilling.Roles on a.RoleId equals b.Id
//                               where a.UserId == UserId
//                               select b.Name).Distinct().ToList();

      


//            final = new UserDetailDto
//            {
//                creationTime = getuser.CreationTime,
//                emailAddress = getuser.EmailAddress,
//                fullName = getuser.FullName,
//                id = UserId,
//                isActive = getuser.IsActive,
//                lastLoginTime = getuser.LastModificationTime,
//                name = getuser.Name,
//                phoneNumber = getuser.PhoneNumber,
//                photoProfile = ipPublic+ getuser.ProfileImg,
//                surname = getuser.Surname,
//                userName = getuser.UserName,
//                siteId = getSiteID,
//                roleName = getRoleName,
                
//            };


//            return final;



//        }
    }
}
