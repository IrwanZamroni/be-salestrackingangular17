﻿using BeBillingSystem.BillingSystem;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.BillingSystem.TransactionToOracle.Dto;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace BeBillingSystem.Report
{
    public class ReportDataExporter : EpPlusExcelExporterBase, IReportDataExporter
    {


        private readonly ILogger<ReportDataExporter> _logger;

        public ReportDataExporter(

             ILogger<ReportDataExporter> logger)
        {


            _logger = logger;
        }
        public FileDto ExportToExcelWaterReadingResult(ViewWaterReadingDto input)
        {

            {
                _logger.LogDebug("ExportToExcelWaterReadingResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Water_Reading_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReminderSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReminderSheet(ExcelWorksheet sheet, ViewWaterReadingDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:J5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Water Reading";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";

            sheet.Cells["A5"].Value = "Project";
            sheet.Cells["B5"].Value = "Cluster";
            sheet.Cells["C5"].Value = "Period";
            sheet.Cells["D5"].Value = "UnitCode";
            sheet.Cells["E5"].Value = "UnitNo";
            sheet.Cells["F5"].Value = "Prev Read";
            sheet.Cells["G5"].Value = "Current Rate";


            sheet.Cells["A5:G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:G5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ListWaterReadingDto)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.Period;
                sheet.Cells["D" + index].Value = data.UnitCode;
                sheet.Cells["E" + index].Value = data.UnitNo;
                sheet.Cells["F" + index].Value = data.PrevRead;
                sheet.Cells["G" + index].Value = data.CurrentRead;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelWarningLetterResult(ViewExportWarningLetterDto input)
        {

            {
                _logger.LogDebug("ExportToExcelWarningLetterResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Warning_letter_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataWarningLetterSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataWarningLetterSheet(ExcelWorksheet sheet, ViewExportWarningLetterDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:J5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Warning Letter";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";

            sheet.Cells["A5"].Value = "Period";
            sheet.Cells["B5"].Value = "Project";
            sheet.Cells["C5"].Value = "Cluster";
            sheet.Cells["D5"].Value = "Invoice Number";
            sheet.Cells["E5"].Value = "UnitCode";
            sheet.Cells["F5"].Value = "UnitNo";
            sheet.Cells["G5"].Value = "SP";
            sheet.Cells["H5"].Value = "Email Address";
            sheet.Cells["I5"].Value = "Send Email Date";


            sheet.Cells["A5:I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:I5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ListWarningLetterDto)
            {
                sheet.Cells["A" + index].Value = data.Period;
                sheet.Cells["B" + index].Value = data.ProjectName;
                sheet.Cells["C" + index].Value = data.ClusterName;
                sheet.Cells["D" + index].Value = data.InvoiceNo;
                sheet.Cells["E" + index].Value = data.UnitCode;
                sheet.Cells["F" + index].Value = data.UnitNo;
                sheet.Cells["G" + index].Value = data.SP;
                sheet.Cells["H" + index].Value = data.EmailCust;
                sheet.Cells["I" + index].Value = data.SendEmailDate == null ? "-" : data.SendEmailDate;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelReportInvoiceResult(ViewReportinvoiceDto input)
        {

            {
                _logger.LogDebug("ExportToExcelReportInvoiceSummaryResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Report_Invoice_Summary_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportInvoiceSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportInvoiceSheet(ExcelWorksheet sheet, ViewReportinvoiceDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:J5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Invoice Summary";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";
            sheet.Cells["A5"].Value = "Period : " + input.periodName + "";

            sheet.Cells["A6"].Value = "Project";
            sheet.Cells["B6"].Value = "Cluster";
            sheet.Cells["C6"].Value = "Invoice No";
            sheet.Cells["D6"].Value = "Invoice Name";
            sheet.Cells["E6"].Value = "Unit Code";
            sheet.Cells["F6"].Value = "Unit No";
            sheet.Cells["G6"].Value = "PS Code";
            sheet.Cells["H6"].Value = "Name";
            sheet.Cells["I6"].Value = "Bill Address";
            sheet.Cells["J6"].Value = "Bill City";
            sheet.Cells["K6"].Value = "HP No";
            sheet.Cells["L6"].Value = "Land Area";
            sheet.Cells["M6"].Value = "Build Area";
            sheet.Cells["N6"].Value = "Prev Balance";
            sheet.Cells["O6"].Value = "Curr Trans";
            sheet.Cells["P6"].Value = "Penalty";
            sheet.Cells["Q6"].Value = "Adjustment Amount";
            sheet.Cells["R6"].Value = "Payment";
            sheet.Cells["S6"].Value = "EndBalance";


            sheet.Cells["A6:S6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A6:S6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 7;
            foreach (var data in input.ReportInvoiceListDto)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.InvoiceNo;
                sheet.Cells["D" + index].Value = data.InvoiceName;
                sheet.Cells["E" + index].Value = data.UnitCode;
                sheet.Cells["F" + index].Value = data.UnitNo;
                sheet.Cells["G" + index].Value = data.PsCode;
                sheet.Cells["H" + index].Value = data.PsName;
                sheet.Cells["I" + index].Value = data.BillAddress;
                sheet.Cells["J" + index].Value = data.BillCity;
                sheet.Cells["K" + index].Value = data.psPhone;
                sheet.Cells["L" + index].Value = data.LandArea;
                sheet.Cells["M" + index].Value = data.BuildArea;
                sheet.Cells["N" + index].Value = data.PrevBalance;
                sheet.Cells["O" + index].Value = data.CurrTrans;
                sheet.Cells["P" + index].Value = data.Penalty;
                sheet.Cells["Q" + index].Value = data.AdjustmentAmount;
                sheet.Cells["R" + index].Value = data.Payment;

                sheet.Cells["S" + index].Value = data.EndBalance;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

            sheet.Cells["N7:N" + (index + 5)].Style.Numberformat.Format = "#,##0";
            sheet.Cells["O7:O" + (index + 5)].Style.Numberformat.Format = "#,##0";
            sheet.Cells["P7:P" + (index + 5)].Style.Numberformat.Format = "#,##0";
            sheet.Cells["Q7:Q" + (index + 5)].Style.Numberformat.Format = "#,##0";
            sheet.Cells["R7:R" + (index + 5)].Style.Numberformat.Format = "#,##0";
            sheet.Cells["S7:S" + (index + 5)].Style.Numberformat.Format = "#,##0";

            return sheet;
        }


        public FileDto ExportToExcelReportInvoiceDetailResult(ViewReportinvoiceDto input)
        {

            {
                _logger.LogDebug("ExportToExcelReportInvoiceDetailResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Report_Invoice_Detail_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportInvoiceDetailSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportInvoiceDetailSheet(ExcelWorksheet sheet, ViewReportinvoiceDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:J5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Invoice Detail";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";
            sheet.Cells["A5"].Value = "Period : " + input.periodName + "";

            sheet.Cells["A6"].Value = "Project";
            sheet.Cells["B6"].Value = "Cluster";
            sheet.Cells["C6"].Value = "Unit Code";
            sheet.Cells["D6"].Value = "Unit No";
            sheet.Cells["E6"].Value = "ID Client";
            sheet.Cells["F6"].Value = "Invoice No";
            sheet.Cells["G6"].Value = "Invoice Name";
            sheet.Cells["H6"].Value = "Item Name";
            sheet.Cells["I6"].Value = "Amount";


            sheet.Cells["A6:I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A6:I6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 7;
            foreach (var data in input.ReportInvoiceListDto)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.UnitCode;
                sheet.Cells["D" + index].Value = data.UnitNo;
                sheet.Cells["E" + index].Value = data.PsCode;
                sheet.Cells["F" + index].Value = data.InvoiceNo;
                sheet.Cells["G" + index].Value = data.InvoiceName;
                sheet.Cells["H" + index].Value = data.ItemName;
                sheet.Cells["I" + index].Value = data.Amount;

                index++;
            }

            //  sheet.Cells["I7:I" + index].Style.Numberformat.Format =  "#,##0";

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }



        public FileDto ExportToExcelErrorDataBulkResult(UploadBulkPaymentDto input)
        {

            {
                _logger.LogDebug("ExportToExcelErrorDataBulkResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_ErrorData_UploadBulk_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportErrorSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportErrorSheet(ExcelWorksheet sheet, UploadBulkPaymentDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A6:G6"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Gagal Proses Save Data";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";

            sheet.Cells["A6"].Value = "ID Client";
            sheet.Cells["B6"].Value = "Unit Code";
            sheet.Cells["C6"].Value = "Unit No";
            sheet.Cells["D6"].Value = "Invoice Number";
            sheet.Cells["E6"].Value = "Transaction Date";
            sheet.Cells["F6"].Value = "Amount";
            sheet.Cells["G6"].Value = "Remark";



            sheet.Cells["A6:G6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A6:G6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 7;
            foreach (var data in input.DetailUploadBulkPaymentList)
            {
                sheet.Cells["A" + index].Value = data.IdClient;
                sheet.Cells["B" + index].Value = data.UnitCode;
                sheet.Cells["C" + index].Value = data.UnitNo;
                sheet.Cells["D" + index].Value = data.InvoiceNumber;
                sheet.Cells["E" + index].Value = data.TransactionDate.ToString("dd-MMM-y");
                sheet.Cells["F" + index].Value = data.Amount;
                sheet.Cells["G" + index].Value = data.RemarksError;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelDailyReportResult(ViewReportDailyDto input)
        {

            {
                _logger.LogDebug("ExportToExcelReportDailtResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Report_Daily_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReporDailySheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReporDailySheet(ExcelWorksheet sheet, ViewReportDailyDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:S5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Daily";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";

            sheet.Cells["A5"].Value = "Project";
            sheet.Cells["B5"].Value = "Cluster";
            sheet.Cells["C5"].Value = "Unit Code";
            sheet.Cells["D5"].Value = "Unit No";
            sheet.Cells["E5"].Value = "Invoice Number";
            sheet.Cells["F5"].Value = "Invoice Name";
            sheet.Cells["G5"].Value = "Receipt Number";
            sheet.Cells["H5"].Value = "Payment Method";
            sheet.Cells["I5"].Value = "Bank Name";
            sheet.Cells["J5"].Value = "Virtual Account";
            sheet.Cells["K5"].Value = "Transaction Date";
            sheet.Cells["L5"].Value = "Amount";
            sheet.Cells["M5"].Value = "Charge";
            sheet.Cells["N5"].Value = "Nett";
            sheet.Cells["O5"].Value = "Cancel Payment";
            sheet.Cells["P5"].Value = "ID Client";
            sheet.Cells["Q5"].Value = "Name";
            sheet.Cells["R5"].Value = "IdKTP";
            sheet.Cells["S5"].Value = "IdNPWP";



            sheet.Cells["A5:S5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:S5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ReportDailyListDto)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.UnitCode;
                sheet.Cells["D" + index].Value = data.UnitNo;
                sheet.Cells["E" + index].Value = data.InvoiceNo;
                sheet.Cells["F" + index].Value = data.TemplateInvoiceName;
                sheet.Cells["G" + index].Value = data.ReceiptNumber;
                sheet.Cells["H" + index].Value = data.PaymentTypeName;
                sheet.Cells["I" + index].Value = data.BankName == null ? "-" : data.BankName;
                sheet.Cells["J" + index].Value = data.VirtualAccNo;
                sheet.Cells["K" + index].Value = data.TransactionDate;
                sheet.Cells["L" + index].Value = data.PaymentAmount;
                sheet.Cells["M" + index].Value = data.Charge;
                sheet.Cells["N" + index].Value = data.net;
                sheet.Cells["O" + index].Value = data.CancelDate;
                sheet.Cells["P" + index].Value = data.PsCode;
                sheet.Cells["Q" + index].Value = data.name;
                sheet.Cells["R" + index].Value = data.KTP;
                sheet.Cells["S" + index].Value = data.NPWP;

                index++;
            }


            sheet.Cells["L6:L" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["M6:M" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["N6:N" + index].Style.Numberformat.Format = "#,##0";

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }



        public FileDto ExportToExcelErrorUploadWaterResult(WaterReadingUploadExcelDto input)
        {

            {
                _logger.LogDebug("ExportToExcelErrorUploadWaterReadingResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_ErrorData_UploadWaterReading_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportErrorWaterSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportErrorWaterSheet(ExcelWorksheet sheet, WaterReadingUploadExcelDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A4:E4"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Gagal Proses Save Data";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";

            sheet.Cells["A4"].Value = "Unit Code";
            sheet.Cells["B4"].Value = "Unit No";
            sheet.Cells["C4"].Value = "PrevRead";
            sheet.Cells["D4"].Value = "CurrRead";
            sheet.Cells["E4"].Value = "Remark";




            sheet.Cells["A4:E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A4:E4"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 5;
            foreach (var data in input.WaterReadingUploadDetailList)
            {
                sheet.Cells["A" + index].Value = data.UnitCode;
                sheet.Cells["B" + index].Value = data.UnitNo;
                sheet.Cells["C" + index].Value = data.PrevRead;
                sheet.Cells["D" + index].Value = data.CurrentRead;
                sheet.Cells["E" + index].Value = data.RemarksError;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelErrorUploadElectricResult(WaterReadingUploadExcelDto input)
        {

            {
                _logger.LogDebug("ExportToExcelErrorUploadElectricReadingResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_ErrorData_UploadElectricReading_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportErrorElectricSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportErrorElectricSheet(ExcelWorksheet sheet, WaterReadingUploadExcelDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A4:E4"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Gagal Proses Save Data";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";

            sheet.Cells["A4"].Value = "Unit Code";
            sheet.Cells["B4"].Value = "Unit No";
            sheet.Cells["C4"].Value = "PrevRead";
            sheet.Cells["D4"].Value = "CurrRead";
            sheet.Cells["E4"].Value = "Remark";




            sheet.Cells["A4:E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A4:E4"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 5;
            foreach (var data in input.WaterReadingUploadDetailList)
            {
                sheet.Cells["A" + index].Value = data.UnitCode;
                sheet.Cells["B" + index].Value = data.UnitNo;
                sheet.Cells["C" + index].Value = data.PrevRead;
                sheet.Cells["D" + index].Value = data.CurrentRead;
                sheet.Cells["E" + index].Value = data.RemarksError;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }

        public FileDto ExportToExcelErrorUploadUnitItemResult(UploadNewUnitItemDto input)
        {

            {
                _logger.LogDebug("ExportToExcelErrorUploadWaterReadingResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_ErrorData_UploadUnitItem_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportErrorUnitItemSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportErrorUnitItemSheet(ExcelWorksheet sheet, UploadNewUnitItemDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A4:F4"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Gagal Proses Save Data";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";

            sheet.Cells["A4"].Value = "Unit Code";
            sheet.Cells["B4"].Value = "Unit No";
            sheet.Cells["C4"].Value = "Bank";
            sheet.Cells["D4"].Value = "Virtual Account Number";
            sheet.Cells["E4"].Value = "Penalty";
            sheet.Cells["F4"].Value = "Remarks";




            sheet.Cells["A4:F4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A4:F4"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 5;
            foreach (var data in input.DetailUploadUnitItemList)
            {
                sheet.Cells["A" + index].Value = data.unitCode;
                sheet.Cells["B" + index].Value = data.unitNo;
                sheet.Cells["C" + index].Value = data.bank;
                sheet.Cells["D" + index].Value = data.vaNo;
                sheet.Cells["E" + index].Value = data.Penalty == true ? "Iya" : "Tidak";
                sheet.Cells["F" + index].Value = data.RemarksError;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }



        public FileDto ExportToExcelErrorUploadAdjResult(UploadExcelChangeAdjInvoiceDto input)
        {

            {
                _logger.LogDebug("ExportToExcelErrorUploadAdjustmentResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_ErrorData_UploadAdjustment_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataReportErrorAdjSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataReportErrorAdjSheet(ExcelWorksheet sheet, UploadExcelChangeAdjInvoiceDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A4:E4"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Gagal Proses Save Data";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";

            sheet.Cells["A4"].Value = "Unit Code";
            sheet.Cells["B4"].Value = "Unit No";
            sheet.Cells["C4"].Value = "Invoice No";
            sheet.Cells["D4"].Value = "Adjustment Nominal";
            sheet.Cells["E4"].Value = "Remark";




            sheet.Cells["A4:E4"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A4:E4"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 5;
            foreach (var data in input.AdjInvoiceUploadDetailList)
            {
                sheet.Cells["A" + index].Value = data.UnitCode;
                sheet.Cells["B" + index].Value = data.UnitNo;
                sheet.Cells["C" + index].Value = data.InvoiceNo;
                sheet.Cells["D" + index].Value = data.AdjNominal;
                sheet.Cells["E" + index].Value = data.RemarksError;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelDetailStetmentReportResult(ViewReportDetailStetmentDto input)
        {

            {
                _logger.LogDebug("ExportToExcelReportDetailStetmentResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_Report_Detail_Statment_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Summary");
                        var sheetWithData = fillDataSummaryReporDetailStetmentSheet(sheet, input);

                        ExcelWorksheet sheet2 = excelPackage.Workbook.Worksheets.Add("Detail");
                        var sheetWithData2 = fillDataDetailReporDetailStetmentSheet(sheet2, input);

                    });

            }

        }
        private ExcelWorksheet fillDataSummaryReporDetailStetmentSheet(ExcelWorksheet sheet, ViewReportDetailStetmentDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:S5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Detail Statement";

            sheet.Cells["A2"].Value = "Printed by : " + input.printBy + "";
            sheet.Cells["A3"].Value = "Printed date : " + Printdate + "";

            sheet.Cells["A5"].Value = "Project";
            sheet.Cells["B5"].Value = "Cluster";
            sheet.Cells["C5"].Value = "Unit Code";
            sheet.Cells["D5"].Value = "Unit No";
            sheet.Cells["E5"].Value = "Period Month";
            sheet.Cells["F5"].Value = "Period Year";
            sheet.Cells["G5"].Value = "Invoice No";
            sheet.Cells["H5"].Value = "Tagihan Invoice";
            sheet.Cells["I5"].Value = "Total Payment";
            sheet.Cells["J5"].Value = "Outstanding Pembayaran";



            sheet.Cells["A5:J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:J5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));

            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ReportSummaryStatmentList)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.UnitCode;
                sheet.Cells["D" + index].Value = data.UnitNo;
                sheet.Cells["E" + index].Value = data.PeriodMonth;
                sheet.Cells["F" + index].Value = data.PeriodYear;
                sheet.Cells["G" + index].Value = data.InvoiceNo;
                sheet.Cells["H" + index].Value = data.tagihaninvoice;
                sheet.Cells["I" + index].Value = data.PaymentAmount;
                sheet.Cells["J" + index].Value = data.outstanding;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            sheet.Cells["H6:H" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["I6:I" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["J6:J" + index].Style.Numberformat.Format = "#,##0";
            return sheet;
        }
        private ExcelWorksheet fillDataDetailReporDetailStetmentSheet(ExcelWorksheet sheet, ViewReportDetailStetmentDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:S5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report Detail Statement";

            sheet.Cells["A2"].Value = "Printed by : " + input.printBy + "";
            sheet.Cells["A3"].Value = "Printed date : " + Printdate + "";

            sheet.Cells["A5"].Value = "Project";
            sheet.Cells["B5"].Value = "Cluster";
            sheet.Cells["C5"].Value = "Unit Code";
            sheet.Cells["D5"].Value = "Unit No";
            sheet.Cells["E5"].Value = "Period Month";
            sheet.Cells["F5"].Value = "Period Year";
            sheet.Cells["G5"].Value = "Invoice No";
            sheet.Cells["H5"].Value = "Customer";
            sheet.Cells["I5"].Value = "BPL";
            sheet.Cells["J5"].Value = "PPN BPL";
            sheet.Cells["K5"].Value = "Water";
            sheet.Cells["L5"].Value = "Previous Balance";
            sheet.Cells["M5"].Value = "Adjustment";
            sheet.Cells["N5"].Value = "Penalty";
            sheet.Cells["O5"].Value = "CurrTrans";
            sheet.Cells["P5"].Value = "Payment";





            sheet.Cells["A5:P5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:P5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ReportDetailStatmentList)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.UnitCode;
                sheet.Cells["D" + index].Value = data.UnitNo;
                sheet.Cells["E" + index].Value = data.PeriodMonth;
                sheet.Cells["F" + index].Value = data.PeriodYear;
                sheet.Cells["G" + index].Value = data.InvoiceNo;
                sheet.Cells["H" + index].Value = data.customer;
                sheet.Cells["I" + index].Value = data.BPL;
                sheet.Cells["J" + index].Value = data.PpnBPL;
                sheet.Cells["K" + index].Value = data.TagihanAir;
                sheet.Cells["L" + index].Value = data.TagihanBulanSebelumnya;
                sheet.Cells["M" + index].Value = data.ADJBPL;
                sheet.Cells["N" + index].Value = data.Penalty;
                sheet.Cells["O" + index].Value = data.CurrTrans;
                sheet.Cells["P" + index].Value = data.Payment;

                index++;
            }
            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

            sheet.Cells["I6:I" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["J6:J" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["K6:K" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["L6:L" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["M6:M" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["N6:N" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["O6:O" + index].Style.Numberformat.Format = "#,##0";
            sheet.Cells["P6:P" + index].Style.Numberformat.Format = "#,##0";


            return sheet;
        }

        public FileDto ExportToExcelJournalToOracle(List<GetBillingJournalTaskListDto> input)
        {

            _logger.LogInformation("ExportToExcelJournalToOracle() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
            var FileName = "Export_ErrorData_JournalToOracle_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".xlsx";

            try
            {
                return CreateExcelPackage(
                FileName,
                excelPackage =>
                {
                    ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                    var sheetWithData = fillDataJournalToOracleSheet(sheet, input);

                });

                _logger.LogInformation("file excel successfully created");
            }
            catch (Exception ex)
            {
                _logger.LogError("file excel failed created : " + ex.Message);
                throw;
            }

        }

        private ExcelWorksheet fillDataJournalToOracleSheet(ExcelWorksheet sheet, List<GetBillingJournalTaskListDto> input)
        {
            sheet.Cells["A1"].Value = "Project Name";
            sheet.Cells["B1"].Value = "Cluster Name";
            sheet.Cells["C1"].Value = "Unit Id";
            sheet.Cells["D1"].Value = "Unit Code";
            sheet.Cells["E1"].Value = "Invoice No";
            sheet.Cells["F1"].Value = "Receipt No";
            sheet.Cells["G1"].Value = "PS Code";
            sheet.Cells["H1"].Value = "Billing Date";
            sheet.Cells["I1"].Value = "Invoice Date";
            sheet.Cells["J1"].Value = "Period";
            sheet.Cells["K1"].Value = "Oracle Desc";
            //sheet.Cells["L1"].Value = "Accounting Date";
            sheet.Cells["L1"].Value = "COA1";
            sheet.Cells["M1"].Value = "COA2";
            sheet.Cells["N1"].Value = "COA3";
            sheet.Cells["O1"].Value = "COA4";
            sheet.Cells["P1"].Value = "COA5";
            sheet.Cells["Q1"].Value = "COA6";
            sheet.Cells["R1"].Value = "COA7";
            sheet.Cells["S1"].Value = "Debet";
            sheet.Cells["T1"].Value = "Credit";
            sheet.Cells["U1"].Value = "Group ID";
            sheet.Cells["A1:U1"].Style.Font.Bold = true;

            var index = 2;
            foreach (var data in input)
            {
                sheet.Cells["A" + index].Value = data.ProjectName;
                sheet.Cells["B" + index].Value = data.ClusterName;
                sheet.Cells["C" + index].Value = data.UnitId;
                sheet.Cells["D" + index].Value = data.UnitCode;
                sheet.Cells["E" + index].Value = data.InvoiceNo;
                sheet.Cells["F" + index].Value = data.ReceiptNumber;
                sheet.Cells["G" + index].Value = data.PsCode;
                sheet.Cells["H" + index].Value = data.BillingDate.ToString("yyyy-MM-dd");
                sheet.Cells["I" + index].Value = data.InvoiceDate.ToString("yyyy-MM-dd");
                sheet.Cells["J" + index].Value = data.PeriodName;
                sheet.Cells["K" + index].Value = data.OracleDesc;
                //sheet.Cells["L" + index].Value = Convert.ToDateTime(data.AccountingDate).ToString("yyyy-MM-dd");
                sheet.Cells["L" + index].Value = data.COA1;
                sheet.Cells["M" + index].Value = data.COA2;
                sheet.Cells["N" + index].Value = data.COA3;
                sheet.Cells["O" + index].Value = data.COA4;
                sheet.Cells["P" + index].Value = data.COA5;
                sheet.Cells["Q" + index].Value = data.COA6;
                sheet.Cells["R" + index].Value = data.COA7;
                sheet.Cells["S" + index].Value = data.Debit.ToString("N0");
                sheet.Cells["T" + index].Value = data.Kredit.ToString("N0");
                //sheet.Cells["V" + index].Value = data.IsTransfered;
                sheet.Cells["U" + index].Value = data.GroupId;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }


        public FileDto ExportToExcelUserResult(ViewUserReportDto input)
        {

            {
                _logger.LogDebug("ExportToExcelUserResult() - Start Generate Excel. Time = {1}{0}", DateTime.Now, Environment.NewLine);
                var FileName = "Export_User_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

                return CreateExcelPackage(
                    FileName,
                    excelPackage =>
                    {
                        ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                        var sheetWithData = fillDataUserSheet(sheet, input);

                    });

            }

        }
        private ExcelWorksheet fillDataUserSheet(ExcelWorksheet sheet, ViewUserReportDto input)
        {
            var Printdate = DateTime.Now.ToString("dd/MM/yyyy");
            sheet.OutLineApplyStyle = true;
            sheet.Cells["A5:J5"].Style.Font.Bold = true;

            sheet.Cells["A1"].Value = "Report User";

            sheet.Cells["A3"].Value = "Printdate : " + Printdate + "";
            sheet.Cells["A4"].Value = "Print By : " + input.printBy + "";

            sheet.Cells["A5"].Value = "User Name";
            sheet.Cells["B5"].Value = "Name";
            sheet.Cells["C5"].Value = "Surname";
            sheet.Cells["D5"].Value = "Roles";
            sheet.Cells["E5"].Value = "Email";
            sheet.Cells["F5"].Value = "Phone number";
            sheet.Cells["G5"].Value = "Active";
            sheet.Cells["H5"].Value = "LastLoginDate";
            sheet.Cells["I5"].Value = "CreatedDate";


            sheet.Cells["A5:I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells["A5:I5"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#8EA9DB"));
            sheet.Cells["A1"].Style.Font.Size = 16;


            var index = 6;
            foreach (var data in input.ListUser)
            {
                sheet.Cells["A" + index].Value = data.UserName;
                sheet.Cells["B" + index].Value = data.Name;
                sheet.Cells["C" + index].Value = data.Surname;
                sheet.Cells["D" + index].Value = data.Roles;
                sheet.Cells["E" + index].Value = data.Email;
                sheet.Cells["F" + index].Value = data.PhoneNumber;
                sheet.Cells["G" + index].Value = data.Active;
                sheet.Cells["H" + index].Value = data.LastLoginDate;
                sheet.Cells["I" + index].Value = data.CreatedDate;

                index++;
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            return sheet;
        }

    }
}
