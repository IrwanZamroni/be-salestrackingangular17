﻿using Abp.Dependency;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.Net.MimeTypes;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Abp.Collections.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml.Style;
using BeBillingSystem.BillingSystem;

namespace BeBillingSystem.Report
{
    public abstract class EpPlusExcelExporterBase : ITransientDependency
    {
        public IAppFolders AppFolders { get; set; }


        protected FileDto CreateExcelPackage(string fileName, Action<ExcelPackage> creator)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                creator(excelPackage);
                Save(excelPackage, file);
            }

            return file;
        }

        protected FileDto CreateExcelReport(string fileName, Action<ExcelPackage> creator)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                creator(excelPackage);
                SaveReport(excelPackage, file);
            }

            return file;
        }

        protected void SaveReport(ExcelPackage excelPackage, FileDto file)
        {
            var filePath = Path.Combine(AppFolders.UploadRootDirectory, AppFolders.TempFileDownloadDirectory, file.FileToken);
            excelPackage.SaveAs(new FileInfo(filePath));
        }

        protected FileDto CreateExcelPackageFromTemplate(string fileName, string templatePath, Action<ExcelPackage> creator)
        {
            var file = new FileDto(fileName, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (FileStream templateDocumentStream = File.OpenRead(templatePath))
            {
                using (var excelPackage = new ExcelPackage(templateDocumentStream))
                {
                    creator(excelPackage);
                    Save(excelPackage, file);
                }
            }

            return file;
        }

        protected void AddHeader(ExcelWorksheet sheet, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, headerTexts[i]);
            }
        }

        protected void AddHeaders(ExcelWorksheet sheet, int headerRow, params string[] headerTexts)
        {
            if (headerTexts.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < headerTexts.Length; i++)
            {
                AddHeader(sheet, i + 1, headerTexts[i], headerRow);
            }
        }

        protected void AddHeader(ExcelWorksheet sheet, int columnIndex, string headerText, int headerRow = 1)
        {
            sheet.Cells[headerRow, columnIndex].Value = headerText;
            sheet.Cells[headerRow, columnIndex].Style.Font.Bold = true;
        }

        protected void AddHeader(ExcelWorksheet sheet, int columnIndex, string headerText)
        {
            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#ffc000");
            sheet.Cells[1, columnIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[1, columnIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, columnIndex].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, columnIndex].Value = headerText;
            sheet.Cells[1, columnIndex].Style.Font.Bold = true;
            sheet.Cells[1, columnIndex].Style.Border.BorderAround(ExcelBorderStyle.Thin);

        }

        protected void AddObjects<T>(ExcelWorksheet sheet, int startRowIndex, IList<T> items, params Func<T, object>[] propertySelectors)
        {
            if (items.IsNullOrEmpty() || propertySelectors.IsNullOrEmpty())
            {
                return;
            }

            for (var i = 0; i < items.Count; i++)
            {
                for (var j = 0; j < propertySelectors.Length; j++)
                {
                    sheet.Cells[i + startRowIndex, j + 1].Value = propertySelectors[j](items[i]);
                    sheet.Cells[i + startRowIndex, j + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                }
            }
        }

        protected void Save(ExcelPackage excelPackage, FileDto file)
        {
            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var webConfigApp = (JObject)appsettingsjson["App"];
            var UploadRootDirectory = webConfigApp.Property("UploadRootDirectory").Value.ToString();

            string paths = UploadRootDirectory + "\\Temp\\Downloads";
            //var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
            var filePath = Path.Combine(paths, file.FileToken);
            excelPackage.SaveAs(new FileInfo(filePath));
        }

        //Start Add Image
        protected void AddImage(ExcelWorksheet oSheet, int rowIndex, int colIndex, string imagePath, int width, int height)
        {
            Bitmap image = new Bitmap(imagePath);
            {
                var excelImage = oSheet.Drawings.AddPicture("Image-" + DateTime.Now, image);
                excelImage.From.Column = colIndex - 1;
                excelImage.From.Row = rowIndex - 1;
                excelImage.SetSize(width, height);
                excelImage.From.ColumnOff = Pixel2MTU(2);
                excelImage.From.RowOff = Pixel2MTU(2);
            }
        }

        public int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }
        //End Add Image
    }
}