﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace BeBillingSystem.Helpers
{
    public static class ServiceHelper
    {

        public static async Task<bool> EmailSendAsync(DynamicEmailInputDto input)
        {
            try
            {
                var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
                var webConfigApp = (JObject)appsettingsjson["App"];

                var smtpHost = webConfigApp.Property("smtpHost").Value.ToString();
                var smtpPort = Int32.Parse(webConfigApp.Property("smtpPort").Value.ToString());
                var emailSmtp = webConfigApp.Property("emailSmtp").Value.ToString();
                var passwordEmailSmtp = webConfigApp.Property("passwordEmailSmtp").Value.ToString();
                var UseDefaultCredentials = Convert.ToBoolean(webConfigApp.Property("UseDefaultCredentials").Value);
                var EnableSsl = Convert.ToBoolean(webConfigApp.Property("EnableSsl").Value);
             

                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(emailSmtp);

                    foreach (var item in input.To)
                    {
                        mailMessage.To.Add(new MailAddress(item));
                    }

                    foreach (var item in input.Cc)
                    {
                        mailMessage.CC.Add(new MailAddress(item));
                    }

                    foreach (var item in input.Bcc)
                    {
                        mailMessage.Bcc.Add(new MailAddress(item));
                    }

                    mailMessage.Subject = input.Subject;
                    mailMessage.Body = input.Body;

                    mailMessage.IsBodyHtml = input.IsHtmlBody;
                    if (input.AttachmentFile != null)
                    {
                        mailMessage.Attachments.Add(new Attachment(input.AttachmentFile));
                    }

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = smtpHost;
                    smtp.Port = smtpPort;
                    smtp.EnableSsl = EnableSsl;

                    NetworkCredential NetworkCred = new NetworkCredential();

                    NetworkCred.UserName = emailSmtp;

                    NetworkCred.Password = passwordEmailSmtp;

                    smtp.UseDefaultCredentials = UseDefaultCredentials;

                    smtp.Credentials = NetworkCred;

                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    await smtp.SendMailAsync(mailMessage);

                    return true;
                }
            }
            catch (Exception ex)
            {
                //Logger.ErrorFormat($"SendAsync() error:{Environment.NewLine}" +
                //                     $"Subject = {input.Subject}{Environment.NewLine}" +
                //                     $"ErrorReason = {ex.Message}");

                return false;
            }
        }

     

}
}
