﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.Helpers
{
    public class HelpersDto
    {
    }
    public class DynamicEmailInputDto
    {
        public List<string> To { get; set; }

        public List<string> Cc { get; set; }

        public List<string> Bcc { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool IsHtmlBody { get; set; }
        public string AttachmentFile { get; set; }
    }
}
