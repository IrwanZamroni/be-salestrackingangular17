﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using BeBillingSystem.Authorization;
using BeBillingSystem.Authorization.Accounts;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.BillingSystem;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.EntityFrameworkCore;
using BeBillingSystem.Migrations;
using BeBillingSystem.Roles.Dto;
using BeBillingSystem.Users.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace BeBillingSystem.Users
{
    //[AbpAuthorize(PermissionNames.Pages_Users)]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IAbpSession _abpSession;
        private readonly LogInManager _logInManager;
        private readonly BeBillingSystemDbContext _contexBilling;
        private readonly FilesHelper _filesHelper;
        private readonly IWebHostEnvironment _hostEnvironment;



        public UserAppService(
              FilesHelper filesHelper,
            IRepository<User, long> repository,
              IWebHostEnvironment hostEnvironment,
            UserManager userManager,
            BeBillingSystemDbContext contexBilling,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            IAbpSession abpSession,
            LogInManager logInManager)
            : base(repository)
        {
            _filesHelper = filesHelper;
            _contexBilling = contexBilling;
            _userManager = userManager;
            _roleManager = roleManager;
            _hostEnvironment = hostEnvironment;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _abpSession = abpSession;
            _logInManager = logInManager;
        }

        public async Task ProsesCreateNewUser(CreateUserDto input)
        {

            UserDto createUser =  await CreateAsync(input);

            foreach (var item in input.SiteId) {
                var addUserSite = new MP_UserSite
                {
                    SiteId = item,
                    UserId = Convert.ToInt32(createUser.Id) 
                    

                };

                _contexBilling.MP_UserSite.Add(addUserSite);
                _contexBilling.SaveChanges();

            }

        }


        public async Task ProsesUpdateUser(UserDto input)
        {

          //  UserDto updateUser = await UpdateAsync(input);


            string imageUrl;

            var updateUser = (from a in _contexBilling.Users
                               where a.Id == input.Id
                               select a).FirstOrDefault();

            if (input.PhotoProfile != null)
            {
              
                imageUrl = UploadImageProfile(input.PhotoProfile);
                GetURLWithoutHost(imageUrl, out imageUrl);
                updateUser.ProfileImg = imageUrl;
            }

           
            updateUser.UserName = input.UserName;
            updateUser.EmailAddress = input.EmailAddress;
            updateUser.Name = input.Name;
            updateUser.PhoneNumber = input.PhoneNumber;
            updateUser.Surname = input.Surname;


            _contexBilling.Users.Update(updateUser);
            _contexBilling.SaveChanges();


            if (input.Password != null)
            {

                CheckErrors(await _userManager.ChangePasswordAsync(updateUser, input.Password));
            }


            var getDataToDelete = (from a in _contexBilling.MP_UserSite
                                   where a.UserId == input.Id
                                   select a).ToList();

            if (getDataToDelete != null)
            {
                _contexBilling.MP_UserSite.RemoveRange(getDataToDelete);
                _contexBilling.SaveChanges();
            }

            foreach (var item in input.SiteId)
            {
                var addUserSite = new MP_UserSite
                {
                    SiteId = item,
                    UserId = Convert.ToInt32(input.Id)


                };
                _contexBilling.MP_UserSite.Add(addUserSite);
                _contexBilling.SaveChanges();

            }

        }


        public PagedResultDto<UserExportInputDto> GetListUser(UserListinputDto input) 
        {
            var userList = new List<UserExportInputDto>();

            var getData = (from a in _contexBilling.Users
                           orderby a.UserName ascending
                           select a).ToList();

            foreach (var item in getData) {

                var getUserRole = (from a in _contexBilling.UserRoles
                                   join b in _contexBilling.Roles on a.RoleId equals b.Id
                                   where a.UserId == item.Id
                                   select b.Name).Distinct().ToList();

                var getLastLogin = (from a in _contexBilling.UserLoginAttempts
                                    where a.UserId == item.Id
                                    orderby a.CreationTime descending
                                    select a).FirstOrDefault();

                var addUser = new UserExportInputDto
                {
                    Id = item.Id,
                    Active = item.IsActive == true ? 1 : 0,
                    CreatedDate = item.CreationTime.ToString("dd MMM yyyy"),
                    Email = item.EmailAddress,
                    LastLoginDate = getLastLogin != null ? getLastLogin.CreationTime.ToString("dd MMM yyyy") : "",
                    Name = item.Name,
                    Surname = item.Surname,
                    UserName = item.UserName,
                    PhoneNumber = item.PhoneNumber,
                    Roles = string.Join(",", getUserRole),
                };

                userList.Add(addUser);
            }



            var dataCount = userList.Count();

            if (input.Search != null)
            {

                userList = userList.Where(x => x.Name.ToLower().Contains(input.Search.ToLower()) || x.Surname.ToLower().Contains(input.Search.ToLower())
                || x.UserName.ToLower().Contains(input.Search.ToLower())
                || x.Email.ToLower().Contains(input.Search.ToLower()) || x.PhoneNumber.ToLower().Contains(input.Search.ToLower())).ToList();

            }


            userList = userList.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();


            return new PagedResultDto<UserExportInputDto>(dataCount, userList.ToList());

 
        
        
        }
        public override async Task<UserDto> CreateAsync(CreateUserDto input)
        {
            CheckCreatePermission();

            string imageUrl;
            if (input.PhotoProfile == null)
            {
                imageUrl = "-";
            }
            else
            {
                imageUrl = UploadImageProfile(input.PhotoProfile);
                GetURLWithoutHost(imageUrl, out imageUrl);
            }

            var user = ObjectMapper.Map<User>(input);

            user.ProfileImg = imageUrl;
            user.TenantId = AbpSession.TenantId;
            user.PhoneNumber = input.PhoneNumber;
            user.IsEmailConfirmed = true;

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            CheckErrors(await _userManager.CreateAsync(user, input.Password));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRolesAsync(user, input.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(user);
        }

        public override async Task<UserDto> UpdateAsync(UserDto input)
        {
            CheckUpdatePermission();


            var user = await _userManager.GetUserByIdAsync(input.Id);

            MapToEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRolesAsync(user, input.RoleNames));
            }

            return await GetAsync(input);
        }

        public override async Task DeleteAsync(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        [AbpAuthorize(PermissionNames.Pages_Users_Activation)]
        public async Task Activate(EntityDto<long> user)
        {
            await Repository.UpdateAsync(user.Id, async (entity) =>
            {
                entity.IsActive = true;
            });
        }

        [AbpAuthorize(PermissionNames.Pages_Users_Activation)]
        public async Task DeActivate(EntityDto<long> user)
        {
            await Repository.UpdateAsync(user.Id, async (entity) =>
            {
                entity.IsActive = false;
            });
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override UserDto MapToEntityDto(User user)
        {
            var roleIds = user.Roles.Select(x => x.RoleId).ToArray();

            var roles = _roleManager.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.NormalizedName);

            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();

            return userDto;
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedUserResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles)
                .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.UserName.Contains(input.Keyword) || x.Name.Contains(input.Keyword) || x.EmailAddress.Contains(input.Keyword))
                .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedUserResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<bool> ChangePassword(ChangePasswordDto input)
        {
            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            var user = await _userManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }
            
            if (await _userManager.CheckPasswordAsync(user, input.CurrentPassword))
            {
                CheckErrors(await _userManager.ChangePasswordAsync(user, input.NewPassword));
            }
            else
            {
                CheckErrors(IdentityResult.Failed(new IdentityError
                {
                    Description = "Incorrect password."
                }));
            }

            return true;
        }

        public async Task<bool> ResetPassword(ResetPasswordDto input)
        {
            if (_abpSession.UserId == null)
            {
                throw new UserFriendlyException("Please log in before attempting to reset password.");
            }
            
            var currentUser = await _userManager.GetUserByIdAsync(_abpSession.GetUserId());
            var loginAsync = await _logInManager.LoginAsync(currentUser.UserName, input.AdminPassword, shouldLockout: false);
            if (loginAsync.Result != AbpLoginResultType.Success)
            {
                throw new UserFriendlyException("Your 'Admin Password' did not match the one on record.  Please try again.");
            }
            
            if (currentUser.IsDeleted || !currentUser.IsActive)
            {
                return false;
            }
            
            var roles = await _userManager.GetRolesAsync(currentUser);
            if (!roles.Contains(StaticRoleNames.Tenants.Admin))
            {
                throw new UserFriendlyException("Only administrators may reset passwords.");
            }

            var user = await _userManager.GetUserByIdAsync(input.UserId);
            if (user != null)
            {
                user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            return true;
        }


        private void GetURLWithoutHost(string path, out string finalpath)
        {
            finalpath = path;
            try
            {
                Regex RegexObj = new Regex("[\\w\\W]*([\\/]Assets[\\w\\W\\s]*)");
                if (RegexObj.IsMatch(path))
                {
                    finalpath = RegexObj.Match(path).Groups[1].Value;
                }
            }
            catch (ArgumentException ex)
            {
            }
        }
        private string UploadImageProfile(string filename)
        {
            try
            {
                return _filesHelper.MoveFiles(filename, @"Temp\Downloads\ProfileUser\", @"Assets\Upload\ProfileUser\");
            }
            catch (Exception ex)
            {
                Logger.DebugFormat("test() - ERROR Exception. Result = {0}", ex.Message);
                throw new UserFriendlyException("Error : {0}", ex.Message);
            }
        }


        
    }
}

