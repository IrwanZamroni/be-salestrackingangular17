using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}