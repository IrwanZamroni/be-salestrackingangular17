﻿using Abp.Application.Services;
using BeBillingSystem.MultiTenancy.Dto;

namespace BeBillingSystem.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

