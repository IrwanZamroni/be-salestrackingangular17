﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using BeBillingSystem.Authorization;

namespace BeBillingSystem
{
    [DependsOn(
        typeof(BeBillingSystemCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class BeBillingSystemApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<BeBillingSystemAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(BeBillingSystemApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
