﻿using Abp.Application.Services;
using BeBillingSystem.BillingSystem.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem
{
    public interface IFilesHelper : IApplicationService
    {
        string MoveFiles(string filename, string oldPath, string newPath);
        string MoveFilesLegalDoc(string filename, string oldPath, string newPath, int? orderNumber);
        string MoveFilesWithRename(string oldFilename, string newFileName, string oldPath, string newPath);
        string CopyKPFile(string filename, string oldPath, string newPath, int? orderNumber);
        string CopyFileTemplateNotif(string filename, string oldPath, string newPath, string projectName, string notifCode);
        string MoveFilesTemplateNotif(string filename, string oldPath, string newPath, string projectName, string notifCode);
        string ConvertIdToCode(int? Id);
        string ConvertDocIdToDocCode(int Id);
        List<LinkPathListDto> GetBase64FileByPhysicalPath(string physicalPath);
        List<LinkPathListDto> GetBase64FileByPhysicalPathFilter(string physicalPath, string fileName);
        string getAbsoluteUri();
        string getAbsoluteUriHttpHardcode();
        string GetURLWithoutHost(string path);
        string GetAbsoluteUriWithoutTail();
    }
}
