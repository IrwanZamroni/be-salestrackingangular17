﻿using System.Threading.Tasks;
using Abp.Application.Services;
using BeBillingSystem.Authorization.Accounts.Dto;

namespace BeBillingSystem.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
