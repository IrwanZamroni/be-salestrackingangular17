﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using BeBillingSystem.Configuration.Dto;

namespace BeBillingSystem.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : BeBillingSystemAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
