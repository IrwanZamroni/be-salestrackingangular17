﻿using System.Threading.Tasks;
using BeBillingSystem.Configuration.Dto;

namespace BeBillingSystem.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
