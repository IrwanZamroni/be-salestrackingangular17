﻿using Abp.Application.Services;
using BeBillingSystem.BillingSystem.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem
{
    public interface IWhatsappHelper : IApplicationService
    {
        Task<bool> SendWhatsApp(WhatsAppInputDto input);
        Task<string> SendWhatsAppWithResult(WhatsAppInputDto input);
    }
}
