﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.UI;
using BeBillingSystem.Authorization;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.BillingSystem.Dto;
using BeBillingSystem.Roles.Dto;
using Dapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using OfficeOpenXml.Style;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace BeBillingSystem.Roles
{
	//[AbpAuthorize(PermissionNames.Pages_Users)]
	public class RoleAppService : AsyncCrudAppService<Role, RoleDto, int, PagedRoleResultRequestDto, CreateRoleDto, RoleDto>, IRoleAppService
    {
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;

        public RoleAppService(IRepository<Role> repository, RoleManager roleManager, UserManager userManager)
            : base(repository)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
		public class DataUserDto
		{
			public int Id { get; set; }
			public string Name { get; set; }
			public Boolean IsDefault { get; set; }
            public string FormattedCreationTime { get; set; }
            public Boolean isStatic { get; set; }

		}

		public class input
		{
		
			public string name { get; set; }


			public string CreationTime { get; set; }

		}

		public class DataRoleDto
		{
			
			public string Name { get; set; }
		
			public string NameRole { get; set; }
			

		}
		public class ParameterUserDto : PagedInputDto
		{
			
			public string searchText { get; set; }
		}


		[HttpPost]
		public List<DataRoleDto> GetRoleByUsername(input input)
		{
			try
			{
				var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
				var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
				var conString = connectionStrings.Property("Default").Value.ToString();

				using (SqlConnection conn = new SqlConnection(conString))
				{
					conn.Open();

					List<DataRoleDto> result = new List<DataRoleDto>();
					string query = "EXEC SP_GetRoleByUsername @name";

					result = conn.Query<DataRoleDto>(query, new
					{
						name = input.name
						
					}).ToList();

					return result;
				}
			}
			catch (Exception ex)
			{
				throw new UserFriendlyException("Error while updating data. Details: " + ex.Message, ex);
			}
		}



		[HttpPost]
		public PagedResultDto<DataUserDto> GetListData(ParameterUserDto input)
		{
			#region Constring DB
			var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
			var connectionStrings = (JObject)appsettingsjson["ConnectionStrings"];
			var Constring = connectionStrings.Property("Default").Value.ToString();

			var dbConstringString = connectionStrings.Property("Default").Value.ToString().Replace(" ", string.Empty).Split(";");

			using SqlConnection conn = new SqlConnection(Constring);

			conn.Open();
			#endregion

			string query = @"
      SELECT id, Name,
       CONVERT(VARCHAR, CreationTime, 106) AS FormattedCreationTime,
       IsDefault, IsStatic
FROM (
    SELECT id, Name, CreationTime, IsDefault, IsStatic,
           ROW_NUMBER() OVER (PARTITION BY Name ORDER BY id) AS RowNum
    FROM dbo.AbpRoles
    WHERE IsDeleted = 0
) AS Subquery
WHERE RowNum = 1;


    ";

			List<DataUserDto> getData = conn.Query<DataUserDto>(query).ToList();

			getData = getData
	.WhereIf(!string.IsNullOrEmpty(input.searchText),
			 item => item.Name.ToLower().Contains(input.searchText.ToLower()) ||
					 item.FormattedCreationTime.ToLower().Contains(input.searchText.ToLower()))
	.ToList();


			var dataCount = getData.Count();

			getData = getData.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

			return new PagedResultDto<DataUserDto>(dataCount, getData.ToList());
		}


		public override async Task<RoleDto> CreateAsync(CreateRoleDto input)
        {
            CheckCreatePermission();

            var role = ObjectMapper.Map<Role>(input);
            role.SetNormalizedName();

            CheckErrors(await _roleManager.CreateAsync(role));

            var grantedPermissions = PermissionManager
                .GetAllPermissions()
                .Where(p => input.GrantedPermissions.Contains(p.Name))
                .ToList();

            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);

            return MapToEntityDto(role);
        }

        public async Task<ListResultDto<RoleListDto>> GetRolesAsync(GetRolesInput input)
        {
            var roles = await _roleManager
                .Roles
                .WhereIf(
                    !input.Permission.IsNullOrWhiteSpace(),
                    r => r.Permissions.Any(rp => rp.Name == input.Permission && rp.IsGranted)
                )
                .ToListAsync();

            return new ListResultDto<RoleListDto>(ObjectMapper.Map<List<RoleListDto>>(roles));
        }
		[HttpPost]
		public override async Task<RoleDto> UpdateAsync(RoleDto input)
        {
            CheckUpdatePermission();

            var role = await _roleManager.GetRoleByIdAsync(input.Id);

            ObjectMapper.Map(input, role);

            CheckErrors(await _roleManager.UpdateAsync(role));

            var grantedPermissions = PermissionManager
                .GetAllPermissions()
                .Where(p => input.GrantedPermissions.Contains(p.Name))
                .ToList();

            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);

            return MapToEntityDto(role);
        }
        [HttpPost]
        public override async Task DeleteAsync(EntityDto<int> input)
        {
            CheckDeletePermission();

            var role = await _roleManager.FindByIdAsync(input.Id.ToString());
            var users = await _userManager.GetUsersInRoleAsync(role.NormalizedName);

            foreach (var user in users)
            {
                CheckErrors(await _userManager.RemoveFromRoleAsync(user, role.NormalizedName));
            }

            CheckErrors(await _roleManager.DeleteAsync(role));
        }

        public Task<ListResultDto<PermissionDto>> GetAllPermissions()
        {
            var permissions = PermissionManager.GetAllPermissions();

            return Task.FromResult(new ListResultDto<PermissionDto>(
                ObjectMapper.Map<List<PermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList()
            ));
        }

        protected override IQueryable<Role> CreateFilteredQuery(PagedRoleResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Permissions)
                .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.Name.Contains(input.Keyword)
                || x.DisplayName.Contains(input.Keyword)
                || x.Description.Contains(input.Keyword));
        }

        protected override async Task<Role> GetEntityByIdAsync(int id)
        {
            return await Repository.GetAllIncluding(x => x.Permissions).FirstOrDefaultAsync(x => x.Id == id);
        }

        protected override IQueryable<Role> ApplySorting(IQueryable<Role> query, PagedRoleResultRequestDto input)
        {
            return query.OrderBy(r => r.DisplayName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<GetRoleForEditOutput> GetRoleForEdit(EntityDto input)
        {
            var permissions = PermissionManager.GetAllPermissions();
            var role = await _roleManager.GetRoleByIdAsync(input.Id);
            var grantedPermissions = (await _roleManager.GetGrantedPermissionsAsync(role)).ToArray();
            var roleEditDto = ObjectMapper.Map<RoleEditDto>(role);

            return new GetRoleForEditOutput
            {
                Role = roleEditDto,
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }
    }
}

