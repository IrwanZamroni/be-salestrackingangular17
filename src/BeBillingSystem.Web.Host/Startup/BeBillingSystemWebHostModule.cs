﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using BeBillingSystem.Configuration;

namespace BeBillingSystem.Web.Host.Startup
{
    [DependsOn(
       typeof(BeBillingSystemWebCoreModule))]
    public class BeBillingSystemWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public BeBillingSystemWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(BeBillingSystemWebHostModule).GetAssembly());
        }
    }
}
