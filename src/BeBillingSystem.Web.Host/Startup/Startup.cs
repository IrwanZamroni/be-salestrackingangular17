﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Abp.AspNetCore;
using Abp.AspNetCore.Mvc.Antiforgery;
using Abp.Castle.Logging.Log4Net;
using Abp.Extensions;
using BeBillingSystem.Configuration;
using BeBillingSystem.Identity;
using Abp.AspNetCore.SignalR.Hubs;
using Abp.Dependency;
using Abp.Json;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System.IO;
using Abp.EntityFrameworkCore;
using BeBillingSystem.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VDI.Demo.EntityFrameworkCore;

namespace BeBillingSystem.Web.Host.Startup
{

    public class Startup
    {
		//AllowOrigin
        //localhost
		private const string _defaultCorsPolicyName = "AllowOrigin";

        private const string _apiVersion = "v1";

        private readonly IConfigurationRoot _appConfiguration;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public IConfiguration Configuration { get; }
      
        public Startup(IWebHostEnvironment env)
        {
            _hostingEnvironment = env;
            _appConfiguration = env.GetAppConfiguration();
          

        }

        public void ConfigureServices(IServiceCollection services)
        {
			//services.AddSingleton<IConfiguration>(Configuration);

			//// Registrasikan DatabaseService
			//services.AddScoped<IDatabaseService, DatabaseService>();
			//// var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
			//services.AddAbpDbContext<BeBillingSystemDbContext>(options =>
			//{
			//    BeBillingSystemDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
			//});
			//var coreSystemDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.CoreSystemDbContext);
			//services.AddDbContext<DemoDbContext>(options => options.UseSqlServer(coreSystemDbContext));
			//services.AddDbContext<NewCommDbContext>(options =>
			//{
			//	options.UseSqlServer(Configuration.GetConnectionString("NewCommDbContext"));
			//	// Configure other options if needed
			//});
			
			var newCommDbContextDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.E3NewCommDbContext);
            services.AddDbContext<NewCommDbContext>(options => options.UseSqlServer(newCommDbContextDbContext));

            var beBillingSystemDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.E3BillingSystemDbContext);
            services.AddDbContext<BeBillingSystemDbContext>(options => options.UseSqlServer(beBillingSystemDbContext));

            var propertySystemDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.PropertySystemDbContext);
            services.AddDbContext<PropertySystemDbContext>(options => options.UseSqlServer(propertySystemDbContext));

            var personalsNewDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.PersonalsNewDbContext);
            services.AddDbContext<PersonalsNewDbContext>(options => options.UseSqlServer(personalsNewDbContext));

            var oracleStageDbContext = _appConfiguration.GetConnectionString(BeBillingSystemConsts.OracleStageDbContext);
            services.AddDbContext<OracleStageDbContext>(options => options.UseSqlServer(oracleStageDbContext));

            //MVC
            services.AddControllersWithViews(
                options => { options.Filters.Add(new AbpAutoValidateAntiforgeryTokenAttribute()); }
            ).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new AbpMvcContractResolver(IocManager.Instance)
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
            });

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            
            //services.AddSignalR();

            // Configure CORS for angular2 UI
            services.AddCors(
                options => options.AddPolicy(
                    _defaultCorsPolicyName,
                    builder => builder
                        .WithOrigins(
                            // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                            _appConfiguration["App:CorsOrigins"]
                                .Split(",", StringSplitOptions.RemoveEmptyEntries)
                                .Select(o => o.RemovePostFix("/"))
                                .ToArray()
                        )
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                )
            );

            // Swagger - Enable this line and the related lines in Configure method to enable swagger UI
            ConfigureSwagger(services);

            // Configure Abp and Dependency Injection
            services.AddAbpWithoutCreatingServiceProvider<BeBillingSystemWebHostModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig(_hostingEnvironment.IsDevelopment()
                        ? "log4net.config"
                        : "log4net.Production.config"
                    )
                )
            );
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; }); // Initializes ABP framework.

            app.UseCors(_defaultCorsPolicyName); // Enable CORS!

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAbpRequestLocalization();


            app.UseEndpoints(endpoints =>
            {
               // endpoints.MapHub<AbpCommonHub>("/signalr");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("defaultWithArea", "{area}/{controller=Home}/{action=Index}/{id?}");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger(c => { c.RouteTemplate = "swagger/{documentName}/swagger.json"; });

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                // specifying the Swagger JSON endpoint.
                options.SwaggerEndpoint($"/swagger/{_apiVersion}/swagger.json", $"BeBillingSystem API {_apiVersion}");
                options.IndexStream = () => Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("BeBillingSystem.Web.Host.wwwroot.swagger.ui.index.html");
                options.DisplayRequestDuration(); // Controls the display of the request duration (in milliseconds) for "Try it out" requests.  
            }); // URL: /swagger
        }
        
        private void ConfigureSwagger(IServiceCollection services)
        {


            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(_apiVersion, new OpenApiInfo
                {
                    Version = _apiVersion,
                    Title = _appConfiguration["App:AppName"] ,
                    Description = "BeBillingSystem",
               
                  
                });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("bearerAuth", new OpenApiSecurityScheme()
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                //add summaries to swagger
                bool canShowSummaries = _appConfiguration.GetValue<bool>("Swagger:ShowSummaries");
                if (canShowSummaries)
                {
                    var hostXmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var hostXmlPath = Path.Combine(AppContext.BaseDirectory, hostXmlFile);
                    options.IncludeXmlComments(hostXmlPath);

                    var applicationXml = $"BeBillingSystem.Application.xml";
                    var applicationXmlPath = Path.Combine(AppContext.BaseDirectory, applicationXml);
                    options.IncludeXmlComments(applicationXmlPath);

                    var webCoreXmlFile = $"BeBillingSystem.Web.Core.xml";
                    var webCoreXmlPath = Path.Combine(AppContext.BaseDirectory, webCoreXmlFile);
                    options.IncludeXmlComments(webCoreXmlPath);
                }
            });
        }



        //private StartupConnectionStrings GetConnectionStrings()
        //{
        //    return new StartupConnectionStrings
        //    {
        //        DecryptedCoreConnectionString = GetConnectionString(DemoConsts.ConnectionStringName),
        //        DecryptedNewCommConnectionString = GetConnectionString(DemoConsts.ConnectionStringNewCommDbContext),
        //        DecryptedPropertySystemConnectionString = GetConnectionString(DemoConsts.ConnectionStringPropertySystemDbContext),
        //        DecryptedPersonalConnectionString = GetConnectionString(DemoConsts.ConnectionStringPersonalsNewDbContext),
        //        DecryptedAccountingConnectionString = GetConnectionString(DemoConsts.ConnectionStringAccountingDbContext),
        //        DecryptedTaxConnectionString = GetConnectionString(DemoConsts.ConnectionStringTAXDbContext),
        //        DecryptedPPOnlineConnectionString = GetConnectionString(DemoConsts.ConnectionStringPPOnline),
        //        DecryptedOracleConnectionString = GetConnectionString(DemoConsts.ConnectionStringOracleStageDbContext),
        //        // DecryptedSIMExecutiveConnectionString = GetConnectionString(DemoConsts.ConnectionStringSIMAExecutive),
        //        DecryptedHumanResourcesConnectionString = GetConnectionString(DemoConsts.ConnectionStringHumanResourceDbContext),
        //        DecryptedSmsCampaignConnectionString = GetConnectionString(DemoConsts.ConnectionStringSMSCampaignDbContext),
        //        DecryptedWorkflowConnectionString = GetConnectionString(DemoConsts.ConnectionStringWorkflowDbContext),
        //        DecryptedDiagramaticConnectionString = GetConnectionString(DemoConsts.ConnectionStringDiagramaticDbContext),
        //        DecryptedOtherAppsConnectionString = GetConnectionString(DemoConsts.ConnectionStringOtherAppsDbContext)
        //    };
        //}
        //private StartupConnectionStrings GetConnectionStrings()
        //{
        //    var GetConnectionString = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

        //    return new StartupConnectionStrings
        //    {
        //        DecryptedCoreConnectionString = GetConnectionString.GetConnectionString(BeBillingSystemConsts.ConnectionStringName),
             
        //        DecryptedPropertySystemConnectionString = GetConnectionString.GetConnectionString(BeBillingSystemConsts.PropertySystemDbContext),
             
        //    };
        //}

        //private string GetConnectionString(string connectionStringName)
        //{
        //    return EncConnString.DecryptString(_appConfiguration.GetConnectionString(connectionStringName));
        //}

        ///// <summary>
        ///// Add every DbContext to the service collection.
        ///// </summary>
        //private void ConfigureDatabases(IServiceCollection services, StartupConnectionStrings connectionStrings)
        //{
        //    services.AddDbContext<BeBillingSystemDbContext>(options =>
        //        options.UseSqlServer(connectionStrings.DecryptedCoreConnectionString));

           

        //    services.AddDbContext<PropertySystemDbContext>(options =>
        //        options.UseSqlServer(connectionStrings.DecryptedPropertySystemConnectionString));

          
        //}
        //private void ValidateConfiguredDatabases(IServiceProvider serviceProvider)
        //{
        //    var connectionChecker = new ConnectionChecker(serviceProvider);

        //    var logger = serviceProvider.GetService<ILogger<Startup>>();

        //    logger.LogInformation($"Checking connections using '{_environmentName}' environment.");

        

        //    connectionChecker.CheckDbAndTableAccessible<PropertySystemDbContext>(x => x.MS_Account);

        //    connectionChecker.CheckDbAndTableAccessible<PersonalsNewDbContext>(x => x.PERSONALS_MEMBER);

        //    LogAndOrThrowConnectionTestResults(
        //        connectionChecker.ConnectionTestResults,
        //        logger
        //    );
        //}

    }
}
