﻿namespace BeBillingSystem.Web.Host
{
    public class AppSettingsHelper
    {
        public static AppSettings GetValue { get; set; } = new AppSettings();
    }

    public class Connectionstrings
    {
        public string CoreConnection { get; set; }
        public string PropertySystemDbContext { get; set; }
        public string PersonalsNewDbContext { get; set; }
        public string PostgresqlDbContext { get; set; }
    }
    public class AppSettings
    {

        public string FMApi { get; set; }
        public string GetFileApi { get; set; }
        public string GetFileMothod { get; set; }
    }
}
