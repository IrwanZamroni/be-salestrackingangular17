using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace BeBillingSystem.Controllers
{
    public abstract class BeBillingSystemControllerBase: AbpController
    {
        protected BeBillingSystemControllerBase()
        {
            LocalizationSourceName = BeBillingSystemConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
