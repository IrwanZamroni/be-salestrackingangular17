﻿//using Abp.IdentityServer4;
using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("AbpPersistedGrants")]
	public class PersistedGrantEntity : Entity<string>
	{
		public virtual string Type { get; set; }

		public virtual string SubjectId { get; set; }

		public virtual string ClientId { get; set; }

		public virtual DateTime CreationTime { get; set; }

		public virtual DateTime? Expiration { get; set; }

		public virtual string Data { get; set; }
	}
}
#if false // Decompilation log
'251' items in cache
------------------
Resolve: 'netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\netstandard.dll'
------------------
Resolve: 'Abp.ZeroCore, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'Abp.ZeroCore, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Load from: 'C:\Users\LENOVO\.nuget\packages\abp.zerocore\3.3.0\lib\netstandard2.0\Abp.ZeroCore.dll'
------------------
Resolve: 'IdentityServer4, Version=2.0.4.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'IdentityServer4, Version=2.0.4.0, Culture=neutral, PublicKeyToken=null'
Load from: 'C:\Users\LENOVO\.nuget\packages\identityserver4\2.0.4\lib\netstandard2.0\IdentityServer4.dll'
------------------
Resolve: 'Microsoft.Extensions.Logging.Abstractions, Version=2.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Found single assembly: 'Microsoft.Extensions.Logging.Abstractions, Version=2.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.extensions.logging.abstractions\2.0.0\lib\netstandard2.0\Microsoft.Extensions.Logging.Abstractions.dll'
------------------
Resolve: 'Abp, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'Abp, Version=3.4.0.0, Culture=neutral, PublicKeyToken=null'
WARN: Version mismatch. Expected: '3.3.0.0', Got: '3.4.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\abp\3.4.0\lib\netstandard2.0\Abp.dll'
------------------
Resolve: 'IdentityServer4.AspNetIdentity, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'IdentityServer4.AspNetIdentity, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null'
Load from: 'C:\Users\LENOVO\.nuget\packages\identityserver4.aspnetidentity\2.0.0\lib\netstandard2.0\IdentityServer4.AspNetIdentity.dll'
------------------
Resolve: 'Microsoft.Extensions.Identity.Core, Version=2.0.1.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Found single assembly: 'Microsoft.Extensions.Identity.Core, Version=2.0.1.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.extensions.identity.core\2.0.1\lib\netstandard2.0\Microsoft.Extensions.Identity.Core.dll'
------------------
Resolve: 'Microsoft.AspNetCore.Identity, Version=2.0.1.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Found single assembly: 'Microsoft.AspNetCore.Identity, Version=2.0.1.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.aspnetcore.identity\2.0.1\lib\netstandard2.0\Microsoft.AspNetCore.Identity.dll'
------------------
Resolve: 'Abp.AutoMapper, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'Abp.AutoMapper, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Load from: 'C:\Users\LENOVO\.nuget\packages\abp.automapper\3.3.0\lib\netstandard2.0\Abp.AutoMapper.dll'
------------------
Resolve: 'AutoMapper, Version=6.2.1.0, Culture=neutral, PublicKeyToken=be96cd2c38ef1005'
Found single assembly: 'AutoMapper, Version=6.2.1.0, Culture=neutral, PublicKeyToken=be96cd2c38ef1005'
Load from: 'C:\Users\LENOVO\.nuget\packages\automapper\6.2.1\lib\netstandard1.3\AutoMapper.dll'
------------------
Resolve: 'System.ComponentModel.Annotations, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel.Annotations, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.Annotations.dll'
------------------
Resolve: 'Microsoft.Extensions.DependencyInjection.Abstractions, Version=2.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Found single assembly: 'Microsoft.Extensions.DependencyInjection.Abstractions, Version=2.0.0.0, Culture=neutral, PublicKeyToken=adb9793829ddae60'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.extensions.dependencyinjection.abstractions\2.0.0\lib\netstandard2.0\Microsoft.Extensions.DependencyInjection.Abstractions.dll'
------------------
Resolve: 'System.IdentityModel.Tokens.Jwt, Version=5.1.5.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'
Found single assembly: 'System.IdentityModel.Tokens.Jwt, Version=5.1.5.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'
Load from: 'C:\Users\LENOVO\.nuget\packages\system.identitymodel.tokens.jwt\5.1.5\lib\netstandard1.4\System.IdentityModel.Tokens.Jwt.dll'
------------------
Resolve: 'Abp.Zero.Common, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Found single assembly: 'Abp.Zero.Common, Version=3.3.0.0, Culture=neutral, PublicKeyToken=null'
Load from: 'C:\Users\LENOVO\.nuget\packages\abp.zero.common\3.3.0\lib\netstandard2.0\Abp.Zero.Common.dll'
------------------
Resolve: 'System.Runtime, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.dll'
------------------
Resolve: 'System.IO.MemoryMappedFiles, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.MemoryMappedFiles, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.MemoryMappedFiles.dll'
------------------
Resolve: 'System.IO.Pipes, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.Pipes, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.Pipes.dll'
------------------
Resolve: 'System.Diagnostics.Process, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.Process, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.Process.dll'
------------------
Resolve: 'System.Security.Cryptography.X509Certificates, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Cryptography.X509Certificates, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Cryptography.X509Certificates.dll'
------------------
Resolve: 'System.Runtime.Extensions, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Extensions, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Extensions.dll'
------------------
Resolve: 'System.Diagnostics.Tools, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.Tools, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.Tools.dll'
------------------
Resolve: 'System.Collections, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Collections, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Collections.dll'
------------------
Resolve: 'System.Collections.NonGeneric, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Collections.NonGeneric, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Collections.NonGeneric.dll'
------------------
Resolve: 'System.Collections.Concurrent, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Collections.Concurrent, Version=4.0.14.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.14.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Collections.Concurrent.dll'
------------------
Resolve: 'System.ObjectModel, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ObjectModel, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ObjectModel.dll'
------------------
Resolve: 'System.Collections.Specialized, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Collections.Specialized, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Collections.Specialized.dll'
------------------
Resolve: 'System.ComponentModel.TypeConverter, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel.TypeConverter, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.TypeConverter.dll'
------------------
Resolve: 'System.ComponentModel.EventBasedAsync, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel.EventBasedAsync, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.EventBasedAsync.dll'
------------------
Resolve: 'System.ComponentModel.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.Primitives.dll'
------------------
Resolve: 'System.ComponentModel, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.dll'
------------------
Resolve: 'Microsoft.Win32.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'Microsoft.Win32.Primitives, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\Microsoft.Win32.Primitives.dll'
------------------
Resolve: 'System.Console, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Console, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Console.dll'
------------------
Resolve: 'System.Data.Common, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Data.Common, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Data.Common.dll'
------------------
Resolve: 'System.Runtime.InteropServices, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.InteropServices, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.InteropServices.dll'
------------------
Resolve: 'System.Diagnostics.TraceSource, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.TraceSource, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.TraceSource.dll'
------------------
Resolve: 'System.Diagnostics.Contracts, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.Contracts, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.Contracts.dll'
------------------
Resolve: 'System.Diagnostics.Debug, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.Debug, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.Debug.dll'
------------------
Resolve: 'System.Diagnostics.TextWriterTraceListener, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.TextWriterTraceListener, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.TextWriterTraceListener.dll'
------------------
Resolve: 'System.Diagnostics.FileVersionInfo, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.FileVersionInfo, Version=4.0.2.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.2.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.FileVersionInfo.dll'
------------------
Resolve: 'System.Diagnostics.StackTrace, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.StackTrace, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.StackTrace.dll'
------------------
Resolve: 'System.Diagnostics.Tracing, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Diagnostics.Tracing, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Diagnostics.Tracing.dll'
------------------
Resolve: 'System.Drawing.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Drawing.Primitives, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Drawing.Primitives.dll'
------------------
Resolve: 'System.Linq.Expressions, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Linq.Expressions, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Linq.Expressions.dll'
------------------
Resolve: 'System.IO.Compression, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
Found single assembly: 'System.IO.Compression, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.Compression.dll'
------------------
Resolve: 'System.IO.Compression.ZipFile, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
Found single assembly: 'System.IO.Compression.ZipFile, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.Compression.ZipFile.dll'
------------------
Resolve: 'System.IO.FileSystem, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.FileSystem, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.FileSystem.dll'
------------------
Resolve: 'System.IO.FileSystem.DriveInfo, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.FileSystem.DriveInfo, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.FileSystem.DriveInfo.dll'
------------------
Resolve: 'System.IO.FileSystem.Watcher, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.FileSystem.Watcher, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.FileSystem.Watcher.dll'
------------------
Resolve: 'System.IO.IsolatedStorage, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.IO.IsolatedStorage, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.IO.IsolatedStorage.dll'
------------------
Resolve: 'System.Linq, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Linq, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Linq.dll'
------------------
Resolve: 'System.Linq.Queryable, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Linq.Queryable, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Linq.Queryable.dll'
------------------
Resolve: 'System.Linq.Parallel, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Linq.Parallel, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Linq.Parallel.dll'
------------------
Resolve: 'System.Threading.Thread, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.Thread, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.Thread.dll'
------------------
Resolve: 'System.Net.Requests, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Requests, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Requests.dll'
------------------
Resolve: 'System.Net.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Primitives, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Primitives.dll'
------------------
Resolve: 'System.Net.HttpListener, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Net.HttpListener, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.HttpListener.dll'
------------------
Resolve: 'System.Net.ServicePoint, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Net.ServicePoint, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.ServicePoint.dll'
------------------
Resolve: 'System.Net.NameResolution, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.NameResolution, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.NameResolution.dll'
------------------
Resolve: 'System.Net.WebClient, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Net.WebClient, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebClient.dll'
------------------
Resolve: 'System.Net.Http, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Http, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Http.dll'
------------------
Resolve: 'System.Net.WebHeaderCollection, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.WebHeaderCollection, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebHeaderCollection.dll'
------------------
Resolve: 'System.Net.WebProxy, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Net.WebProxy, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebProxy.dll'
------------------
Resolve: 'System.Net.Mail, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Net.Mail, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Mail.dll'
------------------
Resolve: 'System.Net.NetworkInformation, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.NetworkInformation, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.NetworkInformation.dll'
------------------
Resolve: 'System.Net.Ping, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Ping, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Ping.dll'
------------------
Resolve: 'System.Net.Security, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Security, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Security.dll'
------------------
Resolve: 'System.Net.Sockets, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.Sockets, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.Sockets.dll'
------------------
Resolve: 'System.Net.WebSockets.Client, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.WebSockets.Client, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebSockets.Client.dll'
------------------
Resolve: 'System.Net.WebSockets, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.WebSockets, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebSockets.dll'
------------------
Resolve: 'System.Runtime.Numerics, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Numerics, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Numerics.dll'
------------------
Resolve: 'System.Threading.Tasks, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.Tasks, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.Tasks.dll'
------------------
Resolve: 'System.Reflection.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Reflection.Primitives, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Reflection.Primitives.dll'
------------------
Resolve: 'System.Resources.ResourceManager, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Resources.ResourceManager, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Resources.ResourceManager.dll'
------------------
Resolve: 'System.Resources.Writer, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Resources.Writer, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Resources.Writer.dll'
------------------
Resolve: 'System.Runtime.CompilerServices.VisualC, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.CompilerServices.VisualC, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.CompilerServices.VisualC.dll'
------------------
Resolve: 'System.Runtime.InteropServices.RuntimeInformation, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.InteropServices.RuntimeInformation, Version=4.0.2.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.2.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.InteropServices.RuntimeInformation.dll'
------------------
Resolve: 'System.Runtime.Serialization.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Serialization.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Serialization.Primitives.dll'
------------------
Resolve: 'System.Runtime.Serialization.Xml, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Serialization.Xml, Version=4.1.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Serialization.Xml.dll'
------------------
Resolve: 'System.Runtime.Serialization.Json, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Serialization.Json, Version=4.0.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.4.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Serialization.Json.dll'
------------------
Resolve: 'System.Runtime.Serialization.Formatters, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Serialization.Formatters, Version=4.0.2.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.2.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Serialization.Formatters.dll'
------------------
Resolve: 'System.Security.Claims, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Claims, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Claims.dll'
------------------
Resolve: 'System.Security.Cryptography.Algorithms, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Cryptography.Algorithms, Version=4.3.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.3.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Cryptography.Algorithms.dll'
------------------
Resolve: 'System.Security.Cryptography.Csp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Cryptography.Csp, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Cryptography.Csp.dll'
------------------
Resolve: 'System.Security.Cryptography.Encoding, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Cryptography.Encoding, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Cryptography.Encoding.dll'
------------------
Resolve: 'System.Security.Cryptography.Primitives, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Cryptography.Primitives, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Cryptography.Primitives.dll'
------------------
Resolve: 'System.Security.Principal, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Security.Principal, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Security.Principal.dll'
------------------
Resolve: 'System.Text.Encoding.Extensions, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Text.Encoding.Extensions, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Text.Encoding.Extensions.dll'
------------------
Resolve: 'System.Text.RegularExpressions, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Text.RegularExpressions, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Text.RegularExpressions.dll'
------------------
Resolve: 'System.Threading, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.dll'
------------------
Resolve: 'System.Threading.Overlapped, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.Overlapped, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.Overlapped.dll'
------------------
Resolve: 'System.Threading.ThreadPool, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.ThreadPool, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.ThreadPool.dll'
------------------
Resolve: 'System.Threading.Tasks.Parallel, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.Tasks.Parallel, Version=4.0.3.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.3.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.Tasks.Parallel.dll'
------------------
Resolve: 'System.Threading.Timer, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Threading.Timer, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Threading.Timer.dll'
------------------
Resolve: 'System.Transactions.Local, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Transactions.Local, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Transactions.Local.dll'
------------------
Resolve: 'System.Web.HttpUtility, Version=0.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
Found single assembly: 'System.Web.HttpUtility, Version=4.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.0.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Web.HttpUtility.dll'
------------------
Resolve: 'System.Xml.ReaderWriter, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.ReaderWriter, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.2.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.ReaderWriter.dll'
------------------
Resolve: 'System.Xml.XDocument, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.XDocument, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.XDocument.dll'
------------------
Resolve: 'System.Xml.XmlSerializer, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.XmlSerializer, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.XmlSerializer.dll'
------------------
Resolve: 'System.Xml.XPath.XDocument, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.XPath.XDocument, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.XPath.XDocument.dll'
------------------
Resolve: 'System.Xml.XPath, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.XPath, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
WARN: Version mismatch. Expected: '0.0.0.0', Got: '4.1.0.0'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.XPath.dll'
------------------
Resolve: 'System.Runtime, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.dll'
------------------
Resolve: 'System.Runtime.Extensions, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Extensions, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Extensions.dll'
------------------
Resolve: 'System.ComponentModel.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.ComponentModel.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.ComponentModel.Primitives.dll'
------------------
Resolve: 'System.Net.WebHeaderCollection, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Net.WebHeaderCollection, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Net.WebHeaderCollection.dll'
------------------
Resolve: 'System.Runtime.Serialization.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Runtime.Serialization.Primitives, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Runtime.Serialization.Primitives.dll'
------------------
Resolve: 'System.Xml.ReaderWriter, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Found single assembly: 'System.Xml.ReaderWriter, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'
Load from: 'C:\Users\LENOVO\.nuget\packages\microsoft.netcore.app\2.0.0\ref\netcoreapp2.0\System.Xml.ReaderWriter.dll'
#endif