﻿//using Abp.IdentityServer4;
namespace BeBillingSystem.EntityFrameworkCore
{
	public enum PaymentPeriodType
	{
		Monthly = 30,
        Annual = 365
	}
}