﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_PaymentAutoDebet")]
	public class TR_PaymentAutoDebet : AuditedEntity
	{
		[Required]
		[StringLength(100)]
		public string ADBKey { get; set; }

		[Required]
		[StringLength(11)]
		public string accountNo { get; set; }

		[Required]
		[StringLength(20)]
		public string custName { get; set; }

		public DateTime transDate { get; set; }

		[Required]
		[StringLength(3)]
		public string curr { get; set; }

		[Column(TypeName = "money")]
		public decimal amount { get; set; }

		[ForeignKey("TR_BookingHeader")]
		public int? bookingHeaderID { get; set; }
		public virtual TR_BookingHeader TR_BookingHeader { get; set; }
	}
}