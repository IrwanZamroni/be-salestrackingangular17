﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_PPOrderPPNo")]
	public class TR_PPOrderPPNo : AuditedEntity
	{
		[Required]
		[StringLength(6)]
		public string PPNo { get; set; }

		[ForeignKey("TR_PPOrder")]
		public int PPOrderID { get; set; }
		//public virtual TR_PPOrder TR_PPOrder { get; set; }

		[ForeignKey("MS_BatchEntry")]
		public int batchID { get; set; }
		public virtual MS_BatchEntry MS_BatchEntry { get; set; }
	}
}