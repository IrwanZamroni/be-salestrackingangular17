﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_ProjectPPOnline")]
	public class MS_ProjectPPOnline : AuditedEntity
	{
		[ForeignKey("LK_CategoryType")]
		public int categoryID { get; set; }
		//public virtual LK_CategoryType LK_CategoryType { get; set; }

		[StringLength(200)]
		public string logo { get; set; }

		[StringLength(200)]
		public string banner { get; set; }

		[StringLength(200)]
		public string termAndCondition { get; set; }

		public DateTime? startDate { get; set; }

		public DateTime? endDate { get; set; }

		public bool? isBuyPPSales { get; set; }

		public bool? isBuyPPCust { get; set; }

		[ForeignKey("MS_ProjectInfo")]
		public int projectInfoID { get; set; }
		//public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

		//public ICollection<MS_CategoryTypePreferred> MS_CategoryTypePreferred { get; set; }

		public ICollection<TR_PriorityPass> TR_PriorityPass { get; set; }
	}
}