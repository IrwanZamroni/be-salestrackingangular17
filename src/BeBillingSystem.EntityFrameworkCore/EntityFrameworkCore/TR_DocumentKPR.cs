﻿using Castle.MicroKernel.SubSystems.Conversion;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class TR_DocumentKPR
	{
		//[ForeignKey("TR_BookingHeader")]
		public int? bookingHeaderID { get; set; }
		//public virtual TR_BookingHeader TR_BookingHeader { get; set; }

		[Required]
		[StringLength(8)]
		public string psCode { get; set; }
		public int? PriorityPassId { get; set; }
		[Required]
		[StringLength(30)]
		public string documentType { get; set; }

		[StringLength(10)]
		public string documentPicType { get; set; }

		public int? documentRef { get; set; }

		[Column(TypeName = "image")]
		public byte[] documentBinary { get; set; }

		public bool isVerified { get; set; }
	}
}