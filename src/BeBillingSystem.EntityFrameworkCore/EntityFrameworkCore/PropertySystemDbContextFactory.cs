﻿using BeBillingSystem.Configuration;
using BeBillingSystem.Web;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace BeBillingSystem.EntityFrameworkCore
{
    public class PropertySystemDbContextFactory : IDesignTimeDbContextFactory<PropertySystemDbContext>
    {

        public PropertySystemDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PropertySystemDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            PropertySystemDbContextConfigurer.Configure(builder, configuration.GetConnectionString(BeBillingSystemConsts.PropertySystemDbContext));

            return new PropertySystemDbContext(builder.Options);
        }
    }
}
