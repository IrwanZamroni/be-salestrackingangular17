﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_BatchEntry")]
	public class MS_BatchEntry : AuditedEntity
	{
		public int batchSeq { get; set; }

		[StringLength(3)]
		[Required]
		public string batchCode { get; set; }

		public int batchStartNum { get; set; }

		public int? batchMaxNum { get; set; }

		public int maxTopupFromOldBatch { get; set; }

		public bool isTopupOnly { get; set; }

		public bool? isBookingFee { get; set; }

		public bool? isConvertOnly { get; set; }

		public bool? isSellOnly { get; set; }

		public bool isActive { get; set; }

		public bool? isRunOut { get; set; }

		public double ppAmt { get; set; }

		public bool? priorityLine { get; set; }

		public string name { get; set; }

		[ForeignKey("MS_ProjectInfo")]
		public int projectInfoID { get; set; }
		public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

		public virtual ICollection<TR_PriorityPass> TR_PriorityPass { get; set; }

		public virtual ICollection<TR_PPOrderPPNo> TR_PPOrderPPNo { get; set; }
	}
}