﻿//using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.Migrations;
using BeBillingSystem.MultiTenancy;
using Microsoft.EntityFrameworkCore;

using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
//using BeBillingSystem.Authorization.Chat;
using BeBillingSystem.Editions;
//using BeBillingSystem.Authorization.Friendships;
//using BeBillingSystem.Authorization.MultiTenancy;
//using BeBillingSystem.Authorization.Accounting;
//using BeBillingSystem.Authorization.Payments;
//using BeBillingSystem.Authorization.OnlineBooking.DemoDB;
using BeBillingSystem.Authorization;

namespace BeBillingSystem.EntityFrameworkCore
{
	//public class DemoDbContext : AbpZeroDbContext<Tenant, Role, User, DemoDbContext>
	//{
	//	/* Define an IDbSet for each entity of the application */

	//	public virtual DbSet<MS_Users> MS_Users { get; set; }
	//	public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

	//	public virtual DbSet<Friendship> Friendships { get; set; }

	//	public virtual DbSet<ChatMessage> ChatMessages { get; set; }

	//	public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

	//	public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

	//	public virtual DbSet<Invoice> Invoices { get; set; }

	//	public DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

	//	public virtual DbSet<MS_Application> MS_Application { get; set; }

	//	public virtual DbSet<MP_UserPersonals> MP_UserPersonals { get; set; }

	//	public virtual DbSet<MS_AD_Auth> MS_AD_Auth { get; set; }

	//	public virtual DbSet<MS_UserRegBy> MS_UserRegBy { get; set; }

	//	public DemoDbContext(DbContextOptions<DemoDbContext> options)
	//		: base(options)
	//	{

	//	}

	//	protected override void OnModelCreating(ModelBuilder modelBuilder)
	//	{
	//		base.OnModelCreating(modelBuilder);
	//		modelBuilder.ChangeAbpTablePrefix<Tenant, Role, User>("");

	//		modelBuilder.Entity<MS_Users>(b =>
	//		{
	//			b.HasIndex(e => new { e.Id });
	//		});

	//		modelBuilder.Entity<BinaryObject>(b =>
	//		{
	//			b.HasIndex(e => new { e.TenantId });
	//		});

	//		modelBuilder.Entity<ChatMessage>(b =>
	//		{
	//			b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
	//			b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
	//			b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
	//			b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
	//		});

	//		modelBuilder.Entity<Friendship>(b =>
	//		{
	//			b.HasIndex(e => new { e.TenantId, e.UserId });
	//			b.HasIndex(e => new { e.TenantId, e.FriendUserId });
	//			b.HasIndex(e => new { e.FriendTenantId, e.UserId });
	//			b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
	//		});

	//		modelBuilder.Entity<Tenant>(b =>
	//		{
	//			b.HasIndex(e => new { e.SubscriptionEndDateUtc });
	//			b.HasIndex(e => new { e.CreationTime });
	//		});

	//		modelBuilder.Entity<SubscriptionPayment>(b =>
	//		{
	//			b.HasIndex(e => new { e.Status, e.CreationTime });
	//			b.HasIndex(e => new { e.PaymentId, e.Gateway });
	//		});

	//		//modelBuilder.ConfigurePersistedGrantEntity();
	//	}
	//}
}
