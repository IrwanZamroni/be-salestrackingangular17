using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace BeBillingSystem.EntityFrameworkCore
{
    public static class BeBillingSystemDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<BeBillingSystemDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<BeBillingSystemDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
     
    }
}
