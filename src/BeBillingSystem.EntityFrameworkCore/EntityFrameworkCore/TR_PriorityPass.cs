﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.BillingSystem;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_PriorityPass")]
	public class TR_PriorityPass : AuditedEntity
	{
		[ForeignKey("LK_PPStatus")]
		public int ppStatusID { get; set; }
		public virtual LK_PPStatus LK_PPStatus { get; set; }

		[Required]
		[StringLength(6)]
		public string PPNo { get; set; }

		[ForeignKey("MS_BatchEntry")]
		public int batchID { get; set; }
		public virtual MS_BatchEntry MS_BatchEntry { get; set; }

		public int batchSeq { get; set; }

		public DateTime buyDate { get; set; }

		[StringLength(30)]
		public string cardNo { get; set; }

		public DateTime? dealingTime { get; set; }

		[StringLength(200)]
		public string idCard { get; set; }

		public int? idSetting { get; set; }

		[StringLength(50)]
		public string kamar { get; set; }

		[StringLength(50)]
		public string lantai { get; set; }

		[Required]
		[StringLength(12)]
		public string memberCode { get; set; }

		[StringLength(6)]
		[Required]
		public string oldPPNo { get; set; }

		[ForeignKey("LK_PaymentType")]
		public int paymentTypeID { get; set; }
		public virtual LK_PaymentType LK_PaymentType { get; set; }

		[Required]
		[StringLength(8)]
		public string psCode { get; set; }

		public DateTime? regTime { get; set; }

		[Required]
		[StringLength(3)]
		public string scmCode { get; set; }

		public int? tableNo { get; set; }


		public int? token { get; set; }

		[StringLength(300)]
		public string remarks { get; set; }

		[ForeignKey("MS_ProjectPPOnline")]
		public int? projectPPOnlineID { get; set; }
		public virtual MS_ProjectPPOnline MS_ProjectPPOnline { get; set; }
	}
}