﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class TR_ProjectSocialMedia : AuditedEntity
	{
		[ForeignKey("MS_ProjectSocialMedia")]
		public int sosialMediaID { get; set; }
		public virtual MS_ProjectSocialMedia MS_ProjectSocialMedia { get; set; }

		[Required]
		[StringLength(100)]
		public string socialMediaLink { get; set; }

		[ForeignKey("MS_ProjectInfo")]
		public int projectInfoID { get; set; }
		public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

		public bool? isActive { get; set; }
	}
}