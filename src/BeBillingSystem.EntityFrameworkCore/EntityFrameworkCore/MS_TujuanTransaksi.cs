﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_TujuanTransaksi : AuditedEntity
	{
		public int entityID { get; set; }

		[Required]
		[StringLength(3)]
		public string tujuanTransaksiCode { get; set; }

		[Required]
		[StringLength(50)]
		public string tujuanTransaksiName { get; set; }

		public int sort { get; set; }

		public virtual ICollection<TR_BookingHeader> TR_BookingHeader { get; set; }

		//public virtual ICollection<TR_UnitOrderHeader> TR_UnitOrderHeader { get; set; }

	}
}