﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_BookingDocument")]
	public class TR_BookingDocument : AuditedEntity
	{
		public int entityID { get; set; }

		[ForeignKey("TR_BookingHeader")]
		public int bookingHeaderID { get; set; }
		public virtual TR_BookingHeader TR_BookingHeader { get; set; }

		//[Required]
		//[StringLength(20)]
		//public string bookCode { get; set; }

		[ForeignKey("MS_DocumentPS")]
		public int docID { get; set; }
		/// <summary>
		/// public virtual MS_DocumentPS MS_DocumentPS { get; set; }
		/// </summary>

		//[Required]
		//[StringLength(5)]
		//public string docCode { get; set; }

		[Required]
		[StringLength(50)]
		public string docNo { get; set; }

		public DateTime docDate { get; set; }

		[Required]
		[StringLength(255)]
		public string remarks { get; set; }

		[StringLength(50)]
		public string oldDocNo { get; set; }

		[StringLength(50)]
		public string tandaTerimaNo { get; set; }

		public DateTime? tandaTerimaDate { get; set; }

		[StringLength(150)]
		public string tandaTerimaFile { get; set; }

		public DateTime? signatureDate { get; set; }

		public DateTime? signedDocumentDate { get; set; }

		public string signedDocumentFile { get; set; }

		public bool? isGenerate { get; set; }

		public string kuasaDireksi { get; set; }

		public string procuration { get; set; }
		public bool? isSentPrivy { get; set; }
		public string docToken { get; set; }

		public DateTime? invitationDate { get; set; }

		public string invitationTime { get; set; }

		public string picName { get; set; }

		public string picPhone { get; set; }
		public string picEmail { get; set; }
		public string lampiranUAJB { get; set; }
		public string simulasiBiayaAJBpdf { get; set; }
		public string simulasiBiayaAJBexcel { get; set; }
		public string lampiranUndPPPU { get; set; }
		public string lampiranSP1PPPU { get; set; }
		public string lampiranSP2PPPU { get; set; }
		public string lampiranSP3PPPU { get; set; }
		//public ICollection<TR_Addendum> TR_Addendum { get; set; }
	}
}