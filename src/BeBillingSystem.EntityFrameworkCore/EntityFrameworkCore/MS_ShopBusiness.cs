﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_ShopBusiness")]
	public class MS_ShopBusiness : AuditedEntity
	{
		public int entityID { get; set; }

		[Required]
		[StringLength(3)]
		public string shopBusinessCode { get; set; }

		[Required]
		[StringLength(50)]
		public string shopBusinessName { get; set; }

		public int sort { get; set; }

		public virtual ICollection<TR_BookingHeader> TR_BookingHeader { get; set; }
	}
}