﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.MultiTenancy;
using BeBillingSystem.BillingSystem;
using System;

namespace BeBillingSystem.EntityFrameworkCore
{
    public class BeBillingSystemDbContext : AbpZeroDbContext<Tenant, Role, User, BeBillingSystemDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public virtual DbSet<TR_AdjustmentDetail> TR_AdjustmentDetail { get; set; }
        public virtual DbSet<MS_TypeEmail> MS_TypeEmail { get; set; }
        public virtual DbSet<TR_ElectricReading> TR_ElectricReading { get; set; }
        public virtual DbSet<MS_OracleMapping> MS_OracleMapping { get; set; }
        public virtual DbSet<MS_Site> MS_Site { get; set; }
        public virtual DbSet<MP_ClusterSite> MP_ClusterSite { get; set; }
        public virtual DbSet<MP_ProjectSite> MP_ProjectSite { get; set; }
        public virtual DbSet<MP_UserSite> MP_UserSite { get; set; }
        public virtual DbSet<MS_ExchangeRate> MS_ExchangeRate { get; set; }
        public virtual DbSet<MS_Period> MS_Period { get; set; }
        public virtual DbSet<MS_UnitStatus> MS_UnitStatus { get; set; }
        public virtual DbSet<MS_UnitData> MS_UnitData { get; set; }
        public virtual DbSet<MS_UnitItemHeader> MS_UnitItemHeader { get; set; }
        public virtual DbSet<MS_UnitItemDetail> MS_UnitItemDetail { get; set; }
        public virtual DbSet<MS_Item> MS_Item { get; set; }
        public virtual DbSet<MS_ItemRate> MS_ItemRate { get; set; }
       
        public virtual DbSet<MS_TemplateInvoiceHeader> MS_TemplateInvoiceHeader { get; set; }
        public virtual DbSet<MS_TemplateInvoiceDetail> MS_TemplateInvoiceDetail { get; set; }
        public virtual DbSet<MS_PPH> MS_PPH { get; set; }
        
        public virtual DbSet<MS_Attribute> MS_Attribute { get; set; }
        public virtual DbSet<MS_Aging> MS_Aging { get; set; }
        public virtual DbSet<MS_CompanyCode> MS_CompanyCode { get; set; }
        public virtual DbSet<MS_Abodemen> MS_Abodemen { get; set; }
        public virtual DbSet<TR_WaterReading> TR_WaterReading { get; set; }
        public virtual DbSet<TR_WarningLetter> TR_WarningLetter { get; set; }
        public virtual DbSet<TR_InvoiceHeader> TR_InvoiceHeader { get; set; }
        public virtual DbSet<TR_InvoiceDetail> TR_InvoiceDetail { get; set; }
        public virtual DbSet<TR_RunningNumber> TR_RunningNumber { get; set; }
        public virtual DbSet<TR_BillingPaymentHeader> TR_BillingPaymentHeader { get; set; }
        public virtual DbSet<TR_BillingPaymentDetail> TR_BillingPaymentDetail { get; set; }
        public virtual DbSet<MS_SettingEmail> MS_SettingEmail { get; set; }
        public virtual DbSet<MS_PaymentType> MS_PaymentType { get; set; }
        public virtual DbSet<MS_PaymentMethod> MS_PaymentMethod { get; set; }
        public virtual DbSet<MS_Bank> MS_Bank { get; set; }
        public virtual DbSet<TR_EmailOutbox> TR_EmailOutbox { get; set; }
        public virtual DbSet<MS_SP> MS_SP { get; set; }
        public virtual DbSet<TR_JournalHeader> TR_JournalHeader { get; set; }
        public virtual DbSet<TR_JournalDetail> TR_JournalDetail { get; set; }
        public virtual DbSet<MS_Parameter> MS_Parameter { get; set; }

       

        public BeBillingSystemDbContext(DbContextOptions<BeBillingSystemDbContext> options)
            : base(options)
        {

        }
    }

  
}
