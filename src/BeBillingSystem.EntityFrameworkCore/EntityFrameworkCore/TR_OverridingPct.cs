﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_OverridingPct")]
	public class TR_OverridingPct : AuditedEntity<string>
	{
		[NotMapped]
		public override string Id
		{
			get
			{
				return entityCode +
					"-" + bookNo +
					"-" + statusCode +
					"-" + asUplineNo;
			}
			set { /* nothing */ }
		}
		[Key]
		[Column(Order = 0)]
		[StringLength(1)]
		public string entityCode { get; set; }

		[Key]
		[Column(Order = 1)]
		[StringLength(20)]
		public string bookNo { get; set; }

		[Key]
		[Column(Order = 2)]
		[StringLength(5)]
		public string statusCode { get; set; }

		[Key]
		[Column(Order = 3)]
		public byte asUplineNo { get; set; }

		public double commPctPaid { get; set; }


		[StringLength(20)]
		public string memberCode { get; set; }

		[Required]
		[StringLength(3)]
		public string commTypeCode { get; set; }

		[Column(TypeName = "money")]
		public decimal unitPrice { get; set; }

		[Required]
		[StringLength(5)]
		public string devCode { get; set; }

		public double? commPctHold { get; set; }

		[Column(TypeName = "money")]
		public decimal nominal { get; set; }

		public bool? isHold { get; set; }

		public bool calculateUseMaster { get; set; }

		[StringLength(20)]
		public string newMemberCode { get; set; }

		public double? newCommPctPaid { get; set; }

		[Column(TypeName = "money")]
		public decimal? pointExisting { get; set; }

		[Column("modifTime")]
		public override DateTime? LastModificationTime { get; set; }

		[Column("modifUN")]
		public override long? LastModifierUserId { get; set; }

		[Column("inputTime")]
		public override DateTime CreationTime { get; set; }

		[Column("inputUN")]
		public override long? CreatorUserId { get; set; }
	}
}