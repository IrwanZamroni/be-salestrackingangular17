﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using BeBillingSystem.Authorization.Roles;
using BeBillingSystem.Authorization.Users;
using BeBillingSystem.BillingSystem;
using BeBillingSystem.Migrations;
using BeBillingSystem.MultiTenancy;
using BeBillingSystem.PropertySystemDB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutoMapper.Internal.ExpressionFactory;
using static System.Net.Mime.MediaTypeNames;

namespace BeBillingSystem.EntityFrameworkCore
{

  
    public class PropertySystemDbContext :DbContext
    {
        public virtual DbSet<MS_Project> MS_Project { get; set; }
        public virtual DbSet<MS_Cluster> MS_Cluster { get; set; }
        public virtual DbSet<MS_UnitCode> MS_UnitCode { get; set; }
        public virtual DbSet<MS_Unit> MS_Unit { get; set; }
		public virtual DbSet<TR_BookingHeader> TR_BookingHeader { get; set; }
		public virtual DbSet<TR_PriorityPass> TR_PriorityPass { get; set; }
		public virtual DbSet<TR_DocumentKPR> TR_DocumentKPR { get; set; }
		public virtual DbSet<MS_BatchEntry> MS_BatchEntry { get; set; }
		public virtual DbSet<MS_ProjectInfo> MS_ProjectInfo { get; set; }
		public virtual DbSet<MS_ProjectOLBooking> MS_ProjectOLBooking { get; set; }
		public virtual DbSet<SYS_RolesProject> SYS_RolesProject { get; set; }


		public PropertySystemDbContext(DbContextOptions<PropertySystemDbContext> options)
            : base(options)
        {
        }
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//modelBuilder.Entity<TR_DocumentKPR>()
			//   .HasKey(c => new { c.Id });
			//modelBuilder.Entity<TR_DocumentKPR>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<TR_HistoryViewDocument>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<MS_Surrounding>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<MS_AssetDevIlustration>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<TR_HistoryRefund>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<TR_HistoryRefundDetail>()
			//.HasKey(c => new { c.Id });

			//modelBuilder.Entity<MS_SK>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<TR_HistoryStatusHandovers>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<TR_HistoryStatusCollections>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<MS_StatusCollection>()
			//	.HasKey(c => new { c.Id });

			//modelBuilder.Entity<MS_StatusHandover>()
			//	.HasKey(c => new { c.Id });

			////>>add by nanda1211 - 12 april 2022
			//modelBuilder.Entity<MS_MappingNotarisBankEmail>()
			//	.HasKey(c => new { c.mappingNotarisBankEmailID });

			//modelBuilder.Entity<MS_MappingNotarisBankPhone>()
			//	.HasKey(c => new { c.mappingNotarisBankPhoneID });

			//modelBuilder.Entity<MS_MappingNotarisBank>()
			//	.HasKey(c => c.mappingNotarisBankID);

			//modelBuilder.Entity<MS_NotarisStaffEmail>()
			//	.HasKey(c => new { c.notarisStaffEmailID });

			//modelBuilder.Entity<MS_NotarisStaffPhone>()
			//	.HasKey(c => new { c.notarisStaffPhoneID });

			//modelBuilder.Entity<MS_NotarisStaff>()
			//	.HasKey(c => c.notarisStaffID);

			//modelBuilder.Entity<MS_NotarisEmail>()
			//	.HasKey(c => new { c.notarisEmailID });

			//modelBuilder.Entity<MS_NotarisPhone>()
			//	.HasKey(c => new { c.notarisPhoneID });

			//modelBuilder.Entity<MS_NotarisCluster>()
			//	.HasKey(c => new { c.notarisClusterID });

			//modelBuilder.Entity<MS_NotarisNew>()
			//	.HasKey(c => c.notarisID);

			//modelBuilder.Entity<TR_PicAJB>()
			//	.HasKey(c => c.Id);
			//<<


			//modelBuilder.Entity<TR_SSPDetailPayment>()
			//   .HasKey(c => new { c.CreationTime });

			//ON DELETE CASCADE
			//modelBuilder.Entity<TR_BookingHeader>()
			//  .HasOne(c => c.MS_Unit)
			//  .WithMany(m => m.TR_BookingHeader)
			//  .HasForeignKey(u => u.unitID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_BookingHeaderInfo>()
			//  .HasOne(c => c.MS_Bank)
			//  .WithMany(m => m.TR_BookingHeaderInfo)
			//  .HasForeignKey(u => u.bankID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SPB>()
			//  .HasOne(c => c.MS_Unit)
			//  .WithMany(m => m.TR_SPB)
			//  .HasForeignKey(u => u.unitID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_InventoryHeader>()
			//  .HasOne(c => c.MS_Account)
			//  .WithMany(m => m.TR_InventoryHeader)
			//  .HasForeignKey(u => u.accID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_PaymentHeader>()
			//  .HasOne(c => c.TR_BookingHeader)
			//  .WithMany(m => m.TR_PaymentHeader)
			//  .HasForeignKey(u => u.bookingHeaderID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SSPDetail>()
			//  .HasOne(c => c.TR_BookingDetail)
			//  .WithMany(m => m.TR_SSPDetail)
			//  .HasForeignKey(u => u.bookingDetailID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_BookingHeaderTerm>()
			//  .HasOne(c => c.MS_Term)
			//  .WithMany(m => m.TR_BookingHeaderTerm)
			//  .HasForeignKey(u => u.termID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_BookingHeaderTermDP>()
			//  .HasOne(c => c.MS_Term)
			//  .WithMany(m => m.TR_BookingHeaderTermDP)
			//  .HasForeignKey(u => u.termID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_PaymentBulk>()
			//  .HasOne(c => c.MS_Unit)
			//  .WithMany(m => m.TR_PaymentBulk)
			//  .HasForeignKey(u => u.unitID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_OfferingLetter>()
			//  .HasOne(c => c.TR_BookingHeader)
			//  .WithMany(m => m.TR_OfferingLetter)
			//  .HasForeignKey(u => u.bookingID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_UpdateRetention>()
			//  .HasOne(c => c.MS_Cluster)
			//  .WithMany(m => m.TR_UpdateRetention)
			//  .HasForeignKey(u => u.clusterID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_Bilyet>()
			//  .HasOne(c => c.TR_RetentionDetail)
			//  .WithMany(m => m.TR_Bilyet)
			//  .HasForeignKey(u => u.retentionDetailID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_SPPeriod>()
			//  .HasOne(c => c.MS_Account)
			//  .WithMany(m => m.MS_SPPeriod)
			//  .HasForeignKey(u => u.accountID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_HistoryEmailBankPortal>()
			//  .HasOne(c => c.TR_OfferingLetter)
			//  .WithMany(m => m.TR_HistoryEmailBankPortal)
			//  .HasForeignKey(u => u.offeringLetterID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_HistoryEmailBankPortal>()
			//  .HasOne(c => c.MS_UserAccountBank)
			//  .WithMany(m => m.TR_HistoryEmailBankPortal)
			//  .HasForeignKey(u => u.userAccountBankID)
			//  .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_EmailBankPortal>()
			//  .HasOne(c => c.MS_Project)
			//  .WithMany(m => m.MS_EmailBankPortal)
			//  .HasForeignKey(u => u.projectID)
			//  .OnDelete(DeleteBehavior.Restrict);
			////

			//modelBuilder.Entity<MS_Account>()
			//			.HasIndex(b => b.accCode)
			//			.IsUnique()
			//			.HasName("accCodeUnique");

			//modelBuilder.Entity<MS_Bank>()
			//			.HasIndex(b => b.bankCode)
			//			.IsUnique()
			//			.HasName("bankCodeUnique");

			//modelBuilder.Entity<MS_BankBranch>()
			//			.HasIndex(b => b.bankBranchCode)
			//			.IsUnique()
			//			.HasName("bankBranchCodeUnique");

			//modelBuilder.Entity<LK_BankLevel>()
			//			.HasIndex(b => b.bankLevelCode)
			//			.IsUnique()
			//			.HasName("bankLevelCodeUnique");

			//modelBuilder.Entity<MS_Company>()
			//			.HasIndex(b => b.coCode)
			//			.IsUnique()
			//			.HasName("coCodeUnique");

			//modelBuilder.Entity<MS_Country>()
			//			.HasIndex(b => b.countryCode)
			//			.IsUnique()
			//			.HasName("countryCodeUnique");

			//modelBuilder.Entity<MS_Department>()
			//			.HasIndex(b => b.departmentCode)
			//			.IsUnique()
			//			.HasName("departmentCodeUnique");

			modelBuilder.Entity<MS_Project>()
						.HasIndex(b => b.projectCode)
						.IsUnique()
						.HasName("projectCodeUnique");

			//modelBuilder.Entity<MS_Area>()
			//			.HasIndex(b => b.areaCode)
			//			.IsUnique()
			//			.HasName("areaCodeUnique");

			//modelBuilder.Entity<LK_Facing>()
			//			.HasIndex(b => b.facingCode)
			//			.IsUnique()
			//			.HasName("facingCodeUnique");

			//modelBuilder.Entity<LK_Item>()
			//			.HasIndex(b => b.itemCode)
			//			.IsUnique()
			//			.HasName("itemCodeUnique");

			//modelBuilder.Entity<LK_UnitStatus>()
			//			.HasIndex(b => b.unitStatusCode)
			//			.IsUnique()
			//			.HasName("unitStatusCodeUnique");

			//modelBuilder.Entity<MS_Category>()
			//			.HasIndex(b => b.categoryCode)
			//			.IsUnique()
			//			.HasName("categoryCodeUnique");

			modelBuilder.Entity<MS_Cluster>()
						.HasIndex(b => b.clusterCode)
						.IsUnique()
						.HasName("clusterCodeUnique");

			//modelBuilder.Entity<MS_Product>()
			//			.HasIndex(b => b.productCode)
			//			.IsUnique()
			//			.HasName("productCodeUnique");

			//modelBuilder.Entity<MS_Zoning>()
			//			.HasIndex(b => b.zoningCode)
			//			.IsUnique()
			//			.HasName("zoningCodeUnique");

			//modelBuilder.Entity<LK_FinType>()
			//			.HasIndex(b => b.finTypeCode)
			//			.IsUnique()
			//			.HasName("finTypeCodeUnique");

			//modelBuilder.Entity<MS_FormulaCode>()
			//			.HasIndex(b => b.formulaCode)
			//			.IsUnique()
			//			.HasName("formulaCodeUnique");

			//modelBuilder.Entity<MS_Entity>()
			//			.HasIndex(b => b.entityCode)
			//			.IsUnique()
			//			.HasName("entityCodeUnique");

			modelBuilder.Entity<MS_Facade>()
						.HasIndex(b => b.facadeCode)
						.IsUnique()
						.HasName("facadeCodeUnique");

			modelBuilder.Entity<TR_BookingHeader>()
						.HasIndex(b => b.bookCode)
						.IsUnique()
						.HasName("bookCodeUnique");

			//modelBuilder.Entity<LK_RentalStatus>()
			//			.HasIndex(b => b.rentalStatusCode)
			//			.IsUnique()
			//			.HasName("rentalStatusCodeUnique");

			//modelBuilder.Entity<LK_CertCode>()
			//			.HasIndex(b => b.certCode)
			//			.IsUnique()
			//			.HasName("certCodeUnique");

			//LippoMaster
			//modelBuilder.Entity<LK_Alloc>()
			//			.HasIndex(b => b.allocCode)
			//			.IsUnique()
			//			.HasName("allocCodeUnique");

			//modelBuilder.Entity<LK_LetterStatus>()
			//			.HasIndex(b => b.letterStatusCode)
			//			.IsUnique()
			//			.HasName("letterStatusCodeUnique");

			//modelBuilder.Entity<LK_PayType>()
			//			.HasIndex(b => b.payTypeCode)
			//			.IsUnique()
			//			.HasName("payTypeCodeUnique");

			modelBuilder.Entity<LK_Promotion>()
						.HasIndex(b => b.promotionCode)
						.IsUnique()
						.HasName("promotionCodeUnique");

			//modelBuilder.Entity<LK_Reason>()
			//			.HasIndex(b => b.reasonCode)
			//			.IsUnique()
			//			.HasName("reasonCodeUnique");

			modelBuilder.Entity<LK_SADStatus>()
						.HasIndex(b => b.statusCode)
						.IsUnique()
						.HasName("statusCodeUnique");

			//modelBuilder.Entity<MS_DocumentPS>()
			//			.HasIndex(b => b.docCode)
			//			.IsUnique()
			//			.HasName("docCodeCodeUnique");

			modelBuilder.Entity<MS_SalesEvent>()
						.HasIndex(b => b.eventCode)
						.IsUnique()
						.HasName("eventCodeUnique");

			modelBuilder.Entity<MS_ShopBusiness>()
						.HasIndex(b => b.shopBusinessCode)
						.IsUnique()
						.HasName("shopBusinessCodeUnique");

			modelBuilder.Entity<MS_TransFrom>()
						.HasIndex(b => b.transCode)
						.IsUnique()
						.HasName("transCodeUnique");

			////Notification
			//modelBuilder.Entity<MS_NotificationType>()
			//			.HasIndex(b => b.notifTypeCode)
			//			.IsUnique()
			//			.HasName("notifTypeCodeUnique");

			////No Delete On Action
			//modelBuilder.Entity<MS_BankBranch>()
			//	.HasOne(t => t.LK_BankLevel)
			//	.WithMany(w => w.MS_BankBranch)
			//	.HasForeignKey(d => d.bankBranchTypeID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_BankBranch>()
			//	.HasOne(t => t.MS_Bank)
			//	.WithMany(w => w.MS_BankBranch)
			//	.HasForeignKey(d => d.bankID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_Position>()
			//	.HasOne(t => t.MS_Department)
			//	.WithMany(w => w.MS_Position)
			//	.HasForeignKey(d => d.departmentID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//OnlineBooking
			//modelBuilder.Entity<TR_UnitOrderDetail>()
			//	.HasOne(t => t.MS_Term)
			//	.WithMany(w => w.TR_UnitOrderDetail)
			//	.HasForeignKey(d => d.termID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_UnitOrderDetail>()
			//	.HasOne(t => t.MS_Unit)
			//	.WithMany(w => w.TR_UnitOrderDetail)
			//	.HasForeignKey(d => d.unitID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_UnitReserved>()
			//	.HasOne(t => t.MS_Term)
			//	.WithMany(w => w.TR_UnitReserved)
			//	.HasForeignKey(d => d.termID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_Unit>()
			//	.HasOne(t => t.MS_TermMain)
			//	.WithMany(w => w.MS_Unit)
			//	.HasForeignKey(d => d.termMainID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_UnitItemPrice>()
			//	.HasOne(t => t.MS_Term)
			//	.WithMany(w => w.MS_UnitItemPrice)
			//	.HasForeignKey(d => d.termID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_UnitReserved>()
			//	.HasOne(t => t.MS_Renovation)
			//	.WithMany(w => w.TR_UnitReserved)
			//	.HasForeignKey(d => d.renovID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_Cluster>()
			//	.HasOne(t => t.MS_Project)
			//	.WithMany(w => w.MS_Cluster)
			//	.HasForeignKey(d => d.projectID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_UnitCode>()
			//	.HasOne(t => t.MS_Project)
			//	.WithMany(w => w.MS_UnitCode)
			//	.HasForeignKey(d => d.projectID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_UnitItemPrice>()
			//	.HasOne(t => t.MS_UnitItem)
			//	.WithMany(w => w.MS_UnitItemPrice)
			//	.HasForeignKey(d => d.unitItemID)
			//	.OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<TR_PPOrderPPNo>()
				.HasOne(t => t.MS_BatchEntry)
				.WithMany(w => w.TR_PPOrderPPNo)
				.HasForeignKey(d => d.batchID)
				.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_CMTemplate>()
			//	.HasOne(t => t.MS_Project)
			//	.WithMany(w => w.MS_CMTemplate)
			//	.HasForeignKey(d => d.projectID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_Voucher>()
			//.HasKey(c => new { c.projectID, c.voucherTypeCode, c.voucherCode });

			//modelBuilder.Entity<TR_SignificantFinancingHeader>()
			//	.HasOne(t => t.MS_Project)
			//	.WithMany(w => w.TR_SignificantFinancingHeader)
			//	.HasForeignKey(d => d.ProjectID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SignificantFinancingHeader>()
			//	.HasOne(t => t.MS_Cluster)
			//	.WithMany(w => w.TR_SignificantFinancingHeader)
			//	.HasForeignKey(d => d.ClusterID)
			//	.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SignificantFinancingDetail>()
			//	.HasOne(t => t.TR_SignificantFinancingHeader)
			//	.WithMany(w => w.TR_SignificantFinancingDetail)
			//	.HasForeignKey(d => d.FinancingHeaderID)
			//	.OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<TR_SignificantFinancingDetail>()
				.HasOne(t => t.TR_BookingHeader)
				.WithMany(w => w.TR_SignificantFinancingDetail)
				.HasForeignKey(d => d.BookingHeaderID)
				.OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_VoucherEventLocation>()
			//.HasKey(c => new { c.voucherEventId, c.IdLocation});as 
			//modelBuilder.Entity<TR_Booking_MemberHistory>()
			//.HasKey(c => new { c.Bookcode });

			//Add by Moko - 2023 jan 09
			//modelBuilder.Entity<TR_HistoryStatusAjb>()
			//	.HasKey(c => new { c.Id });
			//end

			base.OnModelCreating(modelBuilder);
			//Database.SetInitializer<PropertySystemDbContext>(null);




		}

	}
}
