﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_MappingOccKpr
	{
		[NotMapped]
		public string occID { get; set; }
		[Key]
		public int KPRType { get; set; }
		public string description { get; set; }
		public DateTime inputTime { get; set; }
		public int? inputUN { get; set; }
		public string modifUN { get; set; }
		public DateTime? modifTime { get; set; }
	}
}