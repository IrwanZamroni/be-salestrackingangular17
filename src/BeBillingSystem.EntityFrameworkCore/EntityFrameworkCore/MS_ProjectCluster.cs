﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.PropertySystemDB;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_ProjectCluster")]
	public class MS_ProjectCluster : AuditedEntity
	{
		[ForeignKey("MS_ProjectInfo")]
		public int projectInfoID { get; set; }
		public virtual MS_ProjectInfo MS_ProjectInfo { get; set; }

		[ForeignKey("MS_Cluster")]
		public int clusterID { get; set; }
		public virtual MS_Cluster MS_Cluster { get; set; }
	}
}