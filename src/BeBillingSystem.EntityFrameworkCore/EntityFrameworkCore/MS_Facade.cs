﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.PropertySystemDB;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_Facade : AuditedEntity
	{
		public int entityID { get; set; }

		//unique
		[Required]
		[StringLength(5)]
		public string facadeCode { get; set; }

		[Required]
		[StringLength(50)]
		public string facadeName { get; set; }

		[ForeignKey("MS_Project")]
		public int projectID { get; set; }
		public virtual MS_Project MS_Project { get; set; }

		public int clusterID { get; set; }

		public int detailID { get; set; }

		public virtual ICollection<TR_BookingHeader> TR_BookingHeader { get; set; }
	}
}