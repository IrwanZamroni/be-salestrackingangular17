﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.PropertySystemDB;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_ProjectInfo")]
	public class MS_ProjectInfo : AuditedEntity
	{
		[ForeignKey("MS_Project")]
		public int? projectID { get; set; }
		public virtual MS_Project MS_Project { get; set; }

		[Column("productName")]
		public string displayName { get; set; }

		[StringLength(10)]
		public string productCode { get; set; }

		//[StringLength(100)]
		public string projectDesc { get; set; }

		//[StringLength(100)]
		public int? sorting { get; set; }

		[ForeignKey("MS_ProjectKeyFeaturesCollection")]
		public int keyFeaturesCollectionID { get; set; }
		public virtual MS_ProjectKeyFeaturesCollection MS_ProjectKeyFeaturesCollection { get; set; }

		[StringLength(100)]
		public string projectDeveloper { get; set; }

		[StringLength(100)]
		public string projectWebsite { get; set; }

		[StringLength(500)]
		public string projectMarketingOffice { get; set; }

		[StringLength(50)]
		public string projectMarketingPhone { get; set; }

		[StringLength(200)]
		public string projectImageLogo { get; set; }

		[StringLength(100)]
		public string sitePlansImageUrl { get; set; }

		[StringLength(1000)]
		public string sitePlansLegend { get; set; }

		public bool projectStatus { get; set; }
		public bool showalihdana { get; set; }

		[StringLength(500)]
		public string displaySetting { get; set; }

		public bool isOBActive { get; set; }

		public bool isPPOLActive { get; set; }
		public bool? isGenerateVoucher { get; set; }

		public ICollection<MS_ProjectLocation> MS_ProjectLocation { get; set; }

		public ICollection<TR_ProjectSocialMedia> TR_ProjectSocialMedia { get; set; }

		public ICollection<TR_ProjectImageGallery> TR_ProjectImageGallery { get; set; }

		public ICollection<MS_ProjectCluster> MS_ProjectCluster { get; set; }

		public ICollection<MS_ProjectPPOnline> MS_ProjectPPOnline { get; set; }

		public ICollection<MS_BatchEntry> MS_BatchEntry { get; set; }
	}
}