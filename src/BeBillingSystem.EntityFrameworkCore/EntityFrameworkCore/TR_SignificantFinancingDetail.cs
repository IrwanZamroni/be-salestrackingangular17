﻿using Castle.MicroKernel.SubSystems.Conversion;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using Abp.Domain.Entities.Auditing;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("TR_SignificantFinancingDetail")]
	public class TR_SignificantFinancingDetail: AuditedEntity
	{
		[ForeignKey("TR_SignificantFinancingHeader")]
		public int FinancingHeaderID { get; set; }
		//public virtual TR_SignificantFinancingHeader TR_SignificantFinancingHeader { get; set; }

		[ForeignKey("TR_BookingHeader")]
		public int BookingHeaderID { get; set; }
		public virtual TR_BookingHeader TR_BookingHeader { get; set; }

		public DateTime? Handoverdate { get; set; }

		[Column(TypeName = "money")]
		public decimal NetPayment { get; set; }

		[Column(TypeName = "money")]
		public decimal NominalInterest { get; set; }
	}
}