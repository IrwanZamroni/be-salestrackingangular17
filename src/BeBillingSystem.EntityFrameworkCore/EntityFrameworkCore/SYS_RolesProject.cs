﻿using Abp.Domain.Entities.Auditing;
using BeBillingSystem.PropertySystemDB;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("SYS_RolesProject")]
	public class SYS_RolesProject :AuditedEntity
	{
		public int entityID { get; set; }

		public int rolesID { get; set; }

		[ForeignKey("MS_Project")]
		public int projectID { get; set; }
		public virtual MS_Project MS_Project { get; set; }
	}
}