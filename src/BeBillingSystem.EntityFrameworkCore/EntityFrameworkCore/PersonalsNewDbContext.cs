﻿using Abp.EntityFrameworkCore;
using BeBillingSystem.BillingSystem;
using BeBillingSystem.PersonalDB;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.EntityFrameworkCore
{
    public class PersonalsNewDbContext : AbpDbContext
    {
        /* Define an IDbSet for each entity of the application */

       // public DbSet<PERSONALS_MEMBER> PERSONALS_MEMBER { get; set; }
        public DbSet<PERSONALS> PERSONALS { get; set; }
        public DbSet<TR_Email> TR_Email { get; set; }
        public DbSet<TR_Address> TR_Address { get; set; }
        public DbSet<TR_Phone> TR_Phone { get; set; }
		public virtual DbSet<TR_BookingHeader> TR_BookingHeader { get; set; }
		public virtual DbSet<PERSONALS_MEMBER> PERSONALS_MEMBER { get; set; }
		public virtual DbSet<PERSONALS> PERSONAL { get; set; }
		public virtual DbSet<MS_MappingOccKpr> MS_MappingOccKpr { get; set; }
		public PersonalsNewDbContext(DbContextOptions<PersonalsNewDbContext> options)
          : base(options)
        {

        }
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Entity<PERSONALS>()
				.HasKey(c => new { c.entityCode, c.psCode });
			modelBuilder.Entity<MS_MappingOccKpr>()
			  .HasKey(c => new { c.occID, c.KPRType });
			//modelBuilder.Entity<MS_MappingOccKpr>()
			//	.HasKey(c => new { c.occID, c.KPRType });

			//modelBuilder.Entity<LK_PhonePrefix>()
			//	.HasKey(c => new { c.prefix, c.phoneType });

			//modelBuilder.Entity<MS_City>()
			//	.HasKey(c => new { c.entityCode, c.cityCode });

			//modelBuilder.Entity<MS_County>()
			//	.HasKey(c => new { c.countyCode, c.countyDesc });

			//modelBuilder.Entity<MS_Group>()
			//	.HasKey(c => new { c.entityCode, c.groupCode });

			//modelBuilder.Entity<MS_Nation>()
			//	.HasKey(c => new { c.entityCode, c.nationID });

			//modelBuilder.Entity<MS_Occupation>()
			//	.HasKey(c => new { c.entityCode, c.occID });

			//modelBuilder.Entity<MS_PostCode>()
			//	.HasKey(c => new { c.entityCode, c.cityCode, c.postCode });

			//modelBuilder.Entity<MS_Province>()
			//	.HasKey(c => new { c.provinceCode, c.provinceName });

			//modelBuilder.Entity<MS_Regency>()
			//	.HasKey(c => new { c.regencyCode, c.regencyName });

			//modelBuilder.Entity<MS_RelationResident>()
			//	.HasKey(c => new { c.kkCode, c.RefID });

			//modelBuilder.Entity<MS_Street>()
			//	.HasKey(c => new { c.entityCode, c.cityCode, c.postCode, c.streetNo });

			//modelBuilder.Entity<MS_Village>()
			//	.HasKey(c => new { c.villageCode, c.villageName, c.cityCode, c.countyCode, c.regencyCode, c.provinceCode });

			modelBuilder.Entity<PERSONALS>()
				.HasKey(c => new { c.entityCode, c.psCode });

			modelBuilder.Entity<PERSONALS_MEMBER>()
				.HasKey(c => new { c.entityCode, c.psCode, c.scmCode, c.memberCode });

			//modelBuilder.Entity<PotentialDocumentCustomer>()
			//	.HasKey(c => new { c.entityCode, c.idPotential, c.documentType });

			//modelBuilder.Entity<SYS_Counter>()
			//	.HasKey(c => new { c.entityCode, c.psCode });

			//modelBuilder.Entity<SYS_CounterMember>()
			//	.HasKey(c => new { c.entityCode, c.scmCode });

			//modelBuilder.Entity<SYS_RolesAddr>()
			//	.HasKey(c => new { c.entityCode, c.rolesname, c.addrType });

			//modelBuilder.Entity<SYS_UserGroup>()
			//	.HasKey(c => new { c.entityCode, c.userName, c.groupCode });

			modelBuilder.Entity<TR_Address>()
				.HasKey(c => new { c.entityCode, c.psCode, c.refID, c.addrType });

			//modelBuilder.Entity<TR_BankAccount>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.refID, c.BankCode });

			//modelBuilder.Entity<TR_Company>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.refID });

			//modelBuilder.Entity<TR_Document>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.documentType });

			modelBuilder.Entity<TR_Email>()
				.HasKey(c => new { c.entityCode, c.psCode, c.refID });

			//modelBuilder.Entity<TR_EmailInvalid>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.refID, c.email });

			//modelBuilder.Entity<TR_Family>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.refID });

			//modelBuilder.Entity<TR_Group>()
			//	.HasKey(c => new { c.entityCode, c.groupCode, c.psCode });

			//modelBuilder.Entity<TR_ID>()
			//	.HasKey(c => new { c.entityCode, c.psCode, c.refID });

			//modelBuilder.Entity<TR_IDFamily>()
				//.HasKey(c => new { c.psCode, c.familyRefID, c.refID });

			modelBuilder.Entity<TR_Phone>()
				.HasKey(c => new { c.entityCode, c.psCode, c.refID });

			//modelBuilder.ConfigurePersistedGrantEntity();
		}
	}
 

}
