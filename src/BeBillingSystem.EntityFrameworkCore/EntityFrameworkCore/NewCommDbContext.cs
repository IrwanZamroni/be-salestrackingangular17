﻿using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class NewCommDbContext : AbpDbContext
	{
		public virtual DbSet<TR_OverridingPct> TR_OverridingPct { get; set; }
		public virtual DbSet<TR_CommPayment> TR_CommPayment { get; set; }
		public virtual DbSet<MS_Users> MS_Users { get; set; }
		public NewCommDbContext(DbContextOptions<NewCommDbContext> options) : base(options)
		{
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//Database.SetInitializer<NewCommDbContext>(null);

			//modelBuilder
			//    .Entity<MS_Schema>()
			//    .Property(t => t.scmCode)
			//    .HasAnnotation(
			//    "Index",
			//    new IndexAnnotation(new IndexAttribute("scmCodeUnique") { IsUnique = true }));

			//modelBuilder.Entity<MS_SchemaRefferral>()
			//			.HasIndex(b => b.scmCodeReferral)
			//			.IsUnique();

			//modelBuilder.Entity<MS_Schema>()
			//			.HasIndex(b => b.scmCode)
			//			.IsUnique()
			//			.HasName("scmCodeUnique");

			modelBuilder.Entity<TR_OverridingPct>()
				.HasKey(c => new { c.entityCode, c.bookNo, c.statusCode, c.asUplineNo });
			modelBuilder.Entity<MS_Users>(b =>
			{
				b.HasIndex(e => new { e.Id });
			});
			//modelBuilder.Entity<TR_OverridingReferral>()
			//	.HasKey(c => new { c.entityCode, c.bookNo, c.statusCode, c.asUplineNo });

			//modelBuilder.Entity<TR_CommPPDetail>()
			//	.HasKey(c => new { c.entityCode, c.PPNo, c.ProjectCode, c.scmCode, c.asUplineNo, c.reqNo, c.memberCode, c.statusCode });

			//modelBuilder.Entity<TR_OverridingCommPP>()
			//	.HasKey(c => new { c.entityCode, c.PPNo, c.ProjectCode, c.scmCode, c.memberStatus, c.asUplineNo });

			//modelBuilder.Entity<TR_CommPP>()
			//   .HasKey(c => new { c.entityCode, c.PPNo, c.ProjectCode, c.scmCode });

			//modelBuilder.Entity<LK_CommType>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.commTypeCode });

			//modelBuilder.Entity<LK_PointType>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.pointTypeCode });

			//modelBuilder.Entity<LK_Upline>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.uplineNo });

			//modelBuilder.Entity<MS_BobotComm>()
			//	 .HasKey(c => new { c.entityCode, c.projectCode, c.clusterCode, c.scmCode, c.termCode, c.termNo });

			//modelBuilder.Entity<MS_Developer_Schema>()
			//	.HasKey(c => new { c.entityCode, c.propCode, c.devCode, c.scmCode });

			//modelBuilder.Entity<MS_Flag>()
			//	.HasKey(c => new { c.entityCode, c.flagCode });

			//modelBuilder.Entity<MS_PointPct>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.statusCode, c.asUplineNo });

			//modelBuilder.Entity<MS_PPhRange>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.PPhYear, c.PPhRangeHighBound });

			//modelBuilder.Entity<MS_Property>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.propCode });

			//modelBuilder.Entity<MS_Schema>()
			//	.HasKey(c => new { c.entityCode, c.scmCode });

			//modelBuilder.Entity<MS_SchemaRequirement>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.reqNo });

			//modelBuilder.Entity<MS_StatusMember>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.statusCode });

			//modelBuilder.Entity<TR_BudgetPayment>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.propCode, c.devCode, c.bookNo, c.reqNo });

			modelBuilder.Entity<TR_CommPayment>()
				.HasKey(c => new { c.devCode, c.bookNo, c.asUplineNo, c.isHold, c.commNo, c.memberCode, c.commTypeCode, c.reqNo });

			//modelBuilder.Entity<TR_CommPaymentPph>()
			//	.HasKey(c => new { c.devCode, c.bookNo, c.asUplineNo, c.isHold, c.commNo, c.memberCode, c.commTypeCode, c.reqNo, c.pphNo });

			//modelBuilder.Entity<TR_CommPct>()
			//	.HasKey(c => new { c.entityCode, c.devCode, c.bookNo, c.memberCodeR, c.asUplineNo });

			//modelBuilder.Entity<TR_ManagementFee>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.propCode, c.devCode, c.bookNo, c.reqNo });

			//modelBuilder.Entity<TR_SoldUnit>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.devCode, c.bookNo });

			//modelBuilder.Entity<TR_SoldUnitFlag>()
			//	.HasKey(c => new { c.entityCode, c.flagCode, c.devCode, c.bookNo });

			//modelBuilder.Entity<MS_CommPct>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.statusCode, c.asUplineNo, c.validDate, c.minAmt, c.isBF });

			//modelBuilder.Entity<MS_PPhRangeIns>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.pphRangePct, c.validDate, c.modifTime, c.inputTime, c.inputUN, c.TAX_CODE });

			//modelBuilder.Entity<MS_CommParameter>()
			//   .HasKey(c => new { c.code, c.value });

			//modelBuilder.Entity<TR_SoldUnitRequirement>()
			//	.HasKey(c => new { c.entityCode, c.devCode, c.bookNo, c.scmCode, c.reqNo });

			//modelBuilder.Entity<MS_GroupSchema>()
			//.HasKey(c => new { c.entityCode, c.scmCode, c.groupScmCode, c.projectCode, c.clusterCode, c.validFrom });

			//modelBuilder.Entity<MS_GroupCommPct>()
			// .HasKey(c => new { c.asUplineNo, c.entityCode, c.groupScmCode, c.statusCode, c.validDate, c.termCode, c.termNo, c.isBF });

			//modelBuilder.Entity<MS_GroupCommPctNonStd>()
			//.HasKey(c => new { c.asUplineNo, c.entityCode, c.scmCode, c.groupSchemaCode });

			//modelBuilder.Entity<MS_GroupSchemaRequirement>()
			//.HasKey(c => new { c.entityCode, c.groupScmCode, c.reqNo, c.termCode, c.termNo });

			//modelBuilder.Entity<MS_MappingPropDev>()
			//.HasKey(c => new { c.entityCode, c.scmCode, c.propCode, c.devCode });

			//modelBuilder.Entity<MS_CommDev>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.termCode, c.termNo, c.projectCode, c.accCode, c.bankCode, c.bankBranchCode, c.devCode });

			//modelBuilder.Entity<TR_CommDev>()
			//	.HasKey(c => new { c.entityCode, c.scmCode, c.termCode, c.termNo, c.projectCode, c.accCode, c.bankCode, c.bankBranchCode, c.devCode, c.bookNo });

			//modelBuilder.Entity<MS_DocumentComm>()
			//   .HasKey(c => new { c.entityCode, c.scmCode, c.projectCode, c.clusterCode, c.termCode, c.termNo, c.documentType });

			//modelBuilder.Entity<MS_BasicPriceComm>()
			//   .HasKey(c => new { c.entityCode, c.projectCode, c.clusterCode, c.termCode });


			//modelBuilder.Entity<MS_SchemaPP>()
			//	.HasKey(c => new { c.entityCode, c.projectCode, c.scmCode, c.scmPPCode });

			//modelBuilder.Entity<MS_CommPctPP>()
			//	.HasKey(c => new { c.entityCode, c.projectCode, c.scmCode, c.scmPPCode, c.memberStatus, c.asUplineNo });


			//=====ON DELETE NO ACTION=====

			//modelBuilder.Entity<MS_CommPct>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_CommPct)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_StatusMember>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_StatusMember)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<LK_CommType>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.LK_CommType)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<LK_PointType>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.LK_PointType)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_Developer_Schema>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_Developer_Schema)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_PointPct>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_PointPct)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_Property>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_Property)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_PPhRange>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_PPhRange)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<MS_PPhRangeIns>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.MS_PPhRangeIns)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_CommPayment>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.TR_CommPayment)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_CommPaymentPph>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.TR_CommPaymentPph)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_ManagementFee>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.TR_ManagementFee)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SoldUnit>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.TR_SoldUnit)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			//modelBuilder.Entity<TR_SoldUnitRequirement>()
			//    .HasOne(t => t.MS_Schema)
			//    .WithMany(w => w.TR_SoldUnitRequirement)
			//    .HasForeignKey(d => d.schemaID)
			//    .OnDelete(DeleteBehavior.Restrict);

			base.OnModelCreating(modelBuilder);
		}

		//public void Insert(TR_CommPPDetail dataCommPP)
		//{
		//	throw new NotImplementedException();
		//}

		//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		//{
		//    base.OnConfiguring(optionsBuilder);
		//    var builder = new ConfigurationBuilder()
		//        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
		//    IConfigurationRoot config = builder.Build();

		//    optionsBuilder.UseSqlServer(config.GetConnectionString("NewCommDbContext"));
		//}

		//public NewCommDbContext(string nameOrConnectionString)
		//    : base("NewCommDbContext")
		//{

		//}

		//public NewCommDbContext(DbConnection existingConnection)
		//   : base(existingConnection, false)
		//{

		//}

		//public NewCommDbContext(DbConnection existingConnection, bool contextOwnsConnection)
		//    : base(existingConnection, contextOwnsConnection)
		//{

		//}

	}
}
