﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class LK_PaymentType : AuditedEntity
	{
		public int paymentType { get; set; }

		[Required]
		[StringLength(50)]
		public string paymentTypeName { get; set; }

		public short sortNo { get; set; }

		public virtual ICollection<TR_PriorityPass> TR_PriorityPass { get; set; }

		//public virtual ICollection<TR_PPPayment> TR_PPPayment { get; set; }
	}
}