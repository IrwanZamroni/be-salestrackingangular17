﻿//using Abp.IdentityServer4;
using Abp.Domain.Entities.Auditing;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_Users: AuditedEntity
	{
		public long userID { get; set; }
		public string Name { get; set; }
	}
}