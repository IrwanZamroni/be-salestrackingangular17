﻿//using Abp.IdentityServer4;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Security;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeBillingSystem.EntityFrameworkCore
{
	[Table("MS_AD_Auth")]
	public class MS_AD_Auth : AuditedEntity
	{
		public string authenticationSource { get; set; }

		public string ip { get; set; }

		public string userLogin { get; set; }

		public string passLogin { get; set; }

		[NotMapped]
		public string securePassword
		{
			get
			{
				return SimpleStringCipher.Instance.Decrypt(this.passLogin);
			}
			set
			{
				this.passLogin = SimpleStringCipher.Instance.Encrypt(value);
			}
		}
	}
}