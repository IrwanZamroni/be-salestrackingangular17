﻿//using Abp.IdentityServer4;
using BeBillingSystem.Authorization.Users;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using Abp.Domain.Entities.Auditing;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MP_UserPersonals : AuditedEntity
	{
		public string psCode { get; set; }

		public int? userType { get; set; }

		public DateTime? activeDate { get; set; }

		public string domainLogin { get; set; }

		[ForeignKey("User")]
		public long userID { get; set; }
		public virtual User User { get; set; }
	}
}