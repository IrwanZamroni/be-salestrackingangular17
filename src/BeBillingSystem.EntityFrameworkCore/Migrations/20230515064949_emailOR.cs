﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class emailOR : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "IsSendEmailDate",
                table: "TR_BillingPaymentHeader",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isSendEmail",
                table: "TR_BillingPaymentHeader",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSendEmailDate",
                table: "TR_BillingPaymentHeader");

            migrationBuilder.DropColumn(
                name: "isSendEmail",
                table: "TR_BillingPaymentHeader");
        }
    }
}
