﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class letterinvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InvoiceHeaderId",
                table: "TR_WarningLetter",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceHeaderId",
                table: "TR_WarningLetter");
        }
    }
}
