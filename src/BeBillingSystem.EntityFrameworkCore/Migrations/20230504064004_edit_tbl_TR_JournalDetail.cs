﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class edit_tbl_TR_JournalDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BankId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OracleDesc",
                table: "TR_JournalDetail",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentMethodId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BankId",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "OracleDesc",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "PaymentMethodId",
                table: "TR_JournalDetail");
        }
    }
}
