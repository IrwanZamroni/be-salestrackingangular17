﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class check : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdjustmentDate",
                table: "TR_InvoiceHeader");

            migrationBuilder.DropColumn(
                name: "AdjustmentNominal",
                table: "TR_InvoiceHeader");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "AdjustmentDate",
                table: "TR_InvoiceHeader",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "AdjustmentNominal",
                table: "TR_InvoiceHeader",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
