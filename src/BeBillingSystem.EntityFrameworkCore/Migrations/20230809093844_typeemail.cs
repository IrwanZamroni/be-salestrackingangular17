﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class typeemail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TypeEmail",
                table: "MS_SettingEmail",
                newName: "TypeEmailId");

            migrationBuilder.CreateTable(
                name: "MS_TypeEmail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_TypeEmail", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_TypeEmail");

            migrationBuilder.RenameColumn(
                name: "TypeEmailId",
                table: "MS_SettingEmail",
                newName: "TypeEmail");
        }
    }
}
