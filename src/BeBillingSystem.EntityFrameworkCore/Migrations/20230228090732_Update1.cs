﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class Update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_ItemRateBPL");

            migrationBuilder.DropTable(
                name: "MS_ItemRateWater");

            migrationBuilder.DropColumn(
                name: "TemplateInvoiceName",
                table: "MS_UnitItem");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CancelDate",
                table: "TR_BillingPaymentHeader",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<int>(
                name: "TemplateInvoiceId",
                table: "MS_UnitItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MS_ItemRate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<bool>(type: "bit", nullable: false),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false),
                    LandRate = table.Column<int>(type: "int", nullable: false),
                    BuildRate = table.Column<int>(type: "int", nullable: false),
                    Range1Start = table.Column<int>(type: "int", nullable: false),
                    Range1End = table.Column<int>(type: "int", nullable: false),
                    Range1Nominal = table.Column<int>(type: "int", nullable: false),
                    Range2Start = table.Column<int>(type: "int", nullable: false),
                    Range2End = table.Column<int>(type: "int", nullable: false),
                    Range2Nominal = table.Column<int>(type: "int", nullable: false),
                    Range3Start = table.Column<int>(type: "int", nullable: false),
                    Range3End = table.Column<int>(type: "int", nullable: false),
                    Range3Nominal = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SinkingFundPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ItemRate", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_ItemRate");

            migrationBuilder.DropColumn(
                name: "TemplateInvoiceId",
                table: "MS_UnitItem");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CancelDate",
                table: "TR_BillingPaymentHeader",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TemplateInvoiceName",
                table: "MS_UnitItem",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MS_ItemRateBPL",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BuildRate = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<bool>(type: "bit", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LandRate = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    SinkingFundPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ItemRateBPL", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_ItemRateWater",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<bool>(type: "bit", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Range1End = table.Column<int>(type: "int", nullable: false),
                    Range1Nominal = table.Column<int>(type: "int", nullable: false),
                    Range1Start = table.Column<int>(type: "int", nullable: false),
                    Range2End = table.Column<int>(type: "int", nullable: false),
                    Range2Nominal = table.Column<int>(type: "int", nullable: false),
                    Range2Start = table.Column<int>(type: "int", nullable: false),
                    Range3End = table.Column<int>(type: "int", nullable: false),
                    Range3Nominal = table.Column<int>(type: "int", nullable: false),
                    Range3Start = table.Column<int>(type: "int", nullable: false),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    SinkingFundPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ItemRateWater", x => x.Id);
                });
        }
    }
}
