﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class updatetrjournal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BillingPaymentDetailId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BillingPaymentHeaderId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "InvoiceDetailId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "InvoiceHeaderId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "InvoiceItemId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BillingPaymentDetailId",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "BillingPaymentHeaderId",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "InvoiceDetailId",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "InvoiceHeaderId",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "InvoiceItemId",
                table: "TR_JournalDetail");
        }
    }
}
