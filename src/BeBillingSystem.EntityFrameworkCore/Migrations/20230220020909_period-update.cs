﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class periodupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PeriodName",
                table: "MS_Period");

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodMonth",
                table: "MS_Period",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodYear",
                table: "MS_Period",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PeriodMonth",
                table: "MS_Period");

            migrationBuilder.DropColumn(
                name: "PeriodYear",
                table: "MS_Period");

            migrationBuilder.AddColumn<string>(
                name: "PeriodName",
                table: "MS_Period",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
