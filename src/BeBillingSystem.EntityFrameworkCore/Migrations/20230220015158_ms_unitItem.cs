﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class ms_unitItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsHoldSP",
                table: "MS_UnitData",
                newName: "PrintSP");

            migrationBuilder.RenameColumn(
                name: "IsHoldInvoice",
                table: "MS_UnitData",
                newName: "PrintInvoice");

            migrationBuilder.AddColumn<bool>(
                name: "PrintFakturPajak",
                table: "MS_UnitData",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "MS_UnitItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    UnitCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UnitNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TemplateInvoice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VirtualAccNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitItem", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UnitItem");

            migrationBuilder.DropColumn(
                name: "PrintFakturPajak",
                table: "MS_UnitData");

            migrationBuilder.RenameColumn(
                name: "PrintSP",
                table: "MS_UnitData",
                newName: "IsHoldSP");

            migrationBuilder.RenameColumn(
                name: "PrintInvoice",
                table: "MS_UnitData",
                newName: "IsHoldInvoice");
        }
    }
}
