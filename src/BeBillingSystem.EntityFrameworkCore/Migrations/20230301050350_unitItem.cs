﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class unitItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UnitCode",
                table: "MS_UnitItem");

            migrationBuilder.RenameColumn(
                name: "UnitNo",
                table: "MS_UnitItem",
                newName: "UnitDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UnitDataId",
                table: "MS_UnitItem",
                newName: "UnitNo");

            migrationBuilder.AddColumn<string>(
                name: "UnitCode",
                table: "MS_UnitItem",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
