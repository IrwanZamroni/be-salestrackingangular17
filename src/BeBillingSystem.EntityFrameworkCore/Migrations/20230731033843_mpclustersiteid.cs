﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class mpclustersiteid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MP_ClusterSiteId",
                table: "MS_TemplateInvoiceDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LuasAkhir",
                table: "MS_ItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LuasAwal",
                table: "MS_ItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MP_ClusterSiteId",
                table: "MS_TemplateInvoiceDetail");

            migrationBuilder.DropColumn(
                name: "LuasAkhir",
                table: "MS_ItemRate");

            migrationBuilder.DropColumn(
                name: "LuasAwal",
                table: "MS_ItemRate");
        }
    }
}
