﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class perubahanoracle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "COA1",
                table: "MS_OracleMapping",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "COA2",
                table: "MS_OracleMapping",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "COA3",
                table: "MS_OracleMapping",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "COA4",
                table: "MS_OracleMapping",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "COA1",
                table: "MS_OracleMapping");

            migrationBuilder.DropColumn(
                name: "COA2",
                table: "MS_OracleMapping");

            migrationBuilder.DropColumn(
                name: "COA3",
                table: "MS_OracleMapping");

            migrationBuilder.DropColumn(
                name: "COA4",
                table: "MS_OracleMapping");
        }
    }
}
