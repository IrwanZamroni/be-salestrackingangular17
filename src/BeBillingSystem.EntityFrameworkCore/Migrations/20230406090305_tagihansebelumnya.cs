﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class tagihansebelumnya : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SPRefNo",
                table: "TR_WarningLetter",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TagiahanBulanSebelumnya",
                table: "TR_InvoiceHeader",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "MP_ProjectSite",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SPRefNo",
                table: "TR_WarningLetter");

            migrationBuilder.DropColumn(
                name: "TagiahanBulanSebelumnya",
                table: "TR_InvoiceHeader");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "MP_ProjectSite");
        }
    }
}
