﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class addBilling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UnitId",
                table: "TR_WarningLetter",
                newName: "UnitDataId");

            migrationBuilder.RenameColumn(
                name: "UnitCode",
                table: "TR_Invoice",
                newName: "UnitDataId");

            migrationBuilder.AddColumn<int>(
                name: "BuildArea",
                table: "MS_UnitData",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LandArea",
                table: "MS_UnitData",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UnitStatusId",
                table: "MS_UnitData",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UnitType",
                table: "MS_UnitData",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MS_UnitStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnitType = table.Column<int>(type: "int", nullable: false),
                    StatusName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_BillingPaymentDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BillingHeaderId = table.Column<int>(type: "int", nullable: false),
                    InvoiceNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentAmount = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_BillingPaymentDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_BillingPaymentHeader",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnitDataId = table.Column<int>(type: "int", nullable: false),
                    PsCode = table.Column<int>(type: "int", nullable: false),
                    UnitCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UnitNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentMethod = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CardNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TransactionDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BankName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Remaks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AmountPayment = table.Column<int>(type: "int", nullable: false),
                    Charge = table.Column<int>(type: "int", nullable: false),
                    Total = table.Column<int>(type: "int", nullable: false),
                    PrintOfficialReceipt = table.Column<bool>(type: "bit", nullable: false),
                    ReprintOR = table.Column<int>(type: "int", nullable: false),
                    ReceiptNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CancelPayment = table.Column<bool>(type: "bit", nullable: false),
                    IsUploadBulk = table.Column<bool>(type: "bit", nullable: false),
                    UserCancelPayment = table.Column<int>(type: "int", nullable: false),
                    CancelDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_BillingPaymentHeader", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_RunningNumber",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    DocCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Number = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_RunningNumber", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UnitStatus");

            migrationBuilder.DropTable(
                name: "TR_BillingPaymentDetail");

            migrationBuilder.DropTable(
                name: "TR_BillingPaymentHeader");

            migrationBuilder.DropTable(
                name: "TR_RunningNumber");

            migrationBuilder.DropColumn(
                name: "BuildArea",
                table: "MS_UnitData");

            migrationBuilder.DropColumn(
                name: "LandArea",
                table: "MS_UnitData");

            migrationBuilder.DropColumn(
                name: "UnitStatusId",
                table: "MS_UnitData");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "MS_UnitData");

            migrationBuilder.RenameColumn(
                name: "UnitDataId",
                table: "TR_WarningLetter",
                newName: "UnitId");

            migrationBuilder.RenameColumn(
                name: "UnitDataId",
                table: "TR_Invoice",
                newName: "UnitCode");
        }
    }
}
