﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class d1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_OracleMapping");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_OracleMapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA3 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA4 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA5 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA6 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA7 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ItemNameTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    OracleGroup = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OracleTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    SiteId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_OracleMapping", x => x.Id);
                });
        }
    }
}
