﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class updateHeaderInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_Invoice");

            migrationBuilder.RenameColumn(
                name: "InvoiceId",
                table: "TR_BillingPaymentDetail",
                newName: "InvoiceHeaderId");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "TR_BillingPaymentHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransactionCode",
                table: "TR_BillingPaymentHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VA",
                table: "TR_BillingPaymentHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPenalty",
                table: "MS_UnitItemHeader",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UnitName",
                table: "MS_UnitData",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TR_InvoiceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvoiceHeaderId = table.Column<int>(type: "int", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ItemNominal = table.Column<int>(type: "int", nullable: false),
                    PPNNominal = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_InvoiceDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TR_InvoiceHeader",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    PeriodId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    UnitDataId = table.Column<int>(type: "int", nullable: false),
                    PsCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InvoiceNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TemplateInvoiceHeaderId = table.Column<int>(type: "int", nullable: false),
                    TagihanPerbulan = table.Column<int>(type: "int", nullable: false),
                    JumlahYangHarusDibayar = table.Column<int>(type: "int", nullable: false),
                    IsSendEmail = table.Column<bool>(type: "bit", nullable: false),
                    IsSendWhatsapp = table.Column<bool>(type: "bit", nullable: false),
                    RegenerateCount = table.Column<int>(type: "int", nullable: false),
                    InvoiceDoc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InvoiceDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    JatuhTempo = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AdjustmentNominal = table.Column<int>(type: "int", nullable: false),
                    PenaltyNominal = table.Column<int>(type: "int", nullable: false),
                    IsSendEmailDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsSendWADate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_InvoiceHeader", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TR_InvoiceDetail");

            migrationBuilder.DropTable(
                name: "TR_InvoiceHeader");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "TR_BillingPaymentHeader");

            migrationBuilder.DropColumn(
                name: "TransactionCode",
                table: "TR_BillingPaymentHeader");

            migrationBuilder.DropColumn(
                name: "VA",
                table: "TR_BillingPaymentHeader");

            migrationBuilder.DropColumn(
                name: "IsPenalty",
                table: "MS_UnitItemHeader");

            migrationBuilder.DropColumn(
                name: "UnitName",
                table: "MS_UnitData");

            migrationBuilder.RenameColumn(
                name: "InvoiceHeaderId",
                table: "TR_BillingPaymentDetail",
                newName: "InvoiceId");

            migrationBuilder.CreateTable(
                name: "TR_Invoice",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    InvoiceDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    InvoiceDoc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InvoiceNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsSendEmail = table.Column<bool>(type: "bit", nullable: false),
                    IsSendWhatsapp = table.Column<bool>(type: "bit", nullable: false),
                    JumlahYangHarusDibayar = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    PeriodId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PsCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegenerateCount = table.Column<int>(type: "int", nullable: false),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    TagihanPerbulan = table.Column<int>(type: "int", nullable: false),
                    TemplateInvoiceHeaderId = table.Column<int>(type: "int", nullable: false),
                    TypeInvoice = table.Column<int>(type: "int", nullable: false),
                    UnitDataId = table.Column<int>(type: "int", nullable: false),
                    UnitId = table.Column<int>(type: "int", nullable: false),
                    UnitNo = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TR_Invoice", x => x.Id);
                });
        }
    }
}
