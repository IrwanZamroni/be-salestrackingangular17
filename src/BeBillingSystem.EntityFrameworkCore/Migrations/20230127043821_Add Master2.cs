﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class AddMaster2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isActive",
                table: "MS_Site",
                newName: "IsActive");

            migrationBuilder.CreateTable(
                name: "MS_BillingItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PPHId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Aging = table.Column<int>(type: "int", nullable: false),
                    CompanyCode = table.Column<int>(type: "int", nullable: false),
                    IsShowInvoice = table.Column<bool>(type: "bit", nullable: false),
                    IsFakturPajak = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BillingItem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_BillingItemRate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BillingItemId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LandRate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuildRate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VATPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SingkingFundPercentage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ManagementFee = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BillingItemRate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_ExchangeRate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CurrencyName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rate = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ExchangeRate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_OracleMapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    OracleGroup = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OracleTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemNameTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA5 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA6 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA7 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccountType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_OracleMapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_Period",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    PeriodNumber = table.Column<int>(type: "int", nullable: false),
                    PeriodName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CloseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Period", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_PPH",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PPHName = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_PPH", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_UnitData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    UnitId = table.Column<int>(type: "int", nullable: false),
                    UnitCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UnitNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PsCode = table.Column<int>(type: "int", nullable: false),
                    IsHoldInvoice = table.Column<bool>(type: "bit", nullable: false),
                    IsHoldSP = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_BillingItem");

            migrationBuilder.DropTable(
                name: "MS_BillingItemRate");

            migrationBuilder.DropTable(
                name: "MS_ExchangeRate");

            migrationBuilder.DropTable(
                name: "MS_OracleMapping");

            migrationBuilder.DropTable(
                name: "MS_Period");

            migrationBuilder.DropTable(
                name: "MS_PPH");

            migrationBuilder.DropTable(
                name: "MS_UnitData");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "MS_Site",
                newName: "isActive");
        }
    }
}
