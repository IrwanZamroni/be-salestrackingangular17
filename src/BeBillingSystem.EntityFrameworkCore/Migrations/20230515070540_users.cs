﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
      name: "ProfileImg",
      table: "AbpUsers",
      nullable: true
      );

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
