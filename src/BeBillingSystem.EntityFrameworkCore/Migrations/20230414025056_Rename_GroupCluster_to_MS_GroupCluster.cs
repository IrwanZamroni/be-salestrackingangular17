﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class Rename_GroupCluster_to_MS_GroupCluster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupCluster",
                table: "GroupCluster");

            migrationBuilder.RenameTable(
                name: "GroupCluster",
                newName: "MS_GroupCluster");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MS_GroupCluster",
                table: "MS_GroupCluster",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MS_GroupCluster",
                table: "MS_GroupCluster");

            migrationBuilder.RenameTable(
                name: "MS_GroupCluster",
                newName: "GroupCluster");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupCluster",
                table: "GroupCluster",
                column: "Id");
        }
    }
}
