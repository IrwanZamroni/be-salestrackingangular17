﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class isgen2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isGenerateInvoice",
                table: "MS_UnitData",
                newName: "IsGenerateInvoice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsGenerateInvoice",
                table: "MS_UnitData",
                newName: "isGenerateInvoice");
        }
    }
}
