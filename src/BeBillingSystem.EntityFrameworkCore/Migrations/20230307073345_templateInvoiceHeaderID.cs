﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class templateInvoiceHeaderID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceId",
                table: "TR_Invoice",
                newName: "TemplateInvoiceHeaderId");

            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceId",
                table: "MS_TemplateInvoiceDetail",
                newName: "TemplateInvoiceHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceHeaderId",
                table: "TR_Invoice",
                newName: "TemplateInvoiceId");

            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceHeaderId",
                table: "MS_TemplateInvoiceDetail",
                newName: "TemplateInvoiceId");
        }
    }
}
