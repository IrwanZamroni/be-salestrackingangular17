﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class perubahandiItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceId",
                table: "MS_UnitItem",
                newName: "TemplateInvoiceHeaderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceHeaderId",
                table: "MS_UnitItem",
                newName: "TemplateInvoiceId");
        }
    }
}
