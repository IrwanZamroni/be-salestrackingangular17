﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class lumo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "lumpSumRate",
                table: "MS_ItemRate",
                newName: "LumpSumRate");

            migrationBuilder.AlterColumn<int>(
                name: "VATPercentage",
                table: "MS_ItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LumpSumRate",
                table: "MS_ItemRate",
                newName: "lumpSumRate");

            migrationBuilder.AlterColumn<string>(
                name: "VATPercentage",
                table: "MS_ItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
