﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class oraclemapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MS_OracleMapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    ClusterId = table.Column<int>(type: "int", nullable: false),
                    OracleGroup = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OracleTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemNameTransDesc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA3 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA4 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA5 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA6 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    COA7 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccountType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_OracleMapping", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_OracleMapping");
        }
    }
}
