﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class edit_journal_headerdetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceNumber",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "IsUploadOracle",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "ReceiptNumber",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "IsClosing",
                table: "TR_BillingPaymentDetail");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "TR_JournalHeader",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("Relational:ColumnOrder", 0)
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ClusterId",
                table: "TR_JournalHeader",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("Relational:ColumnOrder", 3);

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "TR_JournalHeader",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("Relational:ColumnOrder", 2);

            migrationBuilder.AddColumn<int>(
                name: "SiteId",
                table: "TR_JournalHeader",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("Relational:ColumnOrder", 1);

            migrationBuilder.AlterColumn<int>(
                name: "JournalHeaderId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("Relational:ColumnOrder", 1);

            migrationBuilder.AlterColumn<DateTime>(
                name: "JournalDate",
                table: "TR_JournalDetail",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2")
                .Annotation("Relational:ColumnOrder", 2);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("Relational:ColumnOrder", 0)
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "InvoiceNumber",
                table: "TR_JournalDetail",
                type: "nvarchar(max)",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 4);

            migrationBuilder.AddColumn<bool>(
                name: "IsUploadOracle",
                table: "TR_JournalDetail",
                type: "bit",
                nullable: false,
                defaultValue: false)
                .Annotation("Relational:ColumnOrder", 5);

            migrationBuilder.AddColumn<string>(
                name: "ReceiptNumber",
                table: "TR_JournalDetail",
                type: "nvarchar(max)",
                nullable: true)
                .Annotation("Relational:ColumnOrder", 3);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClusterId",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "TR_JournalHeader");

            migrationBuilder.DropColumn(
                name: "InvoiceNumber",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "IsUploadOracle",
                table: "TR_JournalDetail");

            migrationBuilder.DropColumn(
                name: "ReceiptNumber",
                table: "TR_JournalDetail");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "TR_JournalHeader",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("Relational:ColumnOrder", 0)
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "InvoiceNumber",
                table: "TR_JournalHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsUploadOracle",
                table: "TR_JournalHeader",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ReceiptNumber",
                table: "TR_JournalHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "JournalHeaderId",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .OldAnnotation("Relational:ColumnOrder", 1);

            migrationBuilder.AlterColumn<DateTime>(
                name: "JournalDate",
                table: "TR_JournalDetail",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2")
                .OldAnnotation("Relational:ColumnOrder", 2);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "TR_JournalDetail",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("Relational:ColumnOrder", 0)
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<bool>(
                name: "IsClosing",
                table: "TR_BillingPaymentDetail",
                type: "bit",
                nullable: true);
        }
    }
}
