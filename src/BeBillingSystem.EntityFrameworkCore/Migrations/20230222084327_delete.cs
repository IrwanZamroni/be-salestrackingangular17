﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class delete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFakturPajak",
                table: "MS_Item");

            migrationBuilder.DropColumn(
                name: "IsShowInvoice",
                table: "MS_Item");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFakturPajak",
                table: "MS_Item",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsShowInvoice",
                table: "MS_Item",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
