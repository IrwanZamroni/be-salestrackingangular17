﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class tagihan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TagiahanBulanSebelumnya",
                table: "TR_InvoiceHeader",
                newName: "TagihanBulanSebelumnya");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TagihanBulanSebelumnya",
                table: "TR_InvoiceHeader",
                newName: "TagiahanBulanSebelumnya");
        }
    }
}
