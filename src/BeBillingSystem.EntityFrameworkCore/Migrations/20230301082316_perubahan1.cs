﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class perubahan1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClusterName",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "InvoiceName",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "ProjectCode",
                table: "TR_Invoice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClusterName",
                table: "TR_Invoice",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceName",
                table: "TR_Invoice",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectCode",
                table: "TR_Invoice",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
