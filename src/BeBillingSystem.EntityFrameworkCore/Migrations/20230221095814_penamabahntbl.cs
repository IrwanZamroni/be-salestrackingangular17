﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class penamabahntbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_BillingItem");

            migrationBuilder.DropTable(
                name: "MS_BillingItemRate");

            migrationBuilder.DropColumn(
                name: "Period",
                table: "TR_WarningLetter");

            migrationBuilder.DropColumn(
                name: "Period",
                table: "TR_Invoice");

            migrationBuilder.RenameColumn(
                name: "ProjectCode",
                table: "TR_WaterReading",
                newName: "ProjectId");

            migrationBuilder.RenameColumn(
                name: "ClusterName",
                table: "TR_WaterReading",
                newName: "ClusterId");

            migrationBuilder.RenameColumn(
                name: "InvoiceName",
                table: "TR_Invoice",
                newName: "InvoiceDoc");

            migrationBuilder.RenameColumn(
                name: "TemplateInvoice",
                table: "MS_UnitItem",
                newName: "TemplateInvoiceName");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "TR_WarningLetter",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PeriodId",
                table: "TR_WarningLetter",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "psCode",
                table: "TR_WarningLetter",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsSendEmail",
                table: "TR_Invoice",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSendWhatsapp",
                table: "TR_Invoice",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PeriodId",
                table: "TR_Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RegenerateCount",
                table: "TR_Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TemplateInvoiceId",
                table: "TR_Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MP_TemplateInvoice",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    TemplateInvoiceName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TypeInvoiceId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_TemplateInvoice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MP_TemplateInvoiceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TemplateInvoiceId = table.Column<int>(type: "int", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ItemRateId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MP_TemplateInvoiceDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_Item",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PPHId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Aging = table.Column<int>(type: "int", nullable: false),
                    AttributeId = table.Column<int>(type: "int", nullable: false),
                    CompanyCode = table.Column<int>(type: "int", nullable: false),
                    IsShowInvoice = table.Column<bool>(type: "bit", nullable: false),
                    IsFakturPajak = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_Item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_ItemRateBPL",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<bool>(type: "bit", nullable: false),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false),
                    LandRate = table.Column<int>(type: "int", nullable: false),
                    BuildRate = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<int>(type: "int", nullable: false),
                    SinkingFundPercentage = table.Column<int>(type: "int", nullable: false),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<int>(type: "int", nullable: false),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ItemRateBPL", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_ItemRateWater",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<bool>(type: "bit", nullable: false),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false),
                    Range1Start = table.Column<int>(type: "int", nullable: false),
                    Range1End = table.Column<int>(type: "int", nullable: false),
                    Range1Nominal = table.Column<int>(type: "int", nullable: false),
                    Range2Start = table.Column<int>(type: "int", nullable: false),
                    Range2End = table.Column<int>(type: "int", nullable: false),
                    Range2Nominal = table.Column<int>(type: "int", nullable: false),
                    Range3Start = table.Column<int>(type: "int", nullable: false),
                    Range3End = table.Column<int>(type: "int", nullable: false),
                    Range3Nominal = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<int>(type: "int", nullable: false),
                    SinkingFundPercentage = table.Column<int>(type: "int", nullable: false),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<int>(type: "int", nullable: false),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_ItemRateWater", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_SettingEmail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeEmail = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NoTlp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Cc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Bcc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_SettingEmail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_UnitItemDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnitItemId = table.Column<int>(type: "int", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    ItemRateId = table.Column<int>(type: "int", nullable: false),
                    RateNominal = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitItemDetail", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MP_TemplateInvoice");

            migrationBuilder.DropTable(
                name: "MP_TemplateInvoiceDetail");

            migrationBuilder.DropTable(
                name: "MS_Item");

            migrationBuilder.DropTable(
                name: "MS_ItemRateBPL");

            migrationBuilder.DropTable(
                name: "MS_ItemRateWater");

            migrationBuilder.DropTable(
                name: "MS_SettingEmail");

            migrationBuilder.DropTable(
                name: "MS_UnitItemDetail");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "TR_WarningLetter");

            migrationBuilder.DropColumn(
                name: "PeriodId",
                table: "TR_WarningLetter");

            migrationBuilder.DropColumn(
                name: "psCode",
                table: "TR_WarningLetter");

            migrationBuilder.DropColumn(
                name: "IsSendEmail",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "IsSendWhatsapp",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "PeriodId",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "RegenerateCount",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "TemplateInvoiceId",
                table: "TR_Invoice");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "TR_WaterReading",
                newName: "ProjectCode");

            migrationBuilder.RenameColumn(
                name: "ClusterId",
                table: "TR_WaterReading",
                newName: "ClusterName");

            migrationBuilder.RenameColumn(
                name: "InvoiceDoc",
                table: "TR_Invoice",
                newName: "InvoiceName");

            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceName",
                table: "MS_UnitItem",
                newName: "TemplateInvoice");

            migrationBuilder.AddColumn<DateTime>(
                name: "Period",
                table: "TR_WarningLetter",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Period",
                table: "TR_Invoice",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "MS_BillingItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Aging = table.Column<int>(type: "int", nullable: false),
                    AttributeId = table.Column<int>(type: "int", nullable: false),
                    CompanyCode = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsFakturPajak = table.Column<bool>(type: "bit", nullable: false),
                    IsShowInvoice = table.Column<bool>(type: "bit", nullable: false),
                    ItemName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    PPHId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BillingItem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MS_BillingItemRate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BillingItemId = table.Column<int>(type: "int", nullable: false),
                    BuildRate = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    ExchangeRateId = table.Column<int>(type: "int", nullable: false),
                    FixRate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemRateName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LandRate = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    ManagementFeeNominal = table.Column<int>(type: "int", nullable: false),
                    ManagementFeePercentage = table.Column<int>(type: "int", nullable: false),
                    SinkingFundNominal = table.Column<int>(type: "int", nullable: false),
                    SinkingFundPercentage = table.Column<int>(type: "int", nullable: false),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    VATPercentage = table.Column<int>(type: "int", nullable: false),
                    lumpSumRate = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_BillingItemRate", x => x.Id);
                });
        }
    }
}
