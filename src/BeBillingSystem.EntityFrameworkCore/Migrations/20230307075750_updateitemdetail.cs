﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class updateitemdetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UnitItem");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "MS_UnitItemDetail");

            migrationBuilder.RenameColumn(
                name: "UnitItemId",
                table: "MS_UnitItemDetail",
                newName: "UnitItemHeaderId");

            migrationBuilder.RenameColumn(
                name: "ItemRateId",
                table: "MS_UnitItemDetail",
                newName: "TemplateInvoiceDetailId");

            migrationBuilder.CreateTable(
                name: "MS_UnitItemHeader",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    UnitDataId = table.Column<int>(type: "int", nullable: false),
                    TemplateInvoiceHeaderId = table.Column<int>(type: "int", nullable: false),
                    VirtualAccNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitItemHeader", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_UnitItemHeader");

            migrationBuilder.RenameColumn(
                name: "UnitItemHeaderId",
                table: "MS_UnitItemDetail",
                newName: "UnitItemId");

            migrationBuilder.RenameColumn(
                name: "TemplateInvoiceDetailId",
                table: "MS_UnitItemDetail",
                newName: "ItemRateId");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "MS_UnitItemDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MS_UnitItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    SiteId = table.Column<int>(type: "int", nullable: false),
                    TemplateInvoiceHeaderId = table.Column<int>(type: "int", nullable: false),
                    UnitDataId = table.Column<int>(type: "int", nullable: false),
                    VirtualAccNo = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_UnitItem", x => x.Id);
                });
        }
    }
}
