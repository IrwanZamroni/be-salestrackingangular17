﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class updatetitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SingkingFundPercentage",
                table: "MS_BillingItemRate",
                newName: "SinkingFundPercentage");

            migrationBuilder.RenameColumn(
                name: "SingkingFundNominal",
                table: "MS_BillingItemRate",
                newName: "SinkingFundNominal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SinkingFundPercentage",
                table: "MS_BillingItemRate",
                newName: "SingkingFundPercentage");

            migrationBuilder.RenameColumn(
                name: "SinkingFundNominal",
                table: "MS_BillingItemRate",
                newName: "SingkingFundNominal");
        }
    }
}
