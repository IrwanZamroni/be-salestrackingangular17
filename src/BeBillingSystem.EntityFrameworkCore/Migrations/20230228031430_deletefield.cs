﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class deletefield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PsName",
                table: "TR_BillingPaymentHeader");

            migrationBuilder.DropColumn(
                name: "InvoiceNo",
                table: "TR_BillingPaymentDetail");

            migrationBuilder.AlterColumn<string>(
                name: "PsCode",
                table: "TR_BillingPaymentHeader",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "InvoiceId",
                table: "TR_BillingPaymentDetail",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceId",
                table: "TR_BillingPaymentDetail");

            migrationBuilder.AlterColumn<int>(
                name: "PsCode",
                table: "TR_BillingPaymentHeader",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PsName",
                table: "TR_BillingPaymentHeader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceNo",
                table: "TR_BillingPaymentDetail",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
