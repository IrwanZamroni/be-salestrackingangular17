﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BeBillingSystem.Migrations
{
    public partial class updateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManagementFee",
                table: "MS_BillingItemRate");

            migrationBuilder.AddColumn<int>(
                name: "TypeInvoice",
                table: "TR_Invoice",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Logo",
                table: "MS_Site",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "VATPercentage",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SingkingFundPercentage",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LandRate",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BuildRate",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BillingItemId",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ManagementFeeNominal",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ManagementFeePercentage",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SingkingFundNominal",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "lumpSumRate",
                table: "MS_BillingItemRate",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SiteId",
                table: "MS_Abodemen",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MS_TypeInvoice",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeInvoice = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Template = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MS_TypeInvoice", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MS_TypeInvoice");

            migrationBuilder.DropColumn(
                name: "TypeInvoice",
                table: "TR_Invoice");

            migrationBuilder.DropColumn(
                name: "Logo",
                table: "MS_Site");

            migrationBuilder.DropColumn(
                name: "ManagementFeeNominal",
                table: "MS_BillingItemRate");

            migrationBuilder.DropColumn(
                name: "ManagementFeePercentage",
                table: "MS_BillingItemRate");

            migrationBuilder.DropColumn(
                name: "SingkingFundNominal",
                table: "MS_BillingItemRate");

            migrationBuilder.DropColumn(
                name: "lumpSumRate",
                table: "MS_BillingItemRate");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "MS_Abodemen");

            migrationBuilder.AlterColumn<string>(
                name: "VATPercentage",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "SingkingFundPercentage",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "LandRate",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "BuildRate",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "BillingItemId",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "ManagementFee",
                table: "MS_BillingItemRate",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
