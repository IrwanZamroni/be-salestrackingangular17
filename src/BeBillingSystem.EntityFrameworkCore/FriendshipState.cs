﻿//using Abp.IdentityServer4;
namespace BeBillingSystem.EntityFrameworkCore
{
	public enum FriendshipState
	{
		Accepted = 1,
        Blocked = 2
	}
}