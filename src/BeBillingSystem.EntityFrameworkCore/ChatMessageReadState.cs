﻿//using Abp.IdentityServer4;
namespace BeBillingSystem.EntityFrameworkCore
{
	public enum ChatMessageReadState
	{
		Unread = 1,

        Read = 2
	}
}