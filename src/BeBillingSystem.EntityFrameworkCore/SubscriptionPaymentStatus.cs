﻿//using Abp.IdentityServer4;
namespace BeBillingSystem.EntityFrameworkCore
{
	public enum SubscriptionPaymentStatus
	{
		Processing = 1,
        Completed = 2,
        Failed = 3
	}
}