﻿//using Abp.IdentityServer4;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_Application : CreationAuditedEntity<int>
	{
		[Required, StringLength(50)]
		public string AppName { get; set; }
		[Required, StringLength(500)]
		public string AppDesc { get; set; }
		public int? ParentId { get; set; }
	}
}