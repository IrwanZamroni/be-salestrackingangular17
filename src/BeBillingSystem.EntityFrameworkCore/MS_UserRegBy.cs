﻿//using Abp.IdentityServer4;
using Abp.Domain.Entities.Auditing;

namespace BeBillingSystem.EntityFrameworkCore
{
	public class MS_UserRegBy : AuditedEntity
	{
		public long userID { get; set; }

		public long userRegBy { get; set; }
	}
}