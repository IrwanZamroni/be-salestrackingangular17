﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using BeBillingSystem.EntityFrameworkCore;
using BeBillingSystem.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace BeBillingSystem.Web.Tests
{
    [DependsOn(
        typeof(BeBillingSystemWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class BeBillingSystemWebTestModule : AbpModule
    {
        public BeBillingSystemWebTestModule(BeBillingSystemEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(BeBillingSystemWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(BeBillingSystemWebMvcModule).Assembly);
        }
    }
}