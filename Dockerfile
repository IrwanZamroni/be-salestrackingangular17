#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.


# Tahap 1: Restore dependensi
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS restore
WORKDIR /app
COPY ./src/*/*.csproj ./
RUN for file in *.csproj; do dotnet restore "$file"; done


# Tahap 2: build aplikasi
FROM restore AS build
COPY . .
WORKDIR src/BeBillingSystem.Web.Host/
RUN dotnet build "BeBillingSystem.Web.Host.csproj" -c Release -o /app/build


# Tahap 3: Publish aplikasi
FROM build AS publish
RUN dotnet publish "BeBillingSystem.Web.Host.csproj" -c Release -o /app/publish /p:UseAppHost=false


# Tahap 4: Jalankan aplikasi
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /app
COPY --from=publish /app/publish .
EXPOSE 80
ENTRYPOINT ["dotnet", "BeBillingSystem.Web.Host.dll"]